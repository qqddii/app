package com.android.common.baselibrary.jnative;

import android.content.Context;

public class JavaNative {
    public native static void it(Context context);//init
    public native static String rs(String sourceData);//getRSAsinature
    public native static String er(String sourceData);//encDataByRSA
    public native static String dr(String rsaEncData);//decDataByRSA
    public native static String da(String encData);//decodeByAES
    public native static String ea(String sourceData);//encodeByAES
    public native static String mf(String encodeData);//md5
    public native static byte[] db(byte[] data, int offset, int length, int flag);
    public native static byte[] eb(byte[] data, int offset, int length, int flag);
}
