package com.android.common.baselibrary.jnative;

import android.content.Context;

public class SecUtil {

    public static void init(Context context) {
        JavaNative.it(context);
    }

    public static String getRSAsinatureWithLogin(String sourData) {
        return sourData;//JavaNative.rs(sourData);
    }

    public static String getRSAsinature(String sourData) {
        return JavaNative.rs(sourData);
    }

    public static String encDataByRSA(String sourceData) {
        return JavaNative.er(sourceData);
    }

    public static String decDataByRSA(String encRSAData) {
        return JavaNative.dr(encRSAData);
    }

    public static String decodeByAES(String encData) {
        return JavaNative.da(encData);
    }

    public static String encodeByAES(String sourceData) {
        return JavaNative.ea(sourceData);
    }

    public static String md5(String sourceData) {
        return JavaNative.mf(sourceData);
    }

}
