package com.android.common.baselibrary.util.comutil;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;

import com.android.common.baselibrary.app.BaseApplication;
import com.android.common.baselibrary.jnative.JavaNative;
import com.android.common.baselibrary.jnative.SecUtil;
import com.android.common.baselibrary.log.MLog;
import com.android.common.baselibrary.util.FileUtils;
import com.ta.utdid2.device.UTDevice;

public class AppUtils {
 
    /**
     * 获取应用程序名称
     */
    public static String getAppName(Context context) {
        try {
            PackageManager packageManager = context.getPackageManager();
            PackageInfo packageInfo = packageManager.getPackageInfo(
                    context.getPackageName(), 0);
            int labelRes = packageInfo.applicationInfo.labelRes;
            return context.getResources().getString(labelRes);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
 
    /**
     * [获取应用程序版本名称信息]
     * @param context
     * @return 当前应用的版本名称
     */
    public static String getVersionName(Context context) {
        try {
            PackageManager packageManager = context.getPackageManager();
            PackageInfo packageInfo = packageManager.getPackageInfo(
                    context.getPackageName(), 0);
            return packageInfo.versionName;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getGitCommitVersionName(Context context) {
        byte[] gitCommitVersion = FileUtils.readFileContentOfAssets(context, "commit-msg.txt");
        if (null != gitCommitVersion && gitCommitVersion.length > 0) {
            return new String(gitCommitVersion);
        }
        return getVersionName(context);
    }

    public static long getVersionCode() {
        try {
            Context context = BaseApplication.getInstance();

            PackageManager packageManager = context.getPackageManager();
            PackageInfo packageInfo = packageManager.getPackageInfo(
                    context.getPackageName(), 0);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                return packageInfo.getLongVersionCode();
            } else {
                return packageInfo.versionCode;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static String getVersionCode2Str() {
        return String.valueOf(getVersionCode());
    }
 
 
    /**
     * [获取应用程序版本名称信息]
     * @param context
     * @return 当前应用的版本名称
     */
    public static String getPackageName(Context context) {
        try {
            PackageManager packageManager = context.getPackageManager();
            PackageInfo packageInfo = packageManager.getPackageInfo(
                    context.getPackageName(), 0);
            return packageInfo.packageName;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
 
 
    /**
     * 获取图标 bitmap
     * @param context
     */
    public static Bitmap getBitmap(Context context) {
        PackageManager packageManager = null;
        ApplicationInfo applicationInfo = null;
        try {
            packageManager = context.getApplicationContext()
                    .getPackageManager();
            applicationInfo = packageManager.getApplicationInfo(
                    context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            applicationInfo = null;
        }
        Drawable d = packageManager.getApplicationIcon(applicationInfo); //xxx根据自己的情况获取drawable
        BitmapDrawable bd = (BitmapDrawable) d;
        Bitmap bm = bd.getBitmap();
        return bm;
    }

    public static String getStringResource(Context context, int resourceId) {
        return context.getResources().getString(resourceId);
    }

    public static String getUtdId() {
       String utdId =  UTDevice.getUtdid(BaseApplication.getInstance());
        MLog.d("utdId = " + utdId);
       return utdId;
    }
 
}
