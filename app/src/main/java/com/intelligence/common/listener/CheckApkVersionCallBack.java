package com.intelligence.common.listener;


import com.intelligence.glasses.bean.UpdateInfoBean;

public interface CheckApkVersionCallBack {
    void checkResult(UpdateInfoBean updateInfoBean);
}
