package com.intelligence.common.http.retrofit.netsubscribe;

import com.intelligence.common.http.retrofit.netutils.HttpMethods;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

public class TrainSuscribe {

    public static RequestBody createNewRequestBody(HashMap<String, String> bodyMap) {
        return LoginSubscribe.createNewRequestBody(bodyMap);
    }

    public static RequestBody createNewRequestBodyWithObject(HashMap<String, Object> bodyMap) {
        return LoginSubscribe.createNewRequestBodyWithObject(bodyMap);
    }

    /**
     * 上报训练数据(开始，干预，运行，停止，暂停)
     * @param headerMap
     * @param bodyMap
     * @param subscriber
     */
    public static void postTrainInfo(HashMap<String, String> headerMap, HashMap<String, Object> bodyMap, DisposableObserver<ResponseBody> subscriber) {
        Observable<ResponseBody> observable = HttpMethods.getInstance().getHttpApi().postUserTrainInfo(headerMap, createNewRequestBodyWithObject(bodyMap));
        HttpMethods.getInstance().toSubscribe(observable, subscriber);
    }


    /**
     * 获取最新的训练数据给蓝牙设备
     * @param headerMap
     * @param bodyMap
     * @param subscriber
     */
    public static void getNewTrainInfo2BleDevice(HashMap<String, String> headerMap, HashMap<String, String> bodyMap, DisposableObserver<ResponseBody> subscriber) {
        Observable<ResponseBody> observable = HttpMethods.getInstance().getHttpApi().getNewUserTrainInfo(headerMap, createNewRequestBody(bodyMap));
        HttpMethods.getInstance().toSubscribe(observable, subscriber);
    }

    /**
     * 获取训练时间信息
     * @param headerMap
     * @param bodyMap
     * @param subscriber
     */
    public static void getUserTrainTimeInfo(HashMap<String, String> headerMap, HashMap<String, String> bodyMap, DisposableObserver<ResponseBody> subscriber) {
        Observable<ResponseBody> observable = HttpMethods.getInstance().getHttpApi().getUserTrainTimeInfo(headerMap, createNewRequestBody(bodyMap));
        HttpMethods.getInstance().toSubscribe(observable, subscriber);
    }

    /**
     * 获取视力复查数据
     */
    public static void getReviewData(HashMap<String, Object> headerMap, HashMap<String, Object> bodyMap, DisposableObserver<ResponseBody> subscriber) {
        Observable<ResponseBody> observable = HttpMethods.getInstance().getHttpApi().getReviewSightDataInfo(headerMap, createNewRequestBodyWithObject(bodyMap));
        HttpMethods.getInstance().toSubscribe(observable, subscriber);
    }

    /**
     * 提交设备信息
     */
    public static void postDeivceInfo(HashMap<String, Object> headerMap, HashMap<String, Object> bodyMap, DisposableObserver<ResponseBody> subscriber) {
        Observable<ResponseBody> observable = HttpMethods.getInstance().getHttpApi().postDeviceInfo(headerMap, createNewRequestBodyWithObject(bodyMap));
        HttpMethods.getInstance().toSubscribeWithRetryTimes(observable, subscriber, 2);
    }

    /**
     * 提交用户反馈
     * @param headerMap
     * @param bodyMap
     * @param subscriber
     */
    public static void postFeedBack(HashMap<String, Object> headerMap, HashMap<String, Object> bodyMap, DisposableObserver<ResponseBody> subscriber) {
        Observable<ResponseBody> observable = HttpMethods.getInstance().getHttpApi().postFeedback(headerMap, createNewRequestBodyWithObject(bodyMap));
        HttpMethods.getInstance().toSubscribeWithRetryTimes(observable, subscriber, 2);
    }


    /**
     * 获取用户的反馈信息列表
     * @param headerMap
     * @param bodyMap
     * @param subscriber
     */
    public static void getFeedBackList(HashMap<String, Object> headerMap, HashMap<String, Object> bodyMap, DisposableObserver<ResponseBody> subscriber) {
        Observable<ResponseBody> observable = HttpMethods.getInstance().getHttpApi().getFeedbackList(headerMap, bodyMap);
        HttpMethods.getInstance().toSubscribeWithRetryTimes(observable, subscriber, 2);
    }

    /**
     * 获取常见问题
     * @param headerMap
     * @param bodyMap
     * @param subscriber
     */
    public static void getQuestionList(HashMap<String, Object> headerMap, HashMap<String, Object> bodyMap, DisposableObserver<ResponseBody> subscriber) {
        Observable<ResponseBody> observable = HttpMethods.getInstance().getHttpApi().getQuestionList(headerMap, bodyMap);
        HttpMethods.getInstance().toSubscribeWithRetryTimes(observable, subscriber, 2);
    }

    /**
     * 获取 APP 版本信息  或 固件的最新版本信息
     * @param headerMap
     * @param bodyMap
     * @param subscriber
     */
    public static void getApkVersionInfo(HashMap<String, Object> headerMap, HashMap<String, Object> bodyMap, DisposableObserver<ResponseBody> subscriber) {
        Observable<ResponseBody> observable = HttpMethods.getInstance().getHttpApi().getApkVersionInfo(headerMap, createNewRequestBodyWithObject(bodyMap));
        HttpMethods.getInstance().toSubscribeWithRetryTimes(observable, subscriber, 2);
    }

    /**
     * 获取眼镜初始化运行参数
     * @param headerMap
     * @param bodyMap
     * @param subscriber
     */
    public static void getGlassDeviceInitParam(HashMap<String, Object> headerMap, HashMap<String, Object> bodyMap, DisposableObserver<ResponseBody> subscriber) {
        Observable<ResponseBody> observable = HttpMethods.getInstance().getHttpApi().getGlassDeviceInitParam(headerMap, bodyMap);
        HttpMethods.getInstance().toSubscribeWithRetryTimes(observable, subscriber, 2);
    }


    /**
     * 上报干预反馈数据
     * @param headerMap
     * @param bodyMap
     * @param subscriber
     */
    public static void postInterveneFeedBackData(HashMap<String, Object> headerMap, HashMap<String, Object> bodyMap, DisposableObserver<ResponseBody> subscriber) {
        Observable<ResponseBody> observable = HttpMethods.getInstance().getHttpApi().postInterveneFeedBackData(headerMap, createNewRequestBodyWithObject(bodyMap));
        HttpMethods.getInstance().toSubscribeWithRetryTimes(observable, subscriber, 2);
    }

    /**
     * 上报实时反馈数据
     * @param headerMap
     * @param bodyMap
     * @param subscriber
     */
    public static void postCommonFeedBackData(HashMap<String, Object> headerMap, HashMap<String, Object> bodyMap, DisposableObserver<ResponseBody> subscriber) {
        Observable<ResponseBody> observable = HttpMethods.getInstance().getHttpApi().postCommonFeedBackData(headerMap, createNewRequestBodyWithObject(bodyMap));
        HttpMethods.getInstance().toSubscribeWithRetryTimes(observable, subscriber, 2);
    }

    /**
     * 上报运行参数数据
     * @param headerMap
     * @param bodyMap
     * @param subscriber
     */
    public static void postRunParamData(HashMap<String, Object> headerMap, HashMap<String, Object> bodyMap, DisposableObserver<ResponseBody> subscriber) {
        Observable<ResponseBody> observable = HttpMethods.getInstance().getHttpApi().postRunParamData(headerMap, createNewRequestBodyWithObject(bodyMap));
        HttpMethods.getInstance().toSubscribeWithRetryTimes(observable, subscriber, 2);
    }

    /**
     * 获取运行参数数据
     * @param headerMap
     * @param bodyMap
     * @param subscriber
     */
    public static void queryRunParamData(HashMap<String, Object> headerMap, HashMap<String, Object> bodyMap, DisposableObserver<ResponseBody> subscriber) {
        Observable<ResponseBody> observable = HttpMethods.getInstance().getHttpApi().queryRunParamData(headerMap, createNewRequestBodyWithObject(bodyMap));
        HttpMethods.getInstance().toSubscribeWithRetryTimes(observable, subscriber, 2);
    }

    /**
     * 获取 Html 代码
     * @param headerMap
     * @param bodyMap
     * @param subscriber
     */
    public static void getHtmlCode(HashMap<String, Object> headerMap, HashMap<String, Object> bodyMap, DisposableObserver<ResponseBody> subscriber) {
        Observable<ResponseBody> observable = HttpMethods.getInstance().getHttpApi().getHtmlCode(headerMap,  bodyMap);
        HttpMethods.getInstance().toSubscribeWithRetryTimes(observable, subscriber, 2);
    }

    /**
     * 绑定设备（如果当前设备没有被绑定，则绑定）
     * @param headerMap
     * @param bodyMap
     * @param subscriber
     */
    public static void bindDevice(HashMap<String, Object> headerMap, HashMap<String, Object> bodyMap, DisposableObserver<ResponseBody> subscriber) {
        Observable<ResponseBody> observable = HttpMethods.getInstance().getHttpApi().bindDevice(headerMap,  createNewRequestBodyWithObject(bodyMap));
        HttpMethods.getInstance().toSubscribeWithRetryTimes(observable, subscriber, 2);
    }

}
