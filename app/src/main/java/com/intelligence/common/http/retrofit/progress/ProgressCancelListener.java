package com.intelligence.common.http.retrofit.progress;

public interface ProgressCancelListener {
    void onCancelProgress();
}
