package com.intelligence.common.http.retrofit.netapi;



import com.intelligence.common.http.retrofit.bean.RequestBean;
import com.intelligence.glasses.bean.UserInfoBean;
import com.intelligence.glasses.http.request.IFileUpload;

import java.util.Map;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

/**
 * 存放所有的 Api
 */
public interface HttpApi {

    /**
     * 处理多个主机地址或域名的问题
     * @param username
     * @param password
     * @return
     */
    @Headers({"urlname:retail"})
    @FormUrlEncoded
    @POST("login")
    Observable<ResponseBody> doLoginMdffx(@Field("username") String username,@Field("password") String password);

    @Headers({"urlname:train"})
    @GET("members/datas")
    Observable<ResponseBody> doData(@Query("type") int type,@Query("params") int params);


    @GET("")
    Observable<ResponseBody> getDataForMap(@QueryMap Map<String, Integer> paramMap);

    @GET
    @Streaming
    Observable<ResponseBody> downloadFile(@HeaderMap Map<String, Object> headmap, @Body RequestBody bodymap, @Url String url);

    @POST("api" + URLConstant.MEMBER_PAHT+ "/member/login")
    Observable<ResponseBody> loginAccountWithHeader(@HeaderMap Map<String, String> headmap, @Body RequestBody bodymap);

    /**
     * 注册接口
     * @param headmap
     * @param bodymap
     * @return
     */
    @POST("api" + URLConstant.MEMBER_PAHT + "/member/register")
    Observable<ResponseBody> register(@HeaderMap Map<String, String> headmap, @Body RequestBody bodymap);

    @POST("api" + URLConstant.MEMBER_PAHT +"/member/findPassword")//找回密码
    Observable<ResponseBody> findPasswd(@HeaderMap Map<String, String> headmap, @Body RequestBody bodymap);

    @POST("api" + URLConstant.MEMBER_PAHT + "/member/updatePassword")//修改密码
    Observable<ResponseBody> updatePasswd(@HeaderMap Map<String, String> headmap, @Body RequestBody bodymap);

    @POST("api" + URLConstant.MEMBER_PAHT + "/verfication/SendVerficationCode")
    Observable<ResponseBody> postRegiterInfoWithVerficationCodeWithJson(@HeaderMap Map<String, String> headmap, @Body RequestBody bodymap);

    /**
     * 绑定眼镜信息的接口
     * @param headmap
     * @param bodymap
     * @return
     */

    @POST("api" + URLConstant.MEMBER_PAHT + "/member/upsertEyeData")
    Observable<ResponseBody> bindGlassesInfo(@HeaderMap Map<String, String> headmap, @Body RequestBody bodymap);

    /**
     * 修改视力信息的接口
     * @param headmap
     * @param bodymap
     * @return
     */
    @POST("api/Customer/UpdateMemberEyeData")
    Observable<ResponseBody> updateEyeInfo(@HeaderMap Map<String, String> headmap, @Body RequestBody bodymap);

    /**
     * 获取用户的训练时间信息
     * @param headmap
     * @param bodymap
     * @return
     */
    @POST("api" + URLConstant.MEMBER_PAHT + "/training/getTrainingTime")
    Observable<ResponseBody> getUserTrainTimeInfo(@HeaderMap Map<String, String> headmap, @Body RequestBody bodymap);

    /**
     * 提交用户训练时间信息
     * @param headmap
     * @param bodymap
     * @return
     */
    @POST("api/Customer/*")
    Observable<ResponseBody> postUserTrainTimeInfo(@HeaderMap Map<String, String> headmap, @Body RequestBody bodymap);
    /**
     * 签到提交接口
     * @param headmap
     * @param bodymap
     * @return
     */
    @POST("/api/Customer/SignIn")
    Observable<ResponseBody> postSignInInfo(@HeaderMap Map<String, String> headmap, @Body RequestBody bodymap);

    /**
     * 获取签到信息接口
     * @param headmap
     * @param bodymap
     * @return
     */
    @POST("api/Customer/GetRecentSignIns")
    Observable<ResponseBody> getSignInInfo(@HeaderMap Map<String, String> headmap, @Body RequestBody bodymap);

    @Multipart
    @POST("api"+ URLConstant.UPLOAD_PATH + "/fastdfs/upload")
    Observable<ResponseBody> uploadPortraitImage(@HeaderMap Map<String, String> params, @PartMap Map<String, RequestBody> parMap, @Part MultipartBody.Part file);

    @Multipart
    @POST("api"+ URLConstant.UPLOAD_PATH + "/fastdfs/uploadException")
    Observable<ResponseBody> uploadAppLogZipFile(@HeaderMap Map<String, Object> params, @Part(IFileUpload.PARAMS) RequestBody bodymap, @Part MultipartBody.Part file);

    @FormUrlEncoded
    @POST(URLConstant.OATH_TOKEN_URL)
    Call<ResponseBody> getNewToken(@HeaderMap Map<String, String> headmap, @FieldMap Map<String, String> bodymap);

    @POST(URLConstant.OATH_LOGOUT_TOKEN_URL)
    Observable<ResponseBody> logoutToken(@HeaderMap Map<String, Object> headmap, @Body RequestBody bodymap);

    /**
     * 上报用户的训练数据（双眼训练时，电机的运转数据）
     * @param headmap
     * @param bodymap
     * @return
     */
    @POST("api" + URLConstant.MEMBER_PAHT + "/training/upload")
    Observable<ResponseBody> postUserTrainInfo(@HeaderMap Map<String, String> headmap, @Body RequestBody bodymap);

    /**
     * 获取最新的训练数据给蓝牙设备
     * @param headmap
     * @param bodymap
     * @return
     */
    @POST("api" + URLConstant.MEMBER_PAHT + "/training/recentlyTraining")
    Observable<ResponseBody> getNewUserTrainInfo(@HeaderMap Map<String, String> headmap, @Body RequestBody bodymap);

    /**
     * 视力复查数据
     * @param headmap
     * @param bodymap
     * @return
     */
    @POST("api" + URLConstant.MEMBER_PAHT + "/review/getReviewEyeDegreeData")
    Observable<ResponseBody> getReviewSightDataInfo(@HeaderMap Map<String, Object> headmap, @Body RequestBody bodymap);

    /**
     * 提交设备信息
     * @param headmap
     * @param bodymap
     * @return
     */
    @POST("api" + URLConstant.MEMBER_PAHT + "/other/mobileinfo/upsert")
    Observable<ResponseBody> postDeviceInfo(@HeaderMap Map<String, Object> headmap, @Body RequestBody bodymap);

    /**
     * 提交用户反馈
     * @param headmap
     * @param bodymap
     * @return
     */
    @POST("api" + URLConstant.MEMBER_PAHT + "/feedback/addFeedback")
    Observable<ResponseBody> postFeedback(@HeaderMap Map<String, Object> headmap, @Body RequestBody bodymap);

    /**
     * 获取当前用户的反馈信息列表
     * @param headmap
     * @param bodymap
     * @return
     */
    @GET("api" + URLConstant.MEMBER_PAHT + "/feedback/getFeedbackPages")
    Observable<ResponseBody> getFeedbackList(@HeaderMap Map<String, Object> headmap, @QueryMap Map<String, Object> bodymap);

    /**
     * 获取常见问题
     * @param headmap
     * @param bodymap
     * @return
     */
    @GET("api" + URLConstant.MEMBER_PAHT + "/question/getQuestionPages")
    Observable<ResponseBody> getQuestionList(@HeaderMap Map<String, Object> headmap, @QueryMap Map<String, Object> bodymap);


    /**
     * 获取APP版本信息 或 固件版本信息
     * @param headmap
     * @param bodymap
     * @return
     */
    @POST("api" + URLConstant.MEMBER_PAHT + "/systemUpdate/getLastedSystemUpdate")
    Observable<ResponseBody> getApkVersionInfo(@HeaderMap Map<String, Object> headmap, @Body RequestBody bodymap);


    /**
     * 获取机器运行参数
     * @param headmap
     * @param bodymap
     * @return
     */
    @GET("api" + URLConstant.MEMBER_PAHT + "/member/getDeviceParam")
    Observable<ResponseBody> getGlassDeviceInitParam(@HeaderMap Map<String, Object> headmap, @QueryMap Map<String, Object> bodymap);


    /**
     * 上报干预反馈数据
     * @param headmap
     * @param bodymap
     * @return
     */
    @POST("api" + URLConstant.MEMBER_PAHT + "/training/uploadTrainingInterveneData")
    Observable<ResponseBody> postInterveneFeedBackData(@HeaderMap Map<String, Object> headmap, @Body RequestBody bodymap);

    /**
     * 上报实时反馈数据
     * @param headmap
     * @param bodymap
     * @return
     */
    @POST("api" + URLConstant.MEMBER_PAHT + "/training/uploadTrainingRealTimeData")
    Observable<ResponseBody> postCommonFeedBackData(@HeaderMap Map<String, Object> headmap, @Body RequestBody bodymap);

    /**
     * 上报运行参数数据
     * @param headmap
     * @param bodymap
     * @return
     */
    @POST("api" + URLConstant.MEMBER_PAHT + "/training/uploadDetail")
    Observable<ResponseBody> postRunParamData(@HeaderMap Map<String, Object> headmap, @Body RequestBody bodymap);


    /**
     * 获取运行参数数据
     * @param headmap
     * @param bodymap
     * @return
     */
    @POST("api" + URLConstant.MEMBER_PAHT + "/training/recentlyTrainingDetail")
    Observable<ResponseBody> queryRunParamData(@HeaderMap Map<String, Object> headmap, @Body RequestBody bodymap);

    /**
     * 获取 Html 代码
     * @param headmap
     * @param bodymap
     * @return
     */
    @GET("api"+ URLConstant.MEMBER_PAHT +"/system/info/getInfoByCategoryId")
    Observable<ResponseBody> getHtmlCode(@HeaderMap Map<String, Object> headmap, @QueryMap Map<String, Object> bodymap);

    /**
     * 绑定设备
     * @param headmap
     * @param bodymap
     * @return
     */
    @POST("api" + URLConstant.MEMBER_PAHT + "/member/bindDevice")
    Observable<ResponseBody> bindDevice(@HeaderMap Map<String, Object> headmap, @Body RequestBody bodymap);


}
