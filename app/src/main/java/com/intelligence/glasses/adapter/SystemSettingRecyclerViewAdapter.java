package com.intelligence.glasses.adapter;

import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.intelligence.glasses.bean.SystemSettingBean;
import com.intelligence.glasses.listener.OnChildItemClickListener;
import com.intelligence.glasses.listener.OnItemClickListener;
import com.shitec.bleglasses.R;

import java.util.ArrayList;
import java.util.List;

/**
 * 系统设置适配器
 */
public class SystemSettingRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private Fragment mFragment;
    private List<SystemSettingBean> mDatas = new ArrayList<>();
    private OnItemClickListener mOnItemClickListener;
    private OnChildItemClickListener mOnChildItemClickListener;
    private int index = -1;


    public SystemSettingRecyclerViewAdapter(Fragment fragment) {
        mFragment = fragment;
        mContext = fragment.getActivity();
    }

    public void addDatas(List<SystemSettingBean> datas) {

        if (null != datas && datas .size() > 0) {
            mDatas.addAll(datas);
        }
    }

    public void setDatas(List<SystemSettingBean> datas) {
        if (null != datas && datas.size() > 0) {
            mDatas.clear();
            mDatas.addAll(datas);
        }
    }

    public List<SystemSettingBean> getDatas() {
        return mDatas;
    }



    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.recyclerview_item_systemsettings_layout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        SystemSettingBean systemSettingBean = mDatas.get(position);
        String title = systemSettingBean.getItemTilte();
        ViewHolder viewHolder = (ViewHolder)holder;
        viewHolder.prefixImageView.setImageResource(systemSettingBean.getItemPrefixImageResourceId());
        viewHolder.titleTextView.setText(title);
        viewHolder.sufixImageView.setImageResource(systemSettingBean.getItemSufixImageResource());

        viewHolder.systemSettingsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (null != mOnItemClickListener) {
                    mOnItemClickListener.OnItemClick(position);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        ImageView prefixImageView;
        AppCompatTextView titleTextView;
        ImageView sufixImageView;
        LinearLayout systemSettingsLayout;

        ViewHolder(View view) {
            super(view);
            prefixImageView = view.findViewById(R.id.systemsetings_prefix_img);
            titleTextView = view.findViewById(R.id.function_system_settings_recyclerview_item_tv);
            sufixImageView = view.findViewById(R.id.systemsettings_item_sufiximg);
            systemSettingsLayout = view.findViewById(R.id.systemsettings_item_layout);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public void setmOnItemClickListener(OnItemClickListener mOnItemClickListener) {
        this.mOnItemClickListener = mOnItemClickListener;
    }

    public void setmOnChildItemClickListener(OnChildItemClickListener mOnChildItemClickListener) {
        this.mOnChildItemClickListener = mOnChildItemClickListener;
    }
}
