package com.intelligence.glasses.data.source.remote;

import com.intelligence.glasses.bean.BaseBean;
import com.intelligence.glasses.data.source.BleGlassesDataSource;



public interface BleGlassesRemoteDataSource extends BleGlassesDataSource {

    interface getBleDataCallBac {
        void onBleDataLoaded(Object object);
    }

    void login(BaseBean baseBean);

    void register(BaseBean baseBean);

    void bindlasses(BaseBean baseBean);

    void bindEyeInfo(BaseBean baseBean);

    BaseBean getTaskBean(BaseBean baseBean);

}
