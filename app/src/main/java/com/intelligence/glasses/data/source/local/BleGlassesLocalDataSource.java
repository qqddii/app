package com.intelligence.glasses.data.source.local;

import com.intelligence.glasses.data.source.BleGlassesDataSource;

import java.util.List;

public interface BleGlassesLocalDataSource extends BleGlassesDataSource {
    interface LoadBleDataCallBack {
        void onBleDataLoad(List<Object> list);
    }

}
