package com.intelligence.glasses.listener;

public interface TittleChangeListener {
    public void selectedIndexChanged(int titleIndex);
}
