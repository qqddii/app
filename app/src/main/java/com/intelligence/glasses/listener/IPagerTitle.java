package com.intelligence.glasses.listener;

public interface IPagerTitle {
    String getTitle();
}
