package com.intelligence.glasses.listener;

public interface ItemOfViewPagerOnClickListener {
    public void onClickIndex(int index, int resourceId);
}
