package com.intelligence.glasses.event;

/**
 * Created by Administrator on 2018/10/5.
 */

public class EventConstant {
    public static final int GLASS_BLUETOOTH_EVENT_TYPE = 0;
    public static final int LIGHT_BLUETOOTH_EVENT_TYPE = 1;
    public static final int ALL_BLUETOOTH_EVENT_TYPE = 2;
}
