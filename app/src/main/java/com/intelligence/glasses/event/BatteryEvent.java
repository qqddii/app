package com.intelligence.glasses.event;

public class BatteryEvent {
    private int battery;
    private int runStatus;//（开始，停止，暂停，继续，关闭电源）

    public BatteryEvent(int battery, int runStatus) {
        this.battery = battery;
        this.runStatus = runStatus;
    }

    public int getBattery() {
        return battery;
    }

    public void setBattery(int battery) {
        this.battery = battery;
    }

    public int getRunStatus() {
        return runStatus;
    }

    public void setRunStatus(int runStatus) {
        this.runStatus = runStatus;
    }
}
