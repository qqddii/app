package com.intelligence.glasses.helper;


import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.intelligence.glasses.activity.mine.PersonalInfoActivity;
import com.intelligence.glasses.fragment.MineFragment;
import com.intelligence.glasses.fragment.mine.CommonProblemFragment;
import com.intelligence.glasses.fragment.mine.ContactCustomerServiceFragment;
import com.intelligence.glasses.fragment.mine.FeedBackFragment;
import com.intelligence.glasses.fragment.mine.InterveneKeyIntroduceFragment;
import com.intelligence.glasses.fragment.mine.PersonalInfoFragment;
import com.intelligence.glasses.fragment.mine.ProductIntroduceFragment;
import com.intelligence.glasses.fragment.mine.ReviewDataLineChartFragment;
import com.intelligence.glasses.fragment.mine.SystemSettingsFragment;
import com.intelligence.glasses.fragment.mine.UserAgreementFragment;
import com.shitec.bleglasses.R;

/**
 * Created by Administrator on 2018/11/29.
 */

public class PersonInfoActivityHelper {
    private PersonalInfoActivity mPersonInfoActivity;

    public PersonInfoActivityHelper(PersonalInfoActivity personalInfoActivity) {
        this.mPersonInfoActivity = personalInfoActivity;
    }

    public void addFragment(int fragmentIndex) {
        FragmentManager manager = mPersonInfoActivity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = manager.beginTransaction();

        switch (fragmentIndex) {

            case MineFragment.REVIEW_DATA_INDEX:
                setTextTitle(R.string.review_data_text);
                fragmentTransaction.add(R.id.mineframelayout, new ReviewDataLineChartFragment());
                break;

            case MineFragment.PRODUCT_INTRODUCE_INDEX:
                setTextTitle(R.string.product_introduce_text);
                fragmentTransaction.add(R.id.mineframelayout, new ProductIntroduceFragment());
                break;

            case MineFragment.INTERVENE_KEY_INTRODUCE_INDEX:
                setTextTitle(R.string.intervene_key_introduce_text);
                fragmentTransaction.add(R.id.mineframelayout, new InterveneKeyIntroduceFragment());
                break;

            case MineFragment.FEEDBACK_INDEX:
                 setTextTitle(R.string.feedback_text);
                 fragmentTransaction.add(R.id.mineframelayout, new FeedBackFragment());
                 break;

            case MineFragment.COMMON_PROBLEM_INDEX:
                setTextTitle(R.string.common_problem_text);
                fragmentTransaction.add(R.id.mineframelayout, new CommonProblemFragment());
                break;

            case MineFragment.CONTACT_CUSTOMER_SERVICE_INDEX:
                setTextTitle(R.string.contact_customer_service_text);
                fragmentTransaction.add(R.id.mineframelayout, new ContactCustomerServiceFragment());
                break;

            case MineFragment.SYSTEM_SETTINGS_KEY:
                setTextTitle(R.string.system_settings_text);
                fragmentTransaction.add(R.id.mineframelayout, new SystemSettingsFragment());
                break;

            case MineFragment.PERSONAL_INFO_INDEX:
                setTextTitle(R.string.personal_info_text);
                fragmentTransaction.add(R.id.mineframelayout, new PersonalInfoFragment());
                break;

            case  MineFragment.USER_AGREEMENT_INDEX:
                setTextTitle(R.string.user_agreement_text);
                fragmentTransaction.add(R.id.mineframelayout, new UserAgreementFragment());
                break;

            default:
                break;
        }
        fragmentTransaction.commitAllowingStateLoss();
    }


    private void setTextTitle(int resourceId) {
        mPersonInfoActivity.setTitleText(resourceId);
    }


}
