package com.intelligence.glasses.fragment.mine;

import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alibaba.fastjson.TypeReference;
import com.android.common.baselibrary.util.CommonUtils;
import com.android.common.baselibrary.util.ToastUtil;
import com.android.common.baselibrary.webview.ProgressWebView;
import com.intelligence.common.http.retrofit.netsubscribe.TrainSuscribe;
import com.intelligence.glasses.base.BaseFragment;
import com.intelligence.glasses.bean.HtmlCodeReponseBean;
import com.intelligence.glasses.http.request.IBaseRequest;
import com.intelligence.glasses.http.request.IGetHtmlCode;
import com.intelligence.glasses.util.jsonutil.JsonUtil;
import com.shitec.bleglasses.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.observers.DisposableObserver;
import okhttp3.ResponseBody;


public class InterveneKeyIntroduceFragment extends BaseFragment {

    @BindView(R.id.function_item_title_tv)
    TextView functionItemTextView;

    @BindView(R.id.function_item_backlayout)
    LinearLayout functionItemBackLayout;

    @BindView(R.id.commonwebview)
    ProgressWebView mProgressWebView;

    private List<DisposableObserver> disposableObserverList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_intervene_key_introduce_layout, container, false);
        ButterKnife.bind(this, view);
        initView(view);
        initListener();
        return view;
    }

    private void initView(View view) {
        functionItemTextView.setText(getResources().getText(R.string.intervene_key_introduce_text));
        initData();
    }

    protected void initListener() {
        functionItemBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
            }
        });

    }

    private void initData() {
        getHtmlCode();
    }

    private void getHtmlCode() {
        HashMap<String, Object> headerMap = new HashMap<>();
        HashMap<String, Object> bodyMap = new HashMap<>();
        bodyMap.put(IGetHtmlCode.CATEGORYID, IGetHtmlCode.INTERVENE_KEY_INTRODUCE);

        DisposableObserver disposableObserver = new DisposableObserver<ResponseBody>() {
            @Override
            public void onNext(ResponseBody responseBody) {
                disposableObserverList.remove(this);
                String newStringDataJson = null;
                try {
                    newStringDataJson = new String(responseBody.bytes());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                HtmlCodeReponseBean htmlCodeReponseBean = (HtmlCodeReponseBean) JsonUtil.json2objectWithDataCheck(newStringDataJson, new TypeReference<HtmlCodeReponseBean>() {
                });
                //MLog.d("htmlCodeReponseBean = " + htmlCodeReponseBean);
                showHtmlCode(htmlCodeReponseBean);
            }

            @Override
            public void onError(Throwable e) {
                disposableObserverList.remove(this);
                ToastUtil.showShort("获取数据失败");
            }

            @Override
            public void onComplete() {

            }
        };
        disposableObserverList.add(disposableObserver);
        TrainSuscribe.getHtmlCode(headerMap, bodyMap, disposableObserver);
    }


    private void showHtmlCode(HtmlCodeReponseBean htmlCodeReponseBean) {
        if (null != htmlCodeReponseBean && htmlCodeReponseBean.getStatus().equals(IBaseRequest.SUCCESS)) {
            HtmlCodeReponseBean.DataBean dataBean = htmlCodeReponseBean.getData();
            if (null != dataBean) {
                if (!CommonUtils.isEmpty(dataBean.getContent())) {
                    mProgressWebView.loadDataWithBaseURL(null, dataBean.getContent(), "text/html", "utf-8", null);
                }
            }

        }
    }

    private void disposeAllObserver() {
        synchronized (disposableObserverList) {
            for (DisposableObserver disposableObserver : disposableObserverList) {
                if (null != disposableObserver) {
                    disposableObserver.dispose();
                }
            }
            disposableObserverList.clear();
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        disposeAllObserver();
    }


}
