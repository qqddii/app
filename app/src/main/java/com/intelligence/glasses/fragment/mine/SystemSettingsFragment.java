package com.intelligence.glasses.fragment.mine;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.GridLayoutManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alibaba.fastjson.TypeReference;
import com.android.common.baselibrary.log.MLog;
import com.android.common.baselibrary.util.FileUtils;
import com.android.common.baselibrary.util.ToastUtil;
import com.android.common.baselibrary.util.comutil.CommonUtils;
import com.android.common.baselibrary.util.comutil.security.SignatureUtil;
import com.intelligence.common.http.retrofit.netapi.URLConstant;
import com.intelligence.common.http.retrofit.netsubscribe.LoginSubscribe;
import com.intelligence.common.http.retrofit.netsubscribe.TrainSuscribe;
import com.intelligence.glasses.MyApplication;
import com.intelligence.glasses.adapter.SystemSettingRecyclerViewAdapter;
import com.intelligence.glasses.base.BaseActivity;
import com.intelligence.glasses.base.BaseFragment;
import com.intelligence.glasses.bean.MemberLoginResponseBean;
import com.intelligence.glasses.bean.SystemSettingBean;
import com.intelligence.glasses.bean.bledata.send.SendMachineBleCmdBeaan;
import com.intelligence.glasses.bean.bledata.send.SendUserInfoBleCmdBean;
import com.intelligence.glasses.bean.response.HttpResponseGlassesRunParamBean;
import com.intelligence.glasses.ble.BleDeviceManager;
import com.intelligence.glasses.dialog.DLoadingNumProcessDialog;
import com.intelligence.glasses.dialog.bleconnect.BleDataLogDialog;
import com.intelligence.glasses.dialog.function.AppUpdateDialog;
import com.intelligence.glasses.dialog.function.FirmwareUpdateDialog;
import com.intelligence.glasses.http.request.IBaseRequest;
import com.intelligence.glasses.http.request.IOauthToken;
import com.intelligence.glasses.http.request.IgetLastedSystemUpdate;
import com.intelligence.glasses.http.response.HttpResponseApkVersionInfoBean;
import com.intelligence.glasses.http.response.HttpResponseFiremwareVersionInfoBean;
import com.intelligence.glasses.http.response.HttpResponseGlassInitDataBackBean;
import com.intelligence.glasses.http.response.OauthLogoutBean;
import com.intelligence.glasses.listener.DialogButtonClickListener;
import com.intelligence.glasses.listener.OnItemClickListener;
import com.intelligence.glasses.model.GlassesBleDataModel;
import com.intelligence.glasses.model.TrainModel;
import com.intelligence.glasses.service.DfuService;
import com.intelligence.glasses.util.Config;
import com.intelligence.glasses.util.downloadutil.DownloadUtils;
import com.intelligence.glasses.util.downloadutil.JsDownloadListener;
import com.intelligence.glasses.util.jsonutil.GsonTools;
import com.intelligence.glasses.util.jsonutil.JsonUtil;
import com.intelligence.glasses.util.view.ui.DeviceUtils;
import com.intelligence.glasses.view.recyclerview.GloriousRecyclerView;
import com.intelligence.glasses.view.recyclerview.GridItemDecoration;
import com.shitec.bleglasses.R;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.observers.DisposableObserver;
import no.nordicsemi.android.dfu.DfuProgressListener;
import no.nordicsemi.android.dfu.DfuServiceController;
import no.nordicsemi.android.dfu.DfuServiceInitiator;
import no.nordicsemi.android.dfu.DfuServiceListenerHelper;
import okhttp3.ResponseBody;

/**
 * 系统设置
 */
public class SystemSettingsFragment extends BaseFragment {
    @BindView(R.id.function_item_title_tv)
    TextView functionItemTextView;

    @BindView(R.id.function_item_backlayout)
    LinearLayout functionItemBackLayout;

    @BindView(R.id.system_settingrecyclerview)
    GloriousRecyclerView systemSettingGloriousRecyclerView;

    @BindView(R.id.exitloginbtn)
    Button exitLoginButton;

    SystemSettingRecyclerViewAdapter systemSettingRecyclerViewAdapter;

    List<SystemSettingBean> systemSettingBeans = new ArrayList<>();

    AppUpdateDialog appUpdateDialog = null;
    private DLoadingNumProcessDialog dLoadingNumProcessDialog = null;
    private DLoadingNumProcessDialog firmwareDownloadNumProgressDialog;

    FirmwareUpdateDialog firmwareUpdateDialog = null;

    private List<DisposableObserver> disposableObserverList = new ArrayList<>();
    private Handler apkHandler = null;
    private Handler firmWareHandler = null;
    private long appTotalLength = 0;
    private long binTotalLength = 0;
    private final int UPDATE_DOWNLOAD_PROGREESS = 1;
    private final int DIMISS_APP_DOWNLOAING_DIALOG = 2;
    private AtomicBoolean showAppDownloadingProgress = new AtomicBoolean(true);
    private static AtomicBoolean appDownloading = new AtomicBoolean(false);
    private AtomicBoolean showFirmwareDownloadingProgress = new AtomicBoolean(false);
    private static AtomicBoolean firmwareDownloading = new AtomicBoolean(false);

    private final String NEWAPKDIR = "ShiXingNewApkDir";

    private BleDataLogDialog bleDataLogDialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_systemsettings_layout, container, false);
        ButterKnife.bind(this, view);
        initData();
        initView(view);
        initListener();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        DfuServiceListenerHelper.registerProgressListener(getActivity(), dfuProgressListener);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        DfuServiceListenerHelper.unregisterProgressListener(getActivity(), dfuProgressListener);
    }

    private void initView(View view) {
        functionItemTextView.setText(getResources().getText(R.string.system_settings_text));
        initRecyclerView();
    }

    private void initData() {
        systemSettingBeans.clear();
        String[] settings = getResources().getStringArray(R.array.system_settings);
        SystemSettingBean versionBean = new SystemSettingBean();
        versionBean.setItemPrefixImageResourceId(R.mipmap.banbengengxin);
        versionBean.setItemTilte(settings[0]);
        versionBean.setItemSufixImageResource(R.mipmap.jiantou);


      /*  SystemSettingBean apkupdateBean = new SystemSettingBean();

        apkupdateBean.setItemPrefixImageResourceId(R.mipmap.appgengxin);
        apkupdateBean.setItemTilte(settings[1]);
        apkupdateBean.setItemSufixImageResource(R.mipmap.jiantou);*/

        SystemSettingBean firmwareBean = new SystemSettingBean();

        firmwareBean.setItemPrefixImageResourceId(R.mipmap.gujiangengxin);
        firmwareBean.setItemTilte(settings[2]);
        firmwareBean.setItemSufixImageResource(R.mipmap.jiantou);

        systemSettingBeans.add(versionBean);
        //systemSettingBeans.add(apkupdateBean);
        systemSettingBeans.add(firmwareBean);

        SystemSettingBean userInfoBean = new SystemSettingBean();

        userInfoBean.setItemPrefixImageResourceId(R.mipmap.gujiangengxin);
        userInfoBean.setItemTilte(settings[3]);
        userInfoBean.setItemSufixImageResource(R.mipmap.jiantou);

        SystemSettingBean machineInfoBean = new SystemSettingBean();

        machineInfoBean.setItemPrefixImageResourceId(R.mipmap.gujiangengxin);
        machineInfoBean.setItemTilte(settings[4]);
        machineInfoBean.setItemSufixImageResource(R.mipmap.jiantou);

        systemSettingBeans.add(userInfoBean);
        systemSettingBeans.add(machineInfoBean);
    }

    private void initRecyclerView() {
        /**
         * 初始化 RecyclerView
         */

        Activity activity = getActivity();
        if (null == activity) {
            return;
        }

        final GridLayoutManager manager = new GridLayoutManager(activity, 1);

        systemSettingRecyclerViewAdapter = new SystemSettingRecyclerViewAdapter(this);
        systemSettingGloriousRecyclerView.setAdapter(systemSettingRecyclerViewAdapter);

        GridItemDecoration gridItemDecoration = new GridItemDecoration.Builder(activity)
                .size((int) DeviceUtils.dipToPx(activity, 4))
                .color(R.color.assit_view_division_color)
                .margin(0, 0)
                .isExistHead(false)
                .showHeadDivider(false)
                .showLastDivider(false)
                .build();

        systemSettingGloriousRecyclerView.addItemDecoration(gridItemDecoration);
        systemSettingGloriousRecyclerView.setLayoutManager(manager);
        systemSettingRecyclerViewAdapter.setDatas(systemSettingBeans);
    }

    protected void initListener() {
        functionItemBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
            }
        });

        systemSettingRecyclerViewAdapter.setmOnItemClickListener(new OnItemClickListener() {
            @Override
            public void OnItemClick(int position) {
                SystemSettingBean systemSettingBean = systemSettingBeans.get(position);
                handleItemClick(position);

            }
        });

        exitLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logoutToken();
            }
        });
    }

    private void handleItemClick(int itemIndex) {
        switch (itemIndex) {
            case 0:
                getApkVersionInfo();
                break;

            case 1:

                getFiremwareVersionInfo();
                break;

            case 2:
                showUserInfoLog();
                break;

            case 3:
                showMachineInfoLog();
                break;

            default:
                break;
        }
    }

    private void showAppUpdateDialog(String newVersionInfo, final boolean isNewVersion,final HttpResponseApkVersionInfoBean httpResponseApkVersionInfoBean) {

        if (null == httpResponseApkVersionInfoBean || null == httpResponseApkVersionInfoBean.getData()
                || !httpResponseApkVersionInfoBean.getStatus().equals(IBaseRequest.SUCCESS)) {
            return;
        }
        if (appDownloading.get()) {
            ToastUtil.showShort("正在下载中");
            return;
        }

        if (null == appUpdateDialog) {
            appUpdateDialog = new AppUpdateDialog(getActivity());
            appUpdateDialog.setDialogButtonClickListener(new DialogButtonClickListener() {
                @Override
                public void onButtonClick(int resourceId) {
                    if (resourceId == R.id.updatenewappversionbtn) {
                        if (isNewVersion) {
                            handleAppUpdate(httpResponseApkVersionInfoBean);
                            appUpdateDialog.dismiss();
                        } else {
                            appUpdateDialog.dismiss();
                        }
                    }
                }
            });
        }
        String appupdateVersionInfo = null;
        if (isNewVersion) {
            appUpdateDialog.setNewVersion(true);
            appupdateVersionInfo = getString(R.string.app_update_verion_info_text);
        } else {
            appUpdateDialog.setNewVersion(false);
            appupdateVersionInfo = newVersionInfo;
        }
        appUpdateDialog.showTip(appupdateVersionInfo);
    }

    private void handleAppUpdate(HttpResponseApkVersionInfoBean httpResponseApkVersionInfoBean) {
        //ToastUtil.showShort(httpResponseGetLastedSystemUpdateBean.toString());
        if (null  != httpResponseApkVersionInfoBean &&
                httpResponseApkVersionInfoBean.getStatus().equalsIgnoreCase(IBaseRequest.SUCCESS)) {
            HttpResponseApkVersionInfoBean.DataBean dataBean = httpResponseApkVersionInfoBean.getData();
            if (null != dataBean) {
                String version = dataBean.getVersion();
                String versionFileStr = getApkVersion();
                if (!versionFileStr.equalsIgnoreCase(version)) {
                    /**
                     * 通知栏中提示有版本更新
                     */
                    String apkDownLoadPath = dataBean.getFilePath();
                    apkDownLoadPath = CommonUtils.startWithHttp(apkDownLoadPath) ? apkDownLoadPath : URLConstant.BASE_URL + apkDownLoadPath;
                    String host = "";
                    try {
                        URL url = new URL(apkDownLoadPath);
                        host = url.getHost();
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }

                    String baseUrl = URLConstant.HTTP_PREFIX + host + File.separator;
                    MLog.d("baseUrl = " + baseUrl);
                    final String apkAbsPath = getApkSavePath(dataBean.getVersion());
                    MLog.d("apkAbsPath = " + apkAbsPath);

                    final File apkFile = new File(apkAbsPath);
                    FileUtils.deleteFiles(apkFile.getParentFile());

                    if (!apkFile.getParentFile().exists()) {
                        apkFile.getParentFile().mkdirs();
                    }
                    createApkHandler();
                    showAppDownloadingProgress.set(true);
                    showNumDloadingDialog(0, true);
                    DownloadUtils downloadUtils = new DownloadUtils(baseUrl, new JsDownloadListener() {
                        @Override
                        public void onStartDownload(long length) {
                            //MLog.d("length = " + length + " thread.id = " + Thread.currentThread().getId());
                            appTotalLength = length;
                            appDownloading.set(true);
                        }

                        @Override
                        public void onProgress(int progress) {
                            //MLog.d("progress = " + progress + "Thread.id = " + Thread.currentThread().getId());
                            if (showAppDownloadingProgress.get()) {
                                if (null != apkHandler) {
                                    Message message = apkHandler.obtainMessage();
                                    if (null != message) {
                                        message.what = UPDATE_DOWNLOAD_PROGREESS;
                                        message.obj = Integer.valueOf((int) (progress * 1.0f * 100 / appTotalLength));
                                        message.sendToTarget();
                                    }
                                }
                            } else {
                                if (null != apkHandler) {
                                    Message message = apkHandler.obtainMessage();
                                    if (null != message) {
                                        message.what = DIMISS_APP_DOWNLOAING_DIALOG;
                                        message.sendToTarget();
                                    }
                                }
                            }
                        }

                        @Override
                        public void onFail(String errorInfo) {
                            MLog.d("errorInfo = " + errorInfo);
                            dismissDloadingNumDialog();
                            appDownloading.set(false);
                        }
                    });

                    Observable observable = downloadUtils.getDownApi(apkDownLoadPath);//HttpMethods.getInstance().getHttpApi().downloadFile(new HashMap<String, Object>(), TrainSuscribe.createNewRequestBody(new HashMap<String, String>()), exeUrl);
                    downloadUtils.download(apkFile, observable, new DisposableObserver<InputStream>() {
                        @Override
                        public void onNext(InputStream responseBody) {
                            MLog.d("onNext = responseBody" + responseBody);
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                            MLog.d("onError = " + e.getMessage());
                            dismissDloadingNumDialog();
                            appDownloading.set(false);
                        }

                        @Override
                        public void onComplete() {
                            appDownloading.set(false);
                            dismissDloadingNumDialog();
                            MLog.d("onComplete = file = " + apkFile.length() + " thread.id = " + Thread.currentThread().getId() );

                            String selfSignature = SignatureUtil.getCurrentApplicationSignature(MyApplication.getInstance());
                            List<String> signatureFromApk = null;
                            try {
                                signatureFromApk = SignatureUtil.getSignaturesFromApk(apkFile);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            if (null != signatureFromApk && signatureFromApk.size() > 0
                                    && !CommonUtils.isEmpty(signatureFromApk.get(0))
                                    && signatureFromApk.get(0).equals(selfSignature)) {
                                //ToastUtil.showShort("签名一致");
                                installApk(apkFile);
                            } else {
                                ToastUtil.showShort("签名不一致");
                                try {
                                    apkFile.delete();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    });


                }
            }
        }
    }

    private void handleFirmwareUpdate(HttpResponseFiremwareVersionInfoBean httpResponseFiremwareVersionInfoBean) {
        if (null  != httpResponseFiremwareVersionInfoBean &&
                httpResponseFiremwareVersionInfoBean.getStatus().equalsIgnoreCase(IBaseRequest.SUCCESS)) {
            HttpResponseFiremwareVersionInfoBean.DataBean dataBean = httpResponseFiremwareVersionInfoBean.getData();
            if (null != dataBean) {
                /**
                 * 通知栏中提示有版本更新
                 */
                String binDownLoadPath = dataBean.getFilePath();
                binDownLoadPath = CommonUtils.startWithHttp(binDownLoadPath) ? binDownLoadPath : URLConstant.BASE_URL + binDownLoadPath;
                String host = "";
                try {
                    URL url = new URL(binDownLoadPath);
                    host = url.getHost();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }

                String baseUrl = URLConstant.HTTP_PREFIX + host + File.separator;
                MLog.d("baseUrl = " + baseUrl);
                final String binAbsPath = getBinSavePath(dataBean.getVersion());
                MLog.d("binAbsPath = " + binAbsPath);

                final File binFile = new File(binAbsPath);
                FileUtils.deleteFiles(binFile.getParentFile());

                if (!binFile.getParentFile().exists()) {
                    binFile.getParentFile().mkdirs();
                }
                createFirewareHander();
                showFirmwareDownloadingProgress.set(true);
                showFireWareDialog(0, true);
                DownloadUtils downloadUtils = new DownloadUtils(baseUrl, new JsDownloadListener() {
                    @Override
                    public void onStartDownload(long length) {
                        binTotalLength = length;
                        firmwareDownloading.set(true);
                    }

                    @Override
                    public void onProgress(int progress) {
                        if (showFirmwareDownloadingProgress.get()) {
                            if (null != firmWareHandler) {
                                Message message = firmWareHandler.obtainMessage();
                                if (null != message) {
                                    message.what = UPDATE_DOWNLOAD_PROGREESS;
                                    message.obj = Integer.valueOf((int) (progress * 1.0f * 100 / binTotalLength));
                                    message.sendToTarget();
                                }
                            }
                        } else {
                            if (null != firmWareHandler) {
                                Message message = firmWareHandler.obtainMessage();
                                if (null != message) {
                                    message.what = DIMISS_APP_DOWNLOAING_DIALOG;
                                    message.sendToTarget();
                                }
                            }
                        }

                    }

                    @Override
                    public void onFail(String errorInfo) {
                        MLog.d("errorInfo = " + errorInfo);
                        dismissFirewareDownloadProgressDialog();
                        firmwareDownloading.set(false);
                    }
                });

                Observable observable = downloadUtils.getDownApi(binDownLoadPath);//HttpMethods.getInstance().getHttpApi().downloadFile(new HashMap<String, Object>(), TrainSuscribe.createNewRequestBody(new HashMap<String, String>()), exeUrl);
                downloadUtils.download(binFile, observable, new DisposableObserver<InputStream>() {
                    @Override
                    public void onNext(InputStream responseBody) {
                        MLog.d("onNext = responseBody" + responseBody);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        MLog.d("onError = " + e.getMessage());
                        dismissFirewareDownloadProgressDialog();
                        firmwareDownloading.set(false);
                    }

                    @Override
                    public void onComplete() {
                        firmwareDownloading.set(false);
                        dismissFirewareDownloadProgressDialog();
                        MLog.d("onComplete = binfile = " + binFile.length() + " thread.id = " + Thread.currentThread().getId() );
                        sendBin2Glasses(binFile);
                    }
                });

            }
        }
    }


    private void showFirmwareUpdateDialog(String newVersionInfo, final boolean isNewVersion,final HttpResponseFiremwareVersionInfoBean httpResponseFiremwareVersionInfoBean) {
        if (null == httpResponseFiremwareVersionInfoBean || null == httpResponseFiremwareVersionInfoBean.getData()
                || !httpResponseFiremwareVersionInfoBean.getStatus().equals(IBaseRequest.SUCCESS)) {
            return;
        }

        if (firmwareDownloading.get()) {
            ToastUtil.showShort("正在下载中");
            return;
        }

        if (null == firmwareUpdateDialog) {
            firmwareUpdateDialog = new FirmwareUpdateDialog(getActivity());
            firmwareUpdateDialog.setDialogButtonClickListener(new DialogButtonClickListener() {
                @Override
                public void onButtonClick(int resourceId) {
                    if (resourceId == R.id.updatenewappversionbtn) {
                        if (isNewVersion) {
                            //ToastUtil.showShort("开始下载新版本");
                            handleFirmwareUpdate(httpResponseFiremwareVersionInfoBean);
                            firmwareUpdateDialog.dismiss();
                        } else {
                            firmwareUpdateDialog.dismiss();
                        }
                    } else {
                        firmwareUpdateDialog.dismiss();
                    }
                }
            });
        }
        String firemwareVersionInfo = null;
        if (isNewVersion) {
            firmwareUpdateDialog.setNewVersion(true);
            firemwareVersionInfo = getString(R.string.firmware_update_verion_info_text);
        } else {
            firmwareUpdateDialog.setNewVersion(false);
            firemwareVersionInfo = "已是最新版本";
            //firemwareVersionInfo = newVersionInfo;
        }
        firmwareUpdateDialog.showTip(firemwareVersionInfo);
    }

    private void dimissAppUpdateDialog() {
        if (null != appUpdateDialog) {
            appUpdateDialog.dismiss();
        }
    }

    private void dismissfirmwareDialog() {
        if (null != firmwareUpdateDialog) {
            firmwareUpdateDialog.dismiss();
        }
    }

    private void logoutToken() {
        HashMap<String, Object> headerMap = new HashMap<>();
        HashMap<String, Object> bodyMap = new HashMap<>();
        bodyMap.put(IOauthToken.ACCESS_TOKEN, Config.getConfig().getAccessToken());
        bodyMap.put(IOauthToken.REFRESH_TOKEN, Config.getConfig().getFreshToken());
        LoginSubscribe.logoutToken(headerMap, bodyMap, new DisposableObserver<ResponseBody>() {
            @Override
            public void onNext(ResponseBody responseBody) {
                String responseStr = null;
                try {
                    responseStr = new String(responseBody.bytes());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                OauthLogoutBean oauthLogoutBean = (OauthLogoutBean) JsonUtil.json2objectWithDataCheck(responseStr, new TypeReference<OauthLogoutBean>() {});
                //MLog.d("logoutToken responseStr = " + responseStr);
                if (null != oauthLogoutBean && !CommonUtils.isEmpty(oauthLogoutBean.getStatus())) {
                    Config.getConfig().saveFreshToken(null);
                    Config.getConfig().saveAccessToken(null);
                    Config.getConfig().savePasswd(null);
                    BleDeviceManager.getInstance().stopScan();
                    BleDeviceManager.getInstance().stopScanByMac();
                    BleDeviceManager.getInstance().disconnectGlassesBleDevice(true);
                    GlassesBleDataModel.getInstance().clearModelData();
                    TrainModel.getInstance().clearTrainModelData();
                    //要求用户直接登录
                    BaseActivity.GotoLoginActivity();
                }else {
                    if (null != oauthLogoutBean && !CommonUtils.isEmpty(oauthLogoutBean.getMessage())) {
                        ToastUtil.showShort(oauthLogoutBean.getMessage());
                    } else {
                        ToastUtil.showShort(R.string.interface_error_text);
                    }
                }
            }

            @Override
            public void onError(Throwable e) {
                ToastUtil.showShort(R.string.net_error_textt);
            }

            @Override
            public void onComplete() {

            }
        });
    }

    private String getApkVersion() {
        byte[] apkVersion = FileUtils.readFileContentOfAssets(MyApplication.getInstance(), "commit-msg.txt");
        if (null != apkVersion) {
            return new String(apkVersion);
        }
        return "";
    }

    private String getCurrentFiremwareVersion() {
        return "";
    }

    /**
     *  获取app最新的版本信息
     */
    private void getApkVersionInfo() {

        HashMap<String, Object> headerMap = new HashMap<>();
        HashMap<String, Object> bodyMap = new HashMap<>();
        bodyMap.put(IgetLastedSystemUpdate.FILETYPE, IgetLastedSystemUpdate.FILETYPE_ANDROID);
        DisposableObserver disposableObserver = new DisposableObserver<ResponseBody>() {
            @Override
            public void onNext(ResponseBody responseBody) {
                disposableObserverList.remove(this);
                String jsonBody = null;
                try {
                    jsonBody = new String(responseBody.bytes());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                //HttpResponseApkVersionInfoBean httpResponseGetLastedSystemUpdateBean = GsonTools.changeGsonToBean(jsonBody, HttpResponseApkVersionInfoBean.class);
                HttpResponseApkVersionInfoBean httpResponseGetLastedSystemUpdateBean = (HttpResponseApkVersionInfoBean) JsonUtil.json2objectWithDataCheck(jsonBody, new TypeReference<HttpResponseApkVersionInfoBean>() {});
                handlerApkVersionInfo(httpResponseGetLastedSystemUpdateBean);
            }

            @Override
            public void onError(Throwable e) {
                disposableObserverList.remove(this);
                ToastUtil.showShort(R.string.net_error_textt);
            }

            @Override
            public void onComplete() {

            }
        };
        disposableObserverList.add(disposableObserver);

        TrainSuscribe.getApkVersionInfo(headerMap, bodyMap, disposableObserver);
    }

    /**
     *  获取固件最新的版本信息
     */
    private void getFiremwareVersionInfo() {

        final HashMap<String, Object> headerMap = new HashMap<>();
        HashMap<String, Object> bodyMap = new HashMap<>();
        bodyMap.put(IgetLastedSystemUpdate.FILETYPE, IgetLastedSystemUpdate.FILETYPE_FIRMWARE);
        DisposableObserver disposableObserver = new DisposableObserver<ResponseBody>() {
            @Override
            public void onNext(ResponseBody responseBody) {
                disposableObserverList.remove(this);

                String jsonBody = null;
                try {
                    jsonBody = new String(responseBody.bytes());
                } catch (IOException e) {
                    e.printStackTrace();
                }
               /* HttpResponseFiremwareVersionInfoBean httpResponseFiremwareVersionInfoBean = (HttpResponseFiremwareVersionInfoBean) JsonUtil.json2objectWithDataCheck(jsonBody, new TypeReference<HttpResponseFiremwareVersionInfoBean>() {
                });*/
                //HttpResponseFiremwareVersionInfoBean httpResponseFiremwareVersionInfoBean = GsonTools.changeGsonToBean(jsonBody, HttpResponseFiremwareVersionInfoBean.class);
                HttpResponseFiremwareVersionInfoBean httpResponseFiremwareVersionInfoBean = (HttpResponseFiremwareVersionInfoBean) JsonUtil.json2objectWithDataCheck(jsonBody, new TypeReference<HttpResponseFiremwareVersionInfoBean>() {});
                handleFiremwareVersionInfo(httpResponseFiremwareVersionInfoBean);
            }

            @Override
            public void onError(Throwable e) {
                disposableObserverList.remove(this);
                ToastUtil.showShort(R.string.net_error_textt);
            }

            @Override
            public void onComplete() {

            }
        };
        disposableObserverList.add(disposableObserver);
        TrainSuscribe.getApkVersionInfo(headerMap, bodyMap, disposableObserver);
    }

    private void handlerApkVersionInfo(HttpResponseApkVersionInfoBean httpResponseApkVersionInfoBean) {
        if (null != httpResponseApkVersionInfoBean && httpResponseApkVersionInfoBean.getStatus().equals(IBaseRequest.SUCCESS)) {
            HttpResponseApkVersionInfoBean.DataBean dataBean = httpResponseApkVersionInfoBean.getData();
            String currentVersion = getApkVersion();
            if (!CommonUtils.isEmpty(dataBean.getVersion())
                    && !CommonUtils.isEmpty(currentVersion)
                    && !dataBean.getVersion().equals(currentVersion)) {
                showAppUpdateDialog(dataBean.getVersion() , true, httpResponseApkVersionInfoBean);
            } else {
                showAppUpdateDialog(currentVersion , false, httpResponseApkVersionInfoBean);
            }
        } else {
            ToastUtil.showShort("获取应用版本信息失败");
        }

    }

    private void handleFiremwareVersionInfo(HttpResponseFiremwareVersionInfoBean httpResponseFiremwareVersionInfoBean) {
        /***
         * 通过蓝牙协议查询当前固件信息，比对线上存储的固件版本信息
         */
        if (null != httpResponseFiremwareVersionInfoBean && httpResponseFiremwareVersionInfoBean.getStatus().equals(IBaseRequest.SUCCESS)) {
            HttpResponseFiremwareVersionInfoBean.DataBean dataBean = httpResponseFiremwareVersionInfoBean.getData();
            String currentFiremwareVersion = getCurrentFiremwareVersion();
            if (!CommonUtils.isEmpty(dataBean.getVersion())
                && !CommonUtils.isEmpty(currentFiremwareVersion)
                && !dataBean.getVersion().equals(currentFiremwareVersion)) {
                showFirmwareUpdateDialog(dataBean.getVersion(), true, httpResponseFiremwareVersionInfoBean);
            } else {
                showFirmwareUpdateDialog(currentFiremwareVersion, false, httpResponseFiremwareVersionInfoBean);
            }
        }else {
            ToastUtil.showShort("获取固件版本信息失败");
        }

    }

    private void createApkHandler() {

        if (null == apkHandler) {
            apkHandler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    super.handleMessage(msg);
                    switch (msg.what) {
                        case UPDATE_DOWNLOAD_PROGREESS:
                            showNumDloadingDialog((Integer) msg.obj, false);
                            break;

                        case DIMISS_APP_DOWNLOAING_DIALOG:
                            dismissDloadingNumDialog();
                            break;
                    }
                }
            };
        }

    }

    private void createFirewareHander() {
        if (null == firmWareHandler) {
            firmWareHandler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    super.handleMessage(msg);
                    switch (msg.what) {
                        case UPDATE_DOWNLOAD_PROGREESS:
                            showFireWareDialog((Integer) msg.obj, false);
                            break;

                        case DIMISS_APP_DOWNLOAING_DIALOG:
                            dismissFirewareDownloadProgressDialog();
                            break;
                    }
                }
            };
        }
    }


    private void showFireWareDialog(int progress, boolean forceShow) {
        if (null == firmwareDownloadNumProgressDialog) {
            firmwareDownloadNumProgressDialog = new DLoadingNumProcessDialog(getActivity());
            firmwareDownloadNumProgressDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {
                    showFirmwareDownloadingProgress.set(false);
                }
            });
            firmwareDownloadNumProgressDialog.setCanceledOnTouchOutside(false);
            firmwareDownloadNumProgressDialog.show(progress);
        }

        if (forceShow) {
            firmwareDownloadNumProgressDialog.show();
        }
        if (firmwareDownloadNumProgressDialog.isShowing()) {
            firmwareDownloadNumProgressDialog.setProgrees(progress);
        }

    }

    private void showNumDloadingDialog(int progress, boolean forceShow) {
        if (null == dLoadingNumProcessDialog) {
            dLoadingNumProcessDialog = new DLoadingNumProcessDialog(getActivity());
            dLoadingNumProcessDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {
                    showAppDownloadingProgress.set(false);
                }
            });

            dLoadingNumProcessDialog.setCanceledOnTouchOutside(true);
            dLoadingNumProcessDialog.show(progress);
        }

        if (forceShow) {
            dLoadingNumProcessDialog.show();
        }

        if (dLoadingNumProcessDialog.isShowing()) {
            dLoadingNumProcessDialog.setProgrees(progress);
        }
    }

    private void dismissDloadingNumDialog() {
        if (null != dLoadingNumProcessDialog) {
            dLoadingNumProcessDialog.dismiss();
        }
    }

    private void dismissFirewareDownloadProgressDialog() {
        if (null != firmwareDownloadNumProgressDialog) {
            firmwareDownloadNumProgressDialog.dismiss();
        }
    }

    @Override
    public void onDestroy() {
        dimissAppUpdateDialog();
        dismissDloadingNumDialog();
        dismissfirmwareDialog();
        dismissFirewareDownloadProgressDialog();
        super.onDestroy();
        removeAllDisposableObserver();
    }

    private void removeAllDisposableObserver() {
        for (DisposableObserver disposableObserver : disposableObserverList) {
            if (null != disposableObserver) {
                disposableObserver.dispose();
            }
        }
    }

    private String getApkSavePath(String versionCode) {
        Context context = MyApplication.getInstance();
        String packageName = context.getPackageName();
        String renameNewApkName = packageName + "_" + versionCode + ".apk";
        return MyApplication.getInstance().getHomePath() + File.separator + NEWAPKDIR + File.separator + renameNewApkName;
    }

    private String getBinSavePath(String versionCode) {
        Context context = MyApplication.getInstance();
        String packageName = context.getPackageName();
        String renameNewApkName = packageName + "_" + versionCode + ".bin";
        return MyApplication.getInstance().getHomePath() + File.separator + NEWAPKDIR + File.separator + renameNewApkName;
    }

    private void installApk(File apkFile) {
        MLog.d(Thread.currentThread().getId() + " installApk = " + apkFile.getAbsolutePath() + " ");
        Intent intent = new Intent(Intent.ACTION_VIEW);
        // 由于没有在Activity环境下启动Activity,设置下面的标签
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) { //判读版本是否在7.0以上
            //参数1 上下文, 参数2 Provider主机地址 和配置文件中保持一致   参数3  共享的文件
            Uri apkUri =
                    FileProvider.getUriForFile(MyApplication.getInstance(), MyApplication.getInstance().getPackageName() + ".fileprovider", apkFile);
            //添加这一句表示对目标应用临时授权该Uri所代表的文件
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.setDataAndType(apkUri, "application/vnd.android.package-archive");
        } else {
            intent.setDataAndType(Uri.fromFile(apkFile),
                    "application/vnd.android.package-archive");
        }
        MyApplication.getInstance().startActivity(intent);
    }

    /**
     * 将 bin 文件的内容发送给蓝牙眼镜，执行更新固件操作
     */
    private void sendBin2Glasses(File binFile) {
        ToastUtil.showShort("发送新的固件文件给眼镜，执行更新固件操作");
        String zipFilePath = binFile.getAbsolutePath();
        MLog.i("zipFilePath = " + zipFilePath);
        startUpdate(zipFilePath);
    }

    public void startUpdate(String filepath) {
        String mac = Config.getConfig().getLastConnectBleGlassesMac();
        String name = Config.getConfig().getLastConnectBleGlassesName();

        DfuServiceInitiator starter = new DfuServiceInitiator(mac)
                .setDeviceName(name)
                .setKeepBond(true);
        starter.setUnsafeExperimentalButtonlessServiceInSecureDfuEnabled(true);
        starter.setZip(Uri.fromFile(new File(filepath)), filepath);
        DfuServiceController controller = starter.start(MyApplication.getInstance(), DfuService.class);
    }

    private final DfuProgressListener dfuProgressListener = new DfuProgressListener() {
        @Override
        public void onDeviceConnecting(String deviceAddress) {
//          progressBar.setIndeterminate(true);
//          mTextPercentage.setText(R.string.dfu_status_connecting);
            Log.i("TEST", "onDeviceConnecting: " + deviceAddress);
        }

        @Override
        public void onDeviceConnected(String deviceAddress) {
            Log.i("TEST", "onDeviceConnected: " + deviceAddress);
        }

        @Override
        public void onDfuProcessStarting(String deviceAddress) {
//          progressBar.setIndeterminate(true);
//          mTextPercentage.setText(R.string.dfu_status_starting);
            Log.i("TEST", "onDfuProcessStarting: " + deviceAddress);


        }

        @Override
        public void onDfuProcessStarted(String deviceAddress) {
            Log.i("TEST", "onDfuProcessStarted: " + deviceAddress);
        }

        @Override
        public void onEnablingDfuMode(String deviceAddress) {
            Log.i("TEST", "onEnablingDfuMode: " + deviceAddress);
        }

        @Override
        public void onProgressChanged(String deviceAddress, int percent, float speed, float avgSpeed, int currentPart, int partsTotal) {
            Log.i("TEST", "onProgressChanged: " + deviceAddress + "百分比" + percent + ",speed "
                    + speed + ",avgSpeed " + avgSpeed + ",currentPart " + currentPart
                    + ",partTotal " + partsTotal);
            //tv_show.setText("升级进度：" + percent + "%");
        }

        @Override
        public void onFirmwareValidating(String deviceAddress) {
            Log.i("TEST", "onFirmwareValidating: " + deviceAddress);
        }

        @Override
        public void onDeviceDisconnecting(String deviceAddress) {
            Log.i("TEST", "onDeviceDisconnecting: " + deviceAddress);
        }

        @Override
        public void onDeviceDisconnected(String deviceAddress) {
            Log.i("TEST", "onDeviceDisconnected: " + deviceAddress);
        }

        @Override
        public void onDfuCompleted(String deviceAddress) {
            Log.i("TEST", "onDfuCompleted: " + deviceAddress);
//          progressBar.setIndeterminate(true);
            //progressBar.setVisibility(View.GONE);
        }

        @Override
        public void onDfuAborted(String deviceAddress) {
            Log.i("TEST", "onDfuAborted: " + deviceAddress);
            //progressBar.setVisibility(View.GONE);
        }

        @Override
        public void onError(String deviceAddress, int error, int errorType, String message) {
            Log.i("TEST", "onError: " + deviceAddress + ",message:" + message);
            //progressBar.setVisibility(View.GONE);
        }
    };

    private void showUserInfoLog() {
        String json = FileUtils.read(MyApplication.getInstance(), TrainModel.HTTP_LOGIN_USER_INFO);

        SendUserInfoBleCmdBean sendUserInfoBleCmdBean = TrainModel.getInstance().createUserInfoBleHexDataForTrain(Config.getConfig().getLastSelectedTrainMode() + 1, 1, 1);
        String sendUserInfo = sendUserInfoBleCmdBean.toString();
        if (!CommonUtils.isEmpty(json)) {
            MemberLoginResponseBean memberLoginResponseBean =  GsonTools.changeGsonToBean(json, MemberLoginResponseBean.class);
            if (null != memberLoginResponseBean) {
                showBleDataLogDialog(memberLoginResponseBean.toString() +  "\n发送给眼镜的用户数据\n" + sendUserInfo);
                return;
            }
        }
        showBleDataLogDialog("发送给眼镜的用户数据\n" + sendUserInfo);
    }

    private void showMachineInfoLog() {
        String json = FileUtils.read(MyApplication.getInstance(), TrainModel.HTTP_GLASSES_INIT_MACHINE_FILE_NAME);

        HttpResponseGlassInitDataBackBean machineData = TrainModel.getInstance().getHttpResponseGlassInitDataBackBean();
        String machineDataStr = "初始机器数据为空";
        if (null != machineData && null != machineData.getData()) {
            SendMachineBleCmdBeaan sendMachineBleCmdBeaan = TrainModel.getInstance().createSendMachineBleHexDataForTrain();
            machineDataStr = sendMachineBleCmdBeaan.toString();
        }

        HttpResponseGlassesRunParamBean httpResponseGlassesRunParamBean = TrainModel.getInstance().getHttpResponseGlassesRunParamBean();

        String runParamStr = "运行参数数据为空";
        if (null != httpResponseGlassesRunParamBean) {
            runParamStr = httpResponseGlassesRunParamBean.toString();
        }

        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("\n发送机器数据为：\n" + machineDataStr + "\n发送的运行参数数据为:\n" + runParamStr);


        if (!CommonUtils.isEmpty(json)) {
            HttpResponseGlassInitDataBackBean httpResponseGlassInitDataBackBean = GsonTools.changeGsonToBean(json, HttpResponseGlassInitDataBackBean.class);
            if (null != httpResponseGlassInitDataBackBean) {
                showBleDataLogDialog(httpResponseGlassInitDataBackBean.toString() + stringBuffer.toString());
                return;
            }
        }
        showBleDataLogDialog(  stringBuffer.toString());
    }


    private void showBleDataLogDialog(String content) {
        if (null == bleDataLogDialog) {
            bleDataLogDialog = new BleDataLogDialog(getActivity());
        }
        bleDataLogDialog.showTip(content);
    }

    private void dimissBleDataDialog() {
        if (null != bleDataLogDialog) {
            bleDataLogDialog.dismiss();
        }
    }
}
