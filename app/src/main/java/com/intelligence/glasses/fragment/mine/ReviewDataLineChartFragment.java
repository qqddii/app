package com.intelligence.glasses.fragment.mine;

import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.intelligence.glasses.MyApplication;
import com.intelligence.glasses.base.BaseActivity;
import com.intelligence.glasses.base.BaseFragment;
import com.intelligence.glasses.bean.EyeSightBean;
import com.intelligence.glasses.greendao.greendaobean.ReviewDataEyeSightDBBean;
import com.intelligence.glasses.greendao.greenddaodb.ReviewDataEyeSightBeanDaoOpe;
import com.shitec.bleglasses.BuildConfig;
import com.shitec.bleglasses.R;
import com.android.common.baselibrary.log.MLog;
import com.android.common.baselibrary.util.comutil.CommonUtils;
import com.intelligence.glasses.util.Config;
import com.intelligence.glasses.util.view.ui.DeviceUtils;
import com.intelligence.glasses.view.ReviewDataLineChartWeekView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 复查数据，线性图表
 */

public class ReviewDataLineChartFragment extends BaseFragment {
    private List<String> trainTimeStrList = new ArrayList<>();
    private HashMap<String, Float> trainDateAndTime = new HashMap<>();
    private LinearLayout visionoTextLayout;

    private List<ReviewDataEyeSightDBBean> reviewDataEyeSightDBBeanList = null;

    private boolean showLastSevenTimes = true;//显示最后其次的复查数据

    @BindView(R.id.function_item_backlayout)
    LinearLayout functionItemBackLayout;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_review_detail_line_chart_layout, container, false);
        ButterKnife.bind(this, view);
        initData();
        initView(view);
        initListener();
        return view;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void initView(View view) {
        visionoTextLayout = view.findViewById(R.id.visionlayout);
        initLineChartView(view);

        resetVisionTextViewLayoutWidth();
    }

    private void initData() {
        String memberId = Config.getConfig().getUserServerId();
        if (CommonUtils.isEmpty(memberId)) {
            BaseActivity.GotoLoginActivity();
            return;
        }

        if (BuildConfig.DEBUG) {
            reviewDataEyeSightDBBeanList = createTestData();
        } else {
            reviewDataEyeSightDBBeanList = ReviewDataEyeSightBeanDaoOpe.queryAllByMemeberId(MyApplication.getInstance(), memberId);
        }
        //List<UserInfoDBBean> list = UserInfoBeanDaoOpe.queryUserInfoByServerID(getActivity().getApplicationContext(), memberId);
        //UserInfoDBBean userInfoDBBean = list.get(0);
    }

    private List<ReviewDataEyeSightDBBean> createTestData() {
        List<ReviewDataEyeSightDBBean> list = new ArrayList<>();
        for (int i = 0; i < 8; i++) {
            ReviewDataEyeSightDBBean reviewDataEyeSightDBBean = new ReviewDataEyeSightDBBean();
            reviewDataEyeSightDBBean.setReviewEyeSightTimes(i + 1);
            reviewDataEyeSightDBBean.setTrainTimeLong( 80 * (i + 1));
            reviewDataEyeSightDBBean.setLeftEyeSight( 4.1f + 0.1f * i);
            reviewDataEyeSightDBBean.setRightEyeSight( 4.2f + 0.1f * i);
            reviewDataEyeSightDBBean.setDoubleEyeSight( 4.3f + 0.1f * i);
            list.add(reviewDataEyeSightDBBean);
        }
        return list;
    }

    protected void initListener() {
        functionItemBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
            }
        });
    }

    /**
     * 初始化单柱柱状图
     */
    public void initLineChartView(View view) {
        final LinearLayout llSingle = (LinearLayout) LayoutInflater.from(MyApplication.getInstance()).inflate(R.layout.layout_pro_expense, null);

        final ReviewDataLineChartWeekView mMySingleChartView = view.findViewById(R.id.reviewdatalinechartweekview);
        ImageView leftImageView = view.findViewById(R.id.leftimage);
        ImageView rightImageView = view.findViewById(R.id.rightimage);
        final List<EyeSightBean> eyeSight = new ArrayList<>();
        final List<String> trainDurationStrList = new ArrayList<>();//训练时长
        final List<String> timesStrList = new ArrayList<>();//第几次

        final RelativeLayout rlSingle = view.findViewById(R.id.revieweyesightcharlinelayout);

        getTrainTimeByBottomDate(eyeSight,timesStrList, trainDurationStrList, true);
        mMySingleChartView.setList(eyeSight, timesStrList, trainDurationStrList);
        rlSingle.removeView(llSingle);
        //原理同双柱
        mMySingleChartView.setListener(new ReviewDataLineChartWeekView.getNumberListener() {
            @Override
            public void getNumber(int number, int x, int y) {
                //Log.d("SingleView", "x = " + x + " y = " + y);
                rlSingle.removeView(llSingle);

                TextView tvMoney = (TextView) llSingle.findViewById(R.id.tv_shouru_pro);
                tvMoney.setText(String.valueOf(eyeSight.get(number)));
                llSingle.measure(0, 0);
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT);
                params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);

                int llsingleWdith = llSingle.getMeasuredWidth();
                int llsingleHeight = llSingle.getMeasuredHeight();
                //Log.d("SingleView", "llSingle.getMeasuredWidth() = " + llsingleWdith);
                params.leftMargin = x - llsingleWdith / 2;
                if (params.leftMargin < 0) {
                    params.leftMargin = 0;
                } else if (params.leftMargin > rlSingle.getWidth() - llsingleWdith) {
                    params.leftMargin = rlSingle.getWidth() - llSingle.getMeasuredWidth();
                }
                int textPaddtingBottom = (int) MyApplication.getInstance().getResources().getDimension(R.dimen.train_week_tip_text_paddingbottom_size);
                int marginBottom = (int)(y + textPaddtingBottom );

                llSingle.setPadding(0, 0, 0, marginBottom);
                llSingle.setLayoutParams(params);
                rlSingle.addView(llSingle);
            }
        });

        leftImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateSingleViewDate(mMySingleChartView, eyeSight, timesStrList, trainDurationStrList, rlSingle, llSingle, false);
            }
        });

        rightImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateSingleViewDate(mMySingleChartView, eyeSight, timesStrList, trainDurationStrList, rlSingle, llSingle, true);
            }
        });

    }

    private void updateSingleViewDate(ReviewDataLineChartWeekView mMySingleChartView, List<EyeSightBean> singlelist,  List<String> timesStrList, List<String> dateStrList, RelativeLayout rlSingle, LinearLayout llSingle, boolean isIncrease) {
        mMySingleChartView.clearSelectedRoles();

        getTrainTimeByBottomDate(singlelist, timesStrList, dateStrList,isIncrease);
        mMySingleChartView.setList(singlelist, timesStrList, dateStrList);
        rlSingle.removeView(llSingle);
    }

    /**
     * 获取要显示的复查数据
     * @param eyeSight
     * @param reviewDataTimesList
     * @param trainDurationList
     * @param isIncrease
     */
    private void getTrainTimeByBottomDate(List<EyeSightBean> eyeSight, List<String> reviewDataTimesList, List<String> trainDurationList, boolean isIncrease) {
        int lastTimes = 0;
        /**
         * 当更新的时候，需要检测上次的次数
         */
        if (null != reviewDataTimesList && reviewDataTimesList.size() > 1) {
            String lastTimesStr = null;
            if (isIncrease) {
                lastTimesStr = reviewDataTimesList.get(reviewDataTimesList.size() - 1);
            } else {
                lastTimesStr = reviewDataTimesList.get(1);
            }

            lastTimes = Integer.parseInt(lastTimesStr);
            MLog.d("lastTimes  = " + lastTimes);
        }

        eyeSight.clear();
        reviewDataTimesList.clear();
        trainDurationList.clear();


        int maxRevieTimes = 0;
        if (null != reviewDataEyeSightDBBeanList) {
            maxRevieTimes = reviewDataEyeSightDBBeanList.size();
        }
        reviewDataTimesList.add("0");
        trainDurationList.add("0");

        if (showLastSevenTimes) {
            lastTimes = maxRevieTimes;
        }

        if (isIncrease) {
            int maxTimes = Math.min(maxRevieTimes, lastTimes + 7);
            int minTimes = Math.min(maxTimes - 7, lastTimes + 1);
            minTimes = minTimes > 0 ? minTimes : 0;
            MLog.d(" minTimes = " + minTimes + " maxTimes = " + maxTimes);
            for (int i = minTimes; i < maxTimes; i++) {
                ReviewDataEyeSightDBBean eyeSightDBBean = reviewDataEyeSightDBBeanList.get(i);
                reviewDataTimesList.add(String.valueOf(eyeSightDBBean.getReviewEyeSightTimes()));
                trainDurationList.add(String.valueOf(eyeSightDBBean.getTrainTimeLong()));

                EyeSightBean eyeSightBean = new EyeSightBean();
                eyeSightBean.setLeftEyeSight((float) eyeSightDBBean.getLeftEyeSight());
                eyeSightBean.setRightEyeSight((float) eyeSightDBBean.getRightEyeSight());
                eyeSightBean.setDoubleEyeSight((float) eyeSightDBBean.getDoubleEyeSight());
                eyeSight.add(eyeSightBean);
            }
        } else {
            int minTimes = lastTimes - 7 > 0 ? lastTimes - 7 : 0;
            int maxTimes = Math.min(maxRevieTimes, minTimes + 7);
            MLog.d(" minTimes = " + minTimes + " maxTimes = " + maxTimes);
            for (int i = minTimes; i < maxTimes;  i++) {
                ReviewDataEyeSightDBBean eyeSightDBBean = reviewDataEyeSightDBBeanList.get(i);
                reviewDataTimesList.add(String.valueOf(eyeSightDBBean.getReviewEyeSightTimes()));
                trainDurationList.add(String.valueOf(eyeSightDBBean.getTrainTimeLong()));
                EyeSightBean eyeSightBean = new EyeSightBean();
                eyeSightBean.setLeftEyeSight((float) eyeSightDBBean.getLeftEyeSight());
                eyeSightBean.setRightEyeSight((float) eyeSightDBBean.getRightEyeSight());
                eyeSightBean.setDoubleEyeSight((float) eyeSightDBBean.getDoubleEyeSight());
                eyeSight.add(eyeSightBean);
            }
        }
    }

    private void resetVisionTextViewLayoutWidth() {
        float layoutWidth = getLeftHourTextWidth() * 3f;
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) visionoTextLayout.getLayoutParams();
        MLog.d(" layoutWidth = " + layoutWidth);
        layoutParams.width = (int)layoutWidth;
    }

    private float getLeftHourTextWidth() {
        final String maxText = "5.3";
        Paint   backgroundLinePaint = new Paint();
        Rect mLeftBound = new Rect();
        backgroundLinePaint.setColor(getResources().getColor(R.color.selectLeftColor));
        backgroundLinePaint.setTextSize(DeviceUtils.dipToPx(MyApplication.getInstance(), 13));
        backgroundLinePaint.setTextAlign(Paint.Align.CENTER);
        backgroundLinePaint.getTextBounds(maxText, 0, maxText.length(), mLeftBound);
        return mLeftBound.width();
    }


}
