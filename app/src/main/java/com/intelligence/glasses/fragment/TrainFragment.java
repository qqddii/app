package com.intelligence.glasses.fragment;

import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.common.baselibrary.log.MLog;
import com.android.common.baselibrary.util.CommonUtils;
import com.android.common.baselibrary.util.ToastUtil;
import com.intelligence.glasses.MyApplication;
import com.intelligence.glasses.adapter.TrainTwoModeAdapter;
import com.intelligence.glasses.base.BaseActivity;
import com.intelligence.glasses.base.BaseFragment;
import com.intelligence.glasses.bean.TrainModeBean;
import com.intelligence.glasses.bean.bledata.send.SendUserInfoControlBleCmdBean;
import com.intelligence.glasses.callback.CheckBleMacByServerCallBack;
import com.intelligence.glasses.constant.BleConnectStatus;
import com.intelligence.glasses.dialog.bleconnect.BleConnectStatusDialog;
import com.intelligence.glasses.dialog.bleconnect.BleInputDeviceIDDialog;
import com.intelligence.glasses.dialog.bleconnect.BleScanDialog;
import com.intelligence.glasses.event.BatteryEvent;
import com.intelligence.glasses.greendao.greendaobean.UserInfoDBBean;
import com.intelligence.glasses.greendao.greenddaodb.UserInfoBeanDaoOpe;
import com.intelligence.glasses.listener.DialogButtonClickListener;
import com.intelligence.glasses.listener.ItemOfViewPagerOnClickListener;
import com.intelligence.glasses.listener.MainControlActivityListener;
import com.intelligence.glasses.model.TrainModel;
import com.intelligence.glasses.util.Config;
import com.intelligence.glasses.util.view.dropdownmenu.DropBean;
import com.intelligence.glasses.view.RemoteControllerView;
import com.vise.baseble.model.BluetoothLeDevice;
import com.shitec.bleglasses.R;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2018/11/16.
 */

public class TrainFragment extends BaseFragment implements ItemOfViewPagerOnClickListener {

    @BindView(R.id.backlayout)
    LinearLayout backLayout;

    @BindView(R.id.batterytv)
    AppCompatTextView batteryTextView;

    @BindView(R.id.trainusernametext)
    AppCompatTextView userNameTextView;

    @BindView(R.id.disconnectlayout)
    LinearLayout batteryCloseLayout;

    @BindView(R.id.trainmodespinner)
    AppCompatSpinner trainModeSpinner;

    @BindView(R.id.bleconnectstatustv)
    AppCompatTextView bleConnectStatusTextView;

    @BindView(R.id.ble_connect_statusimg)
    AppCompatImageView bleConnectStatusImg;

    @BindView(R.id.blerecongizetv)
    AppCompatTextView bleRecongizeTextView;

    @BindView(R.id.ble_connect_sur_img)
    AppCompatImageView bleConnectRecongizeImg;

    @BindView(R.id.remotecontrolview)
    RemoteControllerView mRemoteControllerView;

    @BindView(R.id.remotecontrolviewlayout)
    LinearLayout mRemoteControlViewLayout;

    private LinearLayout mBleConnectLayout;
    TrainTwoModeAdapter trainModeAdapter;
    private TextView mBatteryTextView;

    private List<TrainModeBean> mTrainModeBeanList = new ArrayList<>();
    private List<DropBean> mTrainModeDropBeanList = new ArrayList<>();
    private Handler mHander = new Handler();
    private volatile AtomicBoolean  connected = new AtomicBoolean(false);
    private MainControlActivityListener mainControlActivityListener;
    private BleScanDialog bleScanDialog;
    private BleInputDeviceIDDialog  bleInputDeviceIDDialog;
    private BleConnectStatusDialog bleConnectStatusDialog;
    //private BleDeviceListialog mBleDeviceListialog;
    private int sexSelectedSpinnerIndex = -1;

    private UserInfoDBBean mUserInfoDBBean;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerEventBus();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_train_layout, container, false);
        ButterKnife.bind(this, view);
        initData();
        initView(view);
        initListener();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mainControlActivityListener = (MainControlActivityListener) getActivity();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void initView(View view) {
        bleRecongizeTextView.setText("未识别");
        //bleConnectRecongizeImg.setImageResource(R.mipmap.train_top_connect_indicator);
        final List<String> trainModeArray = Arrays.asList(getResources().getStringArray(R.array.traintwomode));
        trainModeAdapter = new TrainTwoModeAdapter(getActivity(), trainModeArray);
        trainModeSpinner.setAdapter(trainModeAdapter);
        int lastSelecteTrainModePostion = Config.getConfig().getLastSelectedTrainMode();
        trainModeSpinner.setSelection(lastSelecteTrainModePostion);
        sexSelectedSpinnerIndex = lastSelecteTrainModePostion;
        mRemoteControllerView.setmCurrentTrainMode(sexSelectedSpinnerIndex);
        trainModeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                //ToastUtil.showShortToast("position = " + position);
                sexSelectedSpinnerIndex = position;
                Config.getConfig().saveLastSelectedTrainMode(position);
                mRemoteControllerView.setmCurrentTrainMode(position);
                mainControlActivityListener.changeTrainModeFromTrainFragment(position + 1);
                //ToastUtil.showShort(trainModeArray.get(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        if (null != mUserInfoDBBean) {
            userNameTextView.setText(mUserInfoDBBean.getName());
        }

        showBatterLevel(Config.getConfig().getLastBattteryLevel());
    }

    private void initData() {
        String serverId = Config.getConfig().getUserServerId();
        if (CommonUtils.isEmpty(serverId)) {
            BaseActivity.GotoLoginActivity();
            return;
        }
        List<UserInfoDBBean> userInfoDBBeanList = UserInfoBeanDaoOpe.queryRawUserInfoByServerID(MyApplication.getInstance(), serverId);
        if (null != userInfoDBBeanList && userInfoDBBeanList.size() == 1) {
            mUserInfoDBBean = userInfoDBBeanList.get(0);
        }

    }

    protected void initListener() {

        mRemoteControllerView.setRemoteControllerClickListener(new RemoteControllerView.OnRemoteControllerClickListener() {
            @Override
            public void topClick() {
                handleRemoteControlViewClick(0);
            }

            @Override
            public void leftClick() {
                handleRemoteControlViewClick(1);
            }

            @Override
            public void rightClick() {
                handleRemoteControlViewClick(2);
            }

            @Override
            public void bottomClick() {
                handleRemoteControlViewClick(3);
            }

            @Override
            public void centerOkClick() {
                handleRemoteControlViewClick(4);
            }
        });

        batteryCloseLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleRemoteControlViewClick(5);
            }
        });

        bleConnectStatusImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainControlActivityListener.updateConnectStatusAndConnect();
            }
        });

        bleConnectRecongizeImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainControlActivityListener.updateConnectStatusAndConnect();
            }
        });

    }

    private void handleRemoteControlViewClick(int position) {
     /*   if (sexSelectedSpinnerIndex == 1 && (position >=0 && position <= 4)) {//矫正模式，禁止点击
            ToastUtil.showShort("矫正模式下不可操作！");
            return;
        }*/

        if (null != mainControlActivityListener) {
            if (!mainControlActivityListener.isConnected()) {
                String mac = Config.getConfig().getLastConnectBleGlassesMac();
                if (CommonUtils.isEmpty(mac)) {
                    showScanTipDialog();
                    return;
                }
            }
        } else {
            ToastUtil.showShort("异常");
            return;
        }

        switch (position) {
            case 0:
                //ToastUtil.showShort("暂停");
                mainControlActivityListener.pauseActionFromTrainFragment();
                break;

            case 1:
                //ToastUtil.showShort("开始");
                mainControlActivityListener.startActionFromTrainFragment();
                break;

            case 2:
                //ToastUtil.showShort("停止");
                mainControlActivityListener.stopActionFromTrainFragment();
                break;

            case 3:
                //ToastUtil.showShort("继续");
                mainControlActivityListener.continueActionFromTrainFragment();
                break;

            case 4:
                //ToastUtil.showShort("干预");
                mainControlActivityListener.interveneActionFromTrainFragment();
                break;

            case 5://关机操作
                //ToastUtil.showShort(getResources().getText(R.string.close_ble_glasses_batterY_text));
                mainControlActivityListener.closeBatteryFromTrainFragment();
                break;

                default:
                    break;
        }
    }

    @Override
    public void onClickIndex(int index, int resourceId) {

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onReceiveBattery(BatteryEvent batteryEvent) {
        if (null != batteryEvent) {
            Config.getConfig().saveLastBatterLevel(batteryEvent.getBattery());
            showBatterLevel(batteryEvent.getBattery());

            if (SendUserInfoControlBleCmdBean.STOP_OPERATION_CODE == batteryEvent.getRunStatus()) {
                /**
                 * 停止状态
                 */
                changeSpinnerStatus(true);

            } else {
                /**
                 * 非停止状态
                 */
                changeSpinnerStatus(false);
            }
        }
    }

    private void showBatterLevel(int batterLevel) {
        batteryTextView.setText("电量:" + batterLevel + "%");
    }

    public void setBleConnectStatus(boolean connected) {
        this.connected.set(connected);
        showBleConnectStatus(connected);
    }

    private void showBleConnectStatus(boolean bleConnected) {
        if (bleConnected) {
            bleConnectStatusTextView.setText("已连接");
            bleConnectStatusImg.setImageResource(R.mipmap.yilianjie);

            bleConnectRecongizeImg.setImageResource(R.mipmap.yilianjie);
            bleRecongizeTextView.setText("已识别");
            //batteryTextView.setVisibility(View.VISIBLE);
        } else {
            bleConnectStatusTextView.setText("未连接");
            bleConnectStatusImg.setImageResource(R.mipmap.weilianjie);

            bleConnectRecongizeImg.setImageResource(R.mipmap.weilianjie);
            bleRecongizeTextView.setText("未识别");
            //batteryTextView.setVisibility(View.INVISIBLE);
        }
    }

    private void changeSpinnerStatus(boolean enable) {
        trainModeAdapter.setItemSelectable(enable);
        trainModeAdapter.notifyDataSetChanged();
        trainModeSpinner.setEnabled(enable);
    }

    /**
     * 扫描二维码
     */
    private void scanBleDeviceQrcoder() {
        if (null != mainControlActivityListener) {
            mainControlActivityListener.scanBleDeviceQrCoder();
        }
    }

    public void showScanTipDialog() {
        if (null == bleScanDialog) {
            bleScanDialog = new BleScanDialog(getActivity());
            bleScanDialog.setDialogButtonClickListener(new DialogButtonClickListener() {
                @Override
                public void onButtonClick(int resourceId) {
                    if (resourceId == R.id.blescanbtn) {
                        scanBleDeviceQrcoder();
                    } else if (resourceId  == R.id.inputdeviceidbtn) {
                        dismissScanTipDialog();
                        showInputDeviceIdDialog();
                    } else if (resourceId  == R.id.searchbledevicebtn) {
                        if (mainControlActivityListener.checkLocationServiceOk()) {
                            dismissScanTipDialog();
                            showConnectStatusDialog(null , BleConnectStatus.SearchListView);
                        }
                    }
                }
            });
        }
        bleScanDialog.showTip();
    }

    public void dismissScanTipDialog() {
        if (null != bleScanDialog) {
            bleScanDialog.dismiss();
        }
    }

    public void showInputDeviceIdDialog() {
        if (null == bleInputDeviceIDDialog) {
            bleInputDeviceIDDialog = new BleInputDeviceIDDialog(getActivity());

            bleInputDeviceIDDialog.setDialogButtonClickListener(new DialogButtonClickListener() {
                @Override
                public void onButtonClick(int resourceId) {
                    if (resourceId == R.id.inputdeviceidsurebtn) {
                        /**
                         * 获取输入的 DeviceID
                         */
                        final String deviceID = bleInputDeviceIDDialog.getInputDeviceIdText();
                        TrainModel.getInstance().checkDeviceBindStatus(deviceID, null,false, new CheckBleMacByServerCallBack() {
                            @Override
                            public void checkStatus(boolean avaliable, String mac) {
                                if (avaliable) {
                                    mainControlActivityListener.connectBleDeviceByMac(deviceID);
                                }
                            }
                        });

                    } else if (resourceId ==  R.id.inputdeviceidcancelbtn) {
                        /**
                         * 输入设备 ID Dialog  的取消按钮，点击取消之后，再显示 扫描或输入 Dialog
                         */
                        showScanTipDialog();

                    }
                }
            });
        }
        bleInputDeviceIDDialog.showTip();
    }

    public void dismissBleInputDeviceIdDialog() {
        if (null != bleInputDeviceIDDialog) {
            bleInputDeviceIDDialog.dismiss();
        }
    }

    public void showConnectStatusDialog(String deviceId, BleConnectStatus bleConnectStatus) {
        if (null == bleConnectStatusDialog) {
            bleConnectStatusDialog = new BleConnectStatusDialog(getActivity());
            bleConnectStatusDialog.setDialogButtonClickListener(new DialogButtonClickListener() {
                @Override
                public void onButtonClick(int resourceId) {
                    switch (resourceId) {
                        case R.id.connectingcancelbtn:
                            /***
                             * 连接中 Dialog 中的 取消 按钮
                             */
                            mainControlActivityListener.quitConnectBleDevice();
                            break;

                        case R.id.ble_connect_fail_retry_btn:
                            /**
                             * 连接失败中 Dialog , 重新连接 按钮
                             */
                             mainControlActivityListener.retryConnectBleDevice();
                             break;

                        case R.id.ble_connect_fail_cancel_btn:
                            /**
                             * 连接失败 Dialog 中的 取消 按钮
                             */
                            mainControlActivityListener.quitConnectBleDevice();
                            break;

                            default:
                                MLog.d("default " + resourceId);
                                break;
                    }
                }
            });
        }
        bleConnectStatusDialog.setConnectDeviceIdText(deviceId);
        bleConnectStatusDialog.showTip(bleConnectStatus);
    }

    public void dismissConnectStatusDialog() {
        if (null != bleConnectStatusDialog) {
            bleConnectStatusDialog.dismiss();
        }
    }

    private void showDialogByBleConnectStatus(BleConnectStatus bleConnectStatus, BluetoothLeDevice bluetoothLeDevice) {
        switch (bleConnectStatus) {
            case Connected:
                /**
                 * 显示连接成功
                 */
                showConnectStatusDialog(bluetoothLeDevice.getName(), bleConnectStatus);
                break;

            case Connecting:
                /**
                 * 显示连接中
                 */
                showConnectStatusDialog(bluetoothLeDevice.getName(), bleConnectStatus);
                break;

            case Disconnected:
                /**
                 * 断开连接
                 */
                break;

            case ConnectedFail:
                /**
                 * 连接失败
                 */
                showConnectStatusDialog(bluetoothLeDevice.getName(), bleConnectStatus);
                break;
        }
    }


    @Override
    public void onDestroy() {
        dismissBleInputDeviceIdDialog();
        dismissScanTipDialog();
        unregisterEventBus();
        super.onDestroy();
    }
}
