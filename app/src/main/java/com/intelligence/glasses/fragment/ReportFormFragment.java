package com.intelligence.glasses.fragment;

import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.widget.AppCompatTextView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.common.baselibrary.log.MLog;
import com.intelligence.glasses.base.LazyLoadFragment;
import com.intelligence.glasses.bean.CatsItemBean;
import com.intelligence.glasses.event.TrainTimeUpdateStatusEvent;
import com.intelligence.glasses.listener.ItemOfViewPagerOnClickListener;
import com.intelligence.glasses.listener.MainControlActivityListener;
import com.intelligence.glasses.listener.TittleChangeListener;
import com.intelligence.glasses.model.TrainModel;
import com.shitec.bleglasses.R;
import com.intelligence.glasses.adapter.TrainTimeAdapter;
import com.intelligence.glasses.util.thirdutil.Utils;
import com.intelligence.glasses.view.HorizontalScrollSelectedView;
import com.intelligence.glasses.view.hozscrollerview.BaseFlexibleTabIndicator;
import com.intelligence.glasses.view.hozscrollerview.ImageTabIndicator;
import com.intelligence.glasses.view.hozscrollerview.MainTabIndicator;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ReportFormFragment extends LazyLoadFragment implements ItemOfViewPagerOnClickListener {

    @BindView(R.id.function_item_title_tv)
    AppCompatTextView titleTextView;

    @BindView(R.id.function_item_backlayout)
    LinearLayout backLayout;

/*    @BindView(R.id.adviselayout)
    LinearLayout adviseLayout;*/

    private HorizontalScrollSelectedView hsMain;
    private TrainTimeAdapter trainTimeAdapter;
    private ViewPager viewPager;
    private MainTabIndicator mainTabIndicator;

    private Handler mHander = new Handler();
    private volatile AtomicBoolean  connected = new AtomicBoolean(false);
    private final List<CatsItemBean> mCategoryList = new ArrayList();
    private MainControlActivityListener mainControlActivityListener;

    List<String> strings = new ArrayList<String>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerEventBus();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container,savedInstanceState);
        //View view = inflater.inflate(R.layout.fragment_reportforms_layout, container, false);
        ButterKnife.bind(this, getContentView());
        initView(getContentView());
        return getContentView();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mainControlActivityListener = (MainControlActivityListener) getActivity();
        initData();
        initViewPager();
        initListener();
        initIndicator();
    }

    private void initView(View view) {
        titleTextView.setTextColor(getResources().getColor(R.color.bleglasses_main_control_title));
        titleTextView.setText("数据报表");
        backLayout.setVisibility(View.INVISIBLE);
        //adviseLayout.setVisibility(View.INVISIBLE);

        hsMain =  view.findViewById(R.id.hd_main);
        mainTabIndicator = view.findViewById(R.id.tabindicator);
        viewPager = view.findViewById(R.id.viewpager);
    }

    private void initData() {
        strings.clear();
        strings.add("月");
        strings.add("周");
        strings.add("今天");
        //strings.add("昨天");
        hsMain.setData(strings);
        TrainModel.getInstance().getAllTrainTimeWithLimit();
    }

    private void initIndicator() {
        mCategoryList.clear();
        mCategoryList.add(new CatsItemBean("月"));
        mCategoryList.add(new CatsItemBean("周"));
        mCategoryList.add(new CatsItemBean("日"));
        //mCategoryList.add(new CatsItemBean("昨天"));


        mainTabIndicator.setTextSize(Utils.dip2px(18));
        mainTabIndicator.setHighLightColor(getResources().getColor(R.color.black));
        mainTabIndicator.setNormalColor(getResources().getColor(R.color.indicator_text_normal_color));
        int horizontalPadding = Utils.dip2px(5.0f);
        mainTabIndicator.setTabPadding(horizontalPadding, 0, horizontalPadding, 0);
        mainTabIndicator.setViewPager(viewPager);
        mainTabIndicator.updateData(mCategoryList);

        mainTabIndicator.setOnTabSelectedListener(new BaseFlexibleTabIndicator.OnTabSelectedListener() {
            public boolean onTabSelected(int position) {

                return true;
            }
        });

        mainTabIndicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageSelected(int i) {

            }

            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            public void onPageScrollStateChanged(int arg0) {
            }
        });

        mainTabIndicator.setOnDisplayListener(new ImageTabIndicator.OnDisplayListener() {
            public void onItemDisplay(int index) {

            }
        });
        mainTabIndicator.setCurrentItem(2);

    }

    protected void initListener() {
        hsMain.setTittleChangeListener(new TittleChangeListener() {
            @Override
            public void selectedIndexChanged(int titleIndex) {
                Log.d("MainActivity", "titleIndex = " + titleIndex);
                viewPager.setCurrentItem(titleIndex);
            }
        });
    }

    private void initViewPager() {
        trainTimeAdapter = new TrainTimeAdapter(strings,this);
        viewPager.setAdapter(trainTimeAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position != hsMain.getCurrentSelectedIndex()) {
                    hsMain.setTitleIndex(position);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        viewPager.setCurrentItem(2);
    }

    @Override
    public void onClickIndex(int index, int resourceId) {
        Toast.makeText(getActivity(), "index = " + index + "  resourceId = " + resourceId, Toast.LENGTH_SHORT).show();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onReceiveUpdateTrainTime(TrainTimeUpdateStatusEvent trainTimeUpdateStatusEvent) {
        MLog.d("onReceiveUpdateTrainTime trainTimeUpdateStatusEvent = " + trainTimeUpdateStatusEvent.toString());
        MLog.d("TrainModel.getInstance().getCurrentUserTrainTimeData().siee = " + TrainModel.getInstance().getCurrentUserTrainTimeData().size());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterEventBus();
    }

    private void startSearchActivity() {
       /* Intent mIntent = new Intent(getActivity(), BleSearchActivity.class);
        startActivityForResult(mIntent, 1200);*/
       if (null != mainControlActivityListener) {
           mainControlActivityListener.startSearchActivityForResultFromTrainFragment();
       }
    }

    @Override
    protected int setContentView() {
        return R.layout.fragment_reportforms_layout;
    }

    @Override
    protected void lazyLoad() {
        MLog.d("lazyLoad() firstInitData = " + firstInitData);
        if (firstInitData) {
            firstInitData = false;
            //initView(getContentView());
        } else {
            networkAvailableNotify();
        }
        if (null != trainTimeAdapter) {
            trainTimeAdapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void networkAvailableNotify() {

    }
}
