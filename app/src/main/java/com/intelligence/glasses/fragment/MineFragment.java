package com.intelligence.glasses.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.GridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.alibaba.fastjson.TypeReference;
import com.android.common.baselibrary.log.MLog;
import com.android.common.baselibrary.util.ActivityStackUtil;
import com.android.common.baselibrary.util.ToastUtil;
import com.android.common.baselibrary.util.comutil.CommonUtils;
import com.bumptech.glide.Glide;
import com.intelligence.common.http.retrofit.netapi.URLConstant;
import com.intelligence.common.http.retrofit.netsubscribe.LoginSubscribe;
import com.intelligence.glasses.MyApplication;
import com.intelligence.glasses.activity.login.PhoneLoginActivity;
import com.intelligence.glasses.activity.mine.AccountManagerActivity;
import com.intelligence.glasses.activity.mine.PersonalInfoActivity;
import com.intelligence.glasses.adapter.AccountManagerRecyclerViewAdapter;
import com.intelligence.glasses.base.BaseFragment;
import com.intelligence.glasses.bean.AccountManagerBean;
import com.intelligence.glasses.bean.SignInResponseBean;
import com.intelligence.glasses.bean.event.PortraitUpdateEventBean;
import com.intelligence.glasses.greendao.greendaobean.UserInfoDBBean;
import com.intelligence.glasses.greendao.greenddaodb.UserInfoBeanDaoOpe;
import com.intelligence.glasses.http.request.IAddSignIn;
import com.intelligence.glasses.listener.OnItemClickListener;
import com.intelligence.glasses.util.Config;
import com.intelligence.glasses.util.jsonutil.JsonUtil;
import com.intelligence.glasses.util.view.ui.DeviceUtils;
import com.intelligence.glasses.view.circleimageview.CircleImageView;
import com.intelligence.glasses.view.grideviewsection.PullToRefreshView;
import com.intelligence.glasses.view.recyclerview.GloriousRecyclerView;
import com.intelligence.glasses.view.recyclerview.GridItemDecoration;
import com.shitec.bleglasses.R;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.observers.DisposableObserver;
import okhttp3.ResponseBody;


public class MineFragment extends BaseFragment implements PullToRefreshView.OnHeaderRefreshListener,
        PullToRefreshView.OnFooterRefreshListener{
    private static final String TAG = MineFragment.class.getName();

    @BindView(R.id.function_item_backlayout)
    LinearLayout backLayout;

    @BindView(R.id.function_item_title_tv)
    AppCompatTextView titleTextView;

    @BindView(R.id.person_portrait_image)
    CircleImageView portraitImageView;

    @BindView(R.id.nametextview)
    AppCompatTextView userNameTextView;

    @BindView(R.id.firststarimg)
    AppCompatImageView firstStarImg;

    @BindView(R.id.secondstarimg)
    AppCompatImageView secondStarImg;

    @BindView(R.id.thirdstarimg)
    AppCompatImageView thirdStarImg;

    @BindView(R.id.fourthstarimg)
    AppCompatImageView fourthStarImg;

    @BindView(R.id.fifthstarimg)
    AppCompatImageView fifthStarImg;

    @BindView(R.id.signinlayout)
    LinearLayout signInLayout;

    @BindView(R.id.accountlayout)
    LinearLayout accountLayout;

    @BindView(R.id.accountmanagerrecyclerview)
    GloriousRecyclerView gloriousRecyclerView;


    AccountManagerRecyclerViewAdapter accountManagerRecyclerViewAdapter;
    List<AccountManagerBean> accountManagerBeanList = new ArrayList<>();

    public static final int REVIEW_DATA_INDEX = 0;//复查数据

    public static final int PRODUCT_INTRODUCE_INDEX = 1;//产品介绍
    public static final int INTERVENE_KEY_INTRODUCE_INDEX = 2;//干预键介绍
    public static final int FEEDBACK_INDEX = 3;//意见反馈
    public static final int COMMON_PROBLEM_INDEX = 4;//常见问题
    public static final int CONTACT_CUSTOMER_SERVICE_INDEX = 5;//联系客服
    public static final int SYSTEM_SETTINGS_KEY = 6;


    public static final int DATA_INIT_INDEX = 8;
    public static final int WEAR_DETAIL_INDEX = 9;

    public static final int GUARDIAN_ACCOUNT_INDEX = 10;


    public static final int VERSION_INFO_INDEX = 11;
    public static final int PERSONAL_INFO_INDEX = 12;
    //public static final int AWARD_INFO_INDEX = 13;
    public static final int OPTOMETRY_INFO_INDEX = 14;//验光信息
    public static final int USER_AGREEMENT_INDEX = 15;//用户协议
    public static final int CUSTOM_DATA_INDEX = 16;//数据定制
    private LinearLayout mAccountLinearLayout;


    private UserInfoDBBean userInfoDBBean;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerEventBus();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mine_layout, container, false);
        ButterKnife.bind(this, view);
        initData();
        initView(view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        MLog.d("MineFragment onActivityCreated()");
        initListener();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        MLog.d(TAG + " onActivityResult() requestCode = " + requestCode + " resultCode = " + resultCode);
    }

    /**
     * 处理更新头像的操作
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handleUpdatePortraitImage(PortraitUpdateEventBean portraitUpdateEventBean) {
        String userServerId = Config.getConfig().getUserServerId();
        List<UserInfoDBBean> userInfoDBBeanList = UserInfoBeanDaoOpe.queryUserInfoByServerID(MyApplication.getInstance(), userServerId);
        if (null != userInfoDBBeanList && userInfoDBBeanList.size() > 0) {
            userInfoDBBean = userInfoDBBeanList.get(0);
        }

        String portraitUrl = userInfoDBBean.getPortrait_image_url();
        if (!CommonUtils.isEmpty(portraitUrl)) {
            String loadUrl = CommonUtils.startWithHttp(portraitUrl) ? portraitUrl : URLConstant.BASE_URL + portraitUrl;
            MLog.d("portraiturl = " + loadUrl);
            Glide.with(this).load(loadUrl).into(portraitImageView);
        }
        userNameTextView.setText(userInfoDBBean.getName());

    }


    private void initData() {
        Context context = getActivity();

        if (null == context) {
            return;
        }

        String userServerId = Config.getConfig().getUserServerId();
        List<UserInfoDBBean> userInfoDBBeanList = UserInfoBeanDaoOpe.queryUserInfoByServerID(context.getApplicationContext(), userServerId);
        if (null != userInfoDBBeanList && userInfoDBBeanList.size() > 0) {
            userInfoDBBean = userInfoDBBeanList.get(0);
        }
        //initRecyclerViewData();
        initFunctionRecyclerViewItem();
    }

    private void initRecyclerViewData() {
        accountManagerBeanList.clear();
        AccountManagerBean memberItemBeaan = new AccountManagerBean();
        memberItemBeaan.setPrefixImageResourceId(R.mipmap.huiyuan);
        memberItemBeaan.setItemTitle("会员");
        memberItemBeaan.setItemContent("立即开通");
        memberItemBeaan.setLight(false);
        memberItemBeaan.setSufixImageResourceId(R.mipmap.jiantou);

        AccountManagerBean integralItemBean = new AccountManagerBean();
        integralItemBean.setPrefixImageResourceId(R.mipmap.leijijifen);
        integralItemBean.setItemTitle("累计积分");
        integralItemBean.setItemContent("1200分");
        integralItemBean.setLight(true);
        integralItemBean.setSufixImageResourceId(R.mipmap.jiantou);

        AccountManagerBean trainTimeAvaiableItemBean = new AccountManagerBean();
        trainTimeAvaiableItemBean.setPrefixImageResourceId(R.mipmap.shengyushijian);
        trainTimeAvaiableItemBean.setItemTitle("剩余时间");
        trainTimeAvaiableItemBean.setItemContent("62小时20分钟");
        trainTimeAvaiableItemBean.setLight(false);
        trainTimeAvaiableItemBean.setSufixImageResourceId(R.mipmap.jiantou);

        AccountManagerBean blanceItemBeaan = new AccountManagerBean();
        blanceItemBeaan.setPrefixImageResourceId(R.mipmap.zhanghaoyue);
        blanceItemBeaan.setItemTitle("账号余额");
        blanceItemBeaan.setItemContent("3200元");
        blanceItemBeaan.setLight(true);
        blanceItemBeaan.setSufixImageResourceId(R.mipmap.jiantou);

        AccountManagerBean totalWearTimeItemBeaan = new AccountManagerBean();
        totalWearTimeItemBeaan.setPrefixImageResourceId(R.mipmap.zongpeidaishijian);
        totalWearTimeItemBeaan.setItemTitle("总佩戴时间");
        totalWearTimeItemBeaan.setItemContent("1562小时20分钟");
        totalWearTimeItemBeaan.setLight(false);
        totalWearTimeItemBeaan.setSufixImageResourceId(R.mipmap.jiantou);

        AccountManagerBean guardianItemBeaan = new AccountManagerBean();
        guardianItemBeaan.setPrefixImageResourceId(R.mipmap.jianhuren);
        guardianItemBeaan.setItemTitle("监护人");
        guardianItemBeaan.setItemContent("立即创建");
        guardianItemBeaan.setLight(true);
        guardianItemBeaan.setShow(true);
        guardianItemBeaan.setSufixImageResourceId(R.mipmap.jiantou);

        accountManagerBeanList.add(memberItemBeaan);
        accountManagerBeanList.add(integralItemBean);
        accountManagerBeanList.add(trainTimeAvaiableItemBean);
        accountManagerBeanList.add(blanceItemBeaan);
        accountManagerBeanList.add(totalWearTimeItemBeaan);
        accountManagerBeanList.add(guardianItemBeaan);
    }

    private void initFunctionRecyclerViewItem() {
        accountManagerBeanList.clear();

        AccountManagerBean reviewDataBean = new AccountManagerBean();
        reviewDataBean.setPrefixImageResourceId(R.mipmap.fucha_shuju);
        reviewDataBean.setItemTitle("复查数据");
        reviewDataBean.setLight(true);
        reviewDataBean.setSufixImageResourceId(R.mipmap.jiantou);

        AccountManagerBean productDataBean = new AccountManagerBean();
        productDataBean.setPrefixImageResourceId(R.mipmap.product_introduce);
        productDataBean.setItemTitle("产品介绍");
        productDataBean.setLight(true);
        productDataBean.setSufixImageResourceId(R.mipmap.jiantou);

        AccountManagerBean ganYuJianDataBean = new AccountManagerBean();
        ganYuJianDataBean.setPrefixImageResourceId(R.mipmap.ganyu_five_hands);
        ganYuJianDataBean.setLight(true);
        ganYuJianDataBean.setItemTitle("干预键介绍");
        ganYuJianDataBean.setSufixImageResourceId(R.mipmap.jiantou);

        AccountManagerBean sugestionDataBean = new AccountManagerBean();
        sugestionDataBean.setPrefixImageResourceId(R.mipmap.feekback_yijian);
        sugestionDataBean.setLight(true);
        sugestionDataBean.setItemTitle("意见反馈");
        sugestionDataBean.setSufixImageResourceId(R.mipmap.jiantou);

        AccountManagerBean commonProblemsDataBean = new AccountManagerBean();
        commonProblemsDataBean.setPrefixImageResourceId(R.mipmap.common_problems);
        commonProblemsDataBean.setLight(true);
        commonProblemsDataBean.setItemTitle("常见问题");
        commonProblemsDataBean.setSufixImageResourceId(R.mipmap.jiantou);

        AccountManagerBean contactServiceDataBean = new AccountManagerBean();
        contactServiceDataBean.setPrefixImageResourceId(R.mipmap.contact_service);
        contactServiceDataBean.setItemTitle("联系客服");
        contactServiceDataBean.setLight(true);
        contactServiceDataBean.setSufixImageResourceId(R.mipmap.jiantou);


        AccountManagerBean systemSettingsDataBean = new AccountManagerBean();
        systemSettingsDataBean.setPrefixImageResourceId(R.mipmap.xitongshezhi);
        systemSettingsDataBean.setItemTitle("系统设置");
        systemSettingsDataBean.setLight(true);
        systemSettingsDataBean.setSufixImageResourceId(R.mipmap.jiantou);

        accountManagerBeanList.add(reviewDataBean);
        accountManagerBeanList.add(productDataBean);
        accountManagerBeanList.add(ganYuJianDataBean);
        accountManagerBeanList.add(sugestionDataBean);
        accountManagerBeanList.add(commonProblemsDataBean);
        accountManagerBeanList.add(contactServiceDataBean);
        accountManagerBeanList.add(systemSettingsDataBean);
    }

    private void initView(View view) {
        titleTextView.setText(getResources().getString(R.string.mine_account_manager_text));
        mAccountLinearLayout = view.findViewById(R.id.accountlayout);
        backLayout.setVisibility(View.INVISIBLE);
        showStar(1);

        //MLog.d("userInfoDBBean = " + userInfoDBBean);
        String portraitUrl = userInfoDBBean.getPortrait_image_url();
        if (!CommonUtils.isEmpty(portraitUrl)) {
            String loadUrl = CommonUtils.startWithHttp(portraitUrl) ? portraitUrl : URLConstant.BASE_URL + portraitUrl;
            MLog.d("portraiturl = " + loadUrl);
            Glide.with(this).load(loadUrl).into(portraitImageView);
        }
        userNameTextView.setText(userInfoDBBean.getName());
        initRecyclerView();
    }

    private void initRecyclerView() {
        Activity activity = getActivity();
        if (null == activity) {
            return;
        }

        final GridLayoutManager manager = new GridLayoutManager(activity, 1);

        accountManagerRecyclerViewAdapter = new AccountManagerRecyclerViewAdapter(this);
        gloriousRecyclerView.setAdapter(accountManagerRecyclerViewAdapter);

        GridItemDecoration gridItemDecoration = new GridItemDecoration.Builder(activity)
                .size((int) DeviceUtils.dipToPx(activity, 1))
                .color(R.color.bleglasses_mine_item_divide_color)
                .margin((int) DeviceUtils.dipToPx(activity, 15), (int) DeviceUtils.dipToPx(activity, 15))
                .isExistHead(false)
                .showHeadDivider(false)
                .showLastDivider(false)
                .build();

        gloriousRecyclerView.addItemDecoration(gridItemDecoration);
        gloriousRecyclerView.setLayoutManager(manager);
        accountManagerRecyclerViewAdapter.setDatas(accountManagerBeanList);
    }

    @Override
    protected void initListener() {
        super.initListener();

 /*       mAccountLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //跳转到个人账户信息界面
                //ToastDialogUtil.showShortToast("账户信息");
                Intent mIntent = new Intent(getActivity(), PersonalInfoActivity.class);
                mIntent.putExtra(PersonalInfoActivity.FRAGMENT_INDEX_KEY, MineFragment.PERSONAL_INFO_INDEX);
                startActivity(mIntent);
            }
        });*/

        accountLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goPersonInfo();
            }
        });

        portraitImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //initConfig(true);
                goPersonInfo();

            }
        });

        signInLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /**
                 * 签到
                 */
                String serverId = Config.getConfig().getUserServerId();
                if (!CommonUtils.isEmpty(serverId)) {
                    postSignInInfo(serverId, null, null, null);
                } else {
                    //**
                    ToastUtil.showShortToast("退出重新登录");
                    ActivityStackUtil.getInstance().finishAllActivity();
                    Intent mIntent = new Intent(getActivity(), PhoneLoginActivity.class);
                    startActivity(mIntent);
                }
            }
        });

        accountManagerRecyclerViewAdapter.setmOnItemClickListener(new OnItemClickListener() {
            @Override
            public void OnItemClick(int position) {
                AccountManagerBean accountManagerBean = accountManagerBeanList.get(position);
                if (accountManagerBean.isLight()) {
                   /* Intent mIntent = new Intent(getActivity(), AccountManagerActivity.class);
                    mIntent.putExtra(AccountManagerActivity.MINE_FRAGMENT_INDEX_KEY, position);
                    startActivity(mIntent);*/

                    Intent mIntent = new Intent(getActivity(), PersonalInfoActivity.class);
                    mIntent.putExtra(PersonalInfoActivity.FRAGMENT_INDEX_KEY, position);
                    startActivity(mIntent);
                }
            }
        });
    }

    @Override
    public void onFooterRefresh(PullToRefreshView view) {
        ToastUtil.showShort("onFooterRefresh");
        //mPullToRefreshView.onFooterRefreshComplete();
    }

    @Override
    public void onHeaderRefresh(PullToRefreshView view) {
        ToastUtil.showShort("onHeaderRefresh");
        //mPullToRefreshView.onHeaderRefreshComplete();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void goPersonInfo() {
        Intent mIntent = new Intent(getActivity(), AccountManagerActivity.class);
        mIntent.putExtra(AccountManagerActivity.MINE_FRAGMENT_INDEX_KEY, AccountManagerActivity.PERSONINFO_PFRAGMENT_INDEX);
        startActivity(mIntent);
    }

    /**
     * 展示几个星星
     *
     * @param stars
     */
    private void showStar(int stars) {
        firstStarImg.setImageResource(R.mipmap.xingxing_gray);
        secondStarImg.setImageResource(R.mipmap.xingxing_gray);
        thirdStarImg.setImageResource(R.mipmap.xingxing_gray);
        fourthStarImg.setImageResource(R.mipmap.xingxing_gray);
        fifthStarImg.setImageResource(R.mipmap.xingxing_gray);

        switch (stars) {
            /**
             * 不使用 break
             */
            case 5:
                fifthStarImg.setImageResource(R.mipmap.xingxing);

            case 4:
                fourthStarImg.setImageResource(R.mipmap.xingxing);

            case 3:
                thirdStarImg.setImageResource(R.mipmap.xingxing);

            case 2:
                secondStarImg.setImageResource(R.mipmap.xingxing);

            case 1:
                firstStarImg.setImageResource(R.mipmap.xingxing);
                break;
        }

    }

    private void postSignInInfo(String memberId, String loginName, String nickName, String name) {
        /**
         * member_id	是	string	用户编号
         login_name	否	string	姓名
         nick_name	否	string	昵称
         name	否	string	真实姓名
         */

        final HashMap<String, String> headerMap = new HashMap<>(1);
        final HashMap<String, String> bodyMap = new HashMap<>();
        bodyMap.put(IAddSignIn.MEMBER_ID, memberId);
        bodyMap.put(IAddSignIn.LOGIN_NAME, loginName);
        bodyMap.put(IAddSignIn.NICK_NAME, nickName);
        bodyMap.put(IAddSignIn.NAME, name);
        //bodyMap.put(IMemberTrainTime.VERSION_CODE, String.valueOf(AppUtils.getVersionCode()));

                LoginSubscribe.postSignInInfo(headerMap, bodyMap, new DisposableObserver<ResponseBody>() {
                    @Override
                    public void onNext(ResponseBody responseBody) {
                        String newStringDataJson = null;
                        try {
                            newStringDataJson = new String(responseBody.bytes());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        //SdLogUtil.writeCommonLog("postSignInInfo newStringDataJson= " + newStringDataJson);
                        SignInResponseBean signInResponseBean = (SignInResponseBean) JsonUtil.json2objectWithDataCheck(newStringDataJson, new TypeReference<SignInResponseBean>() {
                        });
                        //SdLogUtil.writeCommonLog("signInResponseBean = " + signInResponseBean);
                        if (null != signInResponseBean && null != signInResponseBean.getData()) {
                            ToastUtil.showShortToast("" + signInResponseBean.getData().getMsg());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        ToastUtil.showShortToast(e.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterEventBus();
    }
}
