package com.intelligence.glasses.dialog.bleconnect;

import android.content.Context;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;

import com.android.common.baselibrary.util.ToastUtil;
import com.blankj.utilcode.util.StringUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.intelligence.glasses.bean.event.DialogBleConnectEvent;
import com.intelligence.glasses.bean.event.DialogScanEvent;
import com.intelligence.glasses.dialog.CenterScaleDialog;
import com.intelligence.glasses.event.ScanEvent;
import com.intelligence.glasses.listener.DialogButtonClickListener;
import com.intelligence.glasses.util.view.ui.DeviceUtils;
import com.vise.baseble.model.BluetoothLeDevice;
import com.shitec.bleglasses.R;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashSet;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 搜索蓝牙设备，以列表显示
 */
public class BleDeviceListialog extends CenterScaleDialog<BleDeviceListialog> {
    @BindView(R.id.mainrelativelayout)
    RelativeLayout mainLayout;

    @BindView(R.id.blereyclerviewdig)
    RecyclerView bleRecyclerView;

    private int screenWidth;
    private int screenHeight;

    private DialogButtonClickListener mDialogButtonClickListener;

    private Context mContext;

    private String mDeviceID;

    private BaseQuickAdapter baseQuickAdapter;

    private Set<String> deviceSet = new HashSet<>();

    public BleDeviceListialog(Context context) {
        super(context);
        setCancelable(false);
        ButterKnife.bind(this);
        mContext = context;
        initView(context);
        resetSize();
    }

    private void initView(Context context) {
        int[] wh = DeviceUtils.getDeviceSize(context);
        screenWidth = wh[0];
        screenHeight = wh[1];
        initRecyclerView(context);
        initListener();
    }

    private void initRecyclerView(Context context) {
        bleRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        baseQuickAdapter = new BaseQuickAdapter<BluetoothLeDevice, BaseViewHolder>(R.layout.ble_list_item) {
            @Override
            protected void convert(BaseViewHolder helper, BluetoothLeDevice item) {
                AppCompatTextView textView = helper.getView(R.id.tv_content);
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append(item.getName());
                stringBuilder.append("\n");
                stringBuilder.append(item.getAddress());
                textView.setText(stringBuilder.toString());
            }
        };
        bleRecyclerView.setAdapter(baseQuickAdapter);
    }

    private void resetSize() {
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mainLayout.getLayoutParams();
        layoutParams.width = screenWidth * 4 / 5;
        layoutParams.height = screenHeight * 2 / 5;
        mainLayout.setLayoutParams(layoutParams);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.dialog_ble_device_list_layout;
    }

    @Override
    public void onCreateData(Context context) {

    }

    @Override
    public void show() {
        registerEventBus();
        super.show();
        EventBus.getDefault().post(new DialogScanEvent(true));
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onScanEvent(ScanEvent scanEvent) {
        if (scanEvent.isSuccess()) {
            BluetoothLeDevice bluetoothLeDevice = scanEvent.getBluetoothLeDevice();
            String address = bluetoothLeDevice.getAddress();
            if (!StringUtils.isEmpty(address) && !this.deviceSet.contains(address)) {
                this.deviceSet.add(address);
                this.baseQuickAdapter.addData(bluetoothLeDevice);
                return;
            }
            return;
        }
        ToastUtil.showShort((CharSequence) "扫描超时");
    }

    @Override
    public void dismiss() {
        mDeviceID = null;
        super.dismiss();
        unRegisterEventBus();
        clearData();
        EventBus.getDefault().post(new DialogScanEvent(false));
    }

    private void clearData() {
        if (null != deviceSet) {
            deviceSet.clear();
        }
        if (null != baseQuickAdapter) {
            baseQuickAdapter.getData().clear();
        }
    }

    @Override
    public void onClick(View v, int id) {
        callBackButtonClickListener(id);
        switch (id) {
            case R.id.researchbtn:
                clearData();
                EventBus.getDefault().post(new DialogScanEvent(true));
                break;

            case R.id.closebtn:
                dismiss();
                break;
            default:
                break;
        }
    }

    public void setDialogButtonClickListener(DialogButtonClickListener dialogButtonClickListener) {
        mDialogButtonClickListener = dialogButtonClickListener;
    }

    private void callBackButtonClickListener(int resourceId) {
        if (null != mDialogButtonClickListener) {
            mDialogButtonClickListener.onButtonClick(resourceId);
        }
    }

    private void registerEventBus() {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    private void unRegisterEventBus() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    private void initListener() {
        baseQuickAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
                BluetoothLeDevice bluetoothLeDevice = (BluetoothLeDevice) baseQuickAdapter.getItem(i);
                if (null != bluetoothLeDevice) {
                    EventBus.getDefault().post(new DialogBleConnectEvent(bluetoothLeDevice));
                    dismiss();
                }
            }
        });
    }
}
