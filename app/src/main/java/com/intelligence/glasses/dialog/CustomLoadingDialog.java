package com.intelligence.glasses.dialog;

import android.content.Context;
import android.view.View;
import android.widget.ProgressBar;

import com.shitec.bleglasses.R;
import com.intelligence.glasses.view.CommonLoadingView;

public class CustomLoadingDialog extends BaseDialog {

    CommonLoadingView commonLoadingView;

    public CustomLoadingDialog(Context context) {
        super(context);
        initView(context);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.dialog_default_view;
    }

    @Override
    public void onCreateData(Context context) {

    }

    @Override
    public void onClick(View v, int id) {

    }

    private void initView(Context context) {
        commonLoadingView = rootView.findViewById(R.id.loadingView);
        ProgressBar progressBar = new ProgressBar(context);
        commonLoadingView.setLoadingView(progressBar);
        commonLoadingView.load();
    }

    public void setMessage(String msg) {
        commonLoadingView.setMessage(msg);
    }
}
