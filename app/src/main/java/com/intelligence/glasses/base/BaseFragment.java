package com.intelligence.glasses.base;


import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.android.common.baselibrary.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by Administrator on 2018/11/16.
 */

public class BaseFragment extends Fragment {
    public static final int OPERATION_BLE_DEVICE_STOP_INDEX = 0;
    public static final int OPERATION_BLE_DEVICE_PAUSE_INDEX = 1;
    public static final int OPERATION_BLE_DEVICE_INTERVENE_INDEX = 2;




    protected void initOperationDropButton() {


    }

    protected void initListener() {


    }


    protected void onOperationDropDownItemSelected(int position) {
        switch (position) {
            case OPERATION_BLE_DEVICE_STOP_INDEX:
                doStopOperation();
                break;

            case OPERATION_BLE_DEVICE_PAUSE_INDEX:
                doPauseOperation();
                break;

            case OPERATION_BLE_DEVICE_INTERVENE_INDEX:
                doInterveneOperation();
                break;

            default:
                break;
        }
    }


    /**
     * 停止
     */
    private void doStopOperation() {
        ToastUtil.showShortToast("停止");
    }

    /**
     * 暂停
     */
    private void doPauseOperation() {
        ToastUtil.showShortToast("暂停");
    }

    /**
     * 干预
     */
    private void doInterveneOperation() {
        ToastUtil.showShortToast("干预");
    }

    protected void registerEventBus() {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    protected void unregisterEventBus() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //SdLogUtil.writeCommonLog("[" + this.getClass().getSimpleName() + "] BaseFragment onCreate(Bundle savedInstanceState) savedInstanceState = " + savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        //SdLogUtil.writeCommonLog("[" + this.getClass().getSimpleName() + "] BaseFragment onStart()");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //SdLogUtil.writeCommonLog("[" + this.getClass().getSimpleName() + "] BaseFragment onSaveInstanceState() outState = " + outState);
    }

    @Override
    public void onResume() {
        super.onResume();
        //SdLogUtil.writeCommonLog("[" + this.getClass().getSimpleName() + "] BaseFragment onResume()");
    }

    @Override
    public void onPause() {
        super.onPause();
        //SdLogUtil.writeCommonLog("[" + this.getClass().getSimpleName() + "] BaseFragment onPause()");
    }

    @Override
    public void onStop() {
        super.onStop();
        //SdLogUtil.writeCommonLog("[" + this.getClass().getSimpleName() + "] BaseFragment onStop()");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //SdLogUtil.writeCommonLog("[" + this.getClass().getSimpleName() + "] BaseFragment onDestroy()");
    }
}
