package com.intelligence.glasses.activity.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.alibaba.fastjson.TypeReference;
import com.android.common.baselibrary.jnative.SecUtil;
import com.android.common.baselibrary.log.MLog;
import com.android.common.baselibrary.util.FileUtils;
import com.android.common.baselibrary.util.comutil.AppUtils;
import com.intelligence.common.http.retrofit.netsubscribe.LoginSubscribe;
import com.intelligence.common.http.retrofit.netutils.NetUtil;
import com.intelligence.glasses.MyApplication;
import com.intelligence.glasses.activity.bindglasses.BindPersonalInfoActivity;
import com.intelligence.glasses.activity.bindglasses.BindPersonalInfoScrollViewActivity;
import com.intelligence.glasses.base.BaseActivity;
import com.intelligence.glasses.base.BaseBleActivity;
import com.intelligence.glasses.bean.MemberLoginResponseBean;
import com.intelligence.glasses.ble.BleDeviceManager;
import com.intelligence.glasses.constant.ConstantString;
import com.intelligence.glasses.greendao.greendaobean.UserInfoDBBean;
import com.intelligence.glasses.greendao.greenddaodb.UserInfoBeanDaoOpe;
import com.intelligence.glasses.http.request.IBaseRequest;
import com.intelligence.glasses.http.request.ISendVerficationCode;
import com.intelligence.glasses.model.GlassesBleDataModel;
import com.intelligence.glasses.model.TrainModel;
import com.intelligence.glasses.util.downloadutil.DownloadUtils;
import com.intelligence.glasses.util.downloadutil.JsDownloadListener;
import com.intelligence.glasses.util.jsonutil.GsonTools;
import com.intelligence.glasses.util.view.ui.DeviceUtils;
import com.shitec.bleglasses.BuildConfig;
import com.shitec.bleglasses.R;
import com.android.common.baselibrary.util.CommonUtils;
import com.android.common.baselibrary.util.ToastUtil;
import com.android.common.baselibrary.util.comutil.security.Md5Util;
import com.intelligence.glasses.activity.BleMainControlActivity;
import com.intelligence.glasses.activity.register.RegisterByPhoneActivity;
import com.intelligence.glasses.http.request.IMemberLogin;
import com.intelligence.glasses.util.Config;
import com.intelligence.glasses.util.jsonutil.JsonUtil;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import butterknife.BindView;
import io.reactivex.Observable;
import io.reactivex.observers.DisposableObserver;
import okhttp3.ResponseBody;

/**
 * 登录界面
 */
public class PhoneLoginActivity extends BaseBleActivity {
    @BindView(R.id.usernameedittext)
    EditText userNameEditText;

    @BindView(R.id.usernameunderline)
    View userNameUnderLineView;

    @BindView(R.id.passwordedittext)
    EditText passwdEditText;

    @BindView(R.id.passwdunderlineview)
    View passwdUnderLineView;

    @BindView(R.id.forgetpasswd)
    TextView forgetPasswdTextView;

    @BindView(R.id.loginbutton)
    Button loginButton;

    @BindView(R.id.registerbutton)
    Button registerButton;

    private boolean loginWithoutNet = false;

    @Override
    protected void onCreate(@Nullable Bundle bundle) {
        checkLoginUser();
        setContentView(R.layout.activity_phone_login);
        super.onCreate(bundle);
        initView();
        initListener();
        requestPermissions(getPermissions());
    }

    private void initView() {
        restoreUserNameAndPasswd();
    }

    private void initListener() {
       /* visiableCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

                if (isChecked){
                    passwdEditText.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);// 输入为密码且可见
                }else {
                    passwdEditText.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD | InputType.TYPE_CLASS_TEXT);// 设置文本类密码（默认不可见），这两个属性必须同时设置
                }
            }
        });*/

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (loginWithoutNet) {
                    testLoginByLocalJson();
                    return;
                }
                saveUserNameAndPasswd();
                if (verifyInputText()) {
                    changeLoginButtonStatus(false);
                    login(getUserNameText(), getPasswdText());
                } else {
                 ToastUtil.showShortToast("用户名或密码不能为空！");
                }
            }
        });

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mIntent = new Intent(PhoneLoginActivity.this, RegisterByPhoneActivity.class);
                startActivity(mIntent);
            }
        });



        forgetPasswdTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mIntent = new Intent(PhoneLoginActivity.this, ForgetPasswdPasswdActivity.class);
                startActivity(mIntent);
            }
        });
    }

    private String getUserNameText() {
        return userNameEditText.getText().toString();
    }

    private String getPasswdText() {
        return passwdEditText.getText().toString();
    }

    private boolean verifyUserNameText() {
        String userName = getUserNameText();
        if (!CommonUtils.isEmpty(userName)) {
            return true;
        }
        return false;
    }

    private boolean verifyPasswdText() {
        String passwed = getPasswdText();
        if (!CommonUtils.isEmpty(passwed)) {
            return true;
        }
        return false;
    }

    private boolean verifyInputText() {
        if (verifyUserNameText() && verifyPasswdText()) {
            return true;
        }
        return false;
    }

    private void saveUserNameAndPasswd() {
        if (true) {
            Config.getConfig().saveRemeberMeStatus(true);
            String userName = getUserNameText();
            String passwd = getPasswdText();
            if (!CommonUtils.isEmpty(userName)) {
                userName = SecUtil.encodeByAES(userName);
            }

            if (!CommonUtils.isEmpty(passwd)) {
                passwd = SecUtil.encodeByAES(passwd);
            }

            Config.getConfig().saveUserName(userName);
            Config.getConfig().savePasswd(passwd);
        } else {
            Config.getConfig().saveRemeberMeStatus(false);
        }
    }

    private void restoreUserNameAndPasswd() {


        if (true) {
            String userName = Config.getConfig().getUserName();
            String passwd = Config.getConfig().getPasswd();
            if (!CommonUtils.isEmpty(userName)) {
                userName = SecUtil.decodeByAES(userName);
                if(!CommonUtils.isEmpty(userName)) {
                    userNameEditText.setText(userName);
                }
            }

            if (!CommonUtils.isEmpty(passwd)) {
                passwd = SecUtil.decodeByAES(passwd);
                if (!CommonUtils.isEmpty(passwd)) {
                    passwdEditText.setText(passwd);
                }
            }

        }

    }

    private void login(final String userName, final String passwd) {

                HashMap<String, String> headerMap = new HashMap<>();
                HashMap<String, String> bodyMap = new HashMap<>();
                //bodyMap.put(IMemberLogin.VERSION_CODE, AppUtils.getVersionCode2Str());
                bodyMap.put(IMemberLogin.LOGIN_NAME, userName);
                bodyMap.put(IMemberLogin.PASSWORD, Md5Util.getMD5String(SecUtil.getRSAsinatureWithLogin(passwd)));
                bodyMap.put(IMemberLogin.IMEI, DeviceUtils.getIMEI());
                bodyMap.put(IMemberLogin.IP, NetUtil.getIpAddress(MyApplication.getInstance()));
                bodyMap.put(IMemberLogin.PLATFORMTYPE, String.valueOf(ISendVerficationCode.PLATFORM_VALUE_ANDROID));
                bodyMap.put(IMemberLogin.VERSIONNAME, AppUtils.getGitCommitVersionName(MyApplication.getInstance()));

                //bodyMap.put(ISendVerficationCode.PLATFORM, String.valueOf(ISendVerficationCode.PLATFORM_VALUE_ANDROID));
                //mVerificationCodResponseBean = null;
                LoginSubscribe.loginAccount(headerMap, bodyMap, new DisposableObserver<ResponseBody>() {
                    @Override
                    public void onNext(ResponseBody responseBody) {
                        changeLoginButtonStatus(true);
                        MLog.d("login onNext");

                        String jsonBody = null;
                        try {
                            jsonBody = new String(responseBody.bytes());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        MLog.d("Server login jsonBody = " + jsonBody);
                        //SdLogUtil.writeCommonLog(" onNext responseBody = " + jsonBody);
                        MemberLoginResponseBean memberLoginResponseBean = (MemberLoginResponseBean) JsonUtil.json2objectWithDataCheck(jsonBody, new TypeReference<MemberLoginResponseBean>(){});
                        //SdLogUtil.writeCommonLog(" onNext memberLoginResponseBean = " + memberLoginResponseBean);
                        FileUtils.write(MyApplication.getInstance(), TrainModel.HTTP_LOGIN_USER_INFO, GsonTools.createGsonString(memberLoginResponseBean));
                        String json = FileUtils.read(MyApplication.getInstance(), TrainModel.HTTP_LOGIN_USER_INFO);
                        MLog.d("login_response_file_json = " + json);
                        jumpNewActivity(memberLoginResponseBean);
                    }

                    @Override
                    public void onError(Throwable e) {
                        MLog.d("login onError " + e.getMessage());
                        changeLoginButtonStatus(true);
                        e.printStackTrace();

                    }

                    @Override
                    public void onComplete() {
                        MLog.d("login on complete ");

                    }
                });


    }

    private void changeLoginButtonStatus(boolean clickable) {
        if (null != loginButton) {
            loginButton.setClickable(clickable);
        }
    }

    private void jumpNewActivity(MemberLoginResponseBean memberLoginResponseBeanParam) {
        if (null != memberLoginResponseBeanParam) {
            if (memberLoginResponseBeanParam.getStatus().equals(IBaseRequest.SUCCESS)) {
                MemberLoginResponseBean.DataBean mDataBean = memberLoginResponseBeanParam.getData();
                if (null != mDataBean) {
                    Config.getConfig().saveUserServerId(mDataBean.getId());

                    if(haveBindDeviceAndEyeInfo(mDataBean)) {
                        Intent mIntent = new Intent(PhoneLoginActivity.this, BleMainControlActivity.class);
                        mIntent.putExtra(ConstantString.KEY_MAIN_CONTR0L_FROM_LOGIN, true);
                        startActivity(mIntent);
                        insertUserInfo2DB(memberLoginResponseBeanParam);
                        finish();
                    } else {
                        Intent mIntent = new Intent(PhoneLoginActivity.this, BindPersonalInfoActivity.class);
                        startActivity(mIntent);
                        insertUserInfo2DB(memberLoginResponseBeanParam);
                        finish();
                    }
                }
            } else {
                ToastUtil.showShort(memberLoginResponseBeanParam.getMessage());
            }
        }
    }

    private void updateUserInfo(MemberLoginResponseBean memberLoginResponseBeanParam) {
        if (null != memberLoginResponseBeanParam) {
            if (memberLoginResponseBeanParam.getStatus().equals(IBaseRequest.SUCCESS)) {
                MemberLoginResponseBean.DataBean mDataBean = memberLoginResponseBeanParam.getData();
                if (null != mDataBean) {
                    Config.getConfig().saveUserServerId(mDataBean.getId());
                    insertUserInfo2DB(memberLoginResponseBeanParam);
                }
            } else {
                ToastUtil.showShort(memberLoginResponseBeanParam.getMessage());
                if (memberLoginResponseBeanParam.getStatus().equals(IBaseRequest.LOGIN_ERROR_WITH_USERNAME_PASSW)) {
                    /**
                     * 用户名或密码不正确
                     */
                    Config.getConfig().savePasswd(null);
                    Config.getConfig().saveFreshToken(null);
                    Config.getConfig().saveAccessToken(null);
                    BleDeviceManager.getInstance().stopScan();
                    BleDeviceManager.getInstance().stopScanByMac();
                    BleDeviceManager.getInstance().disconnectGlassesBleDevice(true);
                    GlassesBleDataModel.getInstance().clearModelData();
                    TrainModel.getInstance().clearTrainModelData();
                    //要求用户直接登录
                    BaseActivity.GotoLoginActivity();
                }
            }
        }
    }

    private void insertUserInfo2DB(MemberLoginResponseBean memberLoginResponseBean) {
        MemberLoginResponseBean.DataBean dataBean = memberLoginResponseBean.getData();
        String serverId = dataBean.getId();
        if (CommonUtils.isEmpty(serverId)) {
            throw new NullPointerException("serverId can not be null");
        }
        List<UserInfoDBBean> userInfoDBBeanList = UserInfoBeanDaoOpe.queryUserInfoByServerID(MyApplication.getInstance(), serverId);
        UserInfoDBBean userInfoDBBean = null;
        boolean execUpdate = false;
        if (null != userInfoDBBeanList && userInfoDBBeanList.size() > 0) {
            userInfoDBBean = userInfoDBBeanList.get(0);
        }

        if (null != userInfoDBBean && !CommonUtils.isEmpty(userInfoDBBean.getServerId())) {
            /**
             * 本地保存的有用户的视力信息
             */
            execUpdate = true;
        } else {
            /**
             * 本地未保存的有用户的视力信息
             */
            execUpdate = false;
            userInfoDBBean = new UserInfoDBBean();
            userInfoDBBean.setServerId(dataBean.getId());
        }

        userInfoDBBean.setGuardian_id(dataBean.getGuardian_id());
        userInfoDBBean.setLogin_name(dataBean.getLogin_name());
        userInfoDBBean.setNickname(dataBean.getNickname());
        userInfoDBBean.setName(dataBean.getName());
        userInfoDBBean.setDiopter_state(dataBean.getDiopter_state());

        userInfoDBBean.setLeft_eye_degree(dataBean.getLeft_eye_degree());
        userInfoDBBean.setRight_eye_degree(dataBean.getRight_eye_degree());
        userInfoDBBean.setLeft_front_com_degree(dataBean.getLeft_front_com_degree());
        userInfoDBBean.setRight_front_com_degree(dataBean.getRight_front_com_degree());
        userInfoDBBean.setInterpupillary(dataBean.getInterpupillary());



        userInfoDBBean.setCorrect_left_eye_degree(dataBean.getCorrect_left_eye_degree());
        userInfoDBBean.setCorrect_right_eye_degree(dataBean.getCorrect_right_eye_degree());
        userInfoDBBean.setCorrect_binoculus_degree(dataBean.getCorrect_binoculus_degree());


        userInfoDBBean.setNaked_left_eye_degree(dataBean.getNaked_left_eye_degree());
        userInfoDBBean.setNaked_right_eye_degree(dataBean.getNaked_right_eye_degree());
        userInfoDBBean.setNaked_binoculus_degree(dataBean.getNaked_binoculus_degree());

        userInfoDBBean.setAge(dataBean.getAge());
        userInfoDBBean.setBorn_date(dataBean.getBorn_date());
        userInfoDBBean.setCredentials_card(dataBean.getCredentials_card());
        userInfoDBBean.setCredentials_type(dataBean.getCredentials_type());
        userInfoDBBean.setExpiration_time(dataBean.getExpiration_time());

        userInfoDBBean.setPortrait_image_url(dataBean.getFace());

        if (!CommonUtils.isEmpty(dataBean.getPhone())) {
            userInfoDBBean.setPhone(dataBean.getPhone());
        }

        userInfoDBBean.setSex(dataBean.getSex());
        userInfoDBBean.setStatus(dataBean.getStatus());


        userInfoDBBean.setLeft_astigmatism_degree(dataBean.getLeft_astigmatism_degree());
        userInfoDBBean.setRight_astigmatism_degree(dataBean.getRight_astigmatism_degree());
        userInfoDBBean.setLeft_eye_train_degree(dataBean.getLeft_handle_degree());
        userInfoDBBean.setRight_eye_train_degree(dataBean.getRight_handle_degree());

        /**
         *      * isnewuser	bool	是否新用户
         *      * total_money	float	总充值金额
         *      * used_money	float	已使用金额
         *      * total_time	float	总累计小时数
         *      * used_time	float	已使用小时数
         *      * total_score	float	累计总积分
         *      * used_score	float	已使用积分
         *      * status	int	状态0审核中 1审核通过 2审核不通过
         */

        userInfoDBBean.setIsnewuser(dataBean.isIsnewuser());
        userInfoDBBean.setTotal_money(dataBean.getTotal_money());
        userInfoDBBean.setUsed_money(dataBean.getUsed_money());
        userInfoDBBean.setTotal_time(dataBean.getTotal_time());
        userInfoDBBean.setUsed_time(dataBean.getUsed_time());
        userInfoDBBean.setTotal_score(dataBean.getTotal_score());
        userInfoDBBean.setUsed_score(dataBean.getUsed_score());

        userInfoDBBean.setLeft_axial(dataBean.getLeft_axial());
        userInfoDBBean.setRight_axial(dataBean.getRight_axial());



        if (execUpdate) {
            UserInfoBeanDaoOpe.insertOrReplaceData(MyApplication.getInstance(), userInfoDBBean);
        } else {
            userInfoDBBean.setLocalid(UUID.randomUUID().toString());
            UserInfoBeanDaoOpe.insertData(MyApplication.getInstance(), userInfoDBBean);
        }

    }

    private boolean haveBindDeviceAndEyeInfo(MemberLoginResponseBean.DataBean dataBean) {
        if (null != dataBean) {
            if (!CommonUtils.isEmpty(dataBean.getName())
                 && !CommonUtils.isEmpty(dataBean.getBorn_date())) {
                return true;
            }
        }
        return false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        stopBleScanAndAllConnect();
    }

    private void checkLoginUser() {
        boolean loginSuccess = lastLoginSuccess();
        if (BuildConfig.DEBUG) {
            MLog.d("loginSuccess = " + loginSuccess);
        }
        if (loginSuccess) {
            loginInBackGround();
            Intent mIntent = new Intent(PhoneLoginActivity.this, BleMainControlActivity.class);
            startActivity(mIntent);
            finish();
        }
    }

    private boolean lastLoginSuccess() {
        String serverId = Config.getConfig().getUserServerId();
        if (!CommonUtils.isEmpty(serverId)) {
            List<UserInfoDBBean> listBean = UserInfoBeanDaoOpe.queryUserInfoByServerID(MyApplication.getInstance(), serverId);
            if (null != listBean && listBean.size() > 0) {
                UserInfoDBBean userInfoDBBean = listBean.get(0);
                if (null != userInfoDBBean && (!CommonUtils.isEmpty(userInfoDBBean.getPhone()) || !CommonUtils.isEmpty(userInfoDBBean.getLogin_name()))
                    && !CommonUtils.isEmpty(userInfoDBBean.getName())
                    && !CommonUtils.isEmpty(userInfoDBBean.getBorn_date())) {

                    String userName = Config.getConfig().getUserName();
                    String passwd = Config.getConfig().getPasswd();
                    if (!CommonUtils.isEmpty(userName) && !CommonUtils.isEmpty(passwd)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private void loginInBackGround() {
        String userName = Config.getConfig().getUserName();
        String passwd = Config.getConfig().getPasswd();

        if (!CommonUtils.isEmpty(userName)) {
            userName = SecUtil.decodeByAES(userName);
        }

        if (!CommonUtils.isEmpty(passwd)) {
            passwd = SecUtil.decodeByAES(passwd);
        }

        loginWithBackGround(userName, passwd);
    }

    private void loginWithBackGround(final String userName, final String passwd) {
        HashMap<String, String> headerMap = new HashMap<>();
        HashMap<String, String> bodyMap = new HashMap<>();

        bodyMap.put(IMemberLogin.LOGIN_NAME, userName);
        bodyMap.put(IMemberLogin.PASSWORD, Md5Util.getMD5String(SecUtil.getRSAsinatureWithLogin(passwd)));
        bodyMap.put(IMemberLogin.IMEI, DeviceUtils.getIMEI());
        bodyMap.put(IMemberLogin.IP, NetUtil.getIpAddress(MyApplication.getInstance()));
        bodyMap.put(IMemberLogin.PLATFORMTYPE, String.valueOf(ISendVerficationCode.PLATFORM_VALUE_ANDROID));
        bodyMap.put(IMemberLogin.VERSIONNAME, AppUtils.getGitCommitVersionName(MyApplication.getInstance()));

        LoginSubscribe.loginAccount(headerMap, bodyMap, new DisposableObserver<ResponseBody>() {
            @Override
            public void onNext(ResponseBody responseBody) {
                String jsonBody = null;
                try {
                    jsonBody = new String(responseBody.bytes());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                MemberLoginResponseBean memberLoginResponseBean = (MemberLoginResponseBean) JsonUtil.json2objectWithDataCheck(jsonBody, new TypeReference<MemberLoginResponseBean>(){});
                updateUserInfo(memberLoginResponseBean);
                FileUtils.write(MyApplication.getInstance(), TrainModel.HTTP_LOGIN_USER_INFO, GsonTools.createGsonString(memberLoginResponseBean));
                String json = FileUtils.read(MyApplication.getInstance(), TrainModel.HTTP_LOGIN_USER_INFO);
                MLog.d("login_response_file_json = " + json);
            }

            @Override
            public void onError(Throwable e) {
                MLog.d("login onError " + e.getMessage());
                e.printStackTrace();
            }

            @Override
            public void onComplete() {
                MLog.d("login on complete ");
            }
        });
    }

    private void testLoginByLocalJson() {
        String loginResponseJson = "{\"status\":\"success\",\"message\":\"成功\",\"data\":{\"id\":\"bf46fb55-e2cc-4251-b0a0-4c1ad23d961a\",\"guardian_id\":null,\"login_name\":\"13987654321_P\",\"password\":null,\"nickname\":\"13987654321\",\"name\":\"某某\",\"age\":30,\"sex\":1,\"born_date\":\"1989-12-06T16:00:00.000+0000\",\"credentials_type\":0,\"credentials_card\":\"420607198912072819\",\"phone\":\"13987654321\",\"diopter_state\":0,\"left_eye_degree\":200.0,\"right_eye_degree\":300.0,\"integererpupillary\":66.0,\"left_astigmatism_degree\":30.0,\"right_astigmatism_degree\":60.0,\"astigmatism_degree\":0.0,\"left_handle_degree\":115.0,\"right_handle_degree\":180.0,\"right_column_mirror\":null,\"left_column_mirror\":null,\"right_axial\":30.0,\"left_axial\":60.0,\"naked_left_eye_degree\":4.3,\"naked_right_eye_degree\":4.6,\"naked_binoculus_degree\":5.1,\"correct_left_eye_degree\":5.2,\"correct_right_eye_degree\":5.3,\"correct_binoculus_degree\":5.1,\"isnewuser\":null,\"total_money\":null,\"used_money\":null,\"total_time\":null,\"used_time\":null,\"total_score\":null,\"used_score\":null,\"status\":null,\"expiration_time\":null,\"wearingHours\":0,\"face\":null},\"cursor\":null}";
        MemberLoginResponseBean memberLoginResponseBean = (MemberLoginResponseBean) JsonUtil.json2objectWithDataCheck(loginResponseJson, new TypeReference<MemberLoginResponseBean>(){});
        jumpNewActivity(memberLoginResponseBean);
    }

    private void testDownload() {
        String baseUrl = "http://softdown1.hao123.com/";
        //String exeUrl = "http://softdown1.hao123.com/hao123-soft-online-bcs/soft/2018_1_3_Setup.exe";
        String extUrl = "hao123-soft-online-bcs/soft/2018_1_3_Setup.exe";

        DownloadUtils downloadUtils = new DownloadUtils(baseUrl, new JsDownloadListener() {
            @Override
            public void onStartDownload(long length) {
                MLog.d("length = " + length + " thread.id = " + Thread.currentThread().getId());

            }

            @Override
            public void onProgress(int progress) {
                //MLog.d("progress = " + progress);
            }

            @Override
            public void onFail(String errorInfo) {
                MLog.d("errorInfo = " + errorInfo);
            }
        });

        Observable observable = downloadUtils.getDownApi(extUrl);//HttpMethods.getInstance().getHttpApi().downloadFile(new HashMap<String, Object>(), TrainSuscribe.createNewRequestBody(new HashMap<String, String>()), exeUrl);
        File dirFile = getDir("test", Context.MODE_APPEND);
        if (!dirFile.exists()) {
            dirFile.mkdirs();
        }
        final File file = new File(dirFile.getAbsolutePath() + File.separator + "aa.exe");
        if (file.exists()) {
            file.delete();
        }
        downloadUtils.download(file, observable, new DisposableObserver<InputStream>() {
            @Override
            public void onNext(InputStream responseBody) {
                MLog.d("onNext = responseBody" + responseBody);
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                MLog.d("onError = " + e.getMessage());
            }

            @Override
            public void onComplete() {
                MLog.d("onComplete = file = " + file.length() );

            }
        });



    }

}
