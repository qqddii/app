package com.intelligence.glasses.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.alibaba.fastjson.TypeReference;
import com.android.common.baselibrary.jnative.Base64;
import com.android.common.baselibrary.jnative.SecUtil;
import com.android.common.baselibrary.log.MLog;
import com.android.common.baselibrary.util.DateUtil;

import com.android.common.baselibrary.util.ToastUtil;
import com.android.common.baselibrary.util.comutil.CommonUtils;

import com.android.common.baselibrary.util.ThreadPoolUtils;

import com.intelligence.glasses.MyApplication;
import com.intelligence.glasses.activity.bindglasses.BindPersonalInfoActivity;
import com.intelligence.glasses.activity.login.PhoneLoginActivity;
import com.intelligence.glasses.base.BaseBleActivity;
import com.intelligence.glasses.bean.MemberLoginResponseBean;
import com.intelligence.glasses.bean.bledata.HttpRequestTrainJsonListItemBean;
import com.intelligence.glasses.bean.bledata.UserTrainInfoBleDataBean;
import com.intelligence.glasses.bean.request.HttpRequestUserTrainTimeInfoBean;
import com.intelligence.glasses.constant.ConstantString;
import com.intelligence.glasses.greendao.greendaobean.UserInfoDBBean;
import com.intelligence.glasses.greendao.greendaobean.UserInfoTrainBleDataDBBean;
import com.intelligence.glasses.greendao.greenddaodb.UserInfoBeanDaoOpe;
import com.intelligence.glasses.greendao.greenddaodb.UserInfoTrainBleDataBeanDaoOpe;
import com.intelligence.glasses.http.request.IBaseRequest;
import com.intelligence.glasses.http.request.ITrainInfoUpload;
import com.intelligence.glasses.model.TrainModel;
import com.intelligence.glasses.util.Config;
import com.intelligence.glasses.util.jsonutil.JsonUtil;
import com.uuzuche.lib_zxing.activity.CaptureActivity;
import com.uuzuche.lib_zxing.activity.CodeUtils;
import com.vise.baseble.ViseBle;
import com.vise.utils.cipher.Base64Util;
import com.shitec.bleglasses.R;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import butterknife.BindView;


public class MainActivity extends BaseBleActivity {
    @BindView(R.id.gowelcome)
    Button goWelcomeButton;

    @BindView(R.id.goblesearch)
    Button goBleSearchButton;

    @BindView(R.id.goscanqrcode)
    Button goScanQRCode;

    @BindView(R.id.goloading)
    Button goLoading;

    @BindView(R.id.gologin)
    Button goLoginButton;

    @BindView(R.id.updatebutton)
    Button updateButton;

    @BindView(R.id.exitapp)
    Button exitButton;

    @BindView(R.id.testedittext)
    EditText mEditText;

    public static String qrCode = "0C:FC:83:9C:EA:18";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        changeStatusBarTextColor(true);
        setNewTheme();
        setContentView(R.layout.activity_main);
        super.onCreate(savedInstanceState);
        requestPermissions(getPermissions());

    }

    @Override
    protected void onResume() {
        super.onResume();
        initListener();
       // requestBlePermissions();

    }

    private void initListener() {
        goWelcomeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               Intent mIntent = new Intent(MainActivity.this, WelcomeActivity.class);
               startActivity(mIntent);
               //testTrainTimeCollect();
              /*  ScanBleDeviceDialog scanBleDeviceDialog = new ScanBleDeviceDialog(MainActivity.this);
                scanBleDeviceDialog.show();*/
            }
        });
        goBleSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Intent mIntent = new Intent(MainActivity.this, DBOperationActivity.class);
                startActivity(mIntent);*/
                testLoginByLocalJson();
                Intent mIntent = new Intent(MainActivity.this, SendReceiveBleDataActivity.class);
                startActivity(mIntent);
               //stopTrain();

/*                HttpUtil.getToken(new RequestTokenCallBack() {
                    @Override
                    public void getToken(String token) {
                        BaseConstant.TOKEN = token;
                        HashMap<String, String> headerMap = new HashMap<>();
                        //headerMap.put(ISendVerficationCode.BEARER, UrlUtils.encodeUrl(token));
                        //headerMap.put("Authorization", "Bearer " + token);
                        headerMap.put(ISendVerficationCode.AUTHORIZATION, ISendVerficationCode.BEARER + token);
                        HashMap<String, String> bodyMap = new HashMap<>();
                        bodyMap.put(ISendVerficationCode.PHONENUM, "18689486765");
                        bodyMap.put(ISendVerficationCode.IMEI, DeviceUtils.getIMEI());
                        bodyMap.put(ISendVerficationCode.PLATFORM, String.valueOf(ISendVerficationCode.PLATFORM_VALUE_ANDROID));

                        LoginSubscribe.postRegisterWithVerficationCode(headerMap, bodyMap, new DisposableObserver<ResponseBody>() {
                            @Override
                            public void onNext(ResponseBody responseBody) {
                                String jsonBody = null;
                                try {
                                    jsonBody = new String(responseBody.bytes());
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                SdLogUtil.writeCommonLog("ThreadID = " + Thread.currentThread().getId() + " onNext jsonBody = " + jsonBody);
                            }

                            @Override
                            public void onError(Throwable e) {

                            }

                            @Override
                            public void onComplete() {

                            }
                        });



                    }
                });*/
            }
        });
        goScanQRCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(MainActivity.this, CaptureActivity.class), 1);
            }
        });
        goLoading.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String serverId = Config.getConfig().getUserServerId();
                if (!CommonUtils.isEmpty(serverId)) {
                    startActivityForResult(new Intent(MainActivity.this, BleMainControlActivity.class), 1);
                } else {
                    Intent mIntent = new Intent(MainActivity.this, PhoneLoginActivity.class);
                    startActivity(mIntent);
                }
            }
        });

        goLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mIntent = new Intent(MainActivity.this, PhoneLoginActivity.class);
                startActivity(mIntent);
                //testJniMethod();
            }
        });

        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isEnable = mEditText.isEnabled();
                if (isEnable) {
                    mEditText.setEnabled(false);
                    updateButton.setText("不可修改");
                } else {
                    mEditText.setEnabled(true);
                    updateButton.setText("修改");
                }
            }
        });

        exitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* TrainModel trainModel = new TrainModel();
                    trainModel.testUpload();*/
              /*  HttpRequestBleDataBean httpRequestBleDataBean = new HttpRequestBleDataBean();
                httpRequestBleDataBean.setMemberId(Config.getConfig().getUserServerId());
                trainModel.getNewTrainInfo2BleDevice(httpRequestBleDataBean);*/

                TrainModel trainModel = TrainModel.getInstance();
                HttpRequestUserTrainTimeInfoBean trainTimeInfoBean = new HttpRequestUserTrainTimeInfoBean();
                trainTimeInfoBean.setMemberId(Config.getConfig().getUserServerId());
                long agoLong = 365L * 24 * 60 * 60 * 1000;
                trainTimeInfoBean.setBt(DateUtil.tlocalformatter.format(new Date(System.currentTimeMillis() - agoLong)));
                trainTimeInfoBean.setEt(DateUtil.tlocalformatter.format(new Date(System.currentTimeMillis())));
                List<HttpRequestUserTrainTimeInfoBean> httpRequestUserTrainTimeInfoBeanList = trainModel.getUserTrainTimeHttpRequestBean();
                if (null != httpRequestUserTrainTimeInfoBeanList && httpRequestUserTrainTimeInfoBeanList.size() > 0) {
                    for (HttpRequestUserTrainTimeInfoBean bean: httpRequestUserTrainTimeInfoBeanList) {
                        trainModel.getUserTrainTimeinf(bean, null);
                    }
                }

               ViseBle.getInstance().disconnect();
               ViseBle.getInstance().clear();
               //System.exit(1);
            }
        });
    }

    private void testTrainTimeCollect() {
        ThreadPoolUtils.execute(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 3; i++) {
                    startTrain(i);
                    runningTrain(i);
                    stopTrain(i);
                }

                reportStart();
                reportRunning();
                reportStop();
            }
        });

    }

    private void reportStart() {
        TrainModel trainModel = TrainModel.getInstance();

        List<UserTrainInfoBleDataBean> userTrainInfoBleDataBeanList = trainModel.queryUserTrainInfoBleDataFromDBByOperation(ITrainInfoUpload.OPERATION_START_ACTION);
        MLog.d("userTrainInfoBleDataBeanList.size = " + userTrainInfoBleDataBeanList.size());
        if (null != userTrainInfoBleDataBeanList) {
            for (UserTrainInfoBleDataBean itemBean : userTrainInfoBleDataBeanList) {
                //MLog.d("userTrainInfoBleDataBeanList itemBean.getDataList().size() = " + itemBean.getDataList().size());
                trainModel.uploadTrainInfo(itemBean, null);
            }
        }

    }

    private void reportRunning() {
        TrainModel trainModel = TrainModel.getInstance();
        List<UserTrainInfoBleDataBean> userTrainInfoBleDataBeanList = trainModel.queryUserTrainInfoBleDataFromDBByOperation(ITrainInfoUpload.OPERATION_RUNNING_ACTION);
        //MLog.d("userTrainInfoBleDataBeanList.size = " + userTrainInfoBleDataBeanList.size());
        if (null != userTrainInfoBleDataBeanList) {
            for (UserTrainInfoBleDataBean itemBean : userTrainInfoBleDataBeanList) {
                trainModel.uploadTrainInfo(itemBean, null);
            }
        }

    }

    private void reportStop() {

        TrainModel trainModel = TrainModel.getInstance();
        List<UserTrainInfoBleDataBean> userTrainInfoBleDataBeanList = trainModel.queryUserTrainInfoBleDataFromDBByOperation(ITrainInfoUpload.OPERATION_STOP_ACTION);
        //MLog.d("userTrainInfoBleDataBeanList.size = " + userTrainInfoBleDataBeanList.size());
        if (null != userTrainInfoBleDataBeanList) {
            for (UserTrainInfoBleDataBean itemBean : userTrainInfoBleDataBeanList) {
                trainModel.uploadTrainInfo(itemBean, null);
            }
        }

    }

    private void startTrain(int times) {
        //TrainModel trainModel = TrainModel.getInstance();
        UserInfoDBBean userInfoDBBean = UserInfoBeanDaoOpe.queryUserInfoByServerID(MyApplication.getInstance(), Config.getConfig().getUserServerId()).get(0);

        UserTrainInfoBleDataBean userTrainInfoBleDataBean = new UserTrainInfoBleDataBean();
        userTrainInfoBleDataBean.setMemberId(Config.getConfig().getUserServerId());
        userTrainInfoBleDataBean.setLogin_name(userInfoDBBean.getLogin_name());
        userTrainInfoBleDataBean.setDeviceId("b322b88fd1874a26ade6248ddb11de2f");
        userTrainInfoBleDataBean.setOperationType(ITrainInfoUpload.OPERATION_START_ACTION);
        userTrainInfoBleDataBean.setPlatform(1);

        List<HttpRequestTrainJsonListItemBean> httpRequestTrainJsonListItemBeansList = new ArrayList<>();
        HttpRequestTrainJsonListItemBean httpRequestTrainJsonListItemBean = new HttpRequestTrainJsonListItemBean();

        String start_Time = DateUtil.tlocalformatter.format(new Date(getStartTime(times, 0)));
        String collect_Time = DateUtil.tlocalformatter.format(new Date(getStartTime(times,  0)));

        httpRequestTrainJsonListItemBean.setCollect_time(collect_Time);
        httpRequestTrainJsonListItemBean.setStart_time(start_Time);
        Config.getConfig().saveTrainStartTime(collect_Time);
        httpRequestTrainJsonListItemBean.setLeft_counts(11);
        httpRequestTrainJsonListItemBean.setLeft_speed(111);
        httpRequestTrainJsonListItemBean.setLeft_range_up(-450);
        httpRequestTrainJsonListItemBean.setLeft_range_low(-200);

        httpRequestTrainJsonListItemBean.setRight_counts(22);
        httpRequestTrainJsonListItemBean.setRight_speed(222);
        httpRequestTrainJsonListItemBean.setRight_range_up(-350);
        httpRequestTrainJsonListItemBean.setRight_range_low(-150);

        httpRequestTrainJsonListItemBeansList.add(httpRequestTrainJsonListItemBean);
        //httpRequestTrainJsonListItemBeansList.add(httpRequestTrainJsonListItemBean);

        userTrainInfoBleDataBean.setDataList(httpRequestTrainJsonListItemBeansList);

        List<UserInfoTrainBleDataDBBean> insertList = new ArrayList<>();

        for (HttpRequestTrainJsonListItemBean bean : httpRequestTrainJsonListItemBeansList) {

            UserInfoTrainBleDataDBBean userInfoTrainBleDataDBBean = new UserInfoTrainBleDataDBBean();
            userInfoTrainBleDataDBBean.setLocalid(UUID.randomUUID().toString());
            userInfoTrainBleDataDBBean.setServerId(Config.getConfig().getUserServerId());
            userInfoTrainBleDataDBBean.setLogin_name(userTrainInfoBleDataBean.getLogin_name());
            userInfoTrainBleDataDBBean.setDeviceId(userTrainInfoBleDataBean.getDeviceId());

            userInfoTrainBleDataDBBean.setStart_time(bean.getStart_time());
            userInfoTrainBleDataDBBean.setCollect_time(bean.getCollect_time());
            try {
                userInfoTrainBleDataDBBean.setCollect_time_long(DateUtil.tlocalformatter.parse(bean.getCollect_time()).getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            userInfoTrainBleDataDBBean.setLeft_counts(bean.getLeft_counts());
            userInfoTrainBleDataDBBean.setLeft_speed(bean.getLeft_speed());
            userInfoTrainBleDataDBBean.setLeft_range_up(bean.getLeft_range_up());
            userInfoTrainBleDataDBBean.setLeft_range_low(bean.getLeft_range_low());
            userInfoTrainBleDataDBBean.setRight_counts(bean.getRight_counts());
            userInfoTrainBleDataDBBean.setRight_speed(bean.getRight_speed());
            userInfoTrainBleDataDBBean.setRight_range_up(bean.getRight_range_up());
            userInfoTrainBleDataDBBean.setRight_range_low(bean.getRight_range_low());
            userInfoTrainBleDataDBBean.setReportedServer(false);
            userInfoTrainBleDataDBBean.setNotifyBleDevice(false);
            userInfoTrainBleDataDBBean.setOperation(userTrainInfoBleDataBean.getOperationType());
            insertList.add(userInfoTrainBleDataDBBean);
        }

        UserInfoTrainBleDataBeanDaoOpe.insertData(MyApplication.getInstance(), insertList);

     /*   List<UserTrainInfoBleDataBean> userTrainInfoBleDataBeanList = trainModel.queryUserTrainInfoBleDataFromDBByOperation(ITrainInfoUpload.OPERATION_START_ACTION);
        MLog.d("userTrainInfoBleDataBeanList.size = " + userTrainInfoBleDataBeanList.size());
        for (UserTrainInfoBleDataBean itemBean : userTrainInfoBleDataBeanList) {
            MLog.d("userTrainInfoBleDataBeanList itemBean.getDataList().size() = " + itemBean.getDataList().size());
            trainModel.uploadTrainInfo(itemBean, null);
        }*/
    }

    private void stopTrain(int times) {
        TrainModel trainModel = TrainModel.getInstance();
        UserInfoDBBean userInfoDBBean = UserInfoBeanDaoOpe.queryUserInfoByServerID(MyApplication.getInstance(), Config.getConfig().getUserServerId()).get(0);

        UserTrainInfoBleDataBean userTrainInfoBleDataBean = new UserTrainInfoBleDataBean();
        userTrainInfoBleDataBean.setMemberId(Config.getConfig().getUserServerId());
        userTrainInfoBleDataBean.setLogin_name(userInfoDBBean.getLogin_name());
        userTrainInfoBleDataBean.setDeviceId("b322b88fd1874a26ade6248ddb11de2f");
        userTrainInfoBleDataBean.setOperationType(ITrainInfoUpload.OPERATION_STOP_ACTION);
        userTrainInfoBleDataBean.setPlatform(1);

        List<HttpRequestTrainJsonListItemBean> httpRequestTrainJsonListItemBeansList = new ArrayList<>();
        HttpRequestTrainJsonListItemBean httpRequestTrainJsonListItemBean = new HttpRequestTrainJsonListItemBean();
        String start_Time = DateUtil.tlocalformatter.format(new Date(getStartTime(times, 0)));
        String collect_Time = DateUtil.tlocalformatter.format(new Date(getStartTime(times,  2 * 15L * 60 * 1000)));
        httpRequestTrainJsonListItemBean.setStart_time(start_Time);
        httpRequestTrainJsonListItemBean.setCollect_time(collect_Time);
        httpRequestTrainJsonListItemBean.setLeft_counts(11);
        httpRequestTrainJsonListItemBean.setLeft_speed(111);
        httpRequestTrainJsonListItemBean.setLeft_range_up(-450);
        httpRequestTrainJsonListItemBean.setLeft_range_low(-200);

        httpRequestTrainJsonListItemBean.setRight_counts(22);
        httpRequestTrainJsonListItemBean.setRight_speed(222);
        httpRequestTrainJsonListItemBean.setRight_range_up(-350);
        httpRequestTrainJsonListItemBean.setRight_range_low(-150);

        httpRequestTrainJsonListItemBeansList.add(httpRequestTrainJsonListItemBean);
        //httpRequestTrainJsonListItemBeansList.add(httpRequestTrainJsonListItemBean);

        userTrainInfoBleDataBean.setDataList(httpRequestTrainJsonListItemBeansList);

        List<UserInfoTrainBleDataDBBean> insertList = new ArrayList<>();

        for (HttpRequestTrainJsonListItemBean bean : httpRequestTrainJsonListItemBeansList) {

            UserInfoTrainBleDataDBBean userInfoTrainBleDataDBBean = new UserInfoTrainBleDataDBBean();
            userInfoTrainBleDataDBBean.setLocalid(UUID.randomUUID().toString());
            userInfoTrainBleDataDBBean.setServerId(Config.getConfig().getUserServerId());
            userInfoTrainBleDataDBBean.setLogin_name(userTrainInfoBleDataBean.getLogin_name());
            userInfoTrainBleDataDBBean.setDeviceId(userTrainInfoBleDataBean.getDeviceId());

            userInfoTrainBleDataDBBean.setStart_time(Config.getConfig().getTrainStartTime());
            userInfoTrainBleDataDBBean.setCollect_time(bean.getCollect_time());
            try {
                userInfoTrainBleDataDBBean.setCollect_time_long(DateUtil.tlocalformatter.parse(bean.getCollect_time()).getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            userInfoTrainBleDataDBBean.setLeft_counts(bean.getLeft_counts());
            userInfoTrainBleDataDBBean.setLeft_speed(bean.getLeft_speed());
            userInfoTrainBleDataDBBean.setLeft_range_up(bean.getLeft_range_up());
            userInfoTrainBleDataDBBean.setLeft_range_low(bean.getLeft_range_low());
            userInfoTrainBleDataDBBean.setRight_counts(bean.getRight_counts());
            userInfoTrainBleDataDBBean.setRight_speed(bean.getRight_speed());
            userInfoTrainBleDataDBBean.setRight_range_up(bean.getRight_range_up());
            userInfoTrainBleDataDBBean.setRight_range_low(bean.getRight_range_low());
            userInfoTrainBleDataDBBean.setReportedServer(false);
            userInfoTrainBleDataDBBean.setNotifyBleDevice(false);
            userInfoTrainBleDataDBBean.setOperation(userTrainInfoBleDataBean.getOperationType());
            insertList.add(userInfoTrainBleDataDBBean);
        }

        UserInfoTrainBleDataBeanDaoOpe.insertData(MyApplication.getInstance(), insertList);

      /*  List<UserTrainInfoBleDataBean> userTrainInfoBleDataBeanList = trainModel.queryUserTrainInfoBleDataFromDBByOperation(ITrainInfoUpload.OPERATION_STOP_ACTION);
        MLog.d("userTrainInfoBleDataBeanList.size = " + userTrainInfoBleDataBeanList.size());
        for (UserTrainInfoBleDataBean itemBean : userTrainInfoBleDataBeanList) {
            trainModel.uploadTrainInfo(itemBean, null);
        }*/

    }

    private void runningTrain(int times) {
        TrainModel trainModel = TrainModel.getInstance();
        UserInfoDBBean userInfoDBBean = UserInfoBeanDaoOpe.queryUserInfoByServerID(MyApplication.getInstance(), Config.getConfig().getUserServerId()).get(0);

        UserTrainInfoBleDataBean userTrainInfoBleDataBean = new UserTrainInfoBleDataBean();
        userTrainInfoBleDataBean.setMemberId(Config.getConfig().getUserServerId());
        userTrainInfoBleDataBean.setLogin_name(userInfoDBBean.getLogin_name());
        userTrainInfoBleDataBean.setDeviceId("b322b88fd1874a26ade6248ddb11de2f");
        userTrainInfoBleDataBean.setOperationType(ITrainInfoUpload.OPERATION_RUNNING_ACTION);
        userTrainInfoBleDataBean.setPlatform(1);

        List<HttpRequestTrainJsonListItemBean> httpRequestTrainJsonListItemBeansList = new ArrayList<>();
        HttpRequestTrainJsonListItemBean httpRequestTrainJsonListItemBean = new HttpRequestTrainJsonListItemBean();
        //httpRequestTrainJsonListItemBean.setCollect_time(DateUtil.tlocalformatter.format(new Date(System.currentTimeMillis())));
        String start_Time = DateUtil.tlocalformatter.format(new Date(getStartTime(times, 0)));
        String collect_Time = DateUtil.tlocalformatter.format(new Date(getStartTime(times,  15L * 60 * 1000)));
        httpRequestTrainJsonListItemBean.setStart_time(start_Time);
        httpRequestTrainJsonListItemBean.setCollect_time(collect_Time);

        httpRequestTrainJsonListItemBean.setLeft_counts(11);
        httpRequestTrainJsonListItemBean.setLeft_speed(111);
        httpRequestTrainJsonListItemBean.setLeft_range_up(-450);
        httpRequestTrainJsonListItemBean.setLeft_range_low(-200);

        httpRequestTrainJsonListItemBean.setRight_counts(22);
        httpRequestTrainJsonListItemBean.setRight_speed(222);
        httpRequestTrainJsonListItemBean.setRight_range_up(-350);
        httpRequestTrainJsonListItemBean.setRight_range_low(-150);

        httpRequestTrainJsonListItemBeansList.add(httpRequestTrainJsonListItemBean);
        //httpRequestTrainJsonListItemBeansList.add(httpRequestTrainJsonListItemBean);

        userTrainInfoBleDataBean.setDataList(httpRequestTrainJsonListItemBeansList);

        List<UserInfoTrainBleDataDBBean> insertList = new ArrayList<>();

        for (HttpRequestTrainJsonListItemBean bean : httpRequestTrainJsonListItemBeansList) {

            UserInfoTrainBleDataDBBean userInfoTrainBleDataDBBean = new UserInfoTrainBleDataDBBean();
            userInfoTrainBleDataDBBean.setLocalid(UUID.randomUUID().toString());
            userInfoTrainBleDataDBBean.setServerId(Config.getConfig().getUserServerId());
            userInfoTrainBleDataDBBean.setLogin_name(userTrainInfoBleDataBean.getLogin_name());
            userInfoTrainBleDataDBBean.setDeviceId(userTrainInfoBleDataBean.getDeviceId());

            userInfoTrainBleDataDBBean.setStart_time(Config.getConfig().getTrainStartTime());
            userInfoTrainBleDataDBBean.setCollect_time(bean.getCollect_time());
            try {
                userInfoTrainBleDataDBBean.setCollect_time_long(DateUtil.tlocalformatter.parse(bean.getCollect_time()).getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            userInfoTrainBleDataDBBean.setLeft_counts(bean.getLeft_counts());
            userInfoTrainBleDataDBBean.setLeft_speed(bean.getLeft_speed());
            userInfoTrainBleDataDBBean.setLeft_range_up(bean.getLeft_range_up());
            userInfoTrainBleDataDBBean.setLeft_range_low(bean.getLeft_range_low());
            userInfoTrainBleDataDBBean.setRight_counts(bean.getRight_counts());
            userInfoTrainBleDataDBBean.setRight_speed(bean.getRight_speed());
            userInfoTrainBleDataDBBean.setRight_range_up(bean.getRight_range_up());
            userInfoTrainBleDataDBBean.setRight_range_low(bean.getRight_range_low());
            userInfoTrainBleDataDBBean.setReportedServer(false);
            userInfoTrainBleDataDBBean.setNotifyBleDevice(false);
            userInfoTrainBleDataDBBean.setOperation(userTrainInfoBleDataBean.getOperationType());
            insertList.add(userInfoTrainBleDataDBBean);
        }

        UserInfoTrainBleDataBeanDaoOpe.insertData(MyApplication.getInstance(), insertList);

     /*   List<UserTrainInfoBleDataBean> userTrainInfoBleDataBeanList = trainModel.queryUserTrainInfoBleDataFromDBByOperation(ITrainInfoUpload.OPERATION_RUNNING_ACTION);
        MLog.d("userTrainInfoBleDataBeanList.size = " + userTrainInfoBleDataBeanList.size());
        for (UserTrainInfoBleDataBean itemBean : userTrainInfoBleDataBeanList) {
            trainModel.uploadTrainInfo(itemBean, null);
        }*/

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                Bundle bundle = data.getExtras();
                if (null != bundle) {
                    int resultType = bundle.getInt(CodeUtils.RESULT_TYPE);
                    if (resultType == CodeUtils.RESULT_SUCCESS) {
                        String macStr = bundle.getString(CodeUtils.RESULT_STRING);
                        qrCode = macStr;
                        ToastUtil.showShortToast("解析数据为：" + macStr);
                    } else if (resultType == CodeUtils.RESULT_FAILED) {
                        ToastUtil.showShort("解析二维码失败");
                    }
                }
            }
        }
    }

    private long getStartTime(int times, long interTime) {
        try {
            return DateUtil.localformatter.parse("2019-06-27 12:00:00").getTime() + times * 15L * 60 * 1000 + interTime;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

    private void testJniMethod() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 1000000; i++) {
                    String encData = SecUtil.md5("123421412fafeqwfwqf????#@!$!$1325153251@!#!%@!%#%!%!@%#!%@#");
                    MLog.d("encData = " + encData);
                    String decdata = SecUtil.encodeByAES(encData);
                    MLog.d("decdata = " + decdata);
                    String encDataBase64 = null;//JavaNative.eb("1234567890", 1);
                    try {
                        encDataBase64 = new String(Base64.encode("112431434123414".getBytes()));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    MLog.d("encDataBase64 = " + encDataBase64);
                    byte[] decbase64Array = Base64.decode(("MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAOfMaZEVTalAA3Pk" +
                            "gtWm6y4vLvBWklWi5p9G4+EX71Yf7tbtChhGx13j9jXaQx1njp5uI81gUXq+iNPB" +
                            "divTevxujKUxgEurW1bcpl8tqpVKh7FxfEPw/v2toya00sXKeVYiwsTMOiBtYamF" +
                            "wU7acfGWcRVYL9U6xLzR1fU3A8KjAgMBAAECgYEAyqL8NoT1G1yGqC2/4p7FQd6l" +
                            "nh1QV+TMz7jdRc3ywMuxs9oM1SQq7X13o6Rmv7HkD8QhME1fbXpbiyK4958tBw5w" +
                            "cNuG5mPZyTaAOFW1G73rxxaSzmJD89nyMebLhzvv6uzkYyeeFR7l6kSgeQaT1wMV" +
                            "eSYBv0Kzz302FJgpvSECQQD4SHZ8EEb+gyXQZES4iFbnA99ijb9EohpMLG7EUcF5" +
                            "jGO7HoCVO/PEq+U8fzvJ+kaKD33URDGDtzEmYuIQquBHAkEA7wDJAm/idArN3gyY" +
                            "gnqMqRSEyAUIzmIdmoYykGkTS98HVLeEDvN3CySTiL/3iyoE654As8pM0BRqwqRh" +
                            "GnN0xQJADVfHlk9BoXfm1lYtI0WgdfXNUXyQpN2ZUDKrGT3TifazfWDcRCjeptke" +
                            "Dqw/yFD87Xp7pqarioqTx55Uy9SDDQJARUsr/EnmFRDg2SU7vnC7gXYSfU+AWx31" +
                            "SUzpMhdU4eV64aQHZLJP7GohYD+QDMgU/x8jQv0/QgD5Zw9zm9CnpQJBAIbClzPu" +
                            "6v8qQGCsHLh4kjYOaqTJVZfw5v8dFKPonGz35nCtMnGV6wxu3SygezN3O5mv0BG/" +
                            "Dx3b3is09y7XD+Y=").getBytes());

                    MLog.d("decbase64Array.length = " + decbase64Array.length);
                    byte[] encodePriv = Base64.encode(decbase64Array);
                    MLog.d("decbase64Array.length2 = " + encodePriv.length);
                    MLog.d("decbase64Array.encodePriv = " + new String(encodePriv));

                    String decbyBase64 = new String(decbase64Array);
                    MLog.d("decbyBase64 = " + decbyBase64);
                    if (null == decbyBase64) {
                        return;
                    }
                    String encAes = SecUtil.encodeByAES(decbyBase64);
                    MLog.d("SecUtil.getRSAsinatureWithLogin(encAes) = " + SecUtil.getRSAsinatureWithLogin(encAes));
                    SecUtil.decDataByRSA(SecUtil.encDataByRSA("123414!@#$!$#@!%!@%@!%#@!%!#"));

                    MLog.d("SecUtil.decDataByRSA(SecUtil.encDataByRSA(\"1234154321535125\")); = " + SecUtil.decDataByRSA(SecUtil.encDataByRSA("1234154321535125")));

                    try {
                        byte[] db = Base64Util.decode(Base64.encode("动工的功能13142341414！#@@%%！%#@！%#@！%@#".getBytes()));
                        MLog.d("db.getBytes().length = " + db.length);
                        MLog.d("db = " + new String(db));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
        }).start();
    }

    private void testLoginByLocalJson() {
        String loginResponseJson = "{\"status\":\"success\",\"message\":\"成功\",\"data\":{\"id\":\"bf46fb55-e2cc-4251-b0a0-4c1ad23d961a\",\"guardian_id\":null,\"login_name\":\"13987654321_P\",\"password\":null,\"nickname\":\"13987654321\",\"name\":\"某某\",\"age\":30,\"sex\":1,\"born_date\":\"1989-12-06T16:00:00.000+0000\",\"credentials_type\":0,\"credentials_card\":\"420607198912072819\",\"phone\":\"13987654321\",\"diopter_state\":0,\"left_eye_degree\":200.0,\"right_eye_degree\":300.0,\"integererpupillary\":66.0,\"left_astigmatism_degree\":30.0,\"right_astigmatism_degree\":60.0,\"astigmatism_degree\":0.0,\"left_handle_degree\":115.0,\"right_handle_degree\":180.0,\"right_column_mirror\":null,\"left_column_mirror\":null,\"right_axial\":30.0,\"left_axial\":60.0,\"naked_left_eye_degree\":4.3,\"naked_right_eye_degree\":4.6,\"naked_binoculus_degree\":5.1,\"correct_left_eye_degree\":5.2,\"correct_right_eye_degree\":5.3,\"correct_binoculus_degree\":5.1,\"isnewuser\":null,\"total_money\":null,\"used_money\":null,\"total_time\":null,\"used_time\":null,\"total_score\":null,\"used_score\":null,\"status\":null,\"expiration_time\":null,\"wearingHours\":0,\"face\":null},\"cursor\":null}";
        MemberLoginResponseBean memberLoginResponseBean = (MemberLoginResponseBean) JsonUtil.json2objectWithDataCheck(loginResponseJson, new TypeReference<MemberLoginResponseBean>(){});
        jumpNewActivity(memberLoginResponseBean);
    }

    private void jumpNewActivity(MemberLoginResponseBean memberLoginResponseBeanParam) {
        if (null != memberLoginResponseBeanParam) {
            if (memberLoginResponseBeanParam.getStatus().equals(IBaseRequest.SUCCESS)) {
                MemberLoginResponseBean.DataBean mDataBean = memberLoginResponseBeanParam.getData();
                if (null != mDataBean) {
                    Config.getConfig().saveUserServerId(mDataBean.getId());
                    insertUserInfo2DB(memberLoginResponseBeanParam);
                }
            } else {
                ToastUtil.showShort(memberLoginResponseBeanParam.getMessage());
            }
        }
    }

    private void insertUserInfo2DB(MemberLoginResponseBean memberLoginResponseBean) {
        MemberLoginResponseBean.DataBean dataBean = memberLoginResponseBean.getData();
        String serverId = dataBean.getId();
        if (com.android.common.baselibrary.util.CommonUtils.isEmpty(serverId)) {
            throw new NullPointerException("serverId can not be null");
        }
        List<UserInfoDBBean> userInfoDBBeanList = UserInfoBeanDaoOpe.queryUserInfoByServerID(MyApplication.getInstance(), serverId);
        UserInfoDBBean userInfoDBBean = null;
        boolean execUpdate = false;
        if (null != userInfoDBBeanList && userInfoDBBeanList.size() > 0) {
            userInfoDBBean = userInfoDBBeanList.get(0);
        }

        if (null != userInfoDBBean && !com.android.common.baselibrary.util.CommonUtils.isEmpty(userInfoDBBean.getServerId())) {
            /**
             * 本地保存的有用户的视力信息
             */
            execUpdate = true;
        } else {
            /**
             * 本地未保存的有用户的视力信息
             */
            execUpdate = false;
            userInfoDBBean = new UserInfoDBBean();
            userInfoDBBean.setServerId(dataBean.getId());
        }

        userInfoDBBean.setGuardian_id(dataBean.getGuardian_id());
        userInfoDBBean.setLogin_name(dataBean.getLogin_name());
        userInfoDBBean.setNickname(dataBean.getNickname());
        userInfoDBBean.setName(dataBean.getName());
        userInfoDBBean.setDiopter_state(dataBean.getDiopter_state());

        userInfoDBBean.setLeft_eye_degree(dataBean.getLeft_eye_degree());
        userInfoDBBean.setRight_eye_degree(dataBean.getRight_eye_degree());
        userInfoDBBean.setLeft_front_com_degree(dataBean.getLeft_front_com_degree());
        userInfoDBBean.setRight_front_com_degree(dataBean.getRight_front_com_degree());
        userInfoDBBean.setInterpupillary(dataBean.getInterpupillary());



        userInfoDBBean.setCorrect_left_eye_degree(dataBean.getCorrect_left_eye_degree());
        userInfoDBBean.setCorrect_right_eye_degree(dataBean.getCorrect_right_eye_degree());
        userInfoDBBean.setCorrect_binoculus_degree(dataBean.getCorrect_binoculus_degree());


        userInfoDBBean.setNaked_left_eye_degree(dataBean.getNaked_left_eye_degree());
        userInfoDBBean.setNaked_right_eye_degree(dataBean.getNaked_right_eye_degree());
        userInfoDBBean.setNaked_binoculus_degree(dataBean.getNaked_binoculus_degree());

        userInfoDBBean.setAge(dataBean.getAge());
        userInfoDBBean.setBorn_date(dataBean.getBorn_date());
        userInfoDBBean.setCredentials_card(dataBean.getCredentials_card());
        userInfoDBBean.setCredentials_type(dataBean.getCredentials_type());
        userInfoDBBean.setExpiration_time(dataBean.getExpiration_time());

        userInfoDBBean.setPortrait_image_url(dataBean.getFace());

        if (!com.android.common.baselibrary.util.CommonUtils.isEmpty(dataBean.getPhone())) {
            userInfoDBBean.setPhone(dataBean.getPhone());
        }

        userInfoDBBean.setSex(dataBean.getSex());
        userInfoDBBean.setStatus(dataBean.getStatus());


        userInfoDBBean.setLeft_astigmatism_degree(dataBean.getLeft_astigmatism_degree());
        userInfoDBBean.setRight_astigmatism_degree(dataBean.getRight_astigmatism_degree());
        userInfoDBBean.setLeft_eye_train_degree(dataBean.getLeft_handle_degree());
        userInfoDBBean.setRight_eye_train_degree(dataBean.getRight_handle_degree());

        /**
         *      * isnewuser	bool	是否新用户
         *      * total_money	float	总充值金额
         *      * used_money	float	已使用金额
         *      * total_time	float	总累计小时数
         *      * used_time	float	已使用小时数
         *      * total_score	float	累计总积分
         *      * used_score	float	已使用积分
         *      * status	int	状态0审核中 1审核通过 2审核不通过
         */

        userInfoDBBean.setIsnewuser(dataBean.isIsnewuser());
        userInfoDBBean.setTotal_money(dataBean.getTotal_money());
        userInfoDBBean.setUsed_money(dataBean.getUsed_money());
        userInfoDBBean.setTotal_time(dataBean.getTotal_time());
        userInfoDBBean.setUsed_time(dataBean.getUsed_time());
        userInfoDBBean.setTotal_score(dataBean.getTotal_score());
        userInfoDBBean.setUsed_score(dataBean.getUsed_score());

        userInfoDBBean.setLeft_axial(dataBean.getLeft_axial());
        userInfoDBBean.setRight_axial(dataBean.getRight_axial());



        if (execUpdate) {
            UserInfoBeanDaoOpe.insertOrReplaceData(MyApplication.getInstance(), userInfoDBBean);
        } else {
            userInfoDBBean.setLocalid(UUID.randomUUID().toString());
            UserInfoBeanDaoOpe.insertData(MyApplication.getInstance(), userInfoDBBean);
        }

    }

}
