package com.intelligence.glasses.activity.bindglasses;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.AppCompatTextView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.intelligence.glasses.base.BaseBleActivity;
import com.intelligence.glasses.bean.BindPersonalInfoBean;

import com.pnikosis.materialishprogress.BuildConfig;
import com.shitec.bleglasses.R;
import com.android.common.baselibrary.log.MLog;
import com.android.common.baselibrary.util.CommonUtils;
import com.android.common.baselibrary.util.ToastUtil;
import com.intelligence.glasses.adapter.SexAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import butterknife.BindView;

/**
 * 输入视力信息
 */

public class BindPersonalInfoActivity extends BaseBleActivity {

    @BindView(R.id.function_item_title_tv)
    AppCompatTextView titleTextView;

    @BindView(R.id.function_item_backlayout)
    LinearLayout backLayout;

    @BindView(R.id.usernametextview)
    AppCompatEditText userNameEditTextView;

    @BindView(R.id.sexspinner)
    AppCompatSpinner sexSpinner;

    @BindView(R.id.birthdaydatetextview)
    AppCompatEditText birthdayDateeEditText;

    @BindView(R.id.eyestatusradiogroup)
    RadioGroup eyeStatusRadioGroup;

    @BindView(R.id.shortsightednesssradiobutton)
    RadioButton shortSightedNessRadioButton;

    @BindView(R.id.longsightednesssradiobutton)
    RadioButton longSightedNessRadioButton;

    @BindView(R.id.presbyopiasightednesssradiobutton)
    RadioButton presbyopiaSightedNessRadioButton;//老花

    @BindView(R.id.weaksightednesssradiobutton)
    RadioButton weakSightedNessRadioButton;

    @BindView(R.id.lefteyerefraction)
    AppCompatEditText leftEyeRefractionEditText;

    @BindView(R.id.righteyerefraction)
    AppCompatEditText rightEyeRefractionEditText;

    @BindView(R.id.lefteyeastigmatism)
    AppCompatEditText leftEyeAstigmation;

    @BindView(R.id.righteyeastigmatism)
    AppCompatEditText rightEyeAstigmation;

    @BindView(R.id.lefteyeaxialeidttext)
    AppCompatEditText leftEyeAxialeEidtText;

    @BindView(R.id.righteyeaxialeidttext)
    AppCompatEditText rightEyeAxialeEidtText;

    @BindView(R.id.interpupillarydistanceedittext)
    AppCompatEditText interpuillaryDistanceEditText;//瞳距

    @BindView(R.id.register_next_page_button)
    AppCompatButton registeNextPageButton;

    private List<EditText> editTextList = new ArrayList<>();
    private int sexSelectedSpinnerIndex = -1;
    private Pattern pattern = Pattern.compile("[^0-9\\-]");
    private final String TAG = BindPersonalInfoActivity.class.getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle bundle) {
        setContentView(R.layout.activity_bingglasseswithidno);
        super.onCreate(bundle);
        initData();
        initView();
        initListener();

    }

    private void initData() {
        editTextList.clear();
        editTextList.add(userNameEditTextView);
        editTextList.add(birthdayDateeEditText);
        editTextList.add(leftEyeRefractionEditText);
        editTextList.add(rightEyeRefractionEditText);
        editTextList.add(leftEyeAstigmation);
        editTextList.add(rightEyeAstigmation);
        editTextList.add(leftEyeAxialeEidtText);
        editTextList.add(rightEyeAxialeEidtText);
        editTextList.add(interpuillaryDistanceEditText);
    }

    private void initView() {
        //StatusBarCompat.setImmersiveStatusBar(true, mResource.getColor(R.color.main_background_color), this);
        titleTextView.setText("验光数据");
        eyeStatusRadioGroup.check(shortSightedNessRadioButton.getId());
        judgeEyeStatus(eyeStatusRadioGroup.getCheckedRadioButtonId());

        SexAdapter sexAdapter = new SexAdapter(this, Arrays.asList(getResources().getStringArray(R.array.sex)));
        sexSpinner.setAdapter(sexAdapter);
        sexSpinner.setSelection(0);
        sexSelectedSpinnerIndex = 0;
        sexSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                //ToastUtil.showShortToast("position = " + position);
                sexSelectedSpinnerIndex = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void initListener() {
        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        registeNextPageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean allInput = allEditTextInput();
                if (!allInput) {
                    ToastUtil.showShort(R.string.person_info_no_complete_text);
                    return;
                }
                if (inputAllRight()) {
                    putIntentData();
                } else {
                    ToastUtil.showShort(R.string.person_info_no_right_text);
                }
            }
        });

        eyeStatusRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                judgeEyeStatus(i);
            }
        });

        for (final EditText editText: editTextList) {
            editText.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    changeEditTextColor(editText, true);
                }
            });
        }
    }

    private int judgeEyeStatus(int viewID) {
        int i = -1;
        if (viewID == shortSightedNessRadioButton.getId()) {
            if (BuildConfig.DEBUG) {
                ToastUtil.showShort("近视");
            }
            i = 1;
        } else if (viewID == presbyopiaSightedNessRadioButton.getId()) {
            if (BuildConfig.DEBUG) {
                ToastUtil.showShort("老花");
            }
            i = 2;
        } else if (viewID == longSightedNessRadioButton.getId()) {
            if (BuildConfig.DEBUG) {
                ToastUtil.showShort("远视");
            }
            i = 3;
        } else if (viewID == weakSightedNessRadioButton.getId()) {
            if (BuildConfig.DEBUG) {
                ToastUtil.showShort("弱视");
            }
            i = 4;
        }
        return i;
    }

    private String getUserName() {
        return userNameEditTextView.getText().toString();
    }

    /**
     * 获取性别 索引
     * @return
     */
    private String getGender() {
        return String.valueOf(sexSpinner.getSelectedItemPosition());
    }

    private String getBirthDayDate() {
        return birthdayDateeEditText.getText().toString();
    }

    /**
     * 获取视力状态 近视，远视，老花，弱视
     * @return
     */
    private String  getEyeSightStatus() {
        int eyeSightStatus = judgeEyeStatus(eyeStatusRadioGroup.getCheckedRadioButtonId());
        MLog.d("getEyeSightStatus = " + eyeSightStatus);
        return String.valueOf(eyeSightStatus);
    }

    /**
     * 左眼度数
     * @return
     */
    private String getLeftEyeSight() {
        return leftEyeRefractionEditText.getText().toString();
    }

    /**
     * 右眼度数
     * @return
     */
    private String getRightEyeSight() {
        return rightEyeRefractionEditText.getText().toString();
    }

    /**
     * 左眼散光度数
     * @return
     */
    private String getLeftEyeAstigmatism() {
        return  leftEyeAstigmation.getText().toString();
    }

    /**
     * 右眼散光度数
     * @return
     */
    private String getRightEyeAstigmatism() {
        return rightEyeAstigmation.getText().toString();
    }

    /**
     * 左眼轴向
     * @return
     */
    private String getLeftEyeAxialDirection() {
        return leftEyeAxialeEidtText.getText().toString();
    }

    /**
     * 右眼轴向
     * @return
     */
    private String getRightEyeAxialDirection() {
        return rightEyeAxialeEidtText.getText().toString();
    }

    private String getInterpuillaryDistanceText() {
        return interpuillaryDistanceEditText.getText().toString();
    }

    private boolean verifyUserNameText() {
        String userName = getUserName();
        if (!CommonUtils.isEmpty(userName)) {
            return true;
        }
        return false;
    }

    private boolean verifyGender() {
        String gender = getGender();
        if (!CommonUtils.isEmpty(gender) &&
                (gender.equals("0")
                || gender.equals("1"))) {
            return true;
        }
        return false;
    }

    private boolean verifyIDNo() {
        String birdthDayDate = getBirthDayDate();

        if (!CommonUtils.isEmpty(birdthDayDate)) {
            boolean match = isIDNumber(birdthDayDate);
            return match;
        }
        return false;
    }

    public static boolean isIDNumber(String IDNumber) {
        if (IDNumber == null || "".equals(IDNumber)) {
            return false;
        }
        // 定义判别用户身份证号的正则表达式（15位或者18位，最后一位可以为字母）
        String regularExpression = "(^[1-9]\\d{5}(18|19|20)\\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\\d{3}[0-9Xx]$)|" +
                "(^[1-9]\\d{5}\\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\\d{3}$)";
        //假设18位身份证号码:41000119910101123X  410001 19910101 123X
        //^开头
        //[1-9] 第一位1-9中的一个      4
        //\\d{5} 五位数字           10001（前六位省市县地区）
        //(18|19|20)                19（现阶段可能取值范围18xx-20xx年）
        //\\d{2}                    91（年份）
        //((0[1-9])|(10|11|12))     01（月份）
        //(([0-2][1-9])|10|20|30|31)01（日期）
        //\\d{3} 三位数字            123（第十七位奇数代表男，偶数代表女）
        //[0-9Xx] 0123456789Xx其中的一个 X（第十八位为校验值）
        //$结尾

        //假设15位身份证号码:410001910101123  410001 910101 123
        //^开头
        //[1-9] 第一位1-9中的一个      4
        //\\d{5} 五位数字           10001（前六位省市县地区）
        //\\d{2}                    91（年份）
        //((0[1-9])|(10|11|12))     01（月份）
        //(([0-2][1-9])|10|20|30|31)01（日期）
        //\\d{3} 三位数字            123（第十五位奇数代表男，偶数代表女），15位身份证不含X
        //$结尾


        boolean matches = IDNumber.matches(regularExpression);

        //判断第18位校验值
        if (matches) {

            if (IDNumber.length() == 18) {
                try {
                    char[] charArray = IDNumber.toCharArray();
                    //前十七位加权因子
                    int[] idCardWi = {7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2};
                    //这是除以11后，可能产生的11位余数对应的验证码
                    String[] idCardY = {"1", "0", "X", "9", "8", "7", "6", "5", "4", "3", "2"};
                    int sum = 0;
                    for (int i = 0; i < idCardWi.length; i++) {
                        int current = Integer.parseInt(String.valueOf(charArray[i]));
                        int count = current * idCardWi[i];
                        sum += count;
                    }
                    char idCardLast = charArray[17];
                    int idCardMod = sum % 11;
                    if (idCardY[idCardMod].toUpperCase().equals(String.valueOf(idCardLast).toUpperCase())) {
                        return true;
                    } else {
                        MLog.d("身份证最后一位:" + String.valueOf(idCardLast).toUpperCase() +
                                "错误,正确的应该是:" + idCardY[idCardMod].toUpperCase());
                        return false;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    MLog.d("异常:" + IDNumber);
                    return false;
                }
            }

        }
        return matches;
    }

    private boolean verifyEyeSightStatus() {
        String eyeSightStatus = getEyeSightStatus();
        if (!CommonUtils.isEmpty(eyeSightStatus)) {
            int eyeStatus = Integer.parseInt(eyeSightStatus);
            if (eyeStatus >=0 && eyeStatus <= 3) {
                return true;
            }
        }
        return false;
    }
    private boolean verifyLeftEyeSight() {
        String leftEyeSight = getLeftEyeSight();
        if (!CommonUtils.isEmpty(leftEyeSight) && Float.parseFloat(leftEyeSight) <= 1000) {
            return true;
        }
        return false;
    }

    private boolean verifyRightEyeSight() {
        String rightEyeSight = getRightEyeSight();
        if (!CommonUtils.isEmpty(rightEyeSight) && Float.parseFloat(rightEyeSight) <= 1000) {
            return true;
        }
        return false;
    }

    private boolean verifyLefyEyeAstigmatism() {
        String leftEyeAstigmatism = getLeftEyeAstigmatism();
        if (!CommonUtils.isEmpty(leftEyeAstigmatism)) {
            return true;
        }
        return false;
    }

    private boolean verifyRightEyeAstigmatism() {
        String rightEyeAstigmatism = getRightEyeAstigmatism();
        if (!CommonUtils.isEmpty(rightEyeAstigmatism)) {
            return true;
        }
        return false;
    }

    private boolean verifyLeftEyeAxialDirection() {
        String leftEyeAxialDirection = getLeftEyeAxialDirection();
        float defualtValue = -1;
        if (!CommonUtils.isEmpty(leftEyeAxialDirection) && (defualtValue = Float.parseFloat(leftEyeAxialDirection)) >= 0 && defualtValue <=360 ) {
            return true;
        }
        return false;
    }

    private boolean verifyRightEyeAxialDirection() {
        String rightEyeAxialDirection = getRightEyeAxialDirection();
        float defualtValue = -1;
        if (!CommonUtils.isEmpty(rightEyeAxialDirection) && (defualtValue = Float.parseFloat(rightEyeAxialDirection)) >= 0 && defualtValue <=360 ) {
            return true;
        }
        return false;
    }

    private boolean verifyInterpuillaryDistance() {
        String interpuillaryDistance = getInterpuillaryDistanceText();
        if (!CommonUtils.isEmpty(interpuillaryDistance)) {
            return true;
        }
        return false;
    }

    private boolean allEditTextInput() {
        for (EditText editText: editTextList) {
            String content = editText.getText().toString();
            if (CommonUtils.isEmpty(content)) {
                return false;
            }
        }
        return true;
    }

    private boolean inputAllRight() {
        boolean verifyUserName = verifyUserNameText();
        changeEditTextColor(userNameEditTextView, verifyUserName);

        //boolean verifyGender = verifyGender();

        boolean verifyBirthDayDate = verifyIDNo();
        changeEditTextColor(birthdayDateeEditText, verifyBirthDayDate);

        boolean verifyEyeStatus = verifyEyeSightStatus();

        boolean verifyLeftEyeSight = verifyLeftEyeSight();
        changeEditTextColor(leftEyeRefractionEditText, verifyLeftEyeSight);

        boolean verifyRightEyeSight = verifyRightEyeSight();
        changeEditTextColor(rightEyeRefractionEditText, verifyRightEyeSight);

        boolean verifyLefyEyeAstigmatism = verifyLefyEyeAstigmatism();
        changeEditTextColor(leftEyeAstigmation, verifyLefyEyeAstigmatism);

        boolean verifyRightEyeAstigmatism = verifyRightEyeAstigmatism();
        changeEditTextColor(rightEyeAstigmation, verifyRightEyeAstigmatism);

        boolean verifyLeftEyeAxial = verifyLeftEyeAxialDirection();
        changeEditTextColor(leftEyeAxialeEidtText, verifyLeftEyeAxial);

        boolean verifyRightEyeAxial = verifyRightEyeAxialDirection();
        changeEditTextColor(rightEyeAxialeEidtText, verifyRightEyeAxial);

        boolean verifyInterpuillaryDistance = verifyInterpuillaryDistance();
        changeEditTextColor(interpuillaryDistanceEditText, verifyInterpuillaryDistance);

        if (BuildConfig.DEBUG) {
            MLog.d(TAG + "inputAllRight verifyUserName = " + verifyUserName +  "\n" +
                    "verifyIDNo = "  + verifyBirthDayDate + "\n" +
                    "verifyEyeStatus = "  + verifyEyeStatus + "\n" +
                    "verifyLeftEyeSight = "  + verifyLeftEyeSight + "\n" +
                    "verifyRightEyeSight = "  + verifyRightEyeSight + "\n" +
                    "verifyLefyEyeAstigmatism = "  + verifyLefyEyeAstigmatism + "\n" +
                    "verifyRightEyeAstigmatism = "  + verifyRightEyeAstigmatism + "\n" +
                    "verifyLeftEyeAxial = "  + verifyLeftEyeAxial + "\n" +
                    "verifyRightEyeAxial = "  + verifyRightEyeAxial + "\n" +
                    "verifyInterpuillaryDistance = " + verifyInterpuillaryDistance + "\n");
        }

        if (verifyUserName && verifyBirthDayDate
            && verifyEyeStatus
            && verifyLeftEyeSight
            && verifyRightEyeSight
            && verifyLefyEyeAstigmatism
            && verifyRightEyeAstigmatism
            && verifyLeftEyeAxial
            && verifyRightEyeAxial
            && verifyInterpuillaryDistance) {
            return true;
        }

        return false;
    }

    private void changeEditTextColor(EditText editText, boolean verify) {
        String content = editText.getText().toString();
        if (!CommonUtils.isEmpty(content) && !verify) {
            editText.setTextColor(getResources().getColor(R.color.train_status_disconnect_text_color));
        } else {
            editText.setTextColor(getResources().getColor(R.color.bleglasses_yanguang_input_text_color));
        }
    }

    private void putIntentData() {
        Intent mIntent = new Intent(BindPersonalInfoActivity.this, BindPersonalInfoSecondActivity.class);
        BindPersonalInfoBean bindPersonalInfoBean = new BindPersonalInfoBean();
        bindPersonalInfoBean.setUserName(getUserName());
        //bindPersonalInfoBean.setGender(Integer.parseInt(getGender()));
        bindPersonalInfoBean.setIdNo(getBirthDayDate());
        bindPersonalInfoBean.setEyeStatus(Integer.parseInt(getEyeSightStatus()));

        bindPersonalInfoBean.setLeftEyeSight(Float.parseFloat(getLeftEyeSight()));
        bindPersonalInfoBean.setRightEyeSight(Float.parseFloat(getRightEyeSight()));

        bindPersonalInfoBean.setLeftEyeAstigmatism(Float.parseFloat(getLeftEyeAstigmatism()));
        bindPersonalInfoBean.setRightEyeAstigmatism(Float.parseFloat(getRightEyeAstigmatism()));

        bindPersonalInfoBean.setLeftEyeAxialDirection(Float.parseFloat(getLeftEyeAxialDirection()));
        bindPersonalInfoBean.setRightEyeAxialDirection(Float.parseFloat(getRightEyeAxialDirection()));
        bindPersonalInfoBean.setInterpuillaryDistance(Float.parseFloat(getInterpuillaryDistanceText()));

        mIntent.putExtra(BindPersonalInfoSecondActivity.BIND_PERSONAL_EYE_INFO_KEY, bindPersonalInfoBean);
        startActivity(mIntent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        editTextList.clear();
    }
}
