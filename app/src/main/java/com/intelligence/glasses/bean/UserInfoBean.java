package com.intelligence.glasses.bean;

import androidx.annotation.Keep;

/**
 * Created by Administrator on 2018/8/11.
 */

@Keep
public class UserInfoBean extends BaseBean {

    private Long id;
    private String userID;
    private String userName;
    private String password;
    private String phoneNumber;
    private String idNumber;//身份证号
    private String glassMac;//眼镜mac地址
    private String userStatus;
    private String registerType;
    private String loginType;
    private String authToken;
    private String expirationTime;//截止日期
    private String gender;//性别
    private String authType;//本人/监护人
    private String glassId;//眼镜的mac地址
    private String dioptricStatus;//屈光状态
    private String leftEyeDegree;//左眼屈光度
    private String rightEyeDegree;//右眼屈光度
    private String pupillaryDistance;//瞳距
    private String comEyeGlassDegree;//补偿镜片度数

    private String face;//头像链接地址


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getGlassMac() {
        return glassMac;
    }

    public void setGlassMac(String glassMac) {
        this.glassMac = glassMac;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public String getRegisterType() {
        return registerType;
    }

    public void setRegisterType(String registerType) {
        this.registerType = registerType;
    }

    public String getLoginType() {
        return loginType;
    }

    public void setLoginType(String loginType) {
        this.loginType = loginType;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getExpirationTime() {
        return expirationTime;
    }

    public void setExpirationTime(String expirationTime) {
        this.expirationTime = expirationTime;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAuthType() {
        return authType;
    }

    public void setAuthType(String authType) {
        this.authType = authType;
    }

    public String getGlassId() {
        return glassId;
    }

    public void setGlassId(String glassId) {
        this.glassId = glassId;
    }

    public String getDioptricStatus() {
        return dioptricStatus;
    }

    public void setDioptricStatus(String dioptricStatus) {
        this.dioptricStatus = dioptricStatus;
    }

    public String getLeftEyeDegree() {
        return leftEyeDegree;
    }

    public void setLeftEyeDegree(String leftEyeDegree) {
        this.leftEyeDegree = leftEyeDegree;
    }

    public String getRightEyeDegree() {
        return rightEyeDegree;
    }

    public void setRightEyeDegree(String rightEyeDegree) {
        this.rightEyeDegree = rightEyeDegree;
    }

    public String getPupillaryDistance() {
        return pupillaryDistance;
    }

    public void setPupillaryDistance(String pupillaryDistance) {
        this.pupillaryDistance = pupillaryDistance;
    }

    public String getComEyeGlassDegree() {
        return comEyeGlassDegree;
    }

    public void setComEyeGlassDegree(String comEyeGlassDegree) {
        this.comEyeGlassDegree = comEyeGlassDegree;
    }

    public String getFace() {
        return face;
    }

    public void setFace(String face) {
        this.face = face;
    }
}