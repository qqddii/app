package com.intelligence.glasses.bean;

public class SystemSettingBean {
    private int itemPrefixImageResourceId;
    private String itemTilte;
    private int itemSufixImageResource;

    public int getItemPrefixImageResourceId() {
        return itemPrefixImageResourceId;
    }

    public void setItemPrefixImageResourceId(int itemPrefixImageResourceId) {
        this.itemPrefixImageResourceId = itemPrefixImageResourceId;
    }

    public String getItemTilte() {
        return itemTilte;
    }

    public void setItemTilte(String itemTilte) {
        this.itemTilte = itemTilte;
    }

    public int getItemSufixImageResource() {
        return itemSufixImageResource;
    }

    public void setItemSufixImageResource(int itemSufixImageResource) {
        this.itemSufixImageResource = itemSufixImageResource;
    }

    @Override
    public String toString() {
        return "SystemSettingBean{" +
                "itemPrefixImageResourceId=" + itemPrefixImageResourceId +
                ", itemTilte='" + itemTilte + '\'' +
                ", itemSufixImageResource=" + itemSufixImageResource +
                '}';
    }
}
