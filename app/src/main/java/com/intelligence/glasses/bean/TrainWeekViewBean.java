package com.intelligence.glasses.bean;

public class TrainWeekViewBean {
    private float trainTime;
    private String dateStr;

    public float getTrainTime() {
        return trainTime;
    }

    public void setTrainTime(float trainTime) {
        this.trainTime = trainTime;
    }

    public String getDateStr() {
        return dateStr;
    }

    public void setDateStr(String dateStr) {
        this.dateStr = dateStr;
    }
}
