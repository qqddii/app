package com.intelligence.glasses.bean.bledata.receive;

import com.android.common.baselibrary.blebean.BaseParseCmdBean;
import com.android.common.baselibrary.util.comutil.CommonUtils;

/**
 *  接收眼镜上传的训练数据 参数5
 */
public class ReceiveGlassesRunParam5BleDataBean extends BaseParseCmdBean {

    /**
     * 定义	序号	内容
     * 校验码	TxByte1	0xa5
     * 校验码	TxByte2	0x51
     * 命令	TxByte3	0x1f
     * 数据1	TxByte4	0
     * 数据2	TxByte5	0
     * 数据3	TxByte6	0
     * 数据4	TxByte7	0
     * 数据5	TxByte8	0
     * 数据6	TxByte9	0
     * 数据7	TxByte10	0
     * 数据8	TxByte11	0
     * 数据9	TxByte12	0
     * 数据10	TxByte13	0
     * 数据11	TxByte14	0
     * 数据12	TxByte15	0
     * 数据13	TxByte16	0
     * 数据14	TxByte17	0
     * 数据15	TxByte18	0
     * 数据16	TxByte19	MonitorData.CMD
     * 校验码	TxByte20	0xaa
     */

    private final int RXBYTE4_INDEX = 3;
    private final int RXBYTE5_INDEX = 4;
    private final int RXBYTE6_INDEX = 5;
    private final int RXBYTE7_INDEX = 6;
    private final int RXBYTE8_INDEX = 7;
    private final int RXBYTE9_INDEX = 8;
    private final int RXBYTE10_INDEX = 9;
    private final int RXBYTE11_INDEX = 10;
    private final int RXBYTE12_INDEX = 11;
    private final int RXBYTE13_INDEX = 12;
    private final int RXBYTE14_INDEX = 13;
    private final int RXBYTE15_INDEX = 14;
    private final int RXBYTE16_INDEX = 15;
    private final int RXBYTE17_INDEX = 16;
    private final int RXBYTE18_INDEX = 17;

    private int txByte4;
    private int txByte5;
    private int txByte6;
    private int txByte7;
    private int txByte8;
    private int txByte9;
    private int txByte10;
    private int txByte11;
    private int txByte12;
    private int txByte13;
    private int txByte14;
    private int txByte15;
    private int txByte16;
    private int txByte17;
    private int txByte18;


    public static final String USER_MONITOR_CMD = USER_DATA_PREFIX + "1f";


    private final int MONITOR_DATA_CMD_INDEX = 18;//一个字节


    private String monitorData_CMD;



    @Override
    protected BaseParseCmdBean parseBleDataStr2Bean(String bleData) {
        if (!CommonUtils.isEmpty(bleData)
                && bleData.toLowerCase().startsWith(USER_MONITOR_CMD)
                && bleData.toLowerCase().endsWith(USER_DATA_SUFIX)) {

            String rxByte4 = bleData.substring( 2 * RXBYTE4_INDEX, 2 * (RXBYTE4_INDEX + 1));
            setTxByte4(Integer.parseInt(rxByte4, 16));

            String rxByte5 = bleData.substring( 2 * RXBYTE5_INDEX, 2 * (RXBYTE5_INDEX + 1));
            setTxByte5(Integer.parseInt(rxByte5, 16));

            String rxByte6 = bleData.substring( 2 * RXBYTE6_INDEX, 2 * (RXBYTE6_INDEX + 1));
            setTxByte6(Integer.parseInt(rxByte6, 16));

            String rxByte7 = bleData.substring( 2 * RXBYTE7_INDEX, 2 * (RXBYTE7_INDEX + 1));
            setTxByte7(Integer.parseInt(rxByte7, 16));

            String rxByte8 = bleData.substring( 2 * RXBYTE8_INDEX, 2 * (RXBYTE8_INDEX + 1));
            setTxByte8(Integer.parseInt(rxByte8, 16));

            String rxByte9 = bleData.substring( 2 * RXBYTE9_INDEX, 2 * (RXBYTE9_INDEX + 1));
            setTxByte9(Integer.parseInt(rxByte9, 16));

            String rxByte10 = bleData.substring( 2 * RXBYTE10_INDEX, 2 * (RXBYTE10_INDEX + 1));
            setTxByte10(Integer.parseInt(rxByte10, 16));

            String rxByte11 = bleData.substring( 2 * RXBYTE11_INDEX, 2 * (RXBYTE11_INDEX + 1));
            setTxByte11(Integer.parseInt(rxByte11, 16));

            String rxByte12 = bleData.substring( 2 * RXBYTE12_INDEX, 2 * (RXBYTE12_INDEX + 1));
            setTxByte12(Integer.parseInt(rxByte12, 16));

            String rxByte13 = bleData.substring( 2 * RXBYTE13_INDEX, 2 * (RXBYTE13_INDEX + 1));
            setTxByte13(Integer.parseInt(rxByte13, 16));

            String rxByte14 = bleData.substring( 2 * RXBYTE14_INDEX, 2 * (RXBYTE14_INDEX + 1));
            setTxByte14(Integer.parseInt(rxByte14, 16));

            String rxByte15 = bleData.substring( 2 * RXBYTE15_INDEX, 2 * (RXBYTE15_INDEX + 1));
            setTxByte15(Integer.parseInt(rxByte15, 16));

            String rxByte16 = bleData.substring( 2 * RXBYTE16_INDEX, 2 * (RXBYTE16_INDEX + 1));
            setTxByte16(Integer.parseInt(rxByte16, 16));

            String rxByte17 = bleData.substring( 2 * RXBYTE17_INDEX, 2 * (RXBYTE17_INDEX + 1));
            setTxByte17(Integer.parseInt(rxByte17, 16));

            String rxByte18 = bleData.substring( 2 * RXBYTE18_INDEX, 2 * (RXBYTE18_INDEX + 1));
            setTxByte18(Integer.parseInt(rxByte18, 16));


            String monitorData_CMDHexStr = bleData.substring( 2 * MONITOR_DATA_CMD_INDEX, 2 * (MONITOR_DATA_CMD_INDEX + 1));
            setMonitorData_CMD(monitorData_CMDHexStr);

            setParseSuccess(true);
        } else {
            setParseSuccess(false);
        }
        return this;
    }

    public String getMonitorData_CMD() {
        return monitorData_CMD;
    }

    public void setMonitorData_CMD(String monitorData_CMD) {
        this.monitorData_CMD = monitorData_CMD;
    }


    public int getTxByte4() {
        return txByte4;
    }

    public void setTxByte4(int txByte4) {
        this.txByte4 = txByte4;
    }

    public int getTxByte5() {
        return txByte5;
    }

    public void setTxByte5(int txByte5) {
        this.txByte5 = txByte5;
    }

    public int getTxByte6() {
        return txByte6;
    }

    public void setTxByte6(int txByte6) {
        this.txByte6 = txByte6;
    }

    public int getTxByte7() {
        return txByte7;
    }

    public void setTxByte7(int txByte7) {
        this.txByte7 = txByte7;
    }

    public int getTxByte8() {
        return txByte8;
    }

    public void setTxByte8(int txByte8) {
        this.txByte8 = txByte8;
    }

    public int getTxByte9() {
        return txByte9;
    }

    public void setTxByte9(int txByte9) {
        this.txByte9 = txByte9;
    }

    public int getTxByte10() {
        return txByte10;
    }

    public void setTxByte10(int txByte10) {
        this.txByte10 = txByte10;
    }

    public int getTxByte11() {
        return txByte11;
    }

    public void setTxByte11(int txByte11) {
        this.txByte11 = txByte11;
    }

    public int getTxByte12() {
        return txByte12;
    }

    public void setTxByte12(int txByte12) {
        this.txByte12 = txByte12;
    }

    public int getTxByte13() {
        return txByte13;
    }

    public void setTxByte13(int txByte13) {
        this.txByte13 = txByte13;
    }

    public int getTxByte14() {
        return txByte14;
    }

    public void setTxByte14(int txByte14) {
        this.txByte14 = txByte14;
    }

    public int getTxByte15() {
        return txByte15;
    }

    public void setTxByte15(int txByte15) {
        this.txByte15 = txByte15;
    }

    public int getTxByte16() {
        return txByte16;
    }

    public void setTxByte16(int txByte16) {
        this.txByte16 = txByte16;
    }

    public int getTxByte17() {
        return txByte17;
    }

    public void setTxByte17(int txByte17) {
        this.txByte17 = txByte17;
    }

    public int getTxByte18() {
        return txByte18;
    }

    public void setTxByte18(int txByte18) {
        this.txByte18 = txByte18;
    }
}
