package com.intelligence.glasses.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 注册时，填写个人视力信息
 */
public class BindPersonalInfoBean implements Parcelable {
    private String userName;
    //private int gender;
    private String idNo;
    private int eyeStatus;//近视，远视，老花，弱视
    private float leftEyeSight;//左眼度数
    private float rightEyeSight;//右眼度数
    private float leftEyeAstigmatism;//左眼散光
    private float rightEyeAstigmatism;//右眼散光
    private float leftEyeAxialDirection;//左眼轴向
    private float rightEyeAxialDirection;//右眼轴向
    private float interpuillaryDistance;//瞳距

    public BindPersonalInfoBean() {
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

 /*   public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }*/

    public String getIdNo() {
        return idNo;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }

    public int getEyeStatus() {
        return eyeStatus;
    }

    public void setEyeStatus(int eyeStatus) {
        this.eyeStatus = eyeStatus;
    }

    public float getLeftEyeSight() {
        return leftEyeSight;
    }

    public void setLeftEyeSight(float leftEyeSight) {
        this.leftEyeSight = leftEyeSight;
    }

    public float getRightEyeSight() {
        return rightEyeSight;
    }

    public void setRightEyeSight(float rightEyeSight) {
        this.rightEyeSight = rightEyeSight;
    }

    public float getLeftEyeAstigmatism() {
        return leftEyeAstigmatism;
    }

    public void setLeftEyeAstigmatism(float leftEyeAstigmatism) {
        this.leftEyeAstigmatism = leftEyeAstigmatism;
    }

    public float getRightEyeAstigmatism() {
        return rightEyeAstigmatism;
    }

    public void setRightEyeAstigmatism(float rightEyeAstigmatism) {
        this.rightEyeAstigmatism = rightEyeAstigmatism;
    }

    public float getLeftEyeAxialDirection() {
        return leftEyeAxialDirection;
    }

    public void setLeftEyeAxialDirection(float leftEyeAxialDirection) {
        this.leftEyeAxialDirection = leftEyeAxialDirection;
    }

    public float getRightEyeAxialDirection() {
        return rightEyeAxialDirection;
    }

    public void setRightEyeAxialDirection(float rightEyeAxialDirection) {
        this.rightEyeAxialDirection = rightEyeAxialDirection;
    }

    public float getInterpuillaryDistance() {
        return interpuillaryDistance;
    }

    public void setInterpuillaryDistance(float interpuillaryDistance) {
        this.interpuillaryDistance = interpuillaryDistance;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(userName);
        //parcel.writeInt(gender);
        parcel.writeString(idNo);
        parcel.writeInt(eyeStatus);


        parcel.writeFloat(leftEyeSight);
        parcel.writeFloat(rightEyeSight);

        parcel.writeFloat(leftEyeAstigmatism);
        parcel.writeFloat(rightEyeAstigmatism);

        parcel.writeFloat(leftEyeAxialDirection);
        parcel.writeFloat(rightEyeAxialDirection);
        parcel.writeFloat(interpuillaryDistance);
    }

    public static final Creator<BindPersonalInfoBean> CREATOR = new Creator<BindPersonalInfoBean>() {
        @Override
        public BindPersonalInfoBean createFromParcel(Parcel parcel) {
            return new BindPersonalInfoBean(parcel);
        }

        @Override
        public BindPersonalInfoBean[] newArray(int i) {
            return new BindPersonalInfoBean[i];
        }
    };

    public BindPersonalInfoBean(Parcel in) {
        userName = in.readString();
        //gender = in.readInt();
        idNo = in.readString();
        eyeStatus = in.readInt();

        leftEyeSight = in.readFloat();
        rightEyeSight = in.readFloat();

        leftEyeAstigmatism = in.readFloat();
        rightEyeAstigmatism = in.readFloat();

        leftEyeAxialDirection = in.readFloat();
        rightEyeAxialDirection = in.readFloat();
        interpuillaryDistance = in.readFloat();
    }


    @Override
    public String toString() {
        return "BindPersonalInfoBean{" +
                "userName='" + userName + '\'' +
                //", gender=" + gender +
                ", idNo='" + idNo + '\'' +
                ", eyeStatus=" + eyeStatus +
                ", leftEyeSight=" + leftEyeSight +
                ", rightEyeSight=" + rightEyeSight +
                ", leftEyeAstigmatism=" + leftEyeAstigmatism +
                ", rightEyeAstigmatism=" + rightEyeAstigmatism +
                ", leftEyeAxialDirection=" + leftEyeAxialDirection +
                ", rightEyeAxialDirection=" + rightEyeAxialDirection +
                ", interpuillaryDistance=" + interpuillaryDistance +
                '}';
    }
}
