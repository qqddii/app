package com.intelligence.glasses.bean;

public class CommonProblemBean {
    private String title;
    private String Content;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String content) {
        Content = content;
    }
}
