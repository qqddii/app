package com.intelligence.glasses.bean;

import androidx.annotation.Keep;

/**
 * 绑定设备
 */
@Keep
public class MemberBindDeviceResponseBean extends BaseBean {
    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    @Keep
    public class DataBean {
        private int age;//: 30
        private String left_handle_degree;//: 102.1
        private String memberId;//: "1e3d6471-9b99-4533-b1d0-2c31e3d12f66"
        private String right_handle_degree;//: 102.15

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        public String getLeft_handle_degree() {
            return left_handle_degree;
        }

        public void setLeft_handle_degree(String left_handle_degree) {
            this.left_handle_degree = left_handle_degree;
        }

        public String getMemberId() {
            return memberId;
        }

        public void setMemberId(String memberId) {
            this.memberId = memberId;
        }

        public String getRight_handle_degree() {
            return right_handle_degree;
        }

        public void setRight_handle_degree(String right_handle_degree) {
            this.right_handle_degree = right_handle_degree;
        }

        @Override
        public String toString() {
            return "DataBean{" +
                    "age=" + age +
                    ", left_handle_degree='" + left_handle_degree + '\'' +
                    ", memberId='" + memberId + '\'' +
                    ", right_handle_degree='" + right_handle_degree + '\'' +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "MemberBindDeviceResponseBean{" +
                "data=" + data +
                ", status='" + status + '\'' +
                ", message='" + message + '\'' +
                ", cursor='" + cursor + '\'' +
                '}';
    }
}
