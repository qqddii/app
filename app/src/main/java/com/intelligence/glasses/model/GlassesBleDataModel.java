package com.intelligence.glasses.model;

import android.util.SparseArray;

import com.android.common.baselibrary.blebean.BaseCmdBean;
import com.android.common.baselibrary.blebean.BaseParseCmdBean;
import com.android.common.baselibrary.log.BleDataLog;
import com.android.common.baselibrary.log.MLog;
import com.android.common.baselibrary.util.BleHexUtil;
import com.android.common.baselibrary.util.CommonUtils;
import com.android.common.baselibrary.util.DateUtil;
import com.android.common.baselibrary.util.FileUtils;
import com.android.common.baselibrary.util.ThreadPoolUtilsLocal;
import com.intelligence.glasses.MyApplication;
import com.intelligence.glasses.activity.SendReceiveBleDataActivity;
import com.intelligence.glasses.bean.bledata.BleDataBeanConvertUtil;
import com.intelligence.glasses.bean.bledata.receive.ReceiveGlassesFeedbackBleDataBean;
import com.intelligence.glasses.bean.bledata.receive.ReceiveGlassesRunParam1BleDataBean;
import com.intelligence.glasses.bean.bledata.receive.ReceiveGlassesRunParam2BleDataBean;
import com.intelligence.glasses.bean.bledata.receive.ReceiveGlassesRunParam3BleDataBean;
import com.intelligence.glasses.bean.bledata.receive.ReceiveGlassesRunParam4BleDataBean;
import com.intelligence.glasses.bean.bledata.receive.ReceiveGlassesRunParam5BleDataBean;
import com.intelligence.glasses.bean.bledata.receive.ReceiveGlassesUserIdBleDataBean;
import com.intelligence.glasses.bean.bledata.receive.ReceiveInteveneFeedbackBleDataBean;
import com.intelligence.glasses.bean.bledata.send.SendForGetCommonFeedBackDataBean;
import com.intelligence.glasses.bean.bledata.send.SendGlassesQueryUserIdBleCmdBeaan;
import com.intelligence.glasses.bean.bledata.send.SendMachineBleCmdBeaan;
import com.intelligence.glasses.bean.bledata.send.SendReceiveGlassesRunParamBleConfirmDataBean;
import com.intelligence.glasses.bean.bledata.send.SendUserInfoBleCmdBean;
import com.intelligence.glasses.bean.bledata.send.SendUserInfoControlBleCmdBean;
import com.intelligence.glasses.bean.request.HttpRequestGlassesRunParamBean;
import com.intelligence.glasses.bean.response.HttpResponseGlassesRunParamBean;
import com.intelligence.glasses.ble.BleOptHelper;
import com.intelligence.glasses.event.BatteryEvent;
import com.intelligence.glasses.event.CallbackDataEvent;
import com.intelligence.glasses.event.CmdBleDataEvent;
import com.intelligence.glasses.event.ConnectEvent;
import com.intelligence.glasses.event.EventConstant;
import com.intelligence.glasses.event.NotifyDataEvent;
import com.intelligence.glasses.event.RunParamDataEvent;
import com.intelligence.glasses.greendao.greendaobean.UserInfoDBBean;
import com.intelligence.glasses.greendao.greenddaodb.UserInfoBeanDaoOpe;
import com.intelligence.glasses.http.response.HttpResponseGlassInitDataBackBean;
import com.intelligence.glasses.util.Config;
import com.intelligence.glasses.util.jsonutil.GsonTools;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import retrofit2.http.PUT;


public class GlassesBleDataModel {
    private StringBuffer totalCallBackReceiveDataBuffer = new StringBuffer();
    private StringBuffer totalNotifyReceiverDataBuffer = new StringBuffer();
    private StringBuffer currentNotfiyReceiverDataButter = new StringBuffer();
    private SparseArray<String>  glassesRunningArray = new SparseArray<>();
    private volatile AtomicLong currentUserId = new AtomicLong(-1);
    private volatile AtomicInteger glassesCurrentStatus = new AtomicInteger(-1);
    private volatile AtomicBoolean queryCurrentUserIdSuccess = new AtomicBoolean(false);
    private ExecutorService singleExecutorService;

    private static class Innner {
        public final static GlassesBleDataModel single = new GlassesBleDataModel();
    }

    public static GlassesBleDataModel getInstance() {
        return GlassesBleDataModel.Innner.single;
    }


    private GlassesBleDataModel() {
        registerEventBus();
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onReceiveBleDeviceConnnectStatus(ConnectEvent connectEvent) {
        if (connectEvent.isSuccess()) {
            BleOptHelper.bindWriteChannel(connectEvent.getDeviceType());
            BleOptHelper.bindNotifyChannel(connectEvent.getDeviceType());
            /**
             * 发送用户的视力信息和训练数据到蓝牙眼镜
             */
            //sendInitData();
            checkGlassesCurrentStatus();
            TrainModel.getInstance().createTimerAndTask();
            //setDefaultDataInSingleThread();
        } else if (connectEvent.isDisconnected()) {
            setQueryCurrentUserIdSuccess(false);
            setCurrentUserId(-1);
            TrainModel.getInstance().closeTimer();
        } else {
            setQueryCurrentUserIdSuccess(false);
            setCurrentUserId(-1);
            TrainModel.getInstance().closeTimer();
        }
    }

    private void setDefaultDataInSingleThread() {
        createSingleExectorService();
        singleExecutorService.submit(new Runnable() {
            @Override
            public void run() {
                MLog.d("singleThread1 = " + Thread.currentThread().getId());
                String bleLogPath = BleDataLog.getLogDirPath() + BleDataLog.getCommonLogFileName();
                File bleDataFilee = new File(bleLogPath);
                try {
                    boolean deletBleDataLogFile = bleDataFilee.delete();
                    MLog.d("deletBleDataLogFile = " + deletBleDataLogFile);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (!bleDataFilee.exists()){
                            bleDataFilee.createNewFile();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                try {
                    Thread.sleep(1000);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                //sendInitDataDefault();
                queryCurrentUserID();
            }
        });
    }

    /**
     * 检查当前眼镜的运行状态和用户ID等信息
     */
    private void checkGlassesCurrentStatus() {
        final String memeberId = Config.getConfig().getUserServerId();
        if (CommonUtils.isEmpty(memeberId)) {
            return;
        }

        createSingleExectorService();
        singleExecutorService.submit(new Runnable() {
            @Override
            public void run() {
                threadSleep(100);
                queryCurrentUserID();
                queryGlassesCurrentStatus();

                if (getQueryCurrentUserIdSuccess()) {
                    /**
                     * 查询ID成功
                     */
                    if (getCurrentUserId() != -1) {
                        /**
                         * 比较当前用户的 ID
                         */
                        List<UserInfoDBBean> userInfoDBBeanList = UserInfoBeanDaoOpe.queryUserInfoByServerID(MyApplication.getInstance(), memeberId);
                        if (null != userInfoDBBeanList && userInfoDBBeanList.size() > 0) {
                            UserInfoDBBean userInfoDBBean = userInfoDBBeanList.get(0);
                            String phone = userInfoDBBean.getPhone();
                            Long phoneLong = convertPhone2Long(phone);
                            if (false) {
                            //if (phoneLong != 0) {
                                MLog.d("queryGlassUserID = " + getCurrentUserId() + "  DBUserID = " + phoneLong);
                                if (phoneLong == getCurrentUserId()) {
                                    /**
                                     * 当前眼镜运行的数据是当前用户的，不做处理（也可以重新发送数据）
                                     */
                                    boolean prepareSuccess = false;
                                    for (int i = 0; i < 10; i++) {
                                        TrainModel.getInstance().prepareGlassesTrainData(false, false, false);
                                        for (int j = 0; j < 600; j++) {
                                            threadSleep(100);
                                            prepareSuccess = TrainModel.getInstance().glasseTrainDataPreparseSuccess();
                                            if (prepareSuccess) {
                                                break;
                                            }
                                        }

                                        if (prepareSuccess) {
                                            break;
                                        }
                                    }
                                    int cureentStatus = getGlassesCurrentStatus();
                                    if (prepareSuccess &&
                                            (cureentStatus == SendUserInfoControlBleCmdBean.UNKNOW_OPERATION_CODE
                                                    ||cureentStatus == SendUserInfoControlBleCmdBean.STOP_OPERATION_CODE)) {

                                        threadSleep(100);

                                        SendUserInfoBleCmdBean sendUserInfoBleCmdBean = TrainModel.getInstance().createUserInfoBleHexDataForTrain(Config.getConfig().getLastSelectedTrainMode() + 1, 0, 1);
                                        EventBus.getDefault().post(new CmdBleDataEvent(sendUserInfoBleCmdBean.buildCmdByteArray()));

                                        HttpResponseGlassesRunParamBean httpResponseGlassesRunParamBean =  TrainModel.getInstance().getHttpResponseGlassesRunParamBean();
                                        if (null != httpResponseGlassesRunParamBean && !CommonUtils.isEmpty(httpResponseGlassesRunParamBean.getMemberId())) {
                                            /**
                                             * 云端给的运行参数是有效的数据
                                             */
                                            threadSleep(100);

                                            SparseArray<BaseCmdBean> baseCmdBeanArray = TrainModel.getInstance().createSendRunParamsBleHexData();

                                            byte[] retByteArray = new byte[baseCmdBeanArray.size() * 20];

                                            for (int m = 0; m < baseCmdBeanArray.size(); m++) {
                                                BaseCmdBean baseCmdBean = baseCmdBeanArray.get(m);
                                                byte[] sendByte = baseCmdBean.buildCmdByteArray();
                                                System.arraycopy(sendByte, 0, retByteArray, m * 20, 20);
                                            }
                                            EventBus.getDefault().post(new CmdBleDataEvent(retByteArray));
                                        }
                                    }

                                } else {

                                    /**
                                     * 当前眼镜运行的数据是非当前用户的
                                     */
                                    boolean prepareSuccess = false;
                                    for (int i = 0; i < 10; i++) {
                                        TrainModel.getInstance().prepareGlassesTrainData(false, false, false);
                                        for (int j = 0; j < 600; j++) {
                                            threadSleep(100);
                                            prepareSuccess = TrainModel.getInstance().glasseTrainDataPreparseSuccess();
                                            if (prepareSuccess) {
                                                break;
                                            }
                                        }

                                        if (prepareSuccess) {
                                            break;
                                        }
                                    }

                                    int cureentStatus = getGlassesCurrentStatus();

                                    if (prepareSuccess && (cureentStatus == SendUserInfoControlBleCmdBean.UNKNOW_OPERATION_CODE
                                            || cureentStatus == SendUserInfoControlBleCmdBean.STOP_OPERATION_CODE)) {
                                        /**
                                         * 获取到训练数据，并且眼镜处于停止状态
                                         */
                                        SendMachineBleCmdBeaan sendMachineBleCmdBeaan = TrainModel.getInstance().createSendMachineBleHexDataForTrain();
                                        EventBus.getDefault().post(new CmdBleDataEvent(sendMachineBleCmdBeaan.buildCmdByteArray()));

                                        threadSleep(100);
                                        HttpResponseGlassInitDataBackBean httpResponseGlassInitDataBackBean = TrainModel.getInstance().getHttpResponseGlassInitDataBackBean();
                                        HttpResponseGlassInitDataBackBean.DataBean dataBean = httpResponseGlassInitDataBackBean.getData();

                                        SendUserInfoBleCmdBean sendUserInfoBleCmdBean = TrainModel.getInstance().createUserInfoBleHexDataForTrain(Config.getConfig().getLastSelectedTrainMode() + 1, dataBean.getIsNewUser(), 1);
                                        EventBus.getDefault().post(new CmdBleDataEvent(sendUserInfoBleCmdBean.buildCmdByteArray()));

                                        HttpResponseGlassesRunParamBean httpResponseGlassesRunParamBean = TrainModel.getInstance().getHttpResponseGlassesRunParamBean();
                                        if (null != httpResponseGlassesRunParamBean && !CommonUtils.isEmpty(httpResponseGlassesRunParamBean.getMemberId())) {

                                            threadSleep(100);

                                            SparseArray<BaseCmdBean> baseCmdBeanArray = TrainModel.getInstance().createSendRunParamsBleHexData();

                                            byte[] retByteArray = new byte[baseCmdBeanArray.size() * 20];

                                            for (int m = 0; m < baseCmdBeanArray.size(); m++) {
                                                BaseCmdBean baseCmdBean = baseCmdBeanArray.get(m);
                                                byte[] sendByte = baseCmdBean.buildCmdByteArray();
                                                System.arraycopy(sendByte, 0, retByteArray, m * 20, 20);
                                            }
                                            EventBus.getDefault().post(new CmdBleDataEvent(retByteArray));
                                        }

                                    }

                                }
                            } else {
                                /**
                                 * 处理当前眼镜第一次使用没有用户ID的情况， 发送机器数据 ，用户数据
                                 */
                                boolean prepareSuccess = false;
                                for (int i = 0; i < 10; i++) {
                                    TrainModel.getInstance().prepareGlassesTrainData(false, false, false);
                                    for (int j = 0; j < 600; j++) {
                                        threadSleep(100);
                                        prepareSuccess = TrainModel.getInstance().glasseTrainDataPreparseSuccess();
                                        if (prepareSuccess) {
                                            break;
                                        }
                                    }

                                    if (prepareSuccess) {
                                        break;
                                    }

                                }

                                if (prepareSuccess) {
                                    SendMachineBleCmdBeaan sendMachineBleCmdBeaan = TrainModel.getInstance().createSendMachineBleHexDataForTrain();
                                    EventBus.getDefault().post(new CmdBleDataEvent(sendMachineBleCmdBeaan.buildCmdByteArray()));

                                    threadSleep(100);
                                    HttpResponseGlassInitDataBackBean httpResponseGlassInitDataBackBean = TrainModel.getInstance().getHttpResponseGlassInitDataBackBean();
                                    HttpResponseGlassInitDataBackBean.DataBean dataBean = httpResponseGlassInitDataBackBean.getData();

                                    SendUserInfoBleCmdBean sendUserInfoBleCmdBean = TrainModel.getInstance().createUserInfoBleHexDataForTrain(Config.getConfig().getLastSelectedTrainMode() + 1, 1, 1);
                                    EventBus.getDefault().post(new CmdBleDataEvent(sendUserInfoBleCmdBean.buildCmdByteArray()));
                                }
                            }
                        }
                    }

                }
            }
        });
    }


    /**
     * 蓝牙连接成功之后，发送默认的初始化数据（测试阶段）
     */
    private void sendInitDataDefault() {
        try {
            Thread.sleep(500);
        } catch (Exception e) {
            e.printStackTrace();
        }

        sendGlassesMachineDefault();
        try {
            Thread.sleep(200);
        } catch (Exception e) {
            e.printStackTrace();
        }
        sendCurrentUserTrainDataDefault();
    }

    /**
     * 发送眼镜的机器数据到眼镜
     */
    private void sendGlassesMachineDefault() {
        String json = FileUtils.read(MyApplication.getInstance(), SendReceiveBleDataActivity.INPUT_FILE_NAME);
        SendMachineBleCmdBeaan fileSendBean = null;
        if (!CommonUtils.isEmpty(json)) {
            fileSendBean = GsonTools.changeGsonToBean(json, SendMachineBleCmdBeaan.class);
        }

        SendMachineBleCmdBeaan sendMachineBleCmdBeaan = new SendMachineBleCmdBeaan();

        long currentTime = System.currentTimeMillis();
        String[] yearMonth = DateUtil.localformatterDay.format(new Date(currentTime)).split("-");
        String[] hourMinut = DateUtil.hourMinuteSecondFormat.format(new Date(currentTime)).split("\\:");
        sendMachineBleCmdBeaan.setYear(Integer.parseInt(yearMonth[0]));
        sendMachineBleCmdBeaan.setMonth(Integer.parseInt(yearMonth[1]));
        sendMachineBleCmdBeaan.setDay(Integer.parseInt(yearMonth[2]));

        sendMachineBleCmdBeaan.setHour(Integer.parseInt(hourMinut[0]));
        sendMachineBleCmdBeaan.setMinute(Integer.parseInt(hourMinut[1]));
        sendMachineBleCmdBeaan.setSeconds(Integer.parseInt(hourMinut[2]));

      /*  sendMachineBleCmdBeaan.setMaxRunNumber(null != fileSendBean ? fileSendBean.getMaxRunNumber() : 30);
        sendMachineBleCmdBeaan.setStartSpeed(null != fileSendBean ? fileSendBean.getStartSpeed() : 3);
        sendMachineBleCmdBeaan.setSetSpeedInc(null != fileSendBean ? fileSendBean.getSetSpeedInc() : 1);
        sendMachineBleCmdBeaan.setStopSpeed(null != fileSendBean ? fileSendBean.getStopSpeed() : 20);
        sendMachineBleCmdBeaan.setMachineData5(null != fileSendBean ? fileSendBean.getMachineData5() : 0);
        sendMachineBleCmdBeaan.setMachineData6(null != fileSendBean ? fileSendBean.getMachineData6() : 0);
        sendMachineBleCmdBeaan.setMachineData7(null != fileSendBean ? fileSendBean.getMachineData7() : 0);
        sendMachineBleCmdBeaan.setMachineData8(null != fileSendBean ? fileSendBean.getMachineData8() : 0);
        sendMachineBleCmdBeaan.setMachineData9(null != fileSendBean ? fileSendBean.getMachineData9() : 0);*/

        sendMachineBleCmdBeaan.setMaxRunNumber(30);
        sendMachineBleCmdBeaan.setStartSpeed(3);
        sendMachineBleCmdBeaan.setSetSpeedInc(1);
        sendMachineBleCmdBeaan.setStopSpeed(20);
        sendMachineBleCmdBeaan.setCommonSpeed(30);
        sendMachineBleCmdBeaan.setMachineData6(80);
        sendMachineBleCmdBeaan.setMachineData7(20);
        sendMachineBleCmdBeaan.setMachineData8(20);
        sendMachineBleCmdBeaan.setMachineData9(0);


        EventBus.getDefault().post(new CmdBleDataEvent(sendMachineBleCmdBeaan.buildCmdByteArray()));
    }


    /**
     * 发送当前用户的训练数据
     */
    private void sendCurrentUserTrainDataDefault() {
        EventBus.getDefault().post(new CmdBleDataEvent(getUserInfoBeanDefault().buildCmdByteArray()));
    }

    private SendUserInfoBleCmdBean getUserInfoBeanDefault() {
        String json = FileUtils.read(MyApplication.getInstance(), SendReceiveBleDataActivity.INPUT_USER_INFO_FILE_NAME);
        SendUserInfoBleCmdBean fileUserBean = null;
        if (!CommonUtils.isEmpty(json)) {
            fileUserBean = GsonTools.changeGsonToBean(json, SendUserInfoBleCmdBean.class);
        }

        SendUserInfoBleCmdBean sendUserInfoBleCmdBean = new SendUserInfoBleCmdBean();

      /*  sendUserInfoBleCmdBean.setUserAge(null != fileUserBean ? fileUserBean.getUserAge() : 28);
        sendUserInfoBleCmdBean.setUserLeftEyeSightDegree(null != fileUserBean ? fileUserBean.getUserLeftEyeSightDegree() : 400);
        sendUserInfoBleCmdBean.setUserRightSightDegree(null != fileUserBean ? fileUserBean.getUserRightSightDegree() : 400);
        sendUserInfoBleCmdBean.setUserEyeSightType(null != fileUserBean ? fileUserBean.getUserEyeSightType() : 1);
        sendUserInfoBleCmdBean.setUserId(1);
        sendUserInfoBleCmdBean.setUserTrainMode(null != fileUserBean ? fileUserBean.getUserTrainMode() : 1);*/

        sendUserInfoBleCmdBean.setUserAge(28);
        sendUserInfoBleCmdBean.setUserLeftEyeSightDegree(400);
        sendUserInfoBleCmdBean.setUserRightSightDegree(400);
        sendUserInfoBleCmdBean.setUserEyeSightType(1);
        sendUserInfoBleCmdBean.setUserId(1);
        sendUserInfoBleCmdBean.setUserTrainMode(2);
        sendUserInfoBleCmdBean.buildCmdByteArray();
        return sendUserInfoBleCmdBean;
    }


    /**
     * 操作数据事件的反馈监听
     * @param callbackDataEvent
     */
    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onReceiveBleDevicSendCallBackStatus(CallbackDataEvent callbackDataEvent) {

        if (null != callbackDataEvent) {
            int deviceType = callbackDataEvent.getDeviceType();
            if (EventConstant.GLASS_BLUETOOTH_EVENT_TYPE == deviceType) {
                String tmpData = BaseParseCmdBean.bytesToString(callbackDataEvent.getData());
                MLog.d("CallbackData tmpData = " + tmpData);
            } else if (EventConstant.LIGHT_BLUETOOTH_EVENT_TYPE == deviceType) {

            }
        }
    }

    /**
     * 接收数据的事件监听
     * @param notifyDataEvent
     */
    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onReceiveBleDevicResponseStatus(NotifyDataEvent notifyDataEvent) {
        if (null != notifyDataEvent) {
            int deviceType = notifyDataEvent.getDeviceType();
            if (EventConstant.GLASS_BLUETOOTH_EVENT_TYPE == deviceType) {
                handleReceiveBleData(notifyDataEvent.getData());
            } else if (EventConstant.LIGHT_BLUETOOTH_EVENT_TYPE == deviceType) {

            }
        }
    }

    public void clearCallBackDataBuffer() {
        totalCallBackReceiveDataBuffer.setLength(0);
    }

    public void clearNotifyDataBuffer() {
        totalNotifyReceiverDataBuffer.setLength(0);
    }

    public void clearCurrentNotifyDataButter() {
        currentNotfiyReceiverDataButter.setLength(0);
    }

    public StringBuffer getTotalCallBackReceiveDataBuffer() {
        return totalCallBackReceiveDataBuffer;
    }

    public StringBuffer getTotalNotifyReceiverDataBuffer() {
        return totalNotifyReceiverDataBuffer;
    }

    public StringBuffer getCurrentNotfiyReceiverDataButter() {
        return currentNotfiyReceiverDataButter;
    }

    private void registerEventBus() {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    private void unRegisterEventBus() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    /**
     * 处理接受到蓝牙眼镜发送的蓝牙数据
     * @param receiveBleDataArray
     */
    private void handleReceiveBleData(byte[] receiveBleDataArray) {
        String tmpData = BaseParseCmdBean.bytesToString(receiveBleDataArray);
        MLog.d("NotifyData tmpData = " + tmpData);
        totalNotifyReceiverDataBuffer.append(tmpData);//这行代码不能删掉，GlassesBleDataCmdModel 中会获取该值来判断发送的指令是否成功接收
        currentNotfiyReceiverDataButter.setLength(0);
        currentNotfiyReceiverDataButter.append(tmpData);
        boolean isUserIDFeedBack = false;//isUserIdFeedBack(receiveBleDataArray);
        boolean isCommonFeedBack = false;//isCommonFeedBack(receiveBleDataArray);
        boolean isInterveneFeedBack = false;//isInterveneFeedBack(receiveBleDataArray);
        boolean isRunParam = false;//isGlassesRunningParamingBleData(tmpData);

        if ((isUserIDFeedBack = isUserIdFeedBack(receiveBleDataArray))
            || (isCommonFeedBack = isCommonFeedBack(receiveBleDataArray))
            || (isInterveneFeedBack = isInterveneFeedBack(receiveBleDataArray))
            || (isRunParam = isGlassesRunningParamingBleData(tmpData))) {

        }
        MLog.d("handleReceiveBleData isRunParam = " + isRunParam + " isCommonFeedBack = " + isCommonFeedBack + " isInterveneFeedBack = " + isInterveneFeedBack + " isUserIDFeedBack = " + isUserIDFeedBack) ;
        //SdLogUtil.writeCommonLog("handleReceiveBleData isRunParam = " + isRunParam + " isCommonFeedBack = " + isCommonFeedBack + " isInterveneFeedBack = " + isInterveneFeedBack + " isUserIDFeedBack = " + isUserIDFeedBack);
        //BleDataLog.writeBleData("APP接收：[" + BaseParseCmdBean.bytesToStringWithSpace(receiveBleDataArray)+"]" + getDataType(isUserIDFeedBack, isCommonFeedBack, isInterveneFeedBack, isRunParam));
        MLog.d("APPGLASS接收：[" + BaseParseCmdBean.bytesToStringWithSpace(receiveBleDataArray)+"]" + getDataType(isUserIDFeedBack, isCommonFeedBack, isInterveneFeedBack, isRunParam));
    }


    private String getDataType(boolean isUserId, boolean isCommondFeeback, boolean isInterveneFeedBack, boolean isRunParam) {
        if(isUserId) {
            return "用户ID";
        } else if (isCommondFeeback) {
            return "实时反馈";
        } else if (isInterveneFeedBack) {
            return "干预反馈";
        } else if (isRunParam) {
            return "运行参数";
        } else {
            return "未知反馈";
        }
    }

    /**
     * 蓝牙连接成功之后，发送初始化数据
     */
    private void sendInitData() {
        createSingleExectorService();
        singleExecutorService.submit(new Runnable() {
            @Override
            public void run() {
                threadSleep(1000);
                boolean prepareSuccess = false;
                for (int i = 0; i < 30; i++) {
                    if (!(prepareSuccess = TrainModel.getInstance().glasseTrainDataPreparseSuccess())) {
                        TrainModel.getInstance().prepareGlassesTrainData(false, false, false);
                        threadSleep(1000);
                    } else {
                        break;
                    }
                }

                if (prepareSuccess) {
                    SendMachineBleCmdBeaan sendMachineBleCmdBeaan = TrainModel.getInstance().createSendMachineBleHexDataForTrain();
                    EventBus.getDefault().post(new CmdBleDataEvent(sendMachineBleCmdBeaan.buildCmdByteArray()));

                    threadSleep(200);
                    HttpResponseGlassInitDataBackBean.DataBean dataBean = TrainModel.getInstance().getHttpResponseGlassInitDataBackBean().getData();

                    SendUserInfoBleCmdBean sendUserInfoBleCmdBean = TrainModel.getInstance().createUserInfoBleHexDataForTrain(Config.getConfig().getLastSelectedTrainMode() + 1, dataBean.getIsNewUser() ,1);
                    EventBus.getDefault().post(new CmdBleDataEvent(sendUserInfoBleCmdBean.buildCmdByteArray()));

                    threadSleep(200);

                    SparseArray<BaseCmdBean> baseCmdBeanArray = TrainModel.getInstance().createSendRunParamsBleHexData();

                    byte[] retByteArray = new byte[baseCmdBeanArray.size() * 20];

                    for (int i = 0; i < baseCmdBeanArray.size(); i++) {
                        BaseCmdBean baseCmdBean = baseCmdBeanArray.get(i);
                        byte[] sendByte = baseCmdBean.buildCmdByteArray();
                        System.arraycopy(sendByte, 0, retByteArray, i * 20, 20);
                    }
                    EventBus.getDefault().post(new CmdBleDataEvent(retByteArray));
                }
            }
        });
    /*    try {
            Thread.sleep(500);
        } catch (Exception e) {
            e.printStackTrace();
        }

        sendGlassesMachine();
        try {
            Thread.sleep(500);
        } catch (Exception e) {
            e.printStackTrace();
        }
        sendCurrentUserTrainData();*/
    }

    /**
     * 发送指令，查询当前用户的ID
     */
    public void queryCurrentUserID() {
        MLog.d("queryCurrentUserID start");
        setCurrentUserId(-1);
        setQueryCurrentUserIdSuccess(false);
        for (int i = 0; i < 10; i++) {
            EventBus.getDefault().post(new CmdBleDataEvent(getQueryCurrentUserIdByteArray()));
            for (int j = 0; j < 5; j++) {
                MLog.d("i = " + i + " j = " + j + " getCurrentUserId() = " + getCurrentUserId());
                if (getCurrentUserId() != -1) {
                    return;
                }
                threadSleep(100);
            }
        }

    }

    public byte[] getQueryCurrentUserIdByteArray() {
        SendGlassesQueryUserIdBleCmdBeaan sendGlassesQueryUserIdBleCmdBeaan = new SendGlassesQueryUserIdBleCmdBeaan();
        return sendGlassesQueryUserIdBleCmdBeaan.buildCmdByteArray();
    }

    /**
     * 发送完控制数据之后，接收到眼镜的反馈之后，再次查询眼镜当前的状态
     */
    public void queryGlassesCurrentStatusAgain(String monitorCmd) {
        if (!CommonUtils.isEmpty(monitorCmd) && !monitorCmd.equalsIgnoreCase(SendForGetCommonFeedBackDataBean.GLASS_QUERY_STATUS_CMD)) {
            createSingleExectorService();
            singleExecutorService.submit(new Runnable() {
                @Override
                public void run() {
                    threadSleep(100);
                    queryGlassesCurrentStatus();
                }
            });
        }
    }

    /***
     *  查询眼镜当前的状态（训练状态，还是停止状态）
     */
    public void queryGlassesCurrentStatus() {
        setGlassesCurrentStatus(-1);
        for (int i = 0; i < 10; i++) {
            SendForGetCommonFeedBackDataBean commonFeedBackDataBean = new SendForGetCommonFeedBackDataBean();
            EventBus.getDefault().post(new CmdBleDataEvent(commonFeedBackDataBean.buildCmdByteArray()));

            for (int j = 0; j < 5; j++) {
                MLog.d("queryGlassesCurrentStatus i = " + i + " j = " + j + " getGlassesCurrentStatus() = " + getGlassesCurrentStatus());
                if (getGlassesCurrentStatus() != -1) {
                    return;
                }
                threadSleep(100);
            }
        }

    }

    public void queryGlassCurrentStatusByTimer() {
        createSingleExectorService();
        singleExecutorService.submit(new Runnable() {
            @Override
            public void run() {
                queryGlassesCurrentStatus();
            }
        });
    }

    /**
     * 发送眼镜的机器数据到眼镜
     */
    private void sendGlassesMachine() {
        String json = FileUtils.read(MyApplication.getInstance(), SendReceiveBleDataActivity.INPUT_FILE_NAME);
        SendMachineBleCmdBeaan fileSendBean = null;
        if (!CommonUtils.isEmpty(json)) {
            fileSendBean = GsonTools.changeGsonToBean(json, SendMachineBleCmdBeaan.class);
        }

        SendMachineBleCmdBeaan sendMachineBleCmdBeaan = new SendMachineBleCmdBeaan();

        long currentTime = System.currentTimeMillis();
        String[] yearMonth = DateUtil.localformatterDay.format(new Date(currentTime)).split("-");
        String[] hourMinut = DateUtil.hourMinuteSecondFormat.format(new Date(currentTime)).split("\\:");
        sendMachineBleCmdBeaan.setYear(Integer.parseInt(yearMonth[0]));
        sendMachineBleCmdBeaan.setMonth(Integer.parseInt(yearMonth[1]));
        sendMachineBleCmdBeaan.setDay(Integer.parseInt(yearMonth[2]));

        sendMachineBleCmdBeaan.setHour(Integer.parseInt(hourMinut[0]));
        sendMachineBleCmdBeaan.setMinute(Integer.parseInt(hourMinut[1]));
        sendMachineBleCmdBeaan.setSeconds(Integer.parseInt(hourMinut[2]));

        sendMachineBleCmdBeaan.setMaxRunNumber(null != fileSendBean ? fileSendBean.getMaxRunNumber() : 30);
        sendMachineBleCmdBeaan.setStartSpeed(null != fileSendBean ? fileSendBean.getStartSpeed() : 3);
        sendMachineBleCmdBeaan.setSetSpeedInc(null != fileSendBean ? fileSendBean.getSetSpeedInc() : 1);
        sendMachineBleCmdBeaan.setStopSpeed(null != fileSendBean ? fileSendBean.getStopSpeed() : 20);
        sendMachineBleCmdBeaan.setCommonSpeed(null != fileSendBean ? fileSendBean.getCommonSpeed() : 0);
        sendMachineBleCmdBeaan.setMachineData6(null != fileSendBean ? fileSendBean.getMachineData6() : 0);
        sendMachineBleCmdBeaan.setMachineData7(null != fileSendBean ? fileSendBean.getMachineData7() : 0);
        sendMachineBleCmdBeaan.setMachineData8(null != fileSendBean ? fileSendBean.getMachineData8() : 0);
        sendMachineBleCmdBeaan.setMachineData9(null != fileSendBean ? fileSendBean.getMachineData9() : 0);


        EventBus.getDefault().post(new CmdBleDataEvent(sendMachineBleCmdBeaan.buildCmdByteArray()));
    }

    /**
     * 发送当前用户的训练数据
     */
    private void sendCurrentUserTrainData() {
        EventBus.getDefault().post(new CmdBleDataEvent(getUserInfoBean().buildCmdByteArray()));
    }

    private SendUserInfoBleCmdBean getUserInfoBean() {
        String json = FileUtils.read(MyApplication.getInstance(), SendReceiveBleDataActivity.INPUT_FILE_NAME);
        SendUserInfoBleCmdBean fileUserBean = null;
        if (!CommonUtils.isEmpty(json)) {
            fileUserBean = GsonTools.changeGsonToBean(json, SendUserInfoBleCmdBean.class);
        }

        boolean isNewUser = true;
        String paramJson = FileUtils.read(MyApplication.getInstance(), TrainModel.GLASSES_RUNNING_PARAM_FILE_NAME);
        if (!CommonUtils.isEmpty(paramJson)) {
            HttpRequestGlassesRunParamBean httpRequestGlassesRunParamBean = GsonTools.changeGsonToBean(paramJson, HttpRequestGlassesRunParamBean.class);
            if (null != httpRequestGlassesRunParamBean) {
             if (null != fileUserBean) {
                 isNewUser = false;
                 fileUserBean.setNewUser(0);
             }
            }
        }

        List<UserInfoDBBean > userInfoDBBeanList = UserInfoBeanDaoOpe.queryUserInfoByServerID(MyApplication.getInstance(), Config.getConfig().getUserServerId());
        UserInfoDBBean userInfoDBBean = userInfoDBBeanList.get(0);

        SendUserInfoBleCmdBean sendUserInfoBleCmdBean = new SendUserInfoBleCmdBean();

        sendUserInfoBleCmdBean.setUserAge(null != fileUserBean ? fileUserBean.getUserAge() : userInfoDBBean.getAge());
        sendUserInfoBleCmdBean.setUserLeftEyeSightDegree(null != fileUserBean ? fileUserBean.getUserLeftEyeSightDegree() : (int)Math.abs(userInfoDBBean.getLeft_eye_train_degree()));
        sendUserInfoBleCmdBean.setUserRightSightDegree(null != fileUserBean ? fileUserBean.getUserRightSightDegree() : (int)Math.abs(userInfoDBBean.getRight_eye_train_degree()));
        sendUserInfoBleCmdBean.setUserEyeSightType(null != fileUserBean ? fileUserBean.getUserEyeSightType() : (userInfoDBBean.getDiopter_state() + 1));
        sendUserInfoBleCmdBean.setUserId(null != fileUserBean ? fileUserBean.getUserId() : convertPhone2Long(userInfoDBBean.getPhone()));
        sendUserInfoBleCmdBean.setUserTrainMode(null != fileUserBean ? fileUserBean.getUserTrainMode() : 1);
        sendUserInfoBleCmdBean.setNewUser(isNewUser ? 1 : 0);
        sendUserInfoBleCmdBean.setUserLens(null != fileUserBean ? fileUserBean.getUserLens() : sendUserInfoBleCmdBean.getUserTrainMode());
        sendUserInfoBleCmdBean.buildCmdByteArray();
        return sendUserInfoBleCmdBean;
    }
    public static Long convertPhone2Long(String phone) {
        if (!CommonUtils.isEmpty(phone)) {
            if (phone.startsWith("+")) {
                phone = phone.substring(1);
            }
            if (phone.startsWith("86")) {
                phone = phone.substring(2);
            }
            return Long.parseLong(phone);
        }
        return 0L;
    }


    /**
     * 是否是干预反馈
     * @param bleDataArray
     * @return
     */
    private boolean isInterveneFeedBack(byte[] bleDataArray) {
        ReceiveInteveneFeedbackBleDataBean receiveInteveneFeedbackBleDataBean = new ReceiveInteveneFeedbackBleDataBean();
        receiveInteveneFeedbackBleDataBean.parseBleDataByteArray2Bean(bleDataArray);
        boolean parseSuccess = receiveInteveneFeedbackBleDataBean.isParseSuccess();
        if (parseSuccess) {
            TrainModel.getInstance().postInterveneFeedBackBleData(receiveInteveneFeedbackBleDataBean);

            queryGlassesCurrentStatusAgain(receiveInteveneFeedbackBleDataBean.getMonitorCmd());
        }
        return parseSuccess;
    }

    /**
     * 是否是实时反馈
     * @param bleDataArray
     * @return
     */
    private boolean isCommonFeedBack(byte[] bleDataArray) {
        ReceiveGlassesFeedbackBleDataBean receiveGlassesFeedbackBleDataBean = new ReceiveGlassesFeedbackBleDataBean();
        receiveGlassesFeedbackBleDataBean.parseBleDataByteArray2Bean(bleDataArray);
        boolean parseSuccess = receiveGlassesFeedbackBleDataBean.isParseSuccess();
        if (parseSuccess) {
            TrainModel.getInstance().postCommonFeedBackBleData(receiveGlassesFeedbackBleDataBean);
            setGlassesCurrentStatus(Integer.parseInt(receiveGlassesFeedbackBleDataBean.getControlCmd(), 16));
            MLog.d("getGlassesCurrentStatus() = " + getGlassesCurrentStatus());
            EventBus.getDefault().post(new BatteryEvent(receiveGlassesFeedbackBleDataBean.getBattery(), Integer.parseInt(receiveGlassesFeedbackBleDataBean.getControlCmd(), 16)));

            queryGlassesCurrentStatusAgain(receiveGlassesFeedbackBleDataBean.getOperationCmd());
        }
        return parseSuccess;
    }

    /**
     * 是否是用户ID的数据
     * @param bleDataArray
     * @return
     */
    private boolean isUserIdFeedBack(byte[] bleDataArray) {
        ReceiveGlassesUserIdBleDataBean receiveGlassesUserIdBleDataBean = new ReceiveGlassesUserIdBleDataBean();
        receiveGlassesUserIdBleDataBean.parseBleDataByteArray2Bean(bleDataArray);
        boolean parseSuccess = receiveGlassesUserIdBleDataBean.isParseSuccess();
        if (parseSuccess) {
            setQueryCurrentUserIdSuccess(true);
            MLog.d("isUserIdFeedBack receiveGlassesUserIdBleDataBean.getUserId() = " + receiveGlassesUserIdBleDataBean.getUserId());
            setCurrentUserId(receiveGlassesUserIdBleDataBean.getUserId());
        }
        return parseSuccess;
    }

    /**
     * 解析眼镜运行时参数
     * @param bleData
     * @return
     */
    private boolean isGlassesRunningParamingBleData(String bleData) {
        if (!CommonUtils.isEmpty(bleData)) {
            String lowerBleData = bleData.toLowerCase();
            if (lowerBleData.startsWith(ReceiveGlassesRunParam1BleDataBean.USER_MONITOR_CMD)) {
                glassesRunningArray.clear();
                glassesRunningArray.put(0, lowerBleData);
                sendCommonFeedBackRequestCmd(getCmd(lowerBleData));
            } else if (lowerBleData.startsWith(ReceiveGlassesRunParam2BleDataBean.USER_MONITOR_CMD)) {
                glassesRunningArray.put(1, lowerBleData);
                sendCommonFeedBackRequestCmd(getCmd(lowerBleData));
            } else if (lowerBleData.startsWith(ReceiveGlassesRunParam3BleDataBean.USER_MONITOR_CMD)) {
                glassesRunningArray.put(2, lowerBleData);
                sendCommonFeedBackRequestCmd(getCmd(lowerBleData));
            } else if (lowerBleData.startsWith(ReceiveGlassesRunParam4BleDataBean.USER_MONITOR_CMD)) {
                glassesRunningArray.put(3, lowerBleData);
                sendCommonFeedBackRequestCmd(getCmd(lowerBleData));
            }  else if (lowerBleData.startsWith(ReceiveGlassesRunParam5BleDataBean.USER_MONITOR_CMD)) {
                glassesRunningArray.put(4, lowerBleData);
            } else {
                return false;
            }

            if (glassesRunningArray.size() == 5) {
                /**
                 * 眼镜运行参数接受完毕，上传云端，或者保存在本地
                 */
                //sendAllRunParamsData();
                //BleDataBeanConvertUtil.bleDataBean2HttpRequestBean()
                bleDataStr2Bean(glassesRunningArray);
            }

            return true;
        }
        return false;
    }

    private void bleDataStr2Bean(SparseArray<String> glassesRuningParams) {
        SparseArray<BaseParseCmdBean> sparseArray = new SparseArray<>();

        for (int i = 0; i < glassesRuningParams.size(); i++) {
            String bleData = glassesRuningParams.get(i);
            switch (i) {
                case 0:
                    ReceiveGlassesRunParam1BleDataBean receiveGlassesRunParam1BleDataBean = new ReceiveGlassesRunParam1BleDataBean();
                    receiveGlassesRunParam1BleDataBean.parseBleDataByteArray2Bean(BleHexUtil.getRealSendData(bleData));
                    sparseArray.put(i, receiveGlassesRunParam1BleDataBean);
                    break;
                case 1:
                    ReceiveGlassesRunParam2BleDataBean receiveGlassesRunParam2BleDataBean = new ReceiveGlassesRunParam2BleDataBean();
                    receiveGlassesRunParam2BleDataBean.parseBleDataByteArray2Bean(BleHexUtil.getRealSendData(bleData));
                    sparseArray.put(i, receiveGlassesRunParam2BleDataBean);
                    break;
                case 2:
                    ReceiveGlassesRunParam3BleDataBean receiveGlassesRunParam3BleDataBean = new ReceiveGlassesRunParam3BleDataBean();
                    receiveGlassesRunParam3BleDataBean.parseBleDataByteArray2Bean(BleHexUtil.getRealSendData(bleData));
                    sparseArray.put(i, receiveGlassesRunParam3BleDataBean);
                    break;
                case 3:
                    ReceiveGlassesRunParam4BleDataBean receiveGlassesRunParam4BleDataBean = new ReceiveGlassesRunParam4BleDataBean();
                    receiveGlassesRunParam4BleDataBean.parseBleDataByteArray2Bean(BleHexUtil.getRealSendData(bleData));
                    sparseArray.put(i, receiveGlassesRunParam4BleDataBean);
                    break;
                case 4:
                    ReceiveGlassesRunParam5BleDataBean receiveGlassesRunParam5BleDataBean = new ReceiveGlassesRunParam5BleDataBean();
                    receiveGlassesRunParam5BleDataBean.parseBleDataByteArray2Bean(BleHexUtil.getRealSendData(bleData));
                    sparseArray.put(i, receiveGlassesRunParam5BleDataBean);
                    break;

                    default:
                        break;
            }
        }

        HttpRequestGlassesRunParamBean httpRequestGlassesRunParamBean = BleDataBeanConvertUtil.bleDataBean2HttpRequestBean(sparseArray);
        TrainModel.getInstance().postRunParamData(httpRequestGlassesRunParamBean);
      /*  String jsonn = GsonTools.createGsonString(httpRequestGlassesRunParamBean);
        MLog.d("glasses_runing_params_json = " + jsonn);
        FileUtils.write(MyApplication.getInstance(), TrainModel.GLASSES_RUNNING_PARAM_FILE_NAME, jsonn);
        SdLogUtil.writeCommonLog("glasses_runing_params_json = " + jsonn);
        if (BuildConfig.DEBUG) {
            ToastUtil.showShort(jsonn);
        }*/

    }

    /**
     * 集齐运行参数，广播出去
     */

    private void sendAllRunParamsData() {
        RunParamDataEvent event = new RunParamDataEvent();

        byte[] retByteArray = new byte[glassesRunningArray.size() * 20];

        for (int i = 0; i < glassesRunningArray.size();  i++) {
            String bleDataStr = glassesRunningArray.get(i);
            byte[] dataArray = BleHexUtil.getRealSendData(bleDataStr);
            //System.arraycopy(retByteArray, i * 20, sendByte, 0, 20);
            System.arraycopy(dataArray, 0, retByteArray, i * 20, 20);
        }
        event.setData(retByteArray);


        SparseArray<BaseParseCmdBean> sparseArray = new SparseArray<>();

        for (int i = 0; i < glassesRunningArray.size(); i++) {
            String bleData = glassesRunningArray.get(i);
            switch (i) {
                case 0:
                    ReceiveGlassesRunParam1BleDataBean receiveGlassesRunParam1BleDataBean = new ReceiveGlassesRunParam1BleDataBean();
                    receiveGlassesRunParam1BleDataBean.parseBleDataByteArray2Bean(BleHexUtil.getRealSendData(bleData));
                    sparseArray.put(i, receiveGlassesRunParam1BleDataBean);
                    break;
                case 1:
                    ReceiveGlassesRunParam2BleDataBean receiveGlassesRunParam2BleDataBean = new ReceiveGlassesRunParam2BleDataBean();
                    receiveGlassesRunParam2BleDataBean.parseBleDataByteArray2Bean(BleHexUtil.getRealSendData(bleData));
                    sparseArray.put(i, receiveGlassesRunParam2BleDataBean);
                    break;
                case 2:
                    ReceiveGlassesRunParam3BleDataBean receiveGlassesRunParam3BleDataBean = new ReceiveGlassesRunParam3BleDataBean();
                    receiveGlassesRunParam3BleDataBean.parseBleDataByteArray2Bean(BleHexUtil.getRealSendData(bleData));
                    sparseArray.put(i, receiveGlassesRunParam3BleDataBean);
                    break;
                case 3:
                    ReceiveGlassesRunParam4BleDataBean receiveGlassesRunParam4BleDataBean = new ReceiveGlassesRunParam4BleDataBean();
                    receiveGlassesRunParam4BleDataBean.parseBleDataByteArray2Bean(BleHexUtil.getRealSendData(bleData));
                    sparseArray.put(i, receiveGlassesRunParam4BleDataBean);
                    break;

                case 4:
                    ReceiveGlassesRunParam5BleDataBean receiveGlassesRunParam5BleDataBean = new ReceiveGlassesRunParam5BleDataBean();
                    receiveGlassesRunParam5BleDataBean.parseBleDataByteArray2Bean(BleHexUtil.getRealSendData(bleData));
                    sparseArray.put(i, receiveGlassesRunParam5BleDataBean);
                    break;

                default:
                    break;
            }
        }

        HttpRequestGlassesRunParamBean httpRequestGlassesRunParamBean = BleDataBeanConvertUtil.bleDataBean2HttpRequestBean(sparseArray);

        //String jsonn = GsonTools.createGsonString(httpRequestGlassesRunParamBean);
        event.setBeanJson(httpRequestGlassesRunParamBean.toString());
        EventBus.getDefault().post(event);
    }

    private String getCmd(String  receiveData) {
        if (!CommonUtils.isEmpty(receiveData) && receiveData.length() == 40) {
            return receiveData.substring(36, 38);//倒数第3,4位
        }
        return "00";
    }

    private void sendCommonFeedBackRequestCmd(final String lastReceiveCmd) {
        createSingleExectorService();
        singleExecutorService.submit(new Runnable() {
            @Override
            public void run() {
                MLog.d("singleThread2 = " + Thread.currentThread().getId());
                String tmpCmd = lastReceiveCmd;
                if (!CommonUtils.isEmpty(tmpCmd)) {
                    if (tmpCmd.length() > 2) {
                        tmpCmd = tmpCmd.substring(0, 2);
                    }
                } else {
                    tmpCmd = "00";
                }
                SendReceiveGlassesRunParamBleConfirmDataBean receiveGlassesRunParamBleConfirmDataBean = new SendReceiveGlassesRunParamBleConfirmDataBean();
                receiveGlassesRunParamBleConfirmDataBean.setMonitorData_CMD(tmpCmd);
                byte[] requestFeedBack = receiveGlassesRunParamBleConfirmDataBean.buildCmdByteArray();
                EventBus.getDefault().post(new CmdBleDataEvent(requestFeedBack));
            }
        });
    }

    private void createSingleExectorService() {
        if (null == singleExecutorService || singleExecutorService.isShutdown() || singleExecutorService.isTerminated()) {
           singleExecutorService = ThreadPoolUtilsLocal.newSingleThreadPool();
        }
    }

    public long getCurrentUserId() {
        return currentUserId.get();
    }

    public void setCurrentUserId(long currentUserId) {
        this.currentUserId.set(currentUserId);
    }

    public int getGlassesCurrentStatus() {
        return glassesCurrentStatus.get();
    }

    public void setGlassesCurrentStatus(int glassesCurrentStatusInt) {
        this.glassesCurrentStatus.set(glassesCurrentStatusInt);
    }

    public boolean getQueryCurrentUserIdSuccess() {
        return queryCurrentUserIdSuccess.get();
    }

    public void setQueryCurrentUserIdSuccess(boolean queryCurrentUserIdSuccess) {
        this.queryCurrentUserIdSuccess.set(queryCurrentUserIdSuccess);
    }

    private void threadSleep( long millis){
        try {
            Thread.sleep(millis);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clearModelData() {
        totalCallBackReceiveDataBuffer.setLength(0);
        totalNotifyReceiverDataBuffer.setLength(0);
        currentNotfiyReceiverDataButter.setLength(0);
        setCurrentUserId(-1);
        setGlassesCurrentStatus(-1);
        setQueryCurrentUserIdSuccess(false);
    }
}
