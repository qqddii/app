package com.intelligence.glasses.model;

import com.android.common.baselibrary.blebean.BaseParseCmdBean;
import com.android.common.baselibrary.log.MLog;
import com.android.common.baselibrary.util.BleHexUtil;
import com.android.common.baselibrary.util.DateUtil;
import com.android.common.baselibrary.util.ToastUtil;
import com.android.common.baselibrary.util.comutil.CommonUtils;
import com.intelligence.glasses.MyApplication;
import com.intelligence.glasses.bean.bledata.UserTrainInfoBleDataBean;
import com.intelligence.glasses.bean.bledata.receive.ReceiveGlassesFeedbackBleDataBean;
import com.intelligence.glasses.bean.bledata.send.SendGlassesQueryUserIdBleCmdBeaan;
import com.intelligence.glasses.bean.bledata.send.SendGlassesRunParam1BleCmdBeaan;
import com.intelligence.glasses.bean.bledata.send.SendMachineBleCmdBeaan;
import com.intelligence.glasses.bean.bledata.send.SendUserInfoBleCmdBean;
import com.intelligence.glasses.bean.bledata.send.SendUserInfoControlBleCmdBean;
import com.intelligence.glasses.ble.BleDeviceManager;
import com.intelligence.glasses.event.CmdBleDataEvent;
import com.intelligence.glasses.greendao.greendaobean.UserInfoDBBean;
import com.intelligence.glasses.greendao.greendaobean.UserInfoTrainBleDataDBBean;
import com.intelligence.glasses.greendao.greenddaodb.UserInfoBeanDaoOpe;
import com.intelligence.glasses.greendao.greenddaodb.UserInfoTrainBleDataBeanDaoOpe;
import com.intelligence.glasses.util.Config;
import com.shitec.bleglasses.BuildConfig;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class GlassesBleDataCmdModel {
    private Lock lock = new ReentrantLock();
    private Condition writeCondition =  lock.newCondition();
    private Condition readCondition =  lock.newCondition();
    private static class Innner {
        public final static GlassesBleDataCmdModel single = new GlassesBleDataCmdModel();
    }

    public static GlassesBleDataCmdModel getInstance() {
        return GlassesBleDataCmdModel.Innner.single;
    }

    private GlassesBleDataCmdModel() {
        registerEventBus();
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onWriteBleData2GlassesDevice(CmdBleDataEvent cmdBleDataEvent) {
        lock.lock();
        MLog.d(" cmdBleDataEvent Thread.ID = " + Thread.currentThread().getId());
        try {
            if (null != cmdBleDataEvent) {
                byte[] bleData = cmdBleDataEvent.getBleCmdData();
                if (null != bleData && bleData.length > 0) {
                    MLog.d("APPGLASS发送：[" + BaseParseCmdBean.bytesToStringWithSpace(bleData)+"]");
                    //if (handleUserIdWhile()) {
                        BleDeviceManager.getInstance().writeData2GlassesBleDevice(bleData);
                        //BleDataLog.writeBleData("APP发送：[" + BaseParseCmdBean.bytesToStringWithSpace(bleData)+"]");
                        handleSendDataCallBack(bleData);
                    //}
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    private void registerEventBus() {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    private void unRegisterEventBus() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    /**
     * 检测当前的蓝牙是否已经获取到当前用户的ID
     * @return
     */
    private boolean handleUserIdWhile() {
        while (true) {
            if (!GlassesBleDataModel.getInstance().getQueryCurrentUserIdSuccess()) {
                if (BleDeviceManager.getInstance().isGlassesBleDeviceConnected()) {
                    byte[] userIDCmdArray = GlassesBleDataModel.getInstance().getQueryCurrentUserIdByteArray();
                    GlassesBleDataModel.getInstance().clearNotifyDataBuffer();
                    BleDeviceManager.getInstance().writeData2GlassesBleDevice(userIDCmdArray);
                    handleSendDataCallBack(userIDCmdArray);
                } else {
                    return false;
                }
            }else {
                return true;
            }
        }
    }

    private void handleSendDataCallBack(byte[] writeDataArray) {
        String sourData = BaseParseCmdBean.bytesToString(writeDataArray);
        MLog.d("handleSendDataCallBack sourData = " + sourData);
        String lowerSourceData = sourData.toLowerCase();
        boolean receiveSuccess = false;
        String receiveBleData = "";
        for (int i = 0; i < 20; i++) {
            receiveBleData = GlassesBleDataModel.getInstance().getCurrentNotfiyReceiverDataButter().toString().toLowerCase();
            if (!CommonUtils.isEmpty(receiveBleData)
                    && (receiveBleData.startsWith(BaseParseCmdBean.USER_DATA_PREFIX)
                         || receiveBleData.endsWith(BaseParseCmdBean.USER_DATA_SUFIX))) {
                MLog.d(" receiveBleData = " + receiveBleData);
                receiveSuccess = true;
                break;
            }
            waitReceiveDataTime(50);
        }

        if (!receiveSuccess) {
            handleTimeout(lowerSourceData);
            if (!BuildConfig.DEBUG) {
                /**
                 * 正式版本，则不需要往下进行
                 */
                return;
            }
        }

        if (lowerSourceData.startsWith(SendUserInfoBleCmdBean.USER_DATA_PREFIX)) {
            /**
             * 发送的是用户数据
             */
            //SdLogUtil.writeCommonLog("发送用户数据:" + lowerSourceData);

        } else if (lowerSourceData.startsWith(SendUserInfoControlBleCmdBean.CONTROL_DATA_PREFIX)) {
            /**
             * 发送的是控制数据
             */
            String operationCode = lowerSourceData.substring(2 * SendUserInfoControlBleCmdBean.USER_CONTROL_IDNEX, 2 * (SendUserInfoControlBleCmdBean.USER_CONTROL_IDNEX + 1));
            ReceiveGlassesFeedbackBleDataBean receiveMonitorBleDataBean = new ReceiveGlassesFeedbackBleDataBean();
            receiveMonitorBleDataBean.parseBleDataByteArray2Bean(BleHexUtil.stringToBytes(receiveBleData));
            //MLog.d("receiveMonitorBleDataBean = " + receiveMonitorBleDataBean);
            //long operationTime = 0;
            boolean operationSuccess = false;
             if (receiveMonitorBleDataBean.isParseSuccess()) {
                 operationSuccess = operationSuccess(receiveMonitorBleDataBean.getOperationCmd());
             }

            if (operationSuccess) {
                calcOperationCode(Integer.parseInt(operationCode), System.currentTimeMillis());
            }
            //SdLogUtil.writeCommonLog("发送控制数据:" + lowerSourceData);
        } else if (lowerSourceData.startsWith(SendMachineBleCmdBeaan.GLASSES_MACHINE_DATA_PREFIX)) {
            /**
             * 发送的是眼镜机器数据
             */
            //SdLogUtil.writeCommonLog("发送眼镜机器数据:" + lowerSourceData);
        } else if (lowerSourceData.startsWith(SendGlassesRunParam1BleCmdBeaan.GLASSES_MACHINE_DATA_PREFIX)) {
            /**
             * 发送的是运行参数数据（有4帧 4 * 20 = 80 字节）
             */
            //SdLogUtil.writeCommonLog("发送运行参数数据:" + lowerSourceData);
        } else if (lowerSourceData.startsWith(SendGlassesQueryUserIdBleCmdBeaan.GLASSES_MACHINE_DATA_PREFIX)) {
            /**
             * 发送的是查询用户ID的数据
             */
            //SdLogUtil.writeCommonLog("发送查询用户ID的数据:" + lowerSourceData);
        } else {
            /**
             * 发送的是未知数据
             */
            //SdLogUtil.writeCommonLog("发送未知数据:" + lowerSourceData);
        }
    }

    private void handleTimeout(String  lowerSendSourceData) {
        MLog.d(" handleTimeout ");
        /**
         * 根据发送的指令，以及在超时反馈中，做重发处理
         */
        if (lowerSendSourceData.startsWith(SendUserInfoBleCmdBean.USER_DATA_PREFIX)) {
            /**
             * 发送用户数据超时,确认是否重发
             */
            ToastUtil.showLong("发送用户数据失败");
        } else if (lowerSendSourceData.startsWith(SendMachineBleCmdBeaan.GLASSES_MACHINE_DATA_PREFIX)) {
            /**
             * 发送机器数据超时,确认是否重发
             */
            ToastUtil.showLong("发送机器数据失败");
        } else if (lowerSendSourceData.startsWith(SendGlassesRunParam1BleCmdBeaan.GLASSES_MACHINE_DATA_PREFIX)) {
            /**
             * 发送运行参数数据超时,确认是否重发
             */
            ToastUtil.showLong("发送运行参数数据失败");
        } else if (lowerSendSourceData.startsWith(SendUserInfoControlBleCmdBean.CONTROL_DATA_PREFIX) ) {
            /**
             * 发送控制指令超时,确认是否重发
             */
            ToastUtil.showLong("发送控制指令数据失败");
        }
    }

    private void calcOperationCode(int operationCode, long operationTime) {
            switch (operationCode) {
                case SendUserInfoControlBleCmdBean.START_OPERATION_CODE:
                    ToastUtil.showLong("开始成功");
                    //saveOperationCode2DB(operationCode, operationTime);
                    break;

                case SendUserInfoControlBleCmdBean.PASUSE_OPERATION_CODE:
                    ToastUtil.showLong("暂停成功");
                    //saveOperationCode2DB(operationCode, operationTime);
                    break;

                case SendUserInfoControlBleCmdBean.CONTINUE_OPERATION_CODE:
                    ToastUtil.showLong("继续成功");
                    //saveOperationCode2DB(operationCode, operationTime);
                    break;

                case SendUserInfoControlBleCmdBean.STOP_OPERATION_CODE:
                    ToastUtil.showLong("停止成功");
                    //saveOperationCode2DB(operationCode, operationTime);
                    break;

                case SendUserInfoControlBleCmdBean.INTEVENE_OPERATION_CODE:
                    ToastUtil.showLong("干预成功");
                    //saveOperationCode2DB(operationCode, operationTime);
                    break;

                case SendUserInfoControlBleCmdBean.POWER_OFF_OPERATION_CODE:
                    ToastUtil.showLong("关闭电源");
                    //saveOperationCode2DB(operationCode, operationTime);
                    break;

                    default:
                        break;

            }
    }

    private synchronized void saveOperationCode2DB(int operationCode, long operationTime) {
        String serverID = Config.getConfig().getUserServerId();
        if (CommonUtils.isEmpty(serverID)) {
            return;
        }

        List<UserInfoDBBean> userInfoDBBeanList = UserInfoBeanDaoOpe.queryUserInfoByServerID(MyApplication.getInstance(), serverID);
        UserInfoDBBean userInfoDBBean = userInfoDBBeanList.get(0);


        List<UserInfoTrainBleDataDBBean> insertList = new ArrayList<>();

        UserInfoTrainBleDataDBBean userInfoTrainBleDataDBBean = new UserInfoTrainBleDataDBBean();
        userInfoTrainBleDataDBBean.setLocalid(UUID.randomUUID().toString());
        userInfoTrainBleDataDBBean.setServerId(Config.getConfig().getUserServerId());
        userInfoTrainBleDataDBBean.setLogin_name(userInfoDBBean.getLogin_name());
        userInfoTrainBleDataDBBean.setDeviceId(Config.getConfig().getLastConnectBleGlassesMac());

        if (operationTime != 0) {
            userInfoTrainBleDataDBBean.setCollect_time(DateUtil.tlocalformatter.format(new Date(operationTime)));
        } else {
            userInfoTrainBleDataDBBean.setCollect_time(DateUtil.tlocalformatter.format(new Date(System.currentTimeMillis())));
        }

        if (operationCode == SendUserInfoControlBleCmdBean.START_OPERATION_CODE) {
            /**
             * 如果是开始操作，则收集时间和开始时间是一致的
             */
            userInfoTrainBleDataDBBean.setStart_time(userInfoTrainBleDataDBBean.getCollect_time());
        } else {
            /**
             * 获取开始时间
             */
            List<UserInfoTrainBleDataDBBean> userInfoTrainBleDataDBBeanList = UserInfoTrainBleDataBeanDaoOpe.queryLastUserInfoTrainTimeWithOperationCodeyServerID(MyApplication.getInstance(), serverID, SendUserInfoControlBleCmdBean.START_OPERATION_CODE);
            if (null != userInfoTrainBleDataDBBeanList && userInfoTrainBleDataDBBeanList.size() > 0) {
                UserInfoTrainBleDataDBBean userInfoTrainDBBean = userInfoTrainBleDataDBBeanList.get(0);
                String startTime = userInfoTrainDBBean.getStart_time();
                userInfoTrainBleDataDBBean.setStart_time(startTime);
            } else {
                userInfoTrainBleDataDBBean.setStart_time(userInfoTrainBleDataDBBean.getCollect_time());
            }

        }

        try {
            userInfoTrainBleDataDBBean.setCollect_time_long(DateUtil.tlocalformatter.parse(userInfoTrainBleDataDBBean.getCollect_time()).getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
      /*  userInfoTrainBleDataDBBean.setLeft_counts();
        userInfoTrainBleDataDBBean.setLeft_speed();
        userInfoTrainBleDataDBBean.setLeft_range_up();
        userInfoTrainBleDataDBBean.setLeft_range_low();
        userInfoTrainBleDataDBBean.setRight_counts();
        userInfoTrainBleDataDBBean.setRight_speed();
        userInfoTrainBleDataDBBean.setRight_range_up();
        userInfoTrainBleDataDBBean.setRight_range_low();*/
        userInfoTrainBleDataDBBean.setReportedServer(false);
        userInfoTrainBleDataDBBean.setNotifyBleDevice(false);
        userInfoTrainBleDataDBBean.setOperation(operationCode);
        insertList.add(userInfoTrainBleDataDBBean);
        UserInfoTrainBleDataBeanDaoOpe.insertData(MyApplication.getInstance(), insertList);

        reportOperation(operationCode);
    }

    private void reportOperation(int operationCode) {
        TrainModel trainModel = TrainModel.getInstance();
        List<UserTrainInfoBleDataBean> userTrainInfoBleDataBeanList = trainModel.queryUserTrainInfoBleDataFromDBByOperation(operationCode);
        if (null != userTrainInfoBleDataBeanList) {
            MLog.d("operationCode= " + operationCode + " list.size = " + userTrainInfoBleDataBeanList.size());
            for (UserTrainInfoBleDataBean itemBean : userTrainInfoBleDataBeanList) {
                //MLog.d("userTrainInfoBleDataBeanList itemBean.getDataList().size() = " + itemBean.getDataList().size());
                trainModel.uploadTrainInfo(itemBean, null);
            }
        }

        if (operationCode == SendUserInfoControlBleCmdBean.START_OPERATION_CODE) {
            /**
             * 如果是开始操作， 则之前所有的操作，都查询上报一遍。
             */
            List<Integer> operationCodeList = new ArrayList<>();
            operationCodeList.add(SendUserInfoControlBleCmdBean.PASUSE_OPERATION_CODE);
            operationCodeList.add(SendUserInfoControlBleCmdBean.CONTINUE_OPERATION_CODE);
            operationCodeList.add(SendUserInfoControlBleCmdBean.STOP_OPERATION_CODE);
            operationCodeList.add(SendUserInfoControlBleCmdBean.INTEVENE_OPERATION_CODE);
            operationCodeList.add(SendUserInfoControlBleCmdBean.POWER_OFF_OPERATION_CODE);

            for (Integer operCode : operationCodeList) {
                userTrainInfoBleDataBeanList = trainModel.queryUserTrainInfoBleDataFromDBByOperation(operCode);
                if (null != userTrainInfoBleDataBeanList) {
                    MLog.d("operCode" +  operCode + " list.size size = " + userTrainInfoBleDataBeanList.size());

                    for (UserTrainInfoBleDataBean itemBean : userTrainInfoBleDataBeanList) {
                        //MLog.d("userTrainInfoBleDataBeanList itemBean.getDataList().size() = " + itemBean.getDataList().size());
                        trainModel.uploadTrainInfo(itemBean, null);
                    }
                }
            }

        }
    }

        /**
         * 单次等待蓝牙数据的时间
         * @param millis
         */
        private void waitReceiveDataTime ( long millis){
            try {
                Thread.sleep(millis);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private String createOperationStrDate(ReceiveGlassesFeedbackBleDataBean receiveMonitorBleDataBean) {
            StringBuilder dateBuilder = new StringBuilder();

            /*dateBuilder.append(receiveMonitorBleDataBean.getInterveneYear());
            dateBuilder.append("-");
            dateBuilder.append(receiveMonitorBleDataBean.getInterveneMonth() <  10 ? "0" + receiveMonitorBleDataBean.getInterveneMonth() : receiveMonitorBleDataBean.getInterveneMonth());
            dateBuilder.append("-");
            dateBuilder.append(receiveMonitorBleDataBean.getInterveneDay() <  10 ? "0" + receiveMonitorBleDataBean.getInterveneDay() : receiveMonitorBleDataBean.getInterveneDay());

            dateBuilder.append(" ");

            dateBuilder.append(receiveMonitorBleDataBean.getInterveneHour() <  10 ? "0" + receiveMonitorBleDataBean.getInterveneHour() : receiveMonitorBleDataBean.getInterveneHour());
            dateBuilder.append(":");
            dateBuilder.append(receiveMonitorBleDataBean.getInterveneMinute() <  10 ? "0" + receiveMonitorBleDataBean.getInterveneMinute() : receiveMonitorBleDataBean.getInterveneMinute());
            dateBuilder.append(":");
            dateBuilder.append(receiveMonitorBleDataBean.getInterveneSecond() <  10 ? "0" + receiveMonitorBleDataBean.getInterveneSecond() : receiveMonitorBleDataBean.getInterveneSecond());*/

            return dateBuilder.toString();
        }

        private boolean operationSuccess(String operationCode) {
            if (!CommonUtils.isEmpty(operationCode)) {
                int operationCodeInt = Integer.parseInt(operationCode, 16);
                if (operationCodeInt == 0) {
                    return false;
                } else {
                    return  true;
                }
            }
            return false;
        }

    }
