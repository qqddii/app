package com.intelligence.glasses.callback;

import android.app.Dialog;


public interface OnDialogButtonClickListener<D extends Dialog> {
    public void onDialogClick(int buttonId, String buttonText);
}
