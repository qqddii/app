package com.intelligence.glasses.ble;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import com.android.common.baselibrary.blebean.BaseCmdBean;
import com.android.common.baselibrary.blebean.BaseParseCmdBean;
import com.android.common.baselibrary.log.SdLogUtil;
import com.android.common.baselibrary.util.ToastUtil;
import com.android.common.baselibrary.util.comutil.CommonUtils;
import com.android.common.baselibrary.util.ThreadPoolUtilsLocal;

import com.intelligence.glasses.callback.CheckBleMacByServerCallBack;
import com.intelligence.glasses.constant.ConstantString;
import com.intelligence.glasses.event.CallbackDataEvent;
import com.intelligence.glasses.event.ConnectEvent;
import com.intelligence.glasses.event.EventConstant;
import com.intelligence.glasses.event.NotifyDataEvent;
import com.intelligence.glasses.event.ScanEvent;
import com.intelligence.glasses.model.TrainModel;
import com.intelligence.glasses.util.Config;
import com.android.common.baselibrary.log.MLog;
import com.intelligence.glasses.util.MyUtilBytes;
import com.vise.baseble.BuildConfig;
import com.vise.baseble.ViseBle;
import com.vise.baseble.callback.IBleCallback;
import com.vise.baseble.callback.IConnectCallback;
import com.vise.baseble.callback.scan.IScanCallback;
import com.vise.baseble.callback.scan.ScanCallback;
import com.vise.baseble.common.BleConstant;
import com.vise.baseble.common.ConnectState;
import com.vise.baseble.common.PropertyType;
import com.vise.baseble.core.BluetoothGattChannel;
import com.vise.baseble.core.DeviceMirror;
import com.vise.baseble.core.DeviceMirrorPool;
import com.vise.baseble.exception.BleException;
import com.vise.baseble.model.BluetoothLeDevice;
import com.vise.baseble.model.BluetoothLeDeviceStore;
import com.vise.baseble.utils.HexUtil;
import com.vise.log.ViseLog;

import org.greenrobot.eventbus.EventBus;

import java.util.LinkedList;
import java.util.Queue;
import java.util.UUID;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class BleDeviceManager {
    private static BleDeviceManager instance;
    private final static String TAG = BleDeviceManager.class.getSimpleName();
    private DeviceMirrorPool mDeviceMirrorPool;
    private ScheduledExecutorService executorService = null;
    private AtomicInteger threadHashCode = new AtomicInteger(0);
    private Handler threadHandler = new Handler(Looper.getMainLooper());

    /**
     * 操作数据回调
     */
    private IBleCallback mGlassesBleCallback = new IBleCallback() {
        public void onSuccess(byte[] bArr, BluetoothGattChannel bluetoothGattChannel, BluetoothLeDevice bluetoothLeDevice) {
            if (null == bArr) {
                bArr = new byte[0];
            }
            MLog.d("bleCallback onSuccess HexUtil.encodeHexStr(bArr) = " + HexUtil.encodeHexStr(bArr) + " bArr.length = " + bArr.length);
            //SdLogUtil.writeCommonLog("操作数据回调bleCallback onSuccess HexUtil.encodeHexStr(bArr) = " + HexUtil.encodeHexStr(bArr) + " bArr.length = " + bArr.length);
            MyUtilBytes.printByteArray(bArr);
            if (bArr != null) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("callback success:");
                stringBuilder.append(HexUtil.encodeHexStr(bArr));
                ViseLog.i(stringBuilder.toString());
                EventBus.getDefault().post(mGlasseCallbackDataEvent.setData(bArr).setSuccess(true).setBluetoothLeDevice(bluetoothLeDevice).setBluetoothGattChannel(bluetoothGattChannel));
                if (bluetoothGattChannel != null && (bluetoothGattChannel.getPropertyType() == PropertyType.PROPERTY_INDICATE
                        || bluetoothGattChannel.getPropertyType() == PropertyType.PROPERTY_NOTIFY)) {
                    DeviceMirror deviceMirror = mDeviceMirrorPool.getDeviceMirror(bluetoothLeDevice);
                    deviceMirror.setNotifyListener(bluetoothGattChannel.getGattInfoKey(), mGlassesReceiveCallback);
                }
            }
        }

        public void onFailure(BleException bleException) {
            MLog.d("bleCallback onFailure bleException = " + bleException);
            //SdLogUtil.writeCommonLog("操作数据回调bleCallback onFailure bleException = " + bleException);
            if (bleException != null) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("callback fail:");
                stringBuilder.append(bleException.getDescription());
                ViseLog.i(stringBuilder.toString());
                EventBus.getDefault().post(mGlasseCallbackDataEvent.setSuccess(false));
            }
        }
    };

    private IBleCallback mLightBleCallback = new IBleCallback() {
        public void onSuccess(byte[] bArr, BluetoothGattChannel bluetoothGattChannel, BluetoothLeDevice bluetoothLeDevice) {
            MLog.d("bleCallback onSuccess HexUtil.encodeHexStr(bArr) = " + HexUtil.encodeHexStr(bArr));
            MyUtilBytes.printByteArray(bArr);
            if (bArr != null) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("callback success:");
                stringBuilder.append(HexUtil.encodeHexStr(bArr));
                ViseLog.i(stringBuilder.toString());
                EventBus.getDefault().post(mLightCallbackDataEvent.setData(bArr).setSuccess(true).setBluetoothLeDevice(bluetoothLeDevice).setBluetoothGattChannel(bluetoothGattChannel));
                if (bluetoothGattChannel != null && (bluetoothGattChannel.getPropertyType() == PropertyType.PROPERTY_INDICATE
                        || bluetoothGattChannel.getPropertyType() == PropertyType.PROPERTY_NOTIFY)) {
                    DeviceMirror deviceMirror = mDeviceMirrorPool.getDeviceMirror(bluetoothLeDevice);
                    deviceMirror.setNotifyListener(bluetoothGattChannel.getGattInfoKey(), mLightReceiveCallback);
                }
            }
        }

        public void onFailure(BleException bleException) {
            MLog.d("bleCallback onFailure bleException = " + bleException);
            if (bleException != null) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("callback fail:");
                stringBuilder.append(bleException.getDescription());
                ViseLog.i(stringBuilder.toString());
                EventBus.getDefault().post(mLightCallbackDataEvent.setSuccess(false));
            }
        }
    };


    /**
     * 连接回调
     */
    private IConnectCallback mGlassesConnectCallback = new IConnectCallback() {
        public void onConnectSuccess(DeviceMirror deviceMirror) {

            BluetoothLeDevice bluetoothLeDevice = deviceMirror.getBluetoothLeDevice();
            Config.getConfig().saveLastConnectBleGlassesName(bluetoothLeDevice.getName());
            Config.getConfig().saveLastConnectBleGlassesMac(bluetoothLeDevice.getAddress());

            EventBus.getDefault().post(mGlassesConnectEvent.setDeviceMirror(deviceMirror).setSuccess(true));
            MLog.d("Connect Success!"  + Thread.currentThread().getId());
            //SdLogUtil.writeCommonLog(Thread.currentThread() + "[Glasses][Connect Success]" + deviceMirror);
            //mGlassesDeviceMirror = deviceMirror;
            shutDownScheduledExecutorService();
        }

        public void onConnectFailure(BleException bleException) {
            EventBus.getDefault().post(mGlassesConnectEvent.setSuccess(false).setDisconnected(false));
            MLog.e("Connect Failure!");
            //SdLogUtil.writeCommonLog(Thread.currentThread() + "[Glasses][Connect Failure]" + bleException);
            //ToastDialogUtil.showShortToast("蓝牙眼镜连接失败" + Thread.currentThread());
            //mGlassesDeviceMirror = null;
            createScheduleSerivce();

        }

        public void onDisconnect(boolean z) {
            EventBus.getDefault().post(mGlassesConnectEvent.setSuccess(false).setDisconnected(true));
            MLog.e("Disconnect!");
            //SdLogUtil.writeCommonLog(Thread.currentThread() + "[Glasses][Disconnect]" + z);
            //ToastDialogUtil.showShortToast("蓝牙眼镜断开连接" + Thread.currentThread());
            //mGlassesDeviceMirror = null;
            createScheduleSerivce();
        }

        @Override
        public void foundDeviceByMac(BluetoothLeDevice bluetoothLeDevice) {
            MLog.e("foundDeviceByMac! bluetoothLeDevice = " + bluetoothLeDevice);
            setmGlassesBluetoothLeDevice(bluetoothLeDevice);
            String mac = Config.getConfig().getLastConnectBleGlassesMac();
            if (!CommonUtils.isEmpty(mac)) {
                if (null != bluetoothLeDevice && bluetoothLeDevice.getAddress().equals(mac)) {
                    Config.getConfig().saveLastConnectBleGlassesName(bluetoothLeDevice.getName());
                    connect(bluetoothLeDevice, this);
                }
            }
            //SdLogUtil.writeCommonLog("mGlassesConnectCallback foundDeviceByMac bluetoothLeDevice = " + bluetoothLeDevice);
        }

        @Override
        public void onConnectTimeoutOrFailByMac() {
            MLog.e("onConnectTimeoutOrFailByMac");
            /**
             * 通过 mac 地址连接超时或失败的回调
             */
            String mac = Config.getConfig().getLastConnectBleGlassesMac();
            if (!CommonUtils.isEmpty(mac)) {
                createScheduleSerivce();
            } else {
                /**
                 * 用户取消了连接操作
                 */
            }
        }
    };

    private IConnectCallback mLightConnectCallback = new IConnectCallback() {
        public void onConnectSuccess(DeviceMirror deviceMirror) {
            MLog.d("Connect Success!");
            //ToastDialogUtil.showShortToast("光通量连接成功");
            //SdLogUtil.writeCommonLog("[Light][Connect Success]" + deviceMirror);
            //mLightDeviceMirror = deviceMirror;
            EventBus.getDefault().post(mLightConnectEvent.setDeviceMirror(deviceMirror).setSuccess(true));
        }

        public void onConnectFailure(BleException bleException) {
            MLog.d("Connect Failure!");
            //ToastDialogUtil.showShortToast("光通量连接失败");
            //SdLogUtil.writeCommonLog("[Light][Connect Failure]" + bleException);
            //mLightDeviceMirror = null;
            EventBus.getDefault().post(mLightConnectEvent.setSuccess(false).setDisconnected(false));
        }

        public void onDisconnect(boolean z) {
            MLog.d("Disconnect!");
            //ToastDialogUtil.showShortToast("光通量断开连接");
            //SdLogUtil.writeCommonLog("[Light][Disconnect]" + z);
            //mLightDeviceMirror = null;
            EventBus.getDefault().post(mLightConnectEvent.setSuccess(false).setDisconnected(true));
        }

        @Override
        public void foundDeviceByMac(BluetoothLeDevice bluetoothLeDevice) {
            setmLightBluetoothLeDevice(bluetoothLeDevice);
            //SdLogUtil.writeCommonLog("mLightConnectCallback foundDeviceByMac bluetoothLeDevice = " + bluetoothLeDevice);
        }

        @Override
        public void onConnectTimeoutOrFailByMac() {
            /**
             * 通过 mac 地址连接超时或失败的回调
             */
        }
    };

    private BluetoothLeDevice mGlassesBluetoothLeDevice;
    private BluetoothLeDevice mLightBluetoothLeDevice;

    /*private DeviceMirror mGlassesDeviceMirror;
    private DeviceMirror mLightDeviceMirror;*/

    private ConnectEvent mGlassesConnectEvent = new ConnectEvent(EventConstant.GLASS_BLUETOOTH_EVENT_TYPE);
    private ConnectEvent mLightConnectEvent = new ConnectEvent(EventConstant.LIGHT_BLUETOOTH_EVENT_TYPE);

    private NotifyDataEvent mGlassesNotifyDataEvent = new NotifyDataEvent(EventConstant.GLASS_BLUETOOTH_EVENT_TYPE);
    private NotifyDataEvent mLightNotifyDataEvent = new NotifyDataEvent(EventConstant.LIGHT_BLUETOOTH_EVENT_TYPE);

    private CallbackDataEvent mGlasseCallbackDataEvent = new CallbackDataEvent(EventConstant.GLASS_BLUETOOTH_EVENT_TYPE);
    private CallbackDataEvent mLightCallbackDataEvent = new CallbackDataEvent(EventConstant.LIGHT_BLUETOOTH_EVENT_TYPE);




    /**
     * 接收数据回调
     */
    private IBleCallback mGlassesReceiveCallback = new IBleCallback() {
        public void onSuccess(byte[] bArr, BluetoothGattChannel bluetoothGattChannel, BluetoothLeDevice bluetoothLeDevice) {
            String byte2HexStr = HexUtil.encodeHexStr(bArr);
            //SdLogUtil.writeCommonLog("IBleCallback onSuccess HexUtil.ecodeHexStr(bArr) = " + byte2HexStr);
            //SdLogUtil.writeCommonLog("接收数据回调成功[Glasses][IBleCallback onSuccess]" + byte2HexStr + " bArr.length = " + bArr.length);

            //SdLogUtil.writeCommonLog("[Glasses][IBleCallback onSuccess]" + MyUtilBytes.byteArray2String(bArr));

            if (bArr != null) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("notify success:");
                stringBuilder.append(HexUtil.encodeHexStr(bArr));
                ViseLog.i(stringBuilder.toString());
                EventBus.getDefault().post(mGlassesNotifyDataEvent.setData(bArr)
                        .setBluetoothLeDevice(bluetoothLeDevice)
                        .setBluetoothGattChannel(bluetoothGattChannel));
            }
        }

        public void onFailure(BleException bleException) {
            MLog.d("接收数据回调失败receiveCallback  onFailure() BleException = " + bleException);
            //SdLogUtil.writeCommonLog("[Glasses][IBleCallback onFailure]" + bleException);
            if (bleException != null) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("notify fail:");
                stringBuilder.append(bleException.getDescription());
                ViseLog.i(stringBuilder.toString());
            }
        }
    };

    private IBleCallback mLightReceiveCallback = new IBleCallback() {
        public void onSuccess(byte[] bArr, BluetoothGattChannel bluetoothGattChannel, BluetoothLeDevice bluetoothLeDevice) {
            String byte2HexStr = HexUtil.encodeHexStr(bArr);
            /*if (null != byte2HexStr && byte2HexStr.equals("2444543a3101000024")) {

            } else*/ {
                MLog.d("receiveCallback  onSuccess() HexUtil.encodeHexStr(bArr) = " + HexUtil.encodeHexStr(bArr));
                MyUtilBytes.printByteArray(bArr);
            }
            //SdLogUtil.writeCommonLog("[Light][IBleCallback onSuccess]" + MyUtilBytes.byteArray2String(bArr));
            if (bArr != null) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("notify success:");
                stringBuilder.append(HexUtil.encodeHexStr(bArr));
                ViseLog.i(stringBuilder.toString());
                EventBus.getDefault().post(mLightNotifyDataEvent.setData(bArr)
                        .setBluetoothLeDevice(bluetoothLeDevice)
                        .setBluetoothGattChannel(bluetoothGattChannel));
            }
        }

        public void onFailure(BleException bleException) {
            MLog.d("receiveCallback  onFailure() BleException = " + bleException);
            //SdLogUtil.writeCommonLog("[Light][IBleCallback onFailure]" + bleException);
            if (bleException != null) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("notify fail:");
                stringBuilder.append(bleException.getDescription());
                ViseLog.i(stringBuilder.toString());
            }
        }
    };

    private ScanCallback mGlassesScanCallBack = new ScanCallback(new IScanCallback() {
        public void onScanFinish(BluetoothLeDeviceStore bluetoothLeDeviceStore) {
            //MLog.d("scanCallback  onScanFinish() bluetoothLeDeviceStore = " + bluetoothLeDeviceStore);
            //SdLogUtil.writeCommonLog("[IScanCallback][onScanFinish]" + bluetoothLeDeviceStore);
        }

        public void onDeviceFound(BluetoothLeDevice bluetoothLeDevice) {
            //MLog.d("ScanCallback  onDeviceFound() bluetoothLeDevice = " + bluetoothLeDevice);
            //SdLogUtil.writeCommonLog("[IScanCallback][onDeviceFound]" + bluetoothLeDevice);
            if (isGlassesBleDevice(bluetoothLeDevice) || isLightBleDevice(bluetoothLeDevice)) {
                if (isGlassesBleDevice(bluetoothLeDevice)) {
                    EventBus.getDefault().post(mGlassesScanEvent.setSuccess(true).setBluetoothLeDevice(bluetoothLeDevice));
                } else if (isLightBleDevice(bluetoothLeDevice)) {
                    EventBus.getDefault().post(mLightScanEvent.setSuccess(true).setBluetoothLeDevice(bluetoothLeDevice));
                }
            }

        }

        public void onScanTimeout() {
            //MLog.d("scanCallback  onScanTimeout()");
            //SdLogUtil.writeCommonLog("[IScanCallback][onScanTimeout]" );
          /*  mGlassesBluetoothLeDevice = null;
            mLightBluetoothLeDevice = null;*/
            EventBus.getDefault().post(new ScanEvent(EventConstant.ALL_BLUETOOTH_EVENT_TYPE).setSuccess(false).setBluetoothLeDevice(null));
        }
    });

    //private ScanEvent scanEvent = new ScanEvent();

    private ScanEvent mGlassesScanEvent = new ScanEvent(EventConstant.GLASS_BLUETOOTH_EVENT_TYPE);

    private ScanEvent mLightScanEvent = new ScanEvent(EventConstant.LIGHT_BLUETOOTH_EVENT_TYPE);

    private BleDeviceManager() {
    }

    public static BleDeviceManager getInstance() {
        instance = SingleTon.single;
        return instance;
    }

    private static class SingleTon {
        private static final BleDeviceManager single = new BleDeviceManager();
    }

    public void init(Context context) {
        if (context != null) {
            //蓝牙相关配置修改
            ViseBle.config()
                    .setScanTimeout(-1)//扫描超时时间，这里设置为永久扫描
                    .setConnectTimeout(BleConstant.DEFAULT_CONN_TIME)//连接超时时间
                    .setOperateTimeout(BleConstant.DEFAULT_OPERATE_TIME)//设置数据操作超时时间
                    .setConnectRetryCount(BleConstant.DEFAULT_MAX_CONNECT_COUNT)//设置连接失败重试次数
                    .setConnectRetryInterval(BleConstant.DEFAULT_RETRY_INTERVAL)//设置连接失败重试间隔时间
                    .setOperateRetryCount(BleConstant.DEFAULT_RETRY_COUNT)//设置数据操作失败重试次数
                    .setOperateRetryInterval(BleConstant.DEFAULT_RETRY_INTERVAL)//设置数据操作失败重试间隔时间
                    .setMaxConnectCount(2);//设置最大连接设备数量
            //蓝牙信息初始化，全局唯一，必须在应用初始化时调用
            ViseBle.getInstance().init(context);
            mDeviceMirrorPool = ViseBle.getInstance().getDeviceMirrorPool();
            //createScheduleSerivce();
        }
    }

    public void startScan(String str) {
        if (BuildConfig.DEBUG) {
            ToastUtil.showLongToast("开始扫描" );
        }
        //ViseBle.getInstance().startScan(new SingleFilterScanCallback(this.scanCallback).setDeviceMac(str));
        startScanAllBleDevice();

    }

    private void startScanAllBleDevice() {
        ViseBle.getInstance().startScan(mGlassesScanCallBack);
    }

    public void stopScan() {
        ViseBle.getInstance().stopScan(mGlassesScanCallBack);
    }

    public void stopScanByMac() {
        ViseBle.getInstance().stopScanByMac();
    }

   /* private void connect() {
        MLog.d( "connect " + this.mBluetoothLeDevice);
        if (this.mBluetoothLeDevice != null) {
            ViseBle.getInstance().connect(this.mBluetoothLeDevice, this.connectCallback);
        }
    }*/

    private void connect(BluetoothLeDevice bluetoothLeDeviceParam, IConnectCallback connectCallback) {
        MLog.d( "connect " + bluetoothLeDeviceParam);
        //SdLogUtil.writeCommonLog("[BleDeviceManager][connect]" + bluetoothLeDeviceParam);
        if (bluetoothLeDeviceParam != null) {
            ViseBle.getInstance().connect(bluetoothLeDeviceParam, connectCallback);
        }
    }

    public void connectGlassesByMac(String mac) {
        Config.getConfig().saveLastConnectBleGlassesMac(mac);
        MLog.d( "connect mac = " + mac);
        //SdLogUtil.writeCommonLog("[BleDeviceManager][connect]connectGlassesByMac = " + mac);
        if (!CommonUtils.isEmpty(mac)) {
            ViseBle.getInstance().connectByMac(mac, mGlassesConnectCallback);
        }
    }

    public void connectLightByMac(String mac) {
        MLog.d( "connect mac = " + mac);
        //SdLogUtil.writeCommonLog("[BleDeviceManager][connect]connectLightByMac = " + mac);
        if (!CommonUtils.isEmpty(mac)) {
            ViseBle.getInstance().connectByMac(mac, mLightConnectCallback);
        }
    }

    public void connectGlassesBleDevice() {
        Config.getConfig().saveLastConnectBleGlassesMac(mGlassesBluetoothLeDevice.getAddress());
        Config.getConfig().saveLastConnectBleGlassesName(mGlassesBluetoothLeDevice.getName());
        connect(mGlassesBluetoothLeDevice, mGlassesConnectCallback);
    }

    public void connectLightBleDevice() {
        connect(mLightBluetoothLeDevice, mLightConnectCallback);
    }

    private void disconnect(BluetoothLeDevice bluetoothLeDeviceParam) {
        ViseLog.i( "disconnect " + bluetoothLeDeviceParam);
       /* if (null != bluetoothLeDeviceParam) {
            ViseBle.getInstance().disconnect(bluetoothLeDeviceParam);
        }*/
        /**
         * 断开所有设备
         */
        ViseBle.getInstance().disconnect();
    }

    public void disconnectGlassesBleDevice(boolean clearMacValue) {
        if (clearMacValue) {
            Config.getConfig().saveLastConnectBleGlassesMac("");
            Config.getConfig().saveLastConnectBleGlassesName("");
        }
        disconnect(mGlassesBluetoothLeDevice);
    }

    public void disconnectLightBleDevice() {
        disconnect(mLightBluetoothLeDevice);
    }

    private boolean isConnected(BluetoothLeDevice bluetoothLeDeviceParam) {
        return bluetoothLeDeviceParam != null ? ViseBle.getInstance().isConnect(bluetoothLeDeviceParam) : false;
    }

    public boolean isGlassesBleDeviceConnected() {
        return isConnected(mGlassesBluetoothLeDevice);
    }

    public boolean isGlassesBleDeviceConnecting() {
        return ViseBle.getInstance().getConnectState(mGlassesBluetoothLeDevice) == ConnectState.CONNECT_PROCESS;
    }

    public boolean isLightBleDeviceConnected() {
        return isConnected(mLightBluetoothLeDevice);
    }

    public boolean isLightBleDeviceConnecting() {
        return ViseBle.getInstance().getConnectState(mLightBluetoothLeDevice) == ConnectState.CONNECT_PROCESS;
    }

    private void bindChannel(DeviceMirror deviceMirrorParam,IBleCallback iBleCallbackParam, PropertyType propertyType, UUID uuid, UUID uuid2, UUID uuid3) {
        if (deviceMirrorParam != null) {
            BluetoothGattChannel bluetoothGattChannel = new BluetoothGattChannel.Builder()
                    .setBluetoothGatt(deviceMirrorParam.getBluetoothGatt())
                    .setPropertyType(propertyType)
                    .setServiceUUID(uuid)
                    .setCharacteristicUUID(uuid2)
                    .setDescriptorUUID(uuid3).builder();
            MLog.d("bindChannel() ");
            //SdLogUtil.writeCommonLog("bindChannel mGlassesDeviceMirror = " + deviceMirrorParam);
            deviceMirrorParam.bindChannel(iBleCallbackParam, bluetoothGattChannel);
        }
    }

    private void bindChannel(BluetoothLeDevice bluetoothLeDevice ,IBleCallback iBleCallbackParam, PropertyType propertyType, UUID uuid, UUID uuid2, UUID uuid3) {
        DeviceMirror deviceMirror = mDeviceMirrorPool.getDeviceMirror(bluetoothLeDevice);
        if (deviceMirror != null) {
            BluetoothGattChannel bluetoothGattChannel = new BluetoothGattChannel.Builder()
                    .setBluetoothGatt(deviceMirror.getBluetoothGatt())
                    .setPropertyType(propertyType)
                    .setServiceUUID(uuid)
                    .setCharacteristicUUID(uuid2)
                    .setDescriptorUUID(uuid3).builder();
            MLog.d("bindChannel() ");
            //SdLogUtil.writeCommonLog("bindChannel mGlassesDeviceMirror = " + deviceMirror);
            deviceMirror.bindChannel(iBleCallbackParam, bluetoothGattChannel);
        }
    }

    public void bindGlassesChannel(PropertyType propertyType, UUID uuid, UUID uuid2, UUID uuid3) {
        //SdLogUtil.writeCommonLog("bindGlassesChannel mGlassesDeviceMirror = " + mGlassesDeviceMirror);
        //SdLogUtil.writeCommonLog("propertyType = " + propertyType + " bindGlassesChannel uuid = " + uuid + "  uuid2 = " + uuid2);
        //bindChannel(mGlassesDeviceMirror, mGlassesBleCallback, propertyType, uuid, uuid2, uuid3);
        bindChannel(mGlassesBluetoothLeDevice, mGlassesBleCallback, propertyType, uuid, uuid2, uuid3);
    }

    public void bindLightChannel(PropertyType propertyType, UUID uuid, UUID uuid2, UUID uuid3) {
        //bindChannel(mLightDeviceMirror, mLightBleCallback, propertyType, uuid, uuid2, uuid3);
        bindChannel(mLightBluetoothLeDevice, mLightBleCallback, propertyType, uuid, uuid2, uuid3);
    }

    public void registerGlassesNotify(boolean isIndicationParam) {
        DeviceMirror deviceMirror = mDeviceMirrorPool.getDeviceMirror(mGlassesBluetoothLeDevice);
        if (null != deviceMirror) {
            deviceMirror.registerNotify(isIndicationParam);
        }
    }

    public void registerLightNotify(boolean isIndicationParam) {
        DeviceMirror deviceMirror = mDeviceMirrorPool.getDeviceMirror(mLightBluetoothLeDevice);
        if (null != deviceMirror) {
            deviceMirror.registerNotify(isIndicationParam);
        }
    }

    private void writeData(byte[] bArr) {
        writeData2GlassesBleDevice(bArr);
    }

    public void writeData2GlassesBleDevice(byte[] glassessDataArray) {
        DeviceMirror deviceMirror = mDeviceMirrorPool.getDeviceMirror(mGlassesBluetoothLeDevice);
        if (null != deviceMirror) {
            //SdLogUtil.writeCommonLog("writeData2GlassesBleDevice [glassessDataArray.length = " + glassessDataArray.length + "] glassessDataArray =" + BaseParseCmdBean.bytesToStringWithSpace(glassessDataArray));
            //mGlassesDeviceMirror.writeData(glassessDataArray);
            write(deviceMirror, glassessDataArray);
        }
    }

    private void write(final DeviceMirror deviceMirror, byte[] data) {
        if (dataInfoQueue != null) {
            dataInfoQueue.clear();
            dataInfoQueue = splitPacketFor20Byte(data);
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    send(deviceMirror);
                }
            });
        }
    }

    private void send(final DeviceMirror deviceMirror) {
        if (dataInfoQueue != null && !dataInfoQueue.isEmpty()) {
           //DeviceMirror deviceMirror = mDeviceMirrorPool.getDeviceMirror(bluetoothLeDevice);
            if (dataInfoQueue.peek() != null && deviceMirror != null) {
                deviceMirror.writeData(dataInfoQueue.poll());
            }
            if (dataInfoQueue.peek() != null) {
                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        send(deviceMirror);
                    }
                }, 100);
            }
        }
    }

    private Queue<byte[]> dataInfoQueue = new LinkedList<>();
    private Queue<byte[]> splitPacketFor20Byte(byte[] data) {
        Queue<byte[]> dataInfoQueue = new LinkedList<>();
        if (data != null) {
            int index = 0;
            do {
                byte[] surplusData = new byte[data.length - index];
                byte[] currentData;
                System.arraycopy(data, index, surplusData, 0, data.length - index);
                if (surplusData.length <= 20) {
                    currentData = new byte[surplusData.length];
                    System.arraycopy(surplusData, 0, currentData, 0, surplusData.length);
                    index += surplusData.length;
                } else {
                    currentData = new byte[20];
                    System.arraycopy(data, index, currentData, 0, 20);
                    index += 20;
                }
                dataInfoQueue.offer(currentData);
            } while (index < data.length);
        }
        return dataInfoQueue;
    }

    public void writeData2LightBleDevice(byte[] lightDataArray) {
        DeviceMirror deviceMirror = mDeviceMirrorPool.getDeviceMirror(mLightBluetoothLeDevice);
        if (null != deviceMirror) {
            //MyUtilBytes.printByteArray(lightDataArray);
            deviceMirror.writeData(lightDataArray);
        }
    }


    public BluetoothLeDevice getGlassesBluetoothLeDevice() {
        return this.mGlassesBluetoothLeDevice;
    }

    public BluetoothLeDevice getLightBluetoothLeDevice() {
        return this.mLightBluetoothLeDevice;
    }

    public DeviceMirror getGlassesDeviceMirror() {
        return mDeviceMirrorPool.getDeviceMirror(mGlassesBluetoothLeDevice);
    }

    public DeviceMirror getLightDeviceMirror() {
        return mDeviceMirrorPool.getDeviceMirror(mLightBluetoothLeDevice);
    }

    private boolean isGlassesBleDevice(BluetoothLeDevice bluetoothLeDeviceParam) {
        if (null == bluetoothLeDeviceParam) {
            return false;
        }
        BluetoothDevice bluetoothDevice = bluetoothLeDeviceParam.getDevice();
        if (null != bluetoothDevice) {
            String name = bluetoothDevice.getName();
            if (!CommonUtils.isEmpty(name) && name.startsWith(ConstantString.GLAASESS_NAME_COMMONT_PREFIX)) {
                return true;
            }
           /* if (!CommonUtils.isEmpty(name) && name.trim().toLowerCase().startsWith("ble")) {
                return true;
            }*/
        }
        //SdLogUtil.writeCommonLog("isGlassesBleDevice " + bluetoothLeDeviceParam);
        return false;
    }

    private boolean isLightBleDevice(BluetoothLeDevice bluetoothLeDeviceParam) {

        return false;
    }

    public void setmGlassesBluetoothLeDevice(BluetoothLeDevice mGlassesBluetoothLeDevice) {
        this.mGlassesBluetoothLeDevice = mGlassesBluetoothLeDevice;
    }

    public void setmLightBluetoothLeDevice(BluetoothLeDevice mLightBluetoothLeDevice) {
        this.mLightBluetoothLeDevice = mLightBluetoothLeDevice;
    }

    private void createScheduleSerivce() {

        if (null == executorService || executorService.isTerminated() || executorService.isShutdown()) {
            executorService = ThreadPoolUtilsLocal.newScheduledThreadPool();
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    if (threadHashCode.get() != hashCode()) {
                        MLog.e("threadTAG HashCode.get() != hashCode()");
                        return;
                    }

                    final String macAddress = Config.getConfig().getLastConnectBleGlassesMac();
                    MLog.e("macAddress = " + macAddress);
                    if (!CommonUtils.isEmpty(macAddress)) {
                        boolean glassesConnnected = isGlassesBleDeviceConnected();
                        boolean isConnecting = isGlassesBleDeviceConnecting();
                        MLog.e("macAddress = " + macAddress + " glassesConnnected = " + glassesConnnected + " isConnecting = " + isConnecting);
                        if (glassesConnnected || isConnecting) {
                            shutDownScheduledExecutorService();
                        } else {
                            threadHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    boolean isScanning = ViseBle.getInstance().isScanningByMac();
                                    if (isScanning) {
                                        onlyStopScan();
                                    } else {
                                        TrainModel.getInstance().checkDeviceBindStatus(macAddress, null ,  false, new CheckBleMacByServerCallBack() {
                                            @Override
                                            public void checkStatus(boolean avaliable, String mac) {
                                                if (avaliable) {
                                                    connectGlassesByMac(macAddress);
                                                }
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    } else {
                        shutDownScheduledExecutorService();
                    }
                }
            };
            threadHashCode.set(runnable.hashCode());
            executorService.scheduleWithFixedDelay(runnable, 10, 10 , TimeUnit.SECONDS);
        }
    }

    private void shutDownScheduledExecutorService() {
        onlyShutDownExecutorService();
        onlyStopScan();
    }

    private void onlyStopScan() {
        stopScan();
        stopScanByMac();
    }

    private void onlyShutDownExecutorService() {
        if (null != executorService) {
            executorService.shutdownNow();
        }
    }


}
