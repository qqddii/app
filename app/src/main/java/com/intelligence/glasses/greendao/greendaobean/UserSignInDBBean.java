package com.intelligence.glasses.greendao.greendaobean;

import org.greenrobot.greendao.annotation.Entity;

import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Unique;
import org.greenrobot.greendao.annotation.Generated;

/**
 * 用户签到记录
 */
@Entity(nameInDb = "usersignindb_tab")
public class UserSignInDBBean {

    @Unique
    @Id
    @Property(nameInDb = "signinserverid")
    private String signinserverid;
    
    @Property(nameInDb = "member_id")
    private String member_id;

    @Property(nameInDb = "login_name")
    private String login_name;

    @Property(nameInDb = "nick_name")
    private String nick_name;

    @Property(nameInDb = "name")
    private String name;

    @Property(nameInDb = "sign_time")
    private String sign_time;

    @Property(nameInDb = "sign_time_long")
    private Long sign_time_long;

    @Generated(hash = 1220409263)
    public UserSignInDBBean(String signinserverid, String member_id,
            String login_name, String nick_name, String name, String sign_time,
            Long sign_time_long) {
        this.signinserverid = signinserverid;
        this.member_id = member_id;
        this.login_name = login_name;
        this.nick_name = nick_name;
        this.name = name;
        this.sign_time = sign_time;
        this.sign_time_long = sign_time_long;
    }

    @Generated(hash = 806558061)
    public UserSignInDBBean() {
    }

    public String getSigninserverid() {
        return this.signinserverid;
    }

    public void setSigninserverid(String signinserverid) {
        this.signinserverid = signinserverid;
    }

    public String getMember_id() {
        return this.member_id;
    }

    public void setMember_id(String member_id) {
        this.member_id = member_id;
    }

    public String getLogin_name() {
        return this.login_name;
    }

    public void setLogin_name(String login_name) {
        this.login_name = login_name;
    }

    public String getNick_name() {
        return this.nick_name;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSign_time() {
        return this.sign_time;
    }

    public void setSign_time(String sign_time) {
        this.sign_time = sign_time;
    }

    public Long getSign_time_long() {
        return this.sign_time_long;
    }

    public void setSign_time_long(Long sign_time_long) {
        this.sign_time_long = sign_time_long;
    }




}
