package com.intelligence.glasses.greendao.greendaobean;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Index;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;

/**
 * 干预反馈
 */
@Entity(nameInDb = "intevenefeedback_bledata_tab")
public class InterveneFeedBackBleDataDBBean {

    @Id
    @Index
    @Property(nameInDb = "localid")
    private String localid;

    @Property(nameInDb = "userId")
    private String  userId;

    @Property(nameInDb = "currentUserCode")
    private long currentUserCode;

    @Property(nameInDb = "glassesMAC")
    private String glassesMAC;

    @Property(nameInDb = "interveneYear")
    private int interveneYear;

    @Property(nameInDb = "interveneMonth")
    private int interveneMonth;

    @Property(nameInDb = "interveneDay")
    private int interveneDay;

    @Property(nameInDb = "interveneHour")
    private int interveneHour;

    @Property(nameInDb = "interveneMinute")
    private int interveneMinute;

    @Property(nameInDb = "interveneSecond")
    private int interveneSecond;

    @Property(nameInDb = "weekKeyFre")
    private int weekKeyFre;

    @Property(nameInDb = "speedKeyFre")
    private int speedKeyFre;

    @Property(nameInDb = "interveneKeyFre")
    private int interveneKeyFre;

    @Property(nameInDb = "speedKeyFre2")
    private int speedKeyFre2;

    @Property(nameInDb = "interveneKeyFre2")
    private int interveneKeyFre2;

    @Property(nameInDb = "weekAccMinute")
    private int weekAccMinute;

    @Property(nameInDb = "monitorCmd")
    private String monitorCmd;

    @Property(nameInDb = "receiveLocalTime")
    private long receiveLocalTime;

    @Property(nameInDb = "receiveLocalTimeStr")
    private String receiveLocalTimeStr;

    @Property(nameInDb = "isReportedServer")
    private boolean isReportedServer;

    @Property(nameInDb = "reserve1")
    private String reserve1;

    @Generated(hash = 857715994)
    public InterveneFeedBackBleDataDBBean(String localid, String userId,
            long currentUserCode, String glassesMAC, int interveneYear,
            int interveneMonth, int interveneDay, int interveneHour,
            int interveneMinute, int interveneSecond, int weekKeyFre,
            int speedKeyFre, int interveneKeyFre, int speedKeyFre2,
            int interveneKeyFre2, int weekAccMinute, String monitorCmd,
            long receiveLocalTime, String receiveLocalTimeStr,
            boolean isReportedServer, String reserve1) {
        this.localid = localid;
        this.userId = userId;
        this.currentUserCode = currentUserCode;
        this.glassesMAC = glassesMAC;
        this.interveneYear = interveneYear;
        this.interveneMonth = interveneMonth;
        this.interveneDay = interveneDay;
        this.interveneHour = interveneHour;
        this.interveneMinute = interveneMinute;
        this.interveneSecond = interveneSecond;
        this.weekKeyFre = weekKeyFre;
        this.speedKeyFre = speedKeyFre;
        this.interveneKeyFre = interveneKeyFre;
        this.speedKeyFre2 = speedKeyFre2;
        this.interveneKeyFre2 = interveneKeyFre2;
        this.weekAccMinute = weekAccMinute;
        this.monitorCmd = monitorCmd;
        this.receiveLocalTime = receiveLocalTime;
        this.receiveLocalTimeStr = receiveLocalTimeStr;
        this.isReportedServer = isReportedServer;
        this.reserve1 = reserve1;
    }

    @Generated(hash = 1918709401)
    public InterveneFeedBackBleDataDBBean() {
    }

    public String getLocalid() {
        return this.localid;
    }

    public void setLocalid(String localid) {
        this.localid = localid;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getGlassesMAC() {
        return this.glassesMAC;
    }

    public void setGlassesMAC(String glassesMAC) {
        this.glassesMAC = glassesMAC;
    }

    public int getInterveneYear() {
        return this.interveneYear;
    }

    public void setInterveneYear(int interveneYear) {
        this.interveneYear = interveneYear;
    }

    public int getInterveneMonth() {
        return this.interveneMonth;
    }

    public void setInterveneMonth(int interveneMonth) {
        this.interveneMonth = interveneMonth;
    }

    public int getInterveneDay() {
        return this.interveneDay;
    }

    public void setInterveneDay(int interveneDay) {
        this.interveneDay = interveneDay;
    }

    public int getInterveneHour() {
        return this.interveneHour;
    }

    public void setInterveneHour(int interveneHour) {
        this.interveneHour = interveneHour;
    }

    public int getInterveneMinute() {
        return this.interveneMinute;
    }

    public void setInterveneMinute(int interveneMinute) {
        this.interveneMinute = interveneMinute;
    }

    public int getInterveneSecond() {
        return this.interveneSecond;
    }

    public void setInterveneSecond(int interveneSecond) {
        this.interveneSecond = interveneSecond;
    }

    public int getWeekKeyFre() {
        return this.weekKeyFre;
    }

    public void setWeekKeyFre(int weekKeyFre) {
        this.weekKeyFre = weekKeyFre;
    }

    public int getSpeedKeyFre() {
        return this.speedKeyFre;
    }

    public void setSpeedKeyFre(int speedKeyFre) {
        this.speedKeyFre = speedKeyFre;
    }

    public int getInterveneKeyFre() {
        return this.interveneKeyFre;
    }

    public void setInterveneKeyFre(int interveneKeyFre) {
        this.interveneKeyFre = interveneKeyFre;
    }

    public int getSpeedKeyFre2() {
        return this.speedKeyFre2;
    }

    public void setSpeedKeyFre2(int speedKeyFre2) {
        this.speedKeyFre2 = speedKeyFre2;
    }

    public int getInterveneKeyFre2() {
        return this.interveneKeyFre2;
    }

    public void setInterveneKeyFre2(int interveneKeyFre2) {
        this.interveneKeyFre2 = interveneKeyFre2;
    }

    public int getWeekAccMinute() {
        return this.weekAccMinute;
    }

    public void setWeekAccMinute(int weekAccMinute) {
        this.weekAccMinute = weekAccMinute;
    }

    public String getMonitorCmd() {
        return this.monitorCmd;
    }

    public void setMonitorCmd(String monitorCmd) {
        this.monitorCmd = monitorCmd;
    }

    public long getReceiveLocalTime() {
        return this.receiveLocalTime;
    }

    public void setReceiveLocalTime(long receiveLocalTime) {
        this.receiveLocalTime = receiveLocalTime;
    }

    public String getReceiveLocalTimeStr() {
        return this.receiveLocalTimeStr;
    }

    public void setReceiveLocalTimeStr(String receiveLocalTimeStr) {
        this.receiveLocalTimeStr = receiveLocalTimeStr;
    }

    public boolean getIsReportedServer() {
        return this.isReportedServer;
    }

    public void setIsReportedServer(boolean isReportedServer) {
        this.isReportedServer = isReportedServer;
    }

    public String getReserve1() {
        return this.reserve1;
    }

    public void setReserve1(String reserve1) {
        this.reserve1 = reserve1;
    }

    public long getCurrentUserCode() {
        return this.currentUserCode;
    }

    public void setCurrentUserCode(long currentUserCode) {
        this.currentUserCode = currentUserCode;
    }


}
