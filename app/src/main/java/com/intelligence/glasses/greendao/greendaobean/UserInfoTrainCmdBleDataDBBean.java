package com.intelligence.glasses.greendao.greendaobean;

import androidx.annotation.Keep;
import android.text.TextUtils;

import com.google.gson.Gson;

import org.greenrobot.greendao.annotation.Convert;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.converter.PropertyConverter;
import org.greenrobot.greendao.annotation.Generated;

/**
 * 记录下，每次发送给蓝牙设备的训练数据
 */
@Entity(nameInDb = "usertraincmdbledatadb_tab")
public class UserInfoTrainCmdBleDataDBBean {
    /**
     * left_eye_degree	是	string	左眼屈光度
     * right_eye_degree	是	string	右眼屈光度
     * left_astigmatism_degree	是	float	左眼散光度
     * right_astigmatism_degree	是	float	右眼散光度
     * astigmatism_degree	否	float	双眼散光
     * left_counts	是	int	左眼运行次数
     * left_speed	是	int	左眼运行速度
     * left_range_up	是	double	左眼运行区间上限
     * left_range_low	是	double	左眼运行区间下限
     * right_counts	是	int	右眼运行次数
     * right_speed	是	int	右眼运行速度
     * right_range_up	是	double	右眼运行区间上限
     * right_range_low
     */

    @Property(nameInDb = "localid")
    private String localid;

    @Property(nameInDb = "serverId")
    private String serverId;

    @Property(nameInDb = "login_name")
    private String login_name;//";//是	string	用户账号

    @NotNull
    @Property(nameInDb = "send_time")
    private String send_time;//";//	是	date	发送时间

    @NotNull
    @Property(nameInDb = "send_time_long")
    private long send_time_long;//";//	是	date	发送时间

    @Property(nameInDb = "deviceId")
    private String deviceId;//";//	是	string	设备编号

    @Property(nameInDb = "left_eye_degree")
    private double left_eye_degree;//左眼屈光度

    @Property(nameInDb = "right_eye_degree")
    private double right_eye_degree;//右眼屈光度

    @Property(nameInDb = "left_astigmatism_degree")
    private double left_astigmatism_degree;//左眼散光度

    @Property(nameInDb = "right_astigmatism_degree")
    private double right_astigmatism_degree;//右眼散光度

    @Property(nameInDb = "astigmatism_degree")
    private double astigmatism_degree;//双眼散光

    @Property(nameInDb = "left_counts")
    private int left_counts;//左眼运行次数

    @Property(nameInDb = "left_speed")
    private int left_speed;//左眼运行速度

    @Property(nameInDb = "left_range_up")
    private double left_range_up;//左眼运行区间上限

    @Property(nameInDb = "left_range_low")
    private double left_range_low;//左眼运行区间下限

    @Property(nameInDb = "right_counts")
    private int right_counts;//右眼运行次数

    @Property(nameInDb = "right_speed")
    private int right_speed;//右眼运行速度

    @Property(nameInDb = "right_range_up")
    private double right_range_up;//右眼运行区间上限

    @Property(nameInDb = "right_range_low")
    private double right_range_low;//右眼运行区间下限

    @Convert(converter = UserInfoTrainCmdBleDataDBBean.UniqueBeanConverter.class, columnType = String.class)
    @Property(nameInDb = "uniquebean")
    private UserInfoTrainCmdBleDataDBBean.UniqueBean uniqueBean;

    @Generated(hash = 1625997574)
    public UserInfoTrainCmdBleDataDBBean(String localid, String serverId, String login_name, @NotNull String send_time,
            long send_time_long, String deviceId, double left_eye_degree, double right_eye_degree,
            double left_astigmatism_degree, double right_astigmatism_degree, double astigmatism_degree, int left_counts,
            int left_speed, double left_range_up, double left_range_low, int right_counts, int right_speed,
            double right_range_up, double right_range_low, UserInfoTrainCmdBleDataDBBean.UniqueBean uniqueBean) {
        this.localid = localid;
        this.serverId = serverId;
        this.login_name = login_name;
        this.send_time = send_time;
        this.send_time_long = send_time_long;
        this.deviceId = deviceId;
        this.left_eye_degree = left_eye_degree;
        this.right_eye_degree = right_eye_degree;
        this.left_astigmatism_degree = left_astigmatism_degree;
        this.right_astigmatism_degree = right_astigmatism_degree;
        this.astigmatism_degree = astigmatism_degree;
        this.left_counts = left_counts;
        this.left_speed = left_speed;
        this.left_range_up = left_range_up;
        this.left_range_low = left_range_low;
        this.right_counts = right_counts;
        this.right_speed = right_speed;
        this.right_range_up = right_range_up;
        this.right_range_low = right_range_low;
        this.uniqueBean = uniqueBean;
    }

    @Generated(hash = 341049010)
    public UserInfoTrainCmdBleDataDBBean() {
    }

    public String getLocalid() {
        return this.localid;
    }

    public void setLocalid(String localid) {
        this.localid = localid;
    }

    public String getServerId() {
        return this.serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    public String getLogin_name() {
        return this.login_name;
    }

    public void setLogin_name(String login_name) {
        this.login_name = login_name;
    }

    public String getSend_time() {
        return this.send_time;
    }

    public void setSend_time(String send_time) {
        this.send_time = send_time;
    }

    public long getSend_time_long() {
        return this.send_time_long;
    }

    public void setSend_time_long(long send_time_long) {
        this.send_time_long = send_time_long;
    }

    public String getDeviceId() {
        return this.deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public double getLeft_eye_degree() {
        return this.left_eye_degree;
    }

    public void setLeft_eye_degree(double left_eye_degree) {
        this.left_eye_degree = left_eye_degree;
    }

    public double getRight_eye_degree() {
        return this.right_eye_degree;
    }

    public void setRight_eye_degree(double right_eye_degree) {
        this.right_eye_degree = right_eye_degree;
    }

    public double getLeft_astigmatism_degree() {
        return this.left_astigmatism_degree;
    }

    public void setLeft_astigmatism_degree(double left_astigmatism_degree) {
        this.left_astigmatism_degree = left_astigmatism_degree;
    }

    public double getRight_astigmatism_degree() {
        return this.right_astigmatism_degree;
    }

    public void setRight_astigmatism_degree(double right_astigmatism_degree) {
        this.right_astigmatism_degree = right_astigmatism_degree;
    }

    public double getAstigmatism_degree() {
        return this.astigmatism_degree;
    }

    public void setAstigmatism_degree(double astigmatism_degree) {
        this.astigmatism_degree = astigmatism_degree;
    }

    public int getLeft_counts() {
        return this.left_counts;
    }

    public void setLeft_counts(int left_counts) {
        this.left_counts = left_counts;
    }

    public int getLeft_speed() {
        return this.left_speed;
    }

    public void setLeft_speed(int left_speed) {
        this.left_speed = left_speed;
    }

    public double getLeft_range_up() {
        return this.left_range_up;
    }

    public void setLeft_range_up(double left_range_up) {
        this.left_range_up = left_range_up;
    }

    public double getLeft_range_low() {
        return this.left_range_low;
    }

    public void setLeft_range_low(double left_range_low) {
        this.left_range_low = left_range_low;
    }

    public int getRight_counts() {
        return this.right_counts;
    }

    public void setRight_counts(int right_counts) {
        this.right_counts = right_counts;
    }

    public int getRight_speed() {
        return this.right_speed;
    }

    public void setRight_speed(int right_speed) {
        this.right_speed = right_speed;
    }

    public double getRight_range_up() {
        return this.right_range_up;
    }

    public void setRight_range_up(double right_range_up) {
        this.right_range_up = right_range_up;
    }

    public double getRight_range_low() {
        return this.right_range_low;
    }

    public void setRight_range_low(double right_range_low) {
        this.right_range_low = right_range_low;
    }

    public UserInfoTrainCmdBleDataDBBean.UniqueBean getUniqueBean() {
        return this.uniqueBean;
    }

    public void setUniqueBean(UserInfoTrainCmdBleDataDBBean.UniqueBean uniqueBean) {
        this.uniqueBean = uniqueBean;
    }


    public static class UniqueBeanConverter implements PropertyConverter<UserInfoTrainCmdBleDataDBBean.UniqueBean, String> {
        @Override
        public UserInfoTrainCmdBleDataDBBean.UniqueBean convertToEntityProperty(String databaseValue) {
            if (TextUtils.isEmpty(databaseValue)) {
                return  null;
            }
            return new Gson().fromJson(databaseValue, UserInfoTrainCmdBleDataDBBean.UniqueBean.class);
        }

        @Override
        public String convertToDatabaseValue(UserInfoTrainCmdBleDataDBBean.UniqueBean entityProperty) {
            if (null == entityProperty) {
                return  null;
            }
            return new Gson().toJson(entityProperty);
        }
    }

    @Keep//android.support.annotation; 保持属性名不变
    public static class UniqueBean {
        private String memerberid;
        private int eyeIndex;//左眼，右眼
        private int leftInclInterval;//左区间
        private int rightInclInterval;//右区间
        private int speed;//速度
        private int frenquency;//频率

        public UniqueBean() {
        }

        public String getMemerberid() {
            return memerberid;
        }

        public void setMemerberid(String memerberid) {
            this.memerberid = memerberid;
        }

        public int getEyeIndex() {
            return eyeIndex;
        }

        public void setEyeIndex(int eyeIndex) {
            this.eyeIndex = eyeIndex;
        }

        public int getLeftInclInterval() {
            return leftInclInterval;
        }

        public void setLeftInclInterval(int leftInclInterval) {
            this.leftInclInterval = leftInclInterval;
        }

        public int getRightInclInterval() {
            return rightInclInterval;
        }

        public void setRightInclInterval(int rightInclInterval) {
            this.rightInclInterval = rightInclInterval;
        }

        public int getSpeed() {
            return speed;
        }

        public void setSpeed(int speed) {
            this.speed = speed;
        }

        public int getFrenquency() {
            return frenquency;
        }

        public void setFrenquency(int frenquency) {
            this.frenquency = frenquency;
        }
    }

}
