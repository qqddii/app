package com.intelligence.glasses.greendao.greendaobean;

import androidx.annotation.Keep;
import android.text.TextUtils;

import com.google.gson.Gson;

import org.greenrobot.greendao.annotation.Convert;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.converter.PropertyConverter;

@Entity(nameInDb = "usertrainbledatadb_tab")
public class UserInfoTrainBleDataDBBean {

    @Property(nameInDb = "localid")
    private String localid;

    @Property(nameInDb = "serverId")
    private String serverId;

    @Property(nameInDb = "login_name")
    private String login_name;//";//是	string	用户账号

    @Property(nameInDb = "start_time")
    private String start_time;//点击开始按钮的时间

    @NotNull
    @Property(nameInDb = "collect_time")
    private String collect_time;//";//	是	date	采集时间

    @NotNull
    @Property(nameInDb = "collect_time_long")
    private long collect_time_long;//";//	是	date	采集时间

    @Property(nameInDb = "deviceId")
    private String deviceId;//";//	是	string	设备编号

    @Property(nameInDb = "left_counts")
    private int left_counts;//";//	是	int	左眼运行次数

    @Property(nameInDb = "left_speed")
    private int left_speed;//";//	是	int	左眼运行速度

    @Property(nameInDb = "left_range_up")
    private double left_range_up;//";//	是	double	左眼运行区间上限

    @Property(nameInDb = "left_range_low")
    private double left_range_low;//";//	是	double	左眼运行区间下限

    @Property(nameInDb = "right_counts")
    private int right_counts;//";//	是	int	右眼运行次数

    @Property(nameInDb = "right_speed")
    private int right_speed;//";//	是	int	右眼运行速度

    @Property(nameInDb = "right_range_up")
    private double right_range_up;//";//	是	double	右眼运行区间上限

    @Property(nameInDb = "right_range_low")
    private double right_range_low;//";//	是	double	右眼运行区间下限

    @Property(nameInDb = "reportedServer")
    private boolean reportedServer;//是否已上报服务器

    @Property(nameInDb = "notifyBleDevice")
    private boolean notifyBleDevice;//通知蓝牙设备已上报

    @Property(nameInDb = "operation")
    private int operation;//操作行为（0表示定时获取最新的训练数据， 1表示点击干预键时的训练数据）

    @Convert(converter = UserInfoTrainBleDataDBBean.UniqueBeanConverter.class, columnType = String.class)
    @Property(nameInDb = "uniquebean")
    private UserInfoTrainBleDataDBBean.UniqueBean uniqueBean;

    @Generated(hash = 724292652)
    public UserInfoTrainBleDataDBBean(String localid, String serverId, String login_name, String start_time,
            @NotNull String collect_time, long collect_time_long, String deviceId, int left_counts, int left_speed,
            double left_range_up, double left_range_low, int right_counts, int right_speed, double right_range_up,
            double right_range_low, boolean reportedServer, boolean notifyBleDevice, int operation,
            UserInfoTrainBleDataDBBean.UniqueBean uniqueBean) {
        this.localid = localid;
        this.serverId = serverId;
        this.login_name = login_name;
        this.start_time = start_time;
        this.collect_time = collect_time;
        this.collect_time_long = collect_time_long;
        this.deviceId = deviceId;
        this.left_counts = left_counts;
        this.left_speed = left_speed;
        this.left_range_up = left_range_up;
        this.left_range_low = left_range_low;
        this.right_counts = right_counts;
        this.right_speed = right_speed;
        this.right_range_up = right_range_up;
        this.right_range_low = right_range_low;
        this.reportedServer = reportedServer;
        this.notifyBleDevice = notifyBleDevice;
        this.operation = operation;
        this.uniqueBean = uniqueBean;
    }

    @Generated(hash = 213863865)
    public UserInfoTrainBleDataDBBean() {
    }

    public String getLocalid() {
        return this.localid;
    }

    public void setLocalid(String localid) {
        this.localid = localid;
    }

    public String getServerId() {
        return this.serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    public String getLogin_name() {
        return this.login_name;
    }

    public void setLogin_name(String login_name) {
        this.login_name = login_name;
    }

    public String getCollect_time() {
        return this.collect_time;
    }

    public void setCollect_time(String collect_time) {
        this.collect_time = collect_time;
    }

    public String getDeviceId() {
        return this.deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public int getLeft_counts() {
        return this.left_counts;
    }

    public void setLeft_counts(int left_counts) {
        this.left_counts = left_counts;
    }

    public int getLeft_speed() {
        return this.left_speed;
    }

    public void setLeft_speed(int left_speed) {
        this.left_speed = left_speed;
    }

    public double getLeft_range_up() {
        return this.left_range_up;
    }

    public void setLeft_range_up(double left_range_up) {
        this.left_range_up = left_range_up;
    }

    public double getLeft_range_low() {
        return this.left_range_low;
    }

    public void setLeft_range_low(double left_range_low) {
        this.left_range_low = left_range_low;
    }

    public int getRight_counts() {
        return this.right_counts;
    }

    public void setRight_counts(int right_counts) {
        this.right_counts = right_counts;
    }

    public int getRight_speed() {
        return this.right_speed;
    }

    public void setRight_speed(int right_speed) {
        this.right_speed = right_speed;
    }

    public double getRight_range_up() {
        return this.right_range_up;
    }

    public void setRight_range_up(double right_range_up) {
        this.right_range_up = right_range_up;
    }

    public double getRight_range_low() {
        return this.right_range_low;
    }

    public void setRight_range_low(double right_range_low) {
        this.right_range_low = right_range_low;
    }

    public UserInfoTrainBleDataDBBean.UniqueBean getUniqueBean() {
        return this.uniqueBean;
    }

    public void setUniqueBean(UserInfoTrainBleDataDBBean.UniqueBean uniqueBean) {
        this.uniqueBean = uniqueBean;
    }

    public boolean getReportedServer() {
        return this.reportedServer;
    }

    public void setReportedServer(boolean reportedServer) {
        this.reportedServer = reportedServer;
    }

    public boolean getNotifyBleDevice() {
        return this.notifyBleDevice;
    }

    public void setNotifyBleDevice(boolean notifyBleDevice) {
        this.notifyBleDevice = notifyBleDevice;
    }

    public long getCollect_time_long() {
        return this.collect_time_long;
    }

    public void setCollect_time_long(long collect_time_long) {
        this.collect_time_long = collect_time_long;
    }

    public int getOperation() {
        return this.operation;
    }

    public void setOperation(int operation) {
        this.operation = operation;
    }

    @Override
    public String toString() {
        return "UserInfoTrainBleDataDBBean{" +
                "localid='" + localid + '\'' +
                ", serverId='" + serverId + '\'' +
                ", login_name='" + login_name + '\'' +
                ", collect_time='" + collect_time + '\'' +
                ", collect_time_long=" + collect_time_long +
                ", deviceId='" + deviceId + '\'' +
                ", left_counts=" + left_counts +
                ", left_speed=" + left_speed +
                ", left_range_up=" + left_range_up +
                ", left_range_low=" + left_range_low +
                ", right_counts=" + right_counts +
                ", right_speed=" + right_speed +
                ", right_range_up=" + right_range_up +
                ", right_range_low=" + right_range_low +
                ", reportedServer=" + reportedServer +
                ", notifyBleDevice=" + notifyBleDevice +
                ", operation=" + operation +
                ", uniqueBean=" + uniqueBean +
                '}';
    }

    public String getStart_time() {
        return this.start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public static class UniqueBeanConverter implements PropertyConverter<UserInfoTrainBleDataDBBean.UniqueBean, String> {
        @Override
        public UserInfoTrainBleDataDBBean.UniqueBean convertToEntityProperty(String databaseValue) {
            if (TextUtils.isEmpty(databaseValue)) {
                return  null;
            }
            return new Gson().fromJson(databaseValue, UserInfoTrainBleDataDBBean.UniqueBean.class);
        }

        @Override
        public String convertToDatabaseValue(UserInfoTrainBleDataDBBean.UniqueBean entityProperty) {
            if (null == entityProperty) {
                return  null;
            }
            return new Gson().toJson(entityProperty);
        }
    }

    @Keep//android.support.annotation; 保持属性名不变
    public static class UniqueBean {
        private String memerberid;
        private int eyeIndex;//左眼，右眼
        private int leftInclInterval;//左区间
        private int rightInclInterval;//右区间
        private int speed;//速度
        private int frenquency;//频率

        public UniqueBean() {
        }

        public String getMemerberid() {
            return memerberid;
        }

        public void setMemerberid(String memerberid) {
            this.memerberid = memerberid;
        }

        public int getEyeIndex() {
            return eyeIndex;
        }

        public void setEyeIndex(int eyeIndex) {
            this.eyeIndex = eyeIndex;
        }

        public int getLeftInclInterval() {
            return leftInclInterval;
        }

        public void setLeftInclInterval(int leftInclInterval) {
            this.leftInclInterval = leftInclInterval;
        }

        public int getRightInclInterval() {
            return rightInclInterval;
        }

        public void setRightInclInterval(int rightInclInterval) {
            this.rightInclInterval = rightInclInterval;
        }

        public int getSpeed() {
            return speed;
        }

        public void setSpeed(int speed) {
            this.speed = speed;
        }

        public int getFrenquency() {
            return frenquency;
        }

        public void setFrenquency(int frenquency) {
            this.frenquency = frenquency;
        }
    }

}
