package com.intelligence.glasses.greendao.greenddaodb;

import android.content.Context;
import android.database.Cursor;

import com.google.gson.Gson;
import com.intelligence.glasses.greendao.greendaobean.UserInfoTrainCmdBleDataDBBean;
import com.shitec.bleglasses.glasses.greendao.msdao.DaoSession;
import com.shitec.bleglasses.glasses.greendao.msdao.UserInfoTrainCmdBleDataDBBeanDao;


import org.greenrobot.greendao.Property;
import org.greenrobot.greendao.query.QueryBuilder;

import java.util.List;


public class UserInfoTrainCmdBleDataBeanDaoOpe {

    private static String DB_NAME = "userinfotraincmdbledatabeandaoope.db";

    public static boolean useDefineDBName = false;
    /**
     * 添加数据至数据库
     * @param context
     * @param userInfoDBBean
     */
    public static void insertData(Context context, UserInfoTrainCmdBleDataDBBean userInfoDBBean) {
        try {
            if (useDefineDBName) {
                DbManager.getDaoSession(context, DB_NAME).getUserInfoTrainCmdBleDataDBBeanDao().insert(userInfoDBBean);
            } else {
                DbManager.getDaoSession(context).getUserInfoTrainCmdBleDataDBBeanDao().insert(userInfoDBBean);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 将数据实体通过事物添加至数据库
     * @param context
     * @param userInfoDBBeanList
     */
    public static void insertData(Context context, List<UserInfoTrainCmdBleDataDBBean> userInfoDBBeanList) {
        if (null == userInfoDBBeanList || userInfoDBBeanList.size() == 0) {
            return;
        }
        try {
            if (useDefineDBName) {
                DbManager.getDaoSession(context, DB_NAME).getUserInfoTrainCmdBleDataDBBeanDao().insertInTx(userInfoDBBeanList);
            } else {
                DbManager.getDaoSession(context).getUserInfoTrainCmdBleDataDBBeanDao().insertInTx(userInfoDBBeanList);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * 添加数据至数据库，如果存在，将原来的数据覆盖
     * 内部代码判断了，如果存在就update(entity);不存在就insert(entity);z
     * @param context
     * @param userInfoDBBean
     */
    public static void insertOrReplaceData(Context context, UserInfoTrainCmdBleDataDBBean userInfoDBBean) {
        try {
            if (useDefineDBName) {
                DbManager.getDaoSession(context, DB_NAME).getUserInfoTrainCmdBleDataDBBeanDao().insertOrReplace(userInfoDBBean);
            } else {
                DbManager.getDaoSession(context).getUserInfoTrainCmdBleDataDBBeanDao().insertOrReplace(userInfoDBBean);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void insertOrReplaceListData(Context context, List<UserInfoTrainCmdBleDataDBBean> userInfoTrainBleDataDBBeanList) {
        try {
            if (useDefineDBName) {
                DbManager.getDaoSession(context, DB_NAME).getUserInfoTrainCmdBleDataDBBeanDao().insertOrReplaceInTx(userInfoTrainBleDataDBBeanList);
            } else {
                DbManager.getDaoSession(context).getUserInfoTrainCmdBleDataDBBeanDao().insertOrReplaceInTx(userInfoTrainBleDataDBBeanList);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 根据id删除数据库中的数据
     * @param context
     * @param id
     */
    public static void deleteByKeyData(Context context, String id) {
        try {
            QueryBuilder queryBuilder = null;
            if (useDefineDBName) {
                queryBuilder = DbManager.getDaoSession(context, DB_NAME).getUserInfoTrainCmdBleDataDBBeanDao().queryBuilder();
            } else {
                queryBuilder = DbManager.getDaoSession(context).getUserInfoTrainCmdBleDataDBBeanDao().queryBuilder();
            }
            queryBuilder.where(UserInfoTrainCmdBleDataDBBeanDao.Properties.Localid.eq(id)).buildDelete().executeDeleteWithoutDetachingEntities();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 根据 localId 批量删除数据
     * @param context
     * @param idList
     */
    public static void deleteByKeyListData(Context context, List<String> idList) {
        try {
            QueryBuilder queryBuilder = null;
            if (useDefineDBName) {
                queryBuilder = DbManager.getDaoSession(context, DB_NAME).getUserInfoTrainCmdBleDataDBBeanDao().queryBuilder();
            } else {
                queryBuilder = DbManager.getDaoSession(context).getUserInfoTrainCmdBleDataDBBeanDao().queryBuilder();
            }
            queryBuilder.where(UserInfoTrainCmdBleDataDBBeanDao.Properties.Localid.in(idList)).buildDelete().executeDeleteWithoutDetachingEntities();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 删除所有数据
     * @param context
     */
    public static void deleteAllData(Context context) {
        try {
            if (useDefineDBName) {
                DbManager.getDaoSession(context, DB_NAME).getUserInfoTrainCmdBleDataDBBeanDao().deleteAll();
            } else {
                DbManager.getDaoSession(context).getUserInfoTrainCmdBleDataDBBeanDao().deleteAll();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     *
     * @param context
     * @param keyList keyList.size() + 1 == valueList.size()
     * @param valueList 最后一个值是 ServerID
     */
    public static void updateUserInfoByWhereList(Context context,List<String> keyList, List<Object> valueList, List<String> whereAndKeyList, List<Object> whereAndValueList) {
        assert(keyList.size() == valueList.size());

        if (null != whereAndKeyList) {
            assert(whereAndKeyList.size() == whereAndValueList.size());
        }

        try {
            DaoSession daoSession = null;
            if (useDefineDBName) {
                daoSession = DbManager.getDaoSession(context, DB_NAME);
            } else {
                daoSession = DbManager.getDaoSession(context);
            }
            /**
             * StringBuilder 线程不安全
             */
            StringBuilder stringBuffer  = new StringBuilder("");
            stringBuffer.append(" update " + UserInfoTrainCmdBleDataDBBeanDao.TABLENAME);
            stringBuffer.append(" set ");
            int keySize = keyList.size();
            for (int i = 0; i < keySize; i++) {
                if (i != keySize - 1) {
                    stringBuffer.append( " " + keyList.get(i) + " = ? , ");
                } else {
                    /**
                     * 最后一项
                     */
                    stringBuffer.append( " " + keyList.get(i) + " = ? ");
                }
            }

            if (whereAndKeyList.size() > 0) {
                stringBuffer.append("  where ");
                int keyAndSize = whereAndKeyList.size();

                for (int i = 0; i < keyAndSize; i++) {
                    if (i != keyAndSize - 1) {
                        stringBuffer.append( " " + whereAndKeyList.get(i) + " = ? and ");
                    } else {
                        /**
                         * 最后一项
                         */
                        stringBuffer.append( " " + whereAndKeyList.get(i) + " = ? ");
                    }
                }
                valueList.addAll(whereAndValueList);// 将 where 条件值添加到 update value list 集合里面
            }



            Object[] valuesArray = new Object[valueList.size()];
            valueList.toArray(valuesArray);

            //MLog.d("sqlBindArg = " + stringBuffer.toString());
            daoSession.getDatabase().execSQL(stringBuffer.toString(), valuesArray);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 查询所有数据
     * @param context
     * @return
     */
    public static List<UserInfoTrainCmdBleDataDBBean> queryAll(Context context) {
        try {
            if (useDefineDBName) {
                QueryBuilder<UserInfoTrainCmdBleDataDBBean> builder = DbManager.getDaoSession(context, DB_NAME).getUserInfoTrainCmdBleDataDBBeanDao().queryBuilder();
                return  builder.build().list();
            } else {
                QueryBuilder<UserInfoTrainCmdBleDataDBBean> builder = DbManager.getDaoSession(context).getUserInfoTrainCmdBleDataDBBeanDao().queryBuilder();
                return  builder.build().list();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * 分页查询
     * @param pageIndex 当前第几页
     * @param pageSize 每页显示的数据条数
     * @param context
     * @return
     */
    public static List<UserInfoTrainCmdBleDataDBBean> queryPaging(int pageIndex, int pageSize, Context context) {
        try {
            if (useDefineDBName) {
                UserInfoTrainCmdBleDataDBBeanDao userInfoDBBeanDao = DbManager.getDaoSession(context, DB_NAME).getUserInfoTrainCmdBleDataDBBeanDao();
                List<UserInfoTrainCmdBleDataDBBean> listMsg = userInfoDBBeanDao.queryBuilder().offset(pageIndex * pageSize).limit(pageSize).list();
                return listMsg;
            } else {
                UserInfoTrainCmdBleDataDBBeanDao userInfoDBBeanDao = DbManager.getDaoSession(context).getUserInfoTrainCmdBleDataDBBeanDao();
                List<UserInfoTrainCmdBleDataDBBean> listMsg = userInfoDBBeanDao.queryBuilder().offset(pageIndex * pageSize).limit(pageSize).list();
                return listMsg;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static  List<UserInfoTrainCmdBleDataDBBean> queryRawAllUserInfoTrainTimeByServerID(Context context, String serverId) {
        List<UserInfoTrainCmdBleDataDBBean> userInfoTrainTimeDBBeanList = null;
        try {
            String whereSQL = "where " + UserInfoTrainCmdBleDataDBBeanDao.Properties.ServerId.columnName + " = ? ";
            if (useDefineDBName) {
                UserInfoTrainCmdBleDataDBBeanDao userInfoDBBeanDao = DbManager.getDaoSession(context, DB_NAME).getUserInfoTrainCmdBleDataDBBeanDao();
                userInfoTrainTimeDBBeanList = userInfoDBBeanDao.queryRaw(whereSQL, new String[]{serverId});
            } else {
                UserInfoTrainCmdBleDataDBBeanDao userInfoDBBeanDao = DbManager.getDaoSession(context).getUserInfoTrainCmdBleDataDBBeanDao();
                userInfoTrainTimeDBBeanList = userInfoDBBeanDao.queryRaw(whereSQL, new String[]{serverId});
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userInfoTrainTimeDBBeanList;
    }

    public static  List<UserInfoTrainCmdBleDataDBBean> queryRawUserInfoTrainTimeWithSectionByServerID(Context context, String serverId, Long startTime, Long endTime) {
        List<UserInfoTrainCmdBleDataDBBean> userInfoTrainTimeDBBeanList = null;
        try {
            String trainColumnName = UserInfoTrainCmdBleDataDBBeanDao.Properties.Send_time_long.columnName;
            String whereSQL = "where " + UserInfoTrainCmdBleDataDBBeanDao.Properties.ServerId.columnName + " = ? and ( " + trainColumnName + " >= ?  and " + trainColumnName + " =< ? )";
            if (useDefineDBName) {
                UserInfoTrainCmdBleDataDBBeanDao userInfoDBBeanDao = DbManager.getDaoSession(context, DB_NAME).getUserInfoTrainCmdBleDataDBBeanDao();
                userInfoTrainTimeDBBeanList = userInfoDBBeanDao.queryRaw(whereSQL, new String[]{serverId, String.valueOf(startTime), String.valueOf(endTime) });
            } else {
                UserInfoTrainCmdBleDataDBBeanDao userInfoDBBeanDao = DbManager.getDaoSession(context).getUserInfoTrainCmdBleDataDBBeanDao();
                userInfoTrainTimeDBBeanList = userInfoDBBeanDao.queryRaw(whereSQL, new String[]{serverId, String.valueOf(startTime), String.valueOf(endTime) });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userInfoTrainTimeDBBeanList;
    }

    public static  List<UserInfoTrainCmdBleDataDBBean> queryUserInfoTrainTimeWithSectionByServerID(Context context, String serverId, Long startTime, Long endTime) {
        List<UserInfoTrainCmdBleDataDBBean> userInfoTrainTimeDBBeanList = null;
        try {
            Property trainColumnProperty = UserInfoTrainCmdBleDataDBBeanDao.Properties.Send_time_long;
            Property serverIdColumnProperty = UserInfoTrainCmdBleDataDBBeanDao.Properties.ServerId;

            UserInfoTrainCmdBleDataDBBeanDao userInfoDBBeanDao = null;
            if (useDefineDBName) {
                userInfoDBBeanDao = DbManager.getDaoSession(context, DB_NAME).getUserInfoTrainCmdBleDataDBBeanDao();
            } else {
                userInfoDBBeanDao = DbManager.getDaoSession(context).getUserInfoTrainCmdBleDataDBBeanDao();
            }
            QueryBuilder queryBuilder = userInfoDBBeanDao.queryBuilder();
            queryBuilder.where(serverIdColumnProperty.eq(serverId)).and(trainColumnProperty.ge(startTime), trainColumnProperty.le(endTime));
            userInfoTrainTimeDBBeanList = queryBuilder.list();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return userInfoTrainTimeDBBeanList;
    }

    /***
     * 通过 用户 serverId , UserInfoTrainCmdBleDataDBBean.UniqueBean 来查询
     * @param context
     * @param serverId
     * @param uniqueBean
     * @return
     */
    public static  List<UserInfoTrainCmdBleDataDBBean> queryUserInfoTrainBleDataByUniqueBean(Context context, String serverId, UserInfoTrainCmdBleDataDBBean.UniqueBean uniqueBean) {
        List<UserInfoTrainCmdBleDataDBBean> userInfoTrainTimeDBBeanList = null;
        try {
            Property trainColumnProperty = UserInfoTrainCmdBleDataDBBeanDao.Properties.UniqueBean;
            Property serverIdColumnProperty = UserInfoTrainCmdBleDataDBBeanDao.Properties.ServerId;

            UserInfoTrainCmdBleDataDBBeanDao userInfoDBBeanDao = null;
            if (useDefineDBName) {
                userInfoDBBeanDao = DbManager.getDaoSession(context, DB_NAME).getUserInfoTrainCmdBleDataDBBeanDao();
            } else {
                userInfoDBBeanDao = DbManager.getDaoSession(context).getUserInfoTrainCmdBleDataDBBeanDao();
            }
            QueryBuilder queryBuilder = userInfoDBBeanDao.queryBuilder();
            queryBuilder.where(serverIdColumnProperty.eq(serverId), trainColumnProperty.eq(new Gson().toJson(uniqueBean)));
            userInfoTrainTimeDBBeanList = queryBuilder.list();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return userInfoTrainTimeDBBeanList;
    }

    /**
     * 查询该表所有的记录数
     * @param context
     * @return
     */
    public static Long getAllDataCount(Context context) {
        Cursor cursor = null;
        DaoSession daoSession = null;
        try {
            if (useDefineDBName) {
                daoSession = DbManager.getDaoSession(context, DB_NAME);
            } else {
                daoSession = DbManager.getDaoSession(context);
            }
            cursor = daoSession.getDatabase().rawQuery("select count(*) from " + UserInfoTrainCmdBleDataDBBeanDao.TABLENAME, null);
            if (null != cursor) {
                if (cursor.moveToFirst()) {
                    return cursor.getLong(0);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != cursor) {
                cursor.close();
            }
        }
        return 0L;
    }

    /**
     * 查询最早插入的一条记录
     * @param context
     * @return
     */
    public static UserInfoTrainCmdBleDataDBBean queryTheOldestBean(Context context) {
        UserInfoTrainCmdBleDataDBBean userInfoTrainCmdBleDataDBBean = null;
        try {
            UserInfoTrainCmdBleDataDBBeanDao cmdBleDataDBBeanDao = null;
            if (useDefineDBName) {
                cmdBleDataDBBeanDao = DbManager.getDaoSession(context, DB_NAME).getUserInfoTrainCmdBleDataDBBeanDao();
            } else {
                cmdBleDataDBBeanDao = DbManager.getDaoSession(context).getUserInfoTrainCmdBleDataDBBeanDao();
            }
            QueryBuilder queryBuilder = cmdBleDataDBBeanDao.queryBuilder();
            List<UserInfoTrainCmdBleDataDBBean> userInfoTrainCmdBleDataDBBeanList = queryBuilder.orderAsc(UserInfoTrainCmdBleDataDBBeanDao.Properties.Send_time_long).limit(1).list();
            if (null != userInfoTrainCmdBleDataDBBeanList && userInfoTrainCmdBleDataDBBeanList.size() > 0) {
                userInfoTrainCmdBleDataDBBean = userInfoTrainCmdBleDataDBBeanList.get(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userInfoTrainCmdBleDataDBBean;
    }

    /**
     * 删除某个时间点之前的数据
     * @param context
     * @param beforeTime
     */
    public static void deleteDataByBeforeTime(Context context, long beforeTime) {
        try {
            QueryBuilder queryBuilder = null;
            if (useDefineDBName) {
                queryBuilder = DbManager.getDaoSession(context, DB_NAME).getUserInfoTrainCmdBleDataDBBeanDao().queryBuilder();
            } else {
                queryBuilder = DbManager.getDaoSession(context).getUserInfoTrainCmdBleDataDBBeanDao().queryBuilder();
            }
            queryBuilder.where(UserInfoTrainCmdBleDataDBBeanDao.Properties.Send_time_long.le(beforeTime)).buildDelete().executeDeleteWithoutDetachingEntities();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /** 原始查询
     方法1：
     Query<User> query = userDao.queryBuilder().where(
     new StringCondition("_ID IN " +
     "(SELECT USER_ID FROM USER_MESSAGE WHERE READ_FLAG = 0)")
     ).build();
     方法2：
     queryRaw 和queryRawCreate

     Query<User> query = userDao.queryRawCreate(
     ", GROUP G WHERE G.NAME=? AND T.GROUP_ID=G._ID", "admin"
     );
     debug查询
     QueryBuilder.LOG_SQL = true;
     QueryBuilder.LOG_VALUES = true;
     */
}
