package com.intelligence.glasses.greendao.greendaobean;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Index;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Unique;

@Entity(nameInDb = "usertraintimedb_tab")
public class UserInfoTrainTimeDBBean {
    @Index
    @Id
    @Property(nameInDb = "localid")
    private String localid;

    @Property(nameInDb = "serverId")
    private String serverId;

    @Property(nameInDb = "userName")
    private String userName;

    @NotNull
    @Property(nameInDb = "trainDate")
    private Long trainDate;//当天佩戴日期

    @Unique
    @NotNull
    @Property(nameInDb = "trainDateStr")
    private String trainDateStr;//当天佩戴日期

    @Property(nameInDb = "trainTime")
    private float trainTime;//当天佩戴时间

    @Property(nameInDb = "trainCount")
    private int trainCount;//当天佩戴次数

    @NotNull
    @Property(nameInDb = "createTime")
    private String createTime;//本地数据库字段，非后台数据

    @NotNull
    @Property(nameInDb = "updateTime")
    private String updateTime;//本地数据库字段，非后台数据

    @NotNull
    @Property(nameInDb = "updateTimeLong")
    private Long updateTimeLong;//本地数据库字段，非后台数据


    @Generated(hash = 1200280376)
    public UserInfoTrainTimeDBBean(String localid, String serverId, String userName,
            @NotNull Long trainDate, @NotNull String trainDateStr, float trainTime,
            int trainCount, @NotNull String createTime, @NotNull String updateTime,
            @NotNull Long updateTimeLong) {
        this.localid = localid;
        this.serverId = serverId;
        this.userName = userName;
        this.trainDate = trainDate;
        this.trainDateStr = trainDateStr;
        this.trainTime = trainTime;
        this.trainCount = trainCount;
        this.createTime = createTime;
        this.updateTime = updateTime;
        this.updateTimeLong = updateTimeLong;
    }

    @Generated(hash = 1908081993)
    public UserInfoTrainTimeDBBean() {
    }

    public String getLocalid() {
        return this.localid;
    }

    public void setLocalid(String localid) {
        this.localid = localid;
    }

    public String getServerId() {
        return this.serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getTrainDate() {
        return this.trainDate;
    }

    public void setTrainDate(Long trainDate) {
        this.trainDate = trainDate;
    }

    public String getTrainDateStr() {
        return this.trainDateStr;
    }

    public void setTrainDateStr(String trainDateStr) {
        this.trainDateStr = trainDateStr;
    }

    public float getTrainTime() {
        return this.trainTime;
    }

    public void setTrainTime(float trainTime) {
        this.trainTime = trainTime;
    }

    public int getTrainCount() {
        return this.trainCount;
    }

    public void setTrainCount(int trainCount) {
        this.trainCount = trainCount;
    }

    public String getCreateTime() {
        return this.createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return this.updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public Long getUpdateTimeLong() {
        return this.updateTimeLong;
    }

    public void setUpdateTimeLong(Long updateTimeLong) {
        this.updateTimeLong = updateTimeLong;
    }

}
