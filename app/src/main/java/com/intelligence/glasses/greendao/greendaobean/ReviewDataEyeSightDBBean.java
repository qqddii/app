package com.intelligence.glasses.greendao.greendaobean;

import androidx.annotation.Keep;
import android.text.TextUtils;

import com.google.gson.Gson;

import org.greenrobot.greendao.annotation.Convert;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Index;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Unique;
import org.greenrobot.greendao.converter.PropertyConverter;

/**
 * 视力复查 Bean
 */
@Entity(nameInDb = "reviewdataeyesightdb_tab")
public class ReviewDataEyeSightDBBean {

    @Id
    @Index
    @Property(nameInDb = "localid")
    private String localid;

    @Property(nameInDb = "serverrecoredId")
    private String serverRecoredId;//服务端记录的某一次复查记录

    @NotNull
    @Property(nameInDb = "userId")
    private String userId;

    @Property(nameInDb = "userName")
    private String userName;

    @Property(nameInDb = "userType")//用户类型，监护人，使用者
    private String userType;

    @Property(nameInDb = "leftEyeSight")
    private double leftEyeSight;

    @Property(nameInDb = "rightEyeSight")
    private double rightEyeSight;

    @Property(nameInDb = "doubleEyeSight")
    private double doubleEyeSight;

    @Property(nameInDb = "reviewEyeSightTimes")
    private int reviewEyeSightTimes;//复查次数（第几次）

    @Property(nameInDb = "reviewEyeSightDate")
    private String reviewEyeSightDate;//复查日期

    @Property(nameInDb = "trainTimeLong")//训练时长
    private float trainTimeLong;

    @Property(nameInDb = "createTime")
    private String createTime;

    @Unique
    @NotNull
    @Convert(converter = ReviewDataEyeSightDBBean.UniqueBeanConverter.class, columnType = String.class)
    @Property(nameInDb = "uniquebean")
    private ReviewDataEyeSightDBBean.UniqueBean uniqueBean;

    @Generated(hash = 628244143)
    public ReviewDataEyeSightDBBean(String localid, String serverRecoredId, @NotNull String userId, String userName,
            String userType, double leftEyeSight, double rightEyeSight, double doubleEyeSight, int reviewEyeSightTimes,
            String reviewEyeSightDate, float trainTimeLong, String createTime,
            @NotNull ReviewDataEyeSightDBBean.UniqueBean uniqueBean) {
        this.localid = localid;
        this.serverRecoredId = serverRecoredId;
        this.userId = userId;
        this.userName = userName;
        this.userType = userType;
        this.leftEyeSight = leftEyeSight;
        this.rightEyeSight = rightEyeSight;
        this.doubleEyeSight = doubleEyeSight;
        this.reviewEyeSightTimes = reviewEyeSightTimes;
        this.reviewEyeSightDate = reviewEyeSightDate;
        this.trainTimeLong = trainTimeLong;
        this.createTime = createTime;
        this.uniqueBean = uniqueBean;
    }

    @Generated(hash = 431306879)
    public ReviewDataEyeSightDBBean() {
    }

    public String getLocalid() {
        return this.localid;
    }

    public void setLocalid(String localid) {
        this.localid = localid;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserType() {
        return this.userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public double getLeftEyeSight() {
        return this.leftEyeSight;
    }

    public void setLeftEyeSight(double leftEyeSight) {
        this.leftEyeSight = leftEyeSight;
    }

    public double getRightEyeSight() {
        return this.rightEyeSight;
    }

    public void setRightEyeSight(double rightEyeSight) {
        this.rightEyeSight = rightEyeSight;
    }

    public double getDoubleEyeSight() {
        return this.doubleEyeSight;
    }

    public void setDoubleEyeSight(double doubleEyeSight) {
        this.doubleEyeSight = doubleEyeSight;
    }

    public int getReviewEyeSightTimes() {
        return this.reviewEyeSightTimes;
    }

    public void setReviewEyeSightTimes(int reviewEyeSightTimes) {
        this.reviewEyeSightTimes = reviewEyeSightTimes;
    }

    public String getReviewEyeSightDate() {
        return this.reviewEyeSightDate;
    }

    public void setReviewEyeSightDate(String reviewEyeSightDate) {
        this.reviewEyeSightDate = reviewEyeSightDate;
    }

    public float getTrainTimeLong() {
        return this.trainTimeLong;
    }

    public void setTrainTimeLong(float trainTimeLong) {
        this.trainTimeLong = trainTimeLong;
    }

    public String getCreateTime() {
        return this.createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getServerRecoredId() {
        return this.serverRecoredId;
    }

    public void setServerRecoredId(String serverRecoredId) {
        this.serverRecoredId = serverRecoredId;
    }

    public static class UniqueBeanConverter implements PropertyConverter<ReviewDataEyeSightDBBean.UniqueBean, String> {
        @Override
        public ReviewDataEyeSightDBBean.UniqueBean convertToEntityProperty(String databaseValue) {
            if (TextUtils.isEmpty(databaseValue)) {
                return  null;
            }
            return new Gson().fromJson(databaseValue, ReviewDataEyeSightDBBean.UniqueBean.class);
        }

        @Override
        public String convertToDatabaseValue(ReviewDataEyeSightDBBean.UniqueBean entityProperty) {
            if (null == entityProperty) {
                return  null;
            }
            return new Gson().toJson(entityProperty);
        }
    }

    @Keep
    public static class UniqueBean {
        private String memeberId;
        private int reviewTimes;

        public UniqueBean() {
        }

        public String getMemeberId() {
            return memeberId;
        }

        public void setMemeberId(String memeberId) {
            this.memeberId = memeberId;
        }

        public int getReviewTimes() {
            return reviewTimes;
        }

        public void setReviewTimes(int reviewTimes) {
            this.reviewTimes = reviewTimes;
        }
    }

    public ReviewDataEyeSightDBBean.UniqueBean getUniqueBean() {
        return this.uniqueBean;
    }

    public void setUniqueBean(ReviewDataEyeSightDBBean.UniqueBean uniqueBean) {
        this.uniqueBean = uniqueBean;
    }
}
