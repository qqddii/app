package com.intelligence.glasses.greendao.greenddaodb;

import android.content.Context;
import android.database.Cursor;

import com.android.common.baselibrary.log.MLog;
import com.google.gson.Gson;
import com.intelligence.glasses.greendao.greendaobean.UserInfoTrainBleDataDBBean;
import com.shitec.bleglasses.glasses.greendao.msdao.DaoSession;
import com.shitec.bleglasses.glasses.greendao.msdao.UserInfoTrainBleDataDBBeanDao;


import org.greenrobot.greendao.Property;
import org.greenrobot.greendao.query.QueryBuilder;

import java.util.ArrayList;
import java.util.List;


public class UserInfoTrainBleDataBeanDaoOpe {

    private static String DB_NAME = "userinfotrainbledatabeandaoope.db";

    public static boolean useDefineDBName = false;
    /**
     * 添加数据至数据库
     * @param context
     * @param userInfoDBBean
     */
    public static void insertData(Context context, UserInfoTrainBleDataDBBean userInfoDBBean) {
        try {
            if (useDefineDBName) {
                DbManager.getDaoSession(context, DB_NAME).getUserInfoTrainBleDataDBBeanDao().insert(userInfoDBBean);
            } else {
                DbManager.getDaoSession(context).getUserInfoTrainBleDataDBBeanDao().insert(userInfoDBBean);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 将数据实体通过事物添加至数据库
     * @param context
     * @param userInfoDBBeanList
     */
    public static void insertData(Context context, List<UserInfoTrainBleDataDBBean> userInfoDBBeanList) {
        if (null == userInfoDBBeanList || userInfoDBBeanList.size() == 0) {
            return;
        }
        try {
            if (useDefineDBName) {
                DbManager.getDaoSession(context, DB_NAME).getUserInfoTrainBleDataDBBeanDao().insertInTx(userInfoDBBeanList);
            } else {
                DbManager.getDaoSession(context).getUserInfoTrainBleDataDBBeanDao().insertInTx(userInfoDBBeanList);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * 添加数据至数据库，如果存在，将原来的数据覆盖
     * 内部代码判断了，如果存在就update(entity);不存在就insert(entity);z
     * @param context
     * @param userInfoDBBean
     */
    public static void insertOrReplaceData(Context context, UserInfoTrainBleDataDBBean userInfoDBBean) {
        try {
            if (useDefineDBName) {
                DbManager.getDaoSession(context, DB_NAME).getUserInfoTrainBleDataDBBeanDao().insertOrReplace(userInfoDBBean);
            } else {
                DbManager.getDaoSession(context).getUserInfoTrainBleDataDBBeanDao().insertOrReplace(userInfoDBBean);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void insertOrReplaceListData(Context context, List<UserInfoTrainBleDataDBBean> userInfoTrainBleDataDBBeanList) {
        try {
            if (useDefineDBName) {
                DbManager.getDaoSession(context, DB_NAME).getUserInfoTrainBleDataDBBeanDao().insertOrReplaceInTx(userInfoTrainBleDataDBBeanList);
            } else {
                DbManager.getDaoSession(context).getUserInfoTrainBleDataDBBeanDao().insertOrReplaceInTx(userInfoTrainBleDataDBBeanList);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 根据是否已上报给云端，来删除数据
     * @param context
     * @param serverId
     * @param reportedServer
     */
    public static void deleteData(Context context, String serverId, boolean reportedServer) {
        try {
            QueryBuilder queryBuilder = null;
            if (useDefineDBName) {
                queryBuilder = DbManager.getDaoSession(context, DB_NAME).getUserInfoTrainBleDataDBBeanDao().queryBuilder();
            } else {
                queryBuilder = DbManager.getDaoSession(context).getUserInfoTrainBleDataDBBeanDao().queryBuilder();
            }
            queryBuilder.where(UserInfoTrainBleDataDBBeanDao.Properties.ServerId.eq(serverId),
                    UserInfoTrainBleDataDBBeanDao.Properties.ReportedServer.eq(reportedServer))
                    .buildDelete().executeDeleteWithoutDetachingEntities();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    /**
     * 根据id删除数据库中的数据
     * @param context
     * @param id
     */
    public static void deleteByKeyData(Context context, String id) {
        try {
            QueryBuilder queryBuilder = null;
            if (useDefineDBName) {
                queryBuilder = DbManager.getDaoSession(context, DB_NAME).getUserInfoTrainBleDataDBBeanDao().queryBuilder();
            } else {
                queryBuilder = DbManager.getDaoSession(context).getUserInfoTrainBleDataDBBeanDao().queryBuilder();
            }
            queryBuilder.where(UserInfoTrainBleDataDBBeanDao.Properties.Localid.eq(id)).buildDelete().executeDeleteWithoutDetachingEntities();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 根据 localId 批量删除数据
     * @param context
     * @param idList
     */
    public static void deleteByKeyListData(Context context, List<String> idList) {
        try {
            QueryBuilder queryBuilder = null;
            if (useDefineDBName) {
                queryBuilder = DbManager.getDaoSession(context, DB_NAME).getUserInfoTrainBleDataDBBeanDao().queryBuilder();
            } else {
                queryBuilder = DbManager.getDaoSession(context).getUserInfoTrainBleDataDBBeanDao().queryBuilder();
            }
            queryBuilder.where(UserInfoTrainBleDataDBBeanDao.Properties.Localid.in(idList)).buildDelete().executeDeleteWithoutDetachingEntities();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 删除所有数据
     * @param context
     */
    public static void deleteAllData(Context context) {
        try {
            if (useDefineDBName) {
                DbManager.getDaoSession(context, DB_NAME).getUserInfoTrainBleDataDBBeanDao().deleteAll();
            } else {
                DbManager.getDaoSession(context).getUserInfoTrainBleDataDBBeanDao().deleteAll();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     *
     * @param context
     * @param keyList keyList.size() + 1 == valueList.size()
     * @param valueList 最后一个值是 ServerID
     */
    public static void updateUserTrainInfoByWhereList(Context context, List<String> keyList, List<Object> valueList, List<String> whereAndKeyList, List<Object> whereAndValueList) {
        assert(keyList.size() == valueList.size());

        if (null != whereAndKeyList) {
            assert(whereAndKeyList.size() == whereAndValueList.size());
        }

        try {
            DaoSession daoSession = null;
            if (useDefineDBName) {
                daoSession = DbManager.getDaoSession(context, DB_NAME);
            } else {
                daoSession = DbManager.getDaoSession(context);
            }
            /**
             * StringBuilder 线程不安全
             */
            StringBuilder stringBuffer  = new StringBuilder("");
            stringBuffer.append(" update " + UserInfoTrainBleDataDBBeanDao.TABLENAME);
            stringBuffer.append(" set ");
            int keySize = keyList.size();
            for (int i = 0; i < keySize; i++) {
                if (i != keySize - 1) {
                    stringBuffer.append( " " + keyList.get(i) + " = ? , ");
                } else {
                    /**
                     * 最后一项
                     */
                    stringBuffer.append( " " + keyList.get(i) + " = ? ");
                }
            }

            if (whereAndKeyList.size() > 0) {
                stringBuffer.append("  where ");
                int keyAndSize = whereAndKeyList.size();

                for (int i = 0; i < keyAndSize; i++) {
                    if (i != keyAndSize - 1) {
                        stringBuffer.append( " " + whereAndKeyList.get(i) + " = ? and ");
                    } else {
                        /**
                         * 最后一项
                         */
                        stringBuffer.append( " " + whereAndKeyList.get(i) + " = ? ");
                    }
                }
                valueList.addAll(whereAndValueList);// 将 where 条件值添加到 update value list 集合里面
            }



            Object[] valuesArray = new Object[valueList.size()];
            valueList.toArray(valuesArray);

            MLog.d("sqlBindArg = " + stringBuffer.toString());
            daoSession.getDatabase().execSQL(stringBuffer.toString(), valuesArray);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 批量修改训练数据是否已上报的状态
     * @param context
     * @param localIdList
     * @param isReportedServer
     */
    public static synchronized void updateUserTrainInfoReportStatus(Context context,List<String> localIdList, boolean isReportedServer, String serverId) {

        try {
            DaoSession daoSession = null;
            if (useDefineDBName) {
                daoSession = DbManager.getDaoSession(context, DB_NAME);
            } else {
                daoSession = DbManager.getDaoSession(context);
            }
            /**
             * StringBuilder 线程不安全
             */
            StringBuilder stringBuffer  = new StringBuilder("");
            stringBuffer.append(" update " + UserInfoTrainBleDataDBBeanDao.TABLENAME);
            stringBuffer.append(" set ");
            stringBuffer.append(" " + UserInfoTrainBleDataDBBeanDao.Properties.ReportedServer.columnName + " = ?");
            stringBuffer.append(" where ");
            stringBuffer.append(UserInfoTrainBleDataDBBeanDao.Properties.ServerId.columnName);
            stringBuffer.append(" = ");
            stringBuffer.append(" ? ");
            stringBuffer.append(" and ");

            stringBuffer.append(UserInfoTrainBleDataDBBeanDao.Properties.Localid.columnName);
            stringBuffer.append(" in (" );

            int idSize = localIdList.size();

            for (int i = 0; i < idSize; i++) {
                if (i == idSize - 1) {
                    /**
                     * 最后一个
                     */
                    stringBuffer.append(" ? ");
                } else {
                    stringBuffer.append(" ?, ");
                }
            }
            stringBuffer.append(" )" );

            List<Object>  argsList = new ArrayList<>();
            argsList.add(isReportedServer);
            argsList.add(serverId);
            argsList.addAll(localIdList);

            Object[] argsArray = new Object[argsList.size()];

            argsList.toArray(argsArray);


            MLog.d("sqlBindArg = " + stringBuffer.toString());
            daoSession.getDatabase().execSQL(stringBuffer.toString(), argsArray);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 查询所有数据
     * @param context
     * @return
     */
    public static List<UserInfoTrainBleDataDBBean> queryAll(Context context) {
        try {
            if (useDefineDBName) {
                QueryBuilder<UserInfoTrainBleDataDBBean> builder = DbManager.getDaoSession(context, DB_NAME).getUserInfoTrainBleDataDBBeanDao().queryBuilder();
                return  builder.build().list();
            } else {
                QueryBuilder<UserInfoTrainBleDataDBBean> builder = DbManager.getDaoSession(context).getUserInfoTrainBleDataDBBeanDao().queryBuilder();
                return  builder.build().list();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * 分页查询
     * @param pageIndex 当前第几页
     * @param pageSize 每页显示的数据条数
     * @param context
     * @return
     */
    public static List<UserInfoTrainBleDataDBBean> queryPaging(int pageIndex, int pageSize, Context context) {
        try {
            if (useDefineDBName) {
                UserInfoTrainBleDataDBBeanDao userInfoDBBeanDao = DbManager.getDaoSession(context, DB_NAME).getUserInfoTrainBleDataDBBeanDao();
                List<UserInfoTrainBleDataDBBean> listMsg = userInfoDBBeanDao.queryBuilder().offset(pageIndex * pageSize).limit(pageSize).list();
                return listMsg;
            } else {
                UserInfoTrainBleDataDBBeanDao userInfoDBBeanDao = DbManager.getDaoSession(context).getUserInfoTrainBleDataDBBeanDao();
                List<UserInfoTrainBleDataDBBean> listMsg = userInfoDBBeanDao.queryBuilder().offset(pageIndex * pageSize).limit(pageSize).list();
                return listMsg;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static  List<UserInfoTrainBleDataDBBean> queryRawAllUserInfoTrainTimeByServerID(Context context, String serverId) {
        List<UserInfoTrainBleDataDBBean> userInfoTrainTimeDBBeanList = null;
        try {
            String whereSQL = "where " + UserInfoTrainBleDataDBBeanDao.Properties.ServerId.columnName + " = ? ";
            if (useDefineDBName) {
                UserInfoTrainBleDataDBBeanDao userInfoDBBeanDao = DbManager.getDaoSession(context, DB_NAME).getUserInfoTrainBleDataDBBeanDao();
                userInfoTrainTimeDBBeanList = userInfoDBBeanDao.queryRaw(whereSQL, new String[]{serverId});
            } else {
                UserInfoTrainBleDataDBBeanDao userInfoDBBeanDao = DbManager.getDaoSession(context).getUserInfoTrainBleDataDBBeanDao();
                userInfoTrainTimeDBBeanList = userInfoDBBeanDao.queryRaw(whereSQL, new String[]{serverId});
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userInfoTrainTimeDBBeanList;
    }

    public static  List<UserInfoTrainBleDataDBBean> queryRawUserInfoTrainTimeWithSectionByServerID(Context context, String serverId, Long startTime, Long endTime) {
        List<UserInfoTrainBleDataDBBean> userInfoTrainTimeDBBeanList = null;
        try {
            String trainColumnName = UserInfoTrainBleDataDBBeanDao.Properties.Collect_time_long.columnName;
            String whereSQL = "where " + UserInfoTrainBleDataDBBeanDao.Properties.ServerId.columnName + " = ? and ( " + trainColumnName + " >= ?  and " + trainColumnName + " =< ? )";
            if (useDefineDBName) {
                UserInfoTrainBleDataDBBeanDao userInfoDBBeanDao = DbManager.getDaoSession(context, DB_NAME).getUserInfoTrainBleDataDBBeanDao();
                userInfoTrainTimeDBBeanList = userInfoDBBeanDao.queryRaw(whereSQL, new String[]{serverId, String.valueOf(startTime), String.valueOf(endTime) });
            } else {
                UserInfoTrainBleDataDBBeanDao userInfoDBBeanDao = DbManager.getDaoSession(context).getUserInfoTrainBleDataDBBeanDao();
                userInfoTrainTimeDBBeanList = userInfoDBBeanDao.queryRaw(whereSQL, new String[]{serverId, String.valueOf(startTime), String.valueOf(endTime) });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userInfoTrainTimeDBBeanList;
    }

    public static  List<UserInfoTrainBleDataDBBean> queryUserInfoTrainTimeWithSectionByServerID(Context context, String serverId, Long startTime, Long endTime) {
        List<UserInfoTrainBleDataDBBean> userInfoTrainTimeDBBeanList = null;
        try {
            Property trainColumnProperty = UserInfoTrainBleDataDBBeanDao.Properties.Collect_time_long;
            Property serverIdColumnProperty = UserInfoTrainBleDataDBBeanDao.Properties.ServerId;

            UserInfoTrainBleDataDBBeanDao userInfoDBBeanDao = null;
            if (useDefineDBName) {
                userInfoDBBeanDao = DbManager.getDaoSession(context, DB_NAME).getUserInfoTrainBleDataDBBeanDao();
            } else {
                userInfoDBBeanDao = DbManager.getDaoSession(context).getUserInfoTrainBleDataDBBeanDao();
            }
            QueryBuilder queryBuilder = userInfoDBBeanDao.queryBuilder();
            queryBuilder.where(serverIdColumnProperty.eq(serverId)).and(trainColumnProperty.ge(startTime), trainColumnProperty.le(endTime));
            userInfoTrainTimeDBBeanList = queryBuilder.list();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return userInfoTrainTimeDBBeanList;
    }

    public static  List<UserInfoTrainBleDataDBBean> queryLastUserInfoTrainTimeWithOperationCodeyServerID(Context context, String serverId, int operationCode) {
        List<UserInfoTrainBleDataDBBean> userInfoTrainTimeDBBeanList = null;
        try {
            Property trainColumnProperty = UserInfoTrainBleDataDBBeanDao.Properties.Collect_time_long;
            Property serverIdColumnProperty = UserInfoTrainBleDataDBBeanDao.Properties.ServerId;
            Property operationProperty = UserInfoTrainBleDataDBBeanDao.Properties.Operation;

            UserInfoTrainBleDataDBBeanDao userInfoDBBeanDao = null;
            if (useDefineDBName) {
                userInfoDBBeanDao = DbManager.getDaoSession(context, DB_NAME).getUserInfoTrainBleDataDBBeanDao();
            } else {
                userInfoDBBeanDao = DbManager.getDaoSession(context).getUserInfoTrainBleDataDBBeanDao();
            }
            QueryBuilder queryBuilder = userInfoDBBeanDao.queryBuilder();
            queryBuilder.where(serverIdColumnProperty.eq(serverId),operationProperty.eq(operationCode) ).orderDesc(trainColumnProperty).limit(1);
            userInfoTrainTimeDBBeanList = queryBuilder.list();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return userInfoTrainTimeDBBeanList;
    }

    /***
     * 通过 用户 serverId , UserInfoTrainBleDataDBBean.UniqueBean 来查询
     * @param context
     * @param serverId
     * @param uniqueBean
     * @return
     */
    public static  List<UserInfoTrainBleDataDBBean> queryUserInfoTrainBleDataByUniqueBean(Context context, String serverId, UserInfoTrainBleDataDBBean.UniqueBean uniqueBean) {
        List<UserInfoTrainBleDataDBBean> userInfoTrainTimeDBBeanList = null;
        try {
            Property trainColumnProperty = UserInfoTrainBleDataDBBeanDao.Properties.UniqueBean;
            Property serverIdColumnProperty = UserInfoTrainBleDataDBBeanDao.Properties.ServerId;

            UserInfoTrainBleDataDBBeanDao userInfoDBBeanDao = null;
            if (useDefineDBName) {
                userInfoDBBeanDao = DbManager.getDaoSession(context, DB_NAME).getUserInfoTrainBleDataDBBeanDao();
            } else {
                userInfoDBBeanDao = DbManager.getDaoSession(context).getUserInfoTrainBleDataDBBeanDao();
            }
            QueryBuilder queryBuilder = userInfoDBBeanDao.queryBuilder();
            queryBuilder.where(serverIdColumnProperty.eq(serverId), trainColumnProperty.eq(new Gson().toJson(uniqueBean)));
            userInfoTrainTimeDBBeanList = queryBuilder.list();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return userInfoTrainTimeDBBeanList;
    }

    /**
     * 查询是否已上报(哪种操作)给服务器的数据，结果以收集数据的时间降序排序
     * @param context
     * @param serverId
     * @param isReportedServer
     * @return
     */
    public static  List<UserInfoTrainBleDataDBBean> queryUserInfoTrainBleDataByReportedServer(Context context, String serverId, boolean isReportedServer, int operation) {
        List<UserInfoTrainBleDataDBBean> userInfoTrainTimeDBBeanList = null;
        try {
            Property trainColumnProperty = UserInfoTrainBleDataDBBeanDao.Properties.ReportedServer;
            Property serverIdColumnProperty = UserInfoTrainBleDataDBBeanDao.Properties.ServerId;
            Property operationProperty = UserInfoTrainBleDataDBBeanDao.Properties.Operation;

            UserInfoTrainBleDataDBBeanDao userInfoDBBeanDao = null;
            if (useDefineDBName) {
                userInfoDBBeanDao = DbManager.getDaoSession(context, DB_NAME).getUserInfoTrainBleDataDBBeanDao();
            } else {
                userInfoDBBeanDao = DbManager.getDaoSession(context).getUserInfoTrainBleDataDBBeanDao();
            }
            QueryBuilder queryBuilder = userInfoDBBeanDao.queryBuilder();
            queryBuilder.where(serverIdColumnProperty.eq(serverId),
                    trainColumnProperty.eq(isReportedServer),
                    operationProperty.eq(operation))
                    .orderDesc(UserInfoTrainBleDataDBBeanDao.Properties.Collect_time_long);
            userInfoTrainTimeDBBeanList = queryBuilder.list();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return userInfoTrainTimeDBBeanList;
    }

    public static  Long getAllDataCount(Context context) {
        Cursor cursor = null;
        DaoSession daoSession = null;
        try {

            if (useDefineDBName) {
                daoSession = DbManager.getDaoSession(context, DB_NAME);
            } else {
                daoSession = DbManager.getDaoSession(context);
            }
            cursor = daoSession.getDatabase().rawQuery("select count(*) from " + UserInfoTrainBleDataDBBeanDao.TABLENAME, null);
            if (null != cursor) {
                if (cursor.moveToFirst()) {
                    return cursor.getLong(0);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != cursor) {
                cursor.close();
            }
        }
        return 0L;
    }

    /**
     * 本地查询未上报给云端的数量
     * @param context
     * @return
     */
    public static  Long getAllUnReportedDataCount(Context context, String serverId) {
        Cursor cursor = null;
        DaoSession daoSession = null;
        try {

            if (useDefineDBName) {
                daoSession = DbManager.getDaoSession(context, DB_NAME);
            } else {
                daoSession = DbManager.getDaoSession(context);
            }

            StringBuilder sqlBuiler = new StringBuilder();
            sqlBuiler.append("select count(*) from " + UserInfoTrainBleDataDBBeanDao.TABLENAME );
            sqlBuiler.append(" where ");
            sqlBuiler.append(" " + UserInfoTrainBleDataDBBeanDao.Properties.ServerId.columnName + " = ? ");
            sqlBuiler.append(" and " + UserInfoTrainBleDataDBBeanDao.Properties.ReportedServer.columnName + " = ? ");

            MLog.d(" " + sqlBuiler.toString());
            //布尔值被存储为整数 0（false）和 1（true）。
            cursor = daoSession.getDatabase().rawQuery(sqlBuiler.toString(), new String[]{serverId, String.valueOf(0)});
            if (null != cursor) {
                if (cursor.moveToFirst()) {
                    return cursor.getLong(0);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != cursor) {
                cursor.close();
            }
        }
        return 0L;
    }

    public static UserInfoTrainBleDataDBBean queryTheOldestBean(Context context) {
        UserInfoTrainBleDataDBBean userInfoTrainTimeDBBean = null;
        try {
            UserInfoTrainBleDataDBBeanDao userInfoDBBeanDao = null;
            if (useDefineDBName) {
                userInfoDBBeanDao = DbManager.getDaoSession(context, DB_NAME).getUserInfoTrainBleDataDBBeanDao();
            } else {
                userInfoDBBeanDao = DbManager.getDaoSession(context).getUserInfoTrainBleDataDBBeanDao();
            }
            QueryBuilder queryBuilder = userInfoDBBeanDao.queryBuilder();
            List<UserInfoTrainBleDataDBBean> userInfoTrainBleDataDBBeanList = queryBuilder.orderAsc(UserInfoTrainBleDataDBBeanDao.Properties.Collect_time_long).limit(1).list();
            if (null != userInfoTrainBleDataDBBeanList && userInfoTrainBleDataDBBeanList.size() > 0) {
                userInfoTrainTimeDBBean = userInfoTrainBleDataDBBeanList.get(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userInfoTrainTimeDBBean;
    }

    /**
     * 根据收集的时间，以及是否上报云端来删除多余的数据
     * @param context
     * @param reportedServer
     * @param beforeTime
     */
    public static void deleteDataByServerIdAndTime(Context context, boolean reportedServer,long beforeTime) {
        try {
            QueryBuilder queryBuilder = null;
            if (useDefineDBName) {
                queryBuilder = DbManager.getDaoSession(context, DB_NAME).getUserInfoTrainBleDataDBBeanDao().queryBuilder();
            } else {
                queryBuilder = DbManager.getDaoSession(context).getUserInfoTrainBleDataDBBeanDao().queryBuilder();
            }
            queryBuilder.where(UserInfoTrainBleDataDBBeanDao.Properties.ReportedServer.eq(reportedServer),
                    UserInfoTrainBleDataDBBeanDao.Properties.Collect_time_long.le(beforeTime))
                    .buildDelete().executeDeleteWithoutDetachingEntities();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }



    /** 原始查询
     方法1：
     Query<User> query = userDao.queryBuilder().where(
     new StringCondition("_ID IN " +
     "(SELECT USER_ID FROM USER_MESSAGE WHERE READ_FLAG = 0)")
     ).build();
     方法2：
     queryRaw 和queryRawCreate

     Query<User> query = userDao.queryRawCreate(
     ", GROUP G WHERE G.NAME=? AND T.GROUP_ID=G._ID", "admin"
     );
     debug查询
     QueryBuilder.LOG_SQL = true;
     QueryBuilder.LOG_VALUES = true;
     */
}
