package com.intelligence.glasses;

import android.content.Context;
import androidx.multidex.MultiDex;
import android.util.DisplayMetrics;

import com.android.common.baselibrary.app.BaseApplication;
import com.android.common.baselibrary.jnative.SecUtil;
import com.facebook.stetho.Stetho;
import com.intelligence.common.http.OkHttpUtil;
import com.intelligence.common.http.UploadUtils;
import com.intelligence.glasses.ble.BleDeviceManager;
import com.intelligence.glasses.greendao.greenddaodb.DatabaseManager;
import com.intelligence.glasses.greendao.greenddaodb.DatabaseManagerImpl;
import com.intelligence.glasses.model.GlassesBleDataCmdModel;
import com.intelligence.glasses.model.GlassesBleDataModel;
import com.intelligence.glasses.model.TrainModel;
import com.intelligence.glasses.model.UploadDeviceInfoModel;
import com.uuzuche.lib_zxing.BuildConfig;
import com.uuzuche.lib_zxing.activity.ZXingLibrary;

/**
 * Created by Administrator on 2018/6/5.
 */

public class MyApplication extends BaseApplication {
    private static MyApplication mApplication;
    private DatabaseManager databaseManager = new DatabaseManagerImpl();
    public static int H,W;

    @Override
    public void onCreate() {
        super.onCreate();
        getScreen(this);
        createDatabaseManagerImpl();
        mApplication = this;
        initData();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public static MyApplication getInstance() {
        return mApplication;
    }

    private void loadLibrary() {
        //MLog.i("apkmd5 = " + Md5Util.getMD5String(SignatureUtil.getCurrentApplicationSignature(this)));
        System.loadLibrary("cmintsec");
        SecUtil.init(this);
    }

    private void initData() {
        OkHttpUtil.init();
        //SdLogUtil.init();
        initCrashHandler();
        loadLibrary();
        ZXingLibrary.initDisplayOpinion(this);
        BleDeviceManager.getInstance().init(getApplicationContext());
        //Stetho调试工具初始化
        if (BuildConfig.DEBUG) {
            Stetho.initializeWithDefaults(this);
        }
        uploadCrashLogZipFile();
        deleteMoreData();
        registerNetWorkStatusCallBack();
        GlassesBleDataModel.getInstance();
        GlassesBleDataCmdModel.getInstance();
        postDeviceInfoData();
        postTrainBleDataFromDBO();
    }

    private void createDatabaseManagerImpl() {
        databaseManager.startup(this);
    }

    public DatabaseManager getDataBaseManager() {
        return databaseManager;
    }

    private void uploadCrashLogZipFile() {
        UploadUtils.uploadLogFile();
    }

    private void postDeviceInfoData() {
        UploadDeviceInfoModel uploadDeviceInfoModel = new UploadDeviceInfoModel();
        uploadDeviceInfoModel.postDeviInfo();
    }

    private void postTrainBleDataFromDBO() {
        TrainModel.getInstance().handleBleDataInDB2Server();

        /**
         * 准备眼镜的训练数据
         */
        TrainModel.getInstance().prepareGlassesTrainData(false,false, false);

    }

    /**
     * 清理多余的数据
     */
    private void deleteMoreData() {
        TrainModel trainModel = TrainModel.getInstance();
        trainModel.deleteMoreData();
    }

    public void getScreen(Context aty) {
        DisplayMetrics dm = aty.getResources().getDisplayMetrics();
        H=dm.heightPixels;
        W=dm.widthPixels;
    }
}
