package com.intelligence.glasses.http.request;

public interface IAddFeedBack extends IBaseRequest{
    String TITLE = "title";
    String CONTENT = "content";
    String PLATFORM = "platform";
    String CREATEBY = "createBy";
    String MEMBER_ID = "member_id";
}
