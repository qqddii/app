package com.intelligence.glasses.http.response;

import androidx.annotation.Keep;

import com.intelligence.glasses.bean.BaseBean;

@Keep
public class HttpResponseGlassInitDataBackBean extends BaseBean {

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "HttpResponseGlassInitDataBackBean{" +
                "data=" + data +
                ", status='" + status + '\'' +
                ", message='" + message + '\'' +
                ", cursor='" + cursor + '\'' +
                '}';
    }

    @Keep
    public class DataBean {
      private String memberId;
      private int maxRunNumber;//": 800.00,
      private int startSpeed;//": 700.00,
      private int stopSpeed;//": 600.00,
      private int speedInc;//": 200.00,
      private int commonSpeed;
      private int machineData6;
      private int machineData7;
      private int machineData8;
      private int machineData9;
      private String systemTime;//": "2019-08-23 18:23:32"
      private int isNewUser;

        public int getMaxRunNumber() {
            return maxRunNumber;
        }

        public void setMaxRunNumber(int maxRunNumber) {
            this.maxRunNumber = maxRunNumber;
        }

        public int getStartSpeed() {
            return startSpeed;
        }

        public void setStartSpeed(int startSpeed) {
            this.startSpeed = startSpeed;
        }

        public int getStopSpeed() {
            return stopSpeed;
        }

        public void setStopSpeed(int stopSpeed) {
            this.stopSpeed = stopSpeed;
        }

        public int getSpeedInc() {
            return speedInc;
        }

        public void setSpeedInc(int speedInc) {
            this.speedInc = speedInc;
        }

        public int getCommonSpeed() {
            return commonSpeed;
        }

        public void setCommonSpeed(int commonSpeed) {
            this.commonSpeed = commonSpeed;
        }

        public int getMachineData6() {
            return machineData6;
        }

        public void setMachineData6(int machineData6) {
            this.machineData6 = machineData6;
        }

        public int getMachineData7() {
            return machineData7;
        }

        public void setMachineData7(int machineData7) {
            this.machineData7 = machineData7;
        }

        public int getMachineData8() {
            return machineData8;
        }

        public void setMachineData8(int machineData8) {
            this.machineData8 = machineData8;
        }

        public int getMachineData9() {
            return machineData9;
        }

        public void setMachineData9(int machineData9) {
            this.machineData9 = machineData9;
        }

        public String getSystemTime() {
            return systemTime;
        }

        public void setSystemTime(String systemTime) {
            this.systemTime = systemTime;
        }

        public String getMemberId() {
            return memberId;
        }

        public void setMemberId(String memberId) {
            this.memberId = memberId;
        }

        public int getIsNewUser() {
            return isNewUser;
        }

        public void setIsNewUser(int isNewUser) {
            this.isNewUser = isNewUser;
        }

        @Override
        public String toString() {
            return "DataBean{" +
                    "memberId='" + memberId + '\'' + "\n" +
                    ", maxRunNumber=" + maxRunNumber + "\n" +
                    ", startSpeed=" + startSpeed + "\n" +
                    ", stopSpeed=" + stopSpeed + "\n" +
                    ", speedInc=" + speedInc + "\n" +
                    ", commonSpeed=" + commonSpeed + "\n" +
                    ", machineData6=" + machineData6 + "\n" +
                    ", machineData7=" + machineData7 + "\n" +
                    ", machineData8=" + machineData8 + "\n" +
                    ", machineData9=" + machineData9 + "\n" +
                    ", systemTime='" + systemTime + '\'' + "\n" +
                    ", isNewUser=" + isNewUser + "\n" +
                    '}';
        }
    }



}
