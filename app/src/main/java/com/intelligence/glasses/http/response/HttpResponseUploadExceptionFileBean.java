package com.intelligence.glasses.http.response;

import androidx.annotation.Keep;

import com.intelligence.glasses.bean.BaseBean;

@Keep
public class HttpResponseUploadExceptionFileBean extends BaseBean {
    private boolean data;

    public boolean isData() {
        return data;
    }

    public void setData(boolean data) {
        this.data = data;
    }
}
