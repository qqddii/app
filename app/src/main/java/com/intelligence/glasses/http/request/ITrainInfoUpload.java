package com.intelligence.glasses.http.request;

/***
 * 训练数据上报
 */
public interface ITrainInfoUpload extends IBaseRequest{
    String MEMBERID = "memberId";//	是	string	用户ID
    String LOGIN_NAME = "login_name";//是	string	用户账号
    String DEVICEID = "deviceId";//	是	string	设备编号
    String OPERATIONTYPE = "operationType";//0-启动,1-暂停,2-停止,3-干预，4-运行
    String PLATFORM = "platform";//平台 平台类型（0-pc,1-android,2-ios）


    String DATALIST = "dataList";
    String COLLECT_TIME = "collect_time";//	是	date	采集时间
    String START_TIME = "start_time";//开始时间
    String LEFT_COUNTS = "left_counts";//	是	int	左眼运行次数
    String LEFT_SPEED = "left_speed";//	是	int	左眼运行速度
    String LEFT_RANGE_UP = "left_range_up";//	是	double	左眼运行区间上限
    String LEFT_RANGE_LOW = "left_range_low";//	是	double	左眼运行区间下限
    String RIGHT_COUNTS = "right_counts";//	是	int	右眼运行次数
    String RIGHT_SPEED = "right_speed";//	是	int	右眼运行速度
    String RIGHT_RANGE_UP = "right_range_up";//	是	double	右眼运行区间上限
    String RIGHT_RANGE_LOW = "right_range_low";//	是	double	右眼运行区间下限


    int OPERATION_START_ACTION = 0;//启动，开始
    int OPERATION_PAUSE_ACTION = 1;//暂停
    int OPERATION_STOP_ACTION = 2;//定制
    int OPERATION_INTERVENE_ACTION = 3;//点击干预键时的训练数据
    int OPERATION_RUNNING_ACTION = 4;//运行过程中, 定时获取蓝牙设备最新的训练数据
}
