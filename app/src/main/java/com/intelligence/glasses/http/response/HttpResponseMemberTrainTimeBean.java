package com.intelligence.glasses.http.response;

import androidx.annotation.Keep;

import com.intelligence.glasses.bean.BaseBean;

import java.util.List;

/**
 * 训练时间信息
 */
@Keep
public class HttpResponseMemberTrainTimeBean extends BaseBean {
    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    @Keep
    public class DataBean {
        private String memberId;//	string	用户id
        private String trainingDate;//	date	训练日期
        private float trainingHours;//	int	训练小时数
        private int trainingCount;//	int	训练佩戴次数

        public String getMemberId() {
            return memberId;
        }

        public void setMemberId(String memberId) {
            this.memberId = memberId;
        }

        public String getTrainingDate() {
            return trainingDate;
        }

        public void setTrainingDate(String trainingDate) {
            this.trainingDate = trainingDate;
        }

        public float getTrainingHours() {
            return trainingHours;
        }

        public void setTrainingHours(float trainingHours) {
            this.trainingHours = trainingHours;
        }

        public int getTrainingCount() {
            return trainingCount;
        }

        public void setTrainingCount(int trainingCount) {
            this.trainingCount = trainingCount;
        }

        @Override
        public String toString() {
            return "DataBean{" +
                    "memberId='" + memberId + '\'' +
                    ", trainingDate='" + trainingDate + '\'' +
                    ", trainingHours=" + trainingHours +
                    ", trainingCount=" + trainingCount +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "HttpResponseMemberTrainTimeBean{" +
                "data=" + data +
                ", status='" + status + '\'' +
                ", message='" + message + '\'' +
                ", cursor='" + cursor + '\'' +
                '}';
    }
}
