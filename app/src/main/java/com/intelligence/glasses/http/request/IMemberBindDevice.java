package com.intelligence.glasses.http.request;

/**
 * Created by Administrator on 2018/12/1.
 */

public interface IMemberBindDevice extends IBaseRequest {
    String MEMBERID = "memberId";//string	客户ID（用户）
    String NAME = "name";//string	姓名
    String PHONE = "phone";//
    String LOGIN_NAME = "login_name";//
    String DEVICEID = "deviceId";//string	关联设备编号ID
    String BIRTHDAY = "birthday";//生日
    String PROVICE = "province";//省份编号
    String CITY = "city";//市编号
    String AREA = "area";//区编号
    String ADDRESS = "address";//地址
    String AUTHTYPE = "bindType";//int	绑定认证类型，0-本人，1-监护人
    String CREDENTIALSTYPE = "credentialsType";//int	身份证类型 0身份证、1护照
    String CREDENTIALSCARD = "credentials_card";//	string	证件号
    String DIOPTER_STATE = "diopter_state";//int	屈光类型（0，近视；1，远视；2，老花；3，弱视；4,其他）
    String INTERPUPILLARY = "interpupillary";//float	瞳距
    String LEFT_EYE_DEGREE = "left_eye_degree";//float	左眼屈光度
    String RIGHT_EYE_DEGREE = "right_eye_degree";//float	右眼屈光度
    String GENDER_KEY = "gender";//性别

    /**
     * astigmatism_degree	否	float	双眼散光
     * right_column_mirror	是	float	右眼柱镜
     * left_column_mirror	是	float	左眼柱镜
     */

    String ASTIGMATISM_DEGREE = "astigmatism_degree";//否 float	双眼散光
    String RIGHT_COLUMN_MIRROR = "right_column_mirror";//是	float	右眼柱镜
    String LEFT_COLUMN_MIRROR = "left_column_mirror";//是	float	左眼柱镜

    /**
     * 裸眼视力
     */
    String LEFT_OD_DEGREE = "naked_right_eye_degree";//
    String LEFT_OS_DEGREE = "naked_left_eye_degree";//
    String LEFT_OU_DEGREE = "naked_binoculus_degree";//

    /**
     * 最佳视力
     */
    String RIGHT_OD_DEGREE = "correct_right_eye_degree";
    String RIGHT_OS_DEGREE = "correct_left_eye_degree";
    String RIGHT_OU_DEGREE = "correct_binoculus_degree";

    /**
     * 对比度视力
     */
    String OD_CONTRAST_EYE_DEGREE = "contrast_right_eye_degree";
    String OS_CONTRAST_EYE_DEGREE = "contrast_left_eye_degree";
    String OU_CONTRAST_EYE_DEGREE = "contrast_binoculus_degree";

    String LEFT_ASTIGMATISM_DEGREE = "left_astigmatism_degree";//	是	float	左眼散光度
    String RIGHT_ASTIGMATISM_DEGREE = "right_astigmatism_degree";//	是	float	右眼散光度

    String RIGHT_AXIAL = "right_axial";//	//是	float	右眼轴向
    String LEFT_AXIAL = "left_axial";//是	float	左眼轴向

    String LEFT_HANDLE_DEGREE = "left_handle_degree";//	float	左眼屈光度数（处理后）
    String  RIGHT_HANDLE_DEGREE = "left_handle_degree";//	float	右眼屈光度数（处理后）


    /**
     * Response
     *
     * 	绑定状态（0-绑定失败，1-用户不存在，2-绑定成功）
     */
    int RESPONSE_STATUS_FAILUE = 0;
    int RESPONSE_USER_NO_EXIST = 1;
    int RESPONSE_STATUS_SUCCESS = 2;
}