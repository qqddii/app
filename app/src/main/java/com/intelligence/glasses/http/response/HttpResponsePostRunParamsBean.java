package com.intelligence.glasses.http.response;

import androidx.annotation.Keep;

import com.intelligence.glasses.bean.BaseBean;

@Keep
public class HttpResponsePostRunParamsBean extends BaseBean {
    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    @Keep
    public class DataBean {
        private String memberId;
        private String systemTime;
    }
}
