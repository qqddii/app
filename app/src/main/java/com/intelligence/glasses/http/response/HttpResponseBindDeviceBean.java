package com.intelligence.glasses.http.response;

import androidx.annotation.Keep;

import com.intelligence.glasses.bean.BaseBean;

/**
 * 绑定设备响应结果
 */
@Keep
public class HttpResponseBindDeviceBean extends BaseBean {
  private boolean data;

    public boolean isData() {
        return data;
    }

    public void setData(boolean data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "HttpResponseBindDeviceBean{" +
                "data=" + data +
                ", status='" + status + '\'' +
                ", message='" + message + '\'' +
                ", cursor='" + cursor + '\'' +
                '}';
    }
}
