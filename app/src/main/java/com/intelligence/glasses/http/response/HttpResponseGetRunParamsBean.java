package com.intelligence.glasses.http.response;

import androidx.annotation.Keep;

import com.intelligence.glasses.bean.BaseBean;
import com.intelligence.glasses.bean.response.HttpResponseGlassesRunParamBean;

/**
 * 获取运行参数
 */
@Keep
public class HttpResponseGetRunParamsBean extends BaseBean {
    private HttpResponseGlassesRunParamBean data;

    public HttpResponseGlassesRunParamBean getData() {
        return data;
    }

    public void setData(HttpResponseGlassesRunParamBean data) {
        this.data = data;
    }
}
