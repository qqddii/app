package com.intelligence.glasses.util.jsonutil;

/**
 * Created by Administrator on 2018/10/30.
 */

import com.intelligence.glasses.bean.UserInfoBean;

/**
 * 解析登录响应json 字符串
 */
public class LoginReponseJsonParseUtil {

    public static UserInfoBean parseResonseJson(String responseJson) {
        final String USERNAME_KEY = "userName";
        final String GENDER_KEY = "gender";
        final String AUTHTYPE_KEY = "authType";
        final String GLASSID_KEY = "glassId";
        final String DIOPTRICSTATUS_KEY = "dioptricStatus";
        final String LEFTEYEDEGREE_KEY = "leftEyeDegree";
        final String RIGHTEYEDEGREE_KEY = "rightEyeDegree";
        final String PUPILLARYDISTANCE_KEY = "pupillaryDistance";
        final String COMEYEGLASSESDEGREE_KEY = "comEyeGlassesDegree";
        final String USERID_KEY = "userId";
        final String USERSTATUS_KEY = "userStatus";
        final String EXPIRATIONTIME_KEY = "expirationTime";
        final String AUTHTOKEN_KEY = "authToken";



        return null;
    }

}
